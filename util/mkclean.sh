#!/bin/sh

for file in configure \
	    config.status \
	    config.log \
	    config.h \
	    Makefile.in \
	    src/Makefile.in \
	    pixmaps/Makefile.in \
	    Makefile \
	    src/Makefile \
	    pixmaps/Makefile \
	    aclocal.m4
do
rm -f $file
done
