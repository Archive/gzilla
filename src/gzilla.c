/*

gzilla

Copyright 1997 Raph Levien <raph@acm.org>

This code is free for commercial and non-commercial use,
modification, and redistribution, as long as the source code release,
startup screen, or product packaging includes this copyright notice.

Maintaintership taken up by Christopher Reid Palmer <chrisp@innerFireWorks.com>,
17 Jul 1999. Look at the file TODO for my notes on where I want Gzilla to go.

*/

#include <stdio.h>
#include <gtk/gtk.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <netinet/in.h>		/* needed before including gzillasocket.h */

#include "gzilla.h"
#include "gzillabytesink.h"
#include "gzillaweb.h"
#include "gzillamisc.h"
#include "gzillaurl.h"
#include "gtkstatuslabel.h"
#include "gzillabrowser.h"
#include "menus.h"
#include "gzillacache.h"
#include "gzillasocket.h"
#include "gzillabrowser.h"
#include "gzillanav.h"
#include "gzillabookmark.h"
#include "gzilladicache.h"
#include "gzilladns.h"
#include "gzillafile.h"
#include "gzillahttp.h"
#include "gzillaproto.h"

#include "interface.h"

/* contains the browser_window structure */
#include "gzillabrowser.h"

#include "gzw.h"

void gzilla_check_home_dir(char *dir);

void open_url (GzillaByteSink* bytesink, GzillaLinkBlock *linkblock,
	const char *url, BrowserWindow *bw)
	
	{
	char *url_head, *url_tail;

	/* todo: scroll to the name in the url_tail */

	gzilla_bw_add_bytesink (bw, bytesink);
	gzilla_url_parse_hash (&url_head, &url_tail, url);
	
	if (linkblock != NULL)
	gzilla_linkblock_set_base_url (linkblock, url_head);
	gzilla_cache_get_url (bytesink, url_head);
	g_free (url_head);
	g_free (url_tail);
	}


void open_url_raw (GzillaByteSink* bytesink, const char *url)
	{ gzilla_cache_get_url (bytesink, url); }


/* Is this being used? */
void request_url(GzillaByteSink * bytesink, GzillaByteSink * sink2,
	char *url, BrowserWindow *bw)
	
	{
	#ifdef VERBOSE
	g_print("request_url %s\n", url);
	#endif

	open_url (sink2, NULL, url, bw);
	}


void request_url_img (GzillaByteSink *bytesink, GzillaImgSink *imgsink,
	char *url, BrowserWindow *bw)

	{
	#ifdef VERBOSE
	g_print("request_url_img %s\n", url);
	#endif

	gzilla_bw_add_imgsink (bw, imgsink);
	gzilla_dicache_open (imgsink, url, bw);
	}


void follow_link (GzillaByteSink *bytesink, char *url, BrowserWindow *bw)
	{
	#ifdef VERBOSE
	g_print("follow_link %s\n", url);
	#endif
	gzilla_nav_push(url, bw);
	}


void show_page_status (GzillaByteSink *bytesink, GzillaStatusDir dir,
	gboolean abort, GzillaStatusMeaning meaning,
	const char *text, BrowserWindow *bw)

	{
	#ifdef VERBOSE
	g_print("show_page_status %s\n", text);
	#endif
	if (meaning != GZILLA_STATUS_MEANING_SHOW)
	return;

	gzilla_status (text, bw);
	}


/* todo: idle func needs to be passed both bw and string.
Come to think of it, we need to remove the idle func in case of a
browser window change... put the idle tag into browserwindow...
then, the redirect url could just be a field in BrowserWindow
so we don't have to create a new struct. */
gint redirect_idle_func (BrowserWindow *bw)
	{
	gzilla_nav_redirect (bw->redir_url, bw);
	g_free (bw->redir_url);
	bw->redir_url = NULL; /* don't leave pointers hanging around */
	return FALSE;
	}


void redirect (GzillaByteSink *bytesink, char *url, BrowserWindow *bw)
	{
	#ifdef VERBOSE
	g_print("redirect %s\n", url);
	#endif
	bw->redir_url = g_strdup (url);
	gtk_idle_add((GtkFunction) redirect_idle_func, bw);
	}

void title(GzillaByteSink * bytesink, char *title, BrowserWindow *bw)
	{
	#ifdef VERBOSE
	g_print("title %s\n", title);
	#endif
	gzilla_set_page_title (bw, title);
	}

void size_allocate(GtkWidget * widget, GtkAllocation * allocation)
	{
	#ifdef VERBOSE
	g_print("size_allocate (%d, %d)\n",
		allocation->width, allocation->height);
	#endif
	#if 0
	/* todo: figure out how to dispatch set_width calls through gzillaweb */
	if (browser_window->doc_widget[0]!= NULL)
		{
	gtk_page_set_width(browser_window[0]->doc_widget,
		allocation->width - 24);
		}
	#endif
	}


int main (int argc, char *argv[])
	{
	gchar *file;
	BrowserWindow *bw;
	
	/* DNS init precedes GTK init so that the child processes don't inherit
	X connections or any other GTK resources. */
	gzilla_dns_init ();

	gtk_true ();

	gtk_init(&argc, &argv);
	
	/* check that ~/.gzilla exists, create it if it doesn't */
	/* why do we need a whole function for this? */
	file = gzilla_misc_prepend_user_home(".gzilla");
	gzilla_check_home_dir(file);
	g_free(file);

	gzilla_socket_init ();
	gzilla_file_init ();
	gzilla_http_init ();
	gzilla_cache_init ();
	gzilla_dicache_init ();
	gzilla_bw_init ();
	gzilla_proto_init ();

	/* gzilla_nav_init() has been moved into this call because it needs to be
	initialized with the new browser_window structure */
	/* CP: What does 'moved into this call' mean? */
	bw = gzilla_new_browser_window();
	
	/* add the bookmarks to the menus */
	file = gzilla_misc_prepend_user_home(".gzilla/bookmarks.html");
	gzilla_bookmarks_load(file);
	g_free(file);

	if (argc == 2)
		{
		if (gzilla_url_is_absolute(argv[1]))
			{
			gzilla_nav_push(argv[1], bw);
			} else {
			/* CP: Would be wise to verify that URLs stay within those bounds */
			char url[1024];
			if (strlen(argv[1]) + 6 <= sizeof(url))
				{
				/* CP: Just blithely assume it's a file URL?
				Need more sophisticated heuristics. If there is a TLD, then 
				proto = HTTP, for instance. */
				/* CP: Would also be wise to postpend '/' on dirs and domain 
				names. */
				sprintf(url, "file:%s", argv[1]);
				gzilla_nav_push(url, bw);
				}
			}
		}
	gtk_main();
	
	return 0;
	}


void gzilla_check_home_dir(char *dir)
{
	struct stat st;
	
	if (stat(dir, &st) < 0)
		{
		/* directory does not exist, create */
		if ( (mkdir(dir, 0700) < 0))
			{
			fprintf(stderr, "gzilla: error creating directory %s: %s\n",
			dir, strerror(errno));
			return;
			}
		}
	
	if (! (S_IFDIR & st.st_mode))
	{
	/* file exists, but is not directory..
	 * guess we'll just leave it for now
	g_print("%s exists but is not a directory!\n", dir); */
	/* CP: Go ahead and whack the file, muaahahahaha, since we know there's
	no reason for them to have anything but a dir. */
		if ( system("rm -f ~/.gzilla") < 0)
			{
			fprintf(stderr, "Gzilla: could not erase faulty %s: %s\n",
				dir, strerror(errno));
			return;
			}
		if ( (mkdir(dir, 0700) < 0))
			{
			fprintf(stderr, "Gzilla: error creating directory %s: %s\n",
				dir, strerror(errno));
			return;
			}
		} else {
		g_print("directory is ok\n");
		}
	}


#if 0
/* I needed this for debugging because I couldn't set breakpoints at the
system memmove. */
/* CP: I don't get much of this... It's tangly, which makes me tingly. */
void * memmove (void *dest, const void *src, size_t n)
	{
	int i;

	#if 0
	if (dest >= src + n || dest + n <= src)
		memcpy (dest, src, n);
	else
	#endif
	if (dest < src)
		for (i = 0; i < n; i++)
		((char *)dest)[i] = ((char *)src)[i];
	else
		for (i = n - 1; i >= 0; i--)
		((char *)dest)[i] = ((char *)src)[i];
	return dest;
}
#endif
