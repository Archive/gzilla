/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __GTK_GZW_SCROLLER_H__
#define __GTK_GZW_SCROLLER_H__


#include <gdk/gdk.h>
#include <gtk/gtkhscrollbar.h>
#include <gtk/gtkvscrollbar.h>

#include "gzw.h"
#include "gtkgzwview.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_GZW_SCROLLER(obj)          GTK_CHECK_CAST (obj, gtk_gzw_scroller_get_type (), GtkGzwScroller)
#define GTK_GZW_SCROLLER_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_gzw_scroller_get_type (), GtkGzwScrollerClass)
#define GTK_IS_GZW_SCROLLER(obj)       GTK_CHECK_TYPE (obj, gtk_gzw_scroller_get_type ())


typedef struct _GtkGzwScroller       GtkGzwScroller;
typedef struct _GtkGzwScrollerClass  GtkGzwScrollerClass;

struct _GtkGzwScroller
{
  GtkContainer container;

  GtkWidget *viewport;
  GtkWidget *hscrollbar;
  GtkWidget *vscrollbar;

  guint8 hscrollbar_policy;
  guint8 vscrollbar_policy;
};

struct _GtkGzwScrollerClass
{
  GtkContainerClass parent_class;
};


guint          gtk_gzw_scroller_get_type        (void);
GtkWidget*     gtk_gzw_scroller_new             (GtkAdjustment     *hadjustment,
						 GtkAdjustment     *vadjustment);
GtkAdjustment* gtk_gzw_scroller_get_hadjustment (GtkGzwScroller *gzw_scroller);
GtkAdjustment* gtk_gzw_scroller_get_vadjustment (GtkGzwScroller *gzw_scroller);
void           gtk_gzw_scroller_set_policy      (GtkGzwScroller *gzw_scroller,
						 GtkPolicyType      hscrollbar_policy,
						 GtkPolicyType      vscrollbar_policy);
void    gtk_gzw_scroller_set_gzw                (GtkGzwScroller *gzw_scroller,
						 Gzw *gzw);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_GZW_SCROLLER_H__ */
