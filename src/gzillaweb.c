/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

/* A dispatching bytesink that takes a generic HTTP response. */

#include <ctype.h> /* for tolower */
#include <stdio.h>

#include <gtk/gtk.h>

#include <jpeglib.h>		/* needed before including gzillajpeg.h */

#include "gzillaweb.h"
#include "gzillagif.h"
#include "gzillajpeg.h"
#include "gzillahtml.h"
#include "gzillaplain.h"
#include "gzillaimage.h"

#include "gzw.h"
#include "gzwembedgtk.h"

static void gzilla_web_class_init   (GzillaWebClass *klass);
static void gzilla_web_init         (GzillaWeb      *web);
static void gzilla_web_write        (GzillaByteSink *bytesink,
				     char *bytes,
			             gint num);
static void gzilla_web_close        (GzillaByteSink *bytesink);
static void gzilla_web_status       (GzillaByteSink *bytesink,
				     GzillaStatusDir dir,
				     gboolean abort,
				     GzillaStatusMeaning meaning,
				     const char *text);
static void gzilla_web_destroy      (GtkObject      *object);

static void gzilla_web_set_base_url (GzillaLinkBlock *linkblock,
				     const char *url,
				     GzillaByteSink *bytesink);

/* todo: add a destroy method which will also destroy the child */

static GzillaByteSinkClass *parent_class = NULL;

guint
gzilla_web_get_type ()
{
  static guint web_type = 0;

  if (!web_type)
    {
      GtkTypeInfo web_info =
      {
	"GzillaWeb",
	sizeof (GzillaWeb),
	sizeof (GzillaWebClass),
	(GtkClassInitFunc) gzilla_web_class_init,
	(GtkObjectInitFunc) gzilla_web_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      web_type = gtk_type_unique (gzilla_bytesink_get_type (), &web_info);
    }

  return web_type;
}

void
gzilla_web_class_init (GzillaWebClass *class)
{
  GtkObjectClass *object_class;
  GzillaByteSinkClass *bytesink_class;

  parent_class = gtk_type_class (gzilla_bytesink_get_type ());

  object_class = (GtkObjectClass *)class;
  bytesink_class = (GzillaByteSinkClass*) class;

  object_class->destroy = gzilla_web_destroy;

  bytesink_class->write = gzilla_web_write;
  bytesink_class->close = gzilla_web_close;
  bytesink_class->status = gzilla_web_status;
}

void gzilla_web_init (GzillaWeb *web) {
  web->size_header_max = 1024;
  web->size_header = 0;
  web->header = g_new (char, web->size_header_max);

  web->state = 0;

  web->child = NULL;

  web->base_url = NULL;
  web->width = 0;
  web->height = 0;
  web->alt = NULL;
  web->bg_color = 0xd6d6d6; /* hack! */
}

/* Extract a single field from the header, storing the value in field.
 Return TRUE if field found. */

static gboolean
gzilla_web_parse_field (const char *header, char *field, gint size_field,
			const char *fieldname) {
  gint i, j;

  for (i = 0; header[i] != '\0'; i++) {
    for (j = 0; fieldname[j] != '\0'; j++) {
      if (tolower (fieldname[j]) != tolower (header[i + j]))
	break;
    }
    if (fieldname[j] == '\0' && header[i + j] == ':') {
      i += j + 1;
      while (header[i] == ' ') i++;
      for (j = 0; j + 1 < size_field && header[i] != '\n'; j++)
	field[j] = header[i++];
      field[j] = '\0';
      return TRUE;
    } else {
      /* skip to next field */
      while (header[i] != '\0' && header[i] != '\n') i++;
      if (header[i] == '\0') i--;
    }
  }
  return FALSE;
}

/* Return TRUE if the field value matches the key. */
gboolean gzilla_web_match (const char *field, const char *key) {
  gint i;

  for (i = 0; key[i] != '\0'; i++) {
    if (tolower (key[i]) != tolower (field[i]))
      break;
  }
  return (key[i] == '\0' && (field[i] == '\0' || field[i] == ' ' ||
			     field[i] == ';'));
}

/* actually, for a lot of these functions, 1st arg is a linkblock, but
   it's not used. */

static void gzilla_web_handle_request_url (GzillaByteSink *bytesink,
					   GzillaByteSink *sink2,
					   char *url,
					   GzillaLinkBlock *parent) {
  gzilla_linkblock_request_url (parent, sink2, url);
}

static void
gzilla_web_handle_request_url_img (GzillaByteSink *bytesink,
				   GzillaImgSink *imgsink,
				   char *url,
				   GzillaLinkBlock *parent)
{
  gzilla_linkblock_request_url_img (parent, imgsink, url);
}

static void gzilla_web_handle_link (GzillaByteSink *bytesink,
				    char *url,
				    GzillaLinkBlock *parent) {
  gzilla_linkblock_link (parent, url);
}

static void gzilla_web_handle_status (GzillaByteSink *bytesink,
				      GzillaStatusDir dir,
				      gboolean abort,
				      GzillaStatusMeaning meaning,
				      const char *text,
				      GzillaLinkBlock *parent) {
#ifdef VERBOSE
  g_print ("gzilla_web_handler_status: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (dir != GZILLA_STATUS_DIR_DOWN)
    gzilla_linkblock_status (parent, GZILLA_STATUS_DIR_UP,
			     abort, meaning, text);

  if (abort)
    gtk_object_destroy (GTK_OBJECT (bytesink));
}

static void gzilla_web_handle_child_status (GzillaByteSink *bytesink,
					    GzillaStatusDir dir,
					    gboolean abort,
					    GzillaStatusMeaning meaning,
					    const char *text,
					    GzillaByteSink *parent) {
#ifdef VERBOSE
  g_print ("gzilla_web_handle_child_status: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (dir != GZILLA_STATUS_DIR_DOWN)
    gzilla_bytesink_status (parent, GZILLA_STATUS_DIR_UP,
			    abort, meaning, text);

  /* todo: delete bytesink on abort? */
}

static void gzilla_web_handle_title (GzillaByteSink *bytesink,
				     char *title,
				     GzillaLinkBlock *parent) {
  gzilla_linkblock_title (parent, title);
}

/* Parse the header for content-type, and create a new child based on
   that info. This is the main MIME dispatcher. */

static void gzilla_web_dispatch (GzillaWeb *web) {
  char content_type[256];
  char url[1024];
  Gzw *child_gzw;
  GzillaImgSink *imgsink;
  Gzw *gzw;

  if (web->header[9] == '3' && web->header[10] == '0') {
    /* redirect */
    if (gzilla_web_parse_field (web->header, url, sizeof(url),
				"Location")) {
      gzilla_linkblock_redirect (web->linkblock, url);
    }
    return;
  }
  child_gzw = NULL;
  gzw = NULL;
  web->child_linkblock = NULL;
  if (gzilla_web_parse_field (web->header, content_type, sizeof(content_type),
			      "Content-Type")) {

    /* todo: fix this code duplication, especially if the number of
       image types becomes large */
    if (gzilla_web_match (content_type, "image/gif")) {
      if (web->imgsink)
	imgsink = web->imgsink;
      else {
	imgsink = gzilla_image_new (0, 0, NULL, 0xd6d6d6);
	child_gzw = gzilla_image_gzw (imgsink);
      }
      web->child = gzilla_gif_new (imgsink);
    } else if (gzilla_web_match (content_type, "image/jpeg")) {
      if (web->imgsink)
	imgsink = web->imgsink;
      else {
	imgsink = gzilla_image_new (0, 0, NULL, 0xd6d6d6);
	child_gzw = gzilla_image_gzw (imgsink);
      }
      web->child = gzilla_jpeg_new (imgsink);
    } else if (gzilla_web_match (content_type, "text/html")) {
      web->child = gzilla_html_new ();
      /* todo: this should probably be a function of gzw_page */
      gzw = (GZILLA_HTML (web->child))->gzw;
      web->child_linkblock = gzilla_html_get_linkblock (GZILLA_HTML (web->child));
    } else if (gzilla_web_match (content_type, "text/plain")) {
      web->child = gzilla_plain_new ();
      /* todo: this should probably be a function of gzw_page */
      gzw = (GZILLA_PLAIN (web->child))->gzw;
    }

    if (web->child) {
      if (web->base_url) {
	if (web->child_linkblock)
	  gzilla_linkblock_set_base_url (web->child_linkblock, web->base_url);
	g_free (web->base_url);
        web->base_url = NULL;
      }
      if (!web->imgsink) {
#ifdef USE_GZW
	if (gzw == NULL)
	  {
	    /* todo: move creation of gzilla_image from dispatcher
	       to here. */
	    gzw = child_gzw;
	  }
	gzilla_linkblock_have_gzw (web->linkblock, gzw);
#else
	if (child_widget == NULL)
	  child_widget = web->child->widget;
	web->child_widget = child_widget;
	gtk_box_pack_start (GTK_BOX (web->bytesink.widget),
			    child_widget, TRUE, TRUE, 0);
	gtk_widget_show (child_widget);
#endif
      }

      if (web->child_linkblock != NULL)
	{
	  gtk_signal_connect (GTK_OBJECT (web->child_linkblock), "request_url",
			      GTK_SIGNAL_FUNC (gzilla_web_handle_request_url),
			      web->linkblock);
	  gtk_signal_connect (GTK_OBJECT (web->child_linkblock), "request_url_img",
			      GTK_SIGNAL_FUNC (gzilla_web_handle_request_url_img),
			      web->linkblock);
	  gtk_signal_connect (GTK_OBJECT (web->child_linkblock), "link",
			      GTK_SIGNAL_FUNC (gzilla_web_handle_link),
			      web->linkblock);
	  gtk_signal_connect (GTK_OBJECT (web->child_linkblock), "status",
			      GTK_SIGNAL_FUNC (gzilla_web_handle_status),
			      web->linkblock);
	  gtk_signal_connect (GTK_OBJECT (web->child_linkblock), "title",
			      GTK_SIGNAL_FUNC (gzilla_web_handle_title),
			      web->linkblock);
	}

      gtk_signal_connect (GTK_OBJECT (web->child), "status",
			  GTK_SIGNAL_FUNC (gzilla_web_handle_child_status),
			  web);

    }
  }
}

/* gzilla_web_write implements a state machine that (a) canonicalizes
   the line ends in the header to \n and (b) terminates when two line
   ends in a row have been detected.

   Here's the machine:
            \n      \r->\n
         1 -----> 2 -----> 3
         ^                 |
   \r->\n|                 | \n
         |                 v
           \n->\n   \n->\n _
         0 -----> 4 ----> (5)
                           -

   With the addition of c->c edges from nodes 0-4 to node 0. Also,
   node 6 is an error state that just consumes input.

   It would look a lot better if it weren't in ASCII art!

 */

static void gzilla_web_write (GzillaByteSink *bytesink,
			      char *bytes,
			      gint num) {
  GzillaWeb *web;
  gint i;
  char c;
  gint state;

  g_return_if_fail (bytesink != NULL);
  g_return_if_fail (GZILLA_IS_WEB (bytesink));
  g_return_if_fail (bytes != NULL);

  web = GZILLA_WEB (bytesink);

  state = web->state;
  if (web->child != NULL) {
    /* Already parsed header and initialized child, just pass on the
       bytes. */
    gzilla_bytesink_write (web->child, bytes, num);
  } else {
    for (i = 0; i < num && state < 5; i++) {
      if (web->size_header + 2 > web->size_header_max) {
	web->size_header_max <<= 1;
	web->header = g_realloc (web->header, web->size_header_max);
      }
      /* invariant: there is room for at least two more characters */
      c = bytes[i];
      if (state == 0 && c == '\r') {
	web->header[web->size_header++] = '\n';
	state = 1;
      } else if (state == 0 && c == '\n') {
	web->header[web->size_header++] = '\n';
	state = 4;
      } else if (state == 1 && c == '\n') {
	state = 2;
      } else if (state == 2 && c == '\r') {
	web->header[web->size_header++] = '\n';
	state = 3;
      } else if (state == 3 && c == '\n') {
	state = 5;
      } else if (state == 4 && c == '\n') {
	web->header[web->size_header++] = '\n';
	state = 5;
      } else {
	web->header[web->size_header++] = c;
	state = 0;
      }
    }
    if (state == 5) {
      /* Got header! Parse content-type and create a new child. */
      web->header[web->size_header] = '\0';
      gzilla_web_dispatch (web);
      if (web->child) {
	/* Write remainder of block (after header) into child. */
	if (i < num)
	  gzilla_bytesink_write (web->child, bytes + i, num - i);
      } else {
	state = 6;
      }
    }
    web->state = state;
  }
}

static void gzilla_web_close (GzillaByteSink *bytesink) {
  GzillaWeb *web;

  g_return_if_fail (bytesink != NULL);
  g_return_if_fail (GZILLA_IS_WEB (bytesink));

  web = GZILLA_WEB (bytesink);
  if (web->child != NULL) {
    gzilla_bytesink_close (web->child);
  }
  gtk_object_destroy (GTK_OBJECT (bytesink));
}

static void gzilla_web_status (GzillaByteSink *bytesink,
			       GzillaStatusDir dir,
			       gboolean abort,
			       GzillaStatusMeaning meaning,
			       const char *text)
{
  GzillaWeb *web;

  g_return_if_fail (bytesink != NULL);
  g_return_if_fail (GZILLA_IS_WEB (bytesink));

#ifdef VERBOSE
  g_print ("gzilla_web_status: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (dir != GZILLA_STATUS_DIR_UP)
    {
      web = GZILLA_WEB (bytesink);
      if (web->child != NULL)
	gzilla_bytesink_status (web->child,
				GZILLA_STATUS_DIR_DOWN,
				abort,
				meaning,
				text);
      else if (web->imgsink != NULL)
	gzilla_imgsink_status (web->imgsink,
			       GZILLA_STATUS_DIR_DOWN,
			       abort,
			       meaning,
			       text);
    }
  if (abort)
    gtk_object_destroy (GTK_OBJECT (bytesink));
}

static void
gzilla_web_destroy (GtkObject *object)
{
  GzillaWeb *web;

  web = GZILLA_WEB (object);
  if (web->base_url != NULL)
    g_free (web->base_url);
  /* We _could_ free the header right after dispatch. */
  g_free (web->header);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void
gzilla_web_set_base_url (GzillaLinkBlock *linkblock,
			 const char *url,
			 GzillaByteSink *bytesink)
{
  GzillaWeb *web;

  g_return_if_fail (bytesink != NULL);
  g_return_if_fail (GZILLA_IS_WEB (bytesink));
  g_return_if_fail (url != NULL);

  web = GZILLA_WEB (bytesink);
  if (web->child_linkblock != NULL)
    gzilla_linkblock_set_base_url (web->child_linkblock, url);
  else
    web->base_url = g_strdup (url);
}

/* New behavior: when enough bytes have been written, create a Gzw
   to hold the real contents, and throw it to a handler of the
   "have_gzw" signal.
   */

GzillaByteSink *
gzilla_web_new (void)
{
  GzillaWeb *web;

  web = gtk_type_new (gzilla_web_get_type ());

  web->imgsink = NULL;
  web->gzw = NULL;

  web->linkblock = gzilla_linkblock_new ();

  gtk_signal_connect (GTK_OBJECT (web->linkblock), "set_base_url",
		      GTK_SIGNAL_FUNC (gzilla_web_set_base_url),
		      GTK_OBJECT (web));

  return GZILLA_BYTESINK (web);
}

/* This handles aborts coming upstream from imgsinks.

   Generally, this routing gets invoked before the dispatch on the
   content type. After that, the image decoder is responsible for
   plumbing the status connection between the web bytesink and the
   imgsink. */
static void
gzilla_web_imgsink_status_handler (void *data,
				   GzillaImgSink *imgsink,
				   GzillaStatusDir dir,
				   gboolean abort,
				   GzillaStatusMeaning meaning,
				   const char *text)
{
  GzillaByteSink *bytesink = data;

#ifdef VERBOSE
  g_print ("gzilla_web_imgsink_status_handler: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif
  if (dir != GZILLA_STATUS_DIR_DOWN)
    gzilla_bytesink_status (bytesink, GZILLA_STATUS_DIR_UP,
			    abort, meaning, text);
#if 0
  if (abort)
    gtk_object_destroy (GTK_OBJECT (bytesink));
#endif
}

/* Create a new bytesink that decodes an image and feeds the decoded
   image into the given imgsink. */

GzillaByteSink* gzilla_web_new_img_decoder (GzillaImgSink *imgsink) {
  /* todo: implement this. */
  GzillaWeb *web;

  web = gtk_type_new (gzilla_web_get_type ());

  web->imgsink = imgsink;
  web->gzw = NULL;

  web->linkblock = gzilla_linkblock_new ();

  /* hook up status handler */
  gzilla_imgsink_set_status_handler (imgsink, gzilla_web_imgsink_status_handler,
				     &web->bytesink);

  return GZILLA_BYTESINK (web);
}

GzillaLinkBlock *
gzilla_web_get_linkblock (GzillaWeb *web)
{
  return web->linkblock;
}

