/* A Gzw bullet widget. */

#include <gtk/gtk.h>
#include "gzw.h"
#include "gzwbullet.h"

static void
gzw_bullet_size_nego_y (Gzw *gzw, GzwRect *allocation)
{
  gzw->allocation = *allocation;
}

static void
gzw_bullet_paint (Gzw *gzw, GzwRect *rect, GzwPaint *paint)
{
  GtkWidget *widget;
  GzwContainer *container;
  gint x, y;

#ifdef VERBOSE
  g_print ("gzw_bullet_paint (%d, %d) - (%d, %d)",
	   rect->x0, rect->y0, rect->x1, rect->y1);
#endif
  container = gzw_find_container (gzw);
  if (container != NULL)
    {
#ifdef VERBOSE
      g_print (", visible = (%d, %d) - (%d, %d)",
	       container->visible.x0, container->visible.y0,
	       container->visible.x1, container->visible.y1);
#endif
      widget = container->widget;
      gzw_paint_to_screen (widget, container, rect, paint);
      x = gzw->allocation.x0 + container->x_offset;
      y = gzw->allocation.y0 + container->y_offset;
      switch (((GzwBullet *)gzw)->type) {
      case GZW_BULLET_DISC:
	gdk_draw_arc (widget->window,
		      widget->style->fg_gc[widget->state],
		      TRUE,
		      x + 2,
		      y + 1,
		      4,
		      4,
		      0,
		      360*64);
	gdk_draw_arc (widget->window,
		      widget->style->fg_gc[widget->state],
		      FALSE,
		      x + 2,
		      y + 1,
		      4,
		      4,
		      0,
		      360*64);
	break;
      case GZW_BULLET_CIRCLE:
	gdk_draw_arc (widget->window,
		      widget->style->fg_gc[widget->state],
		      FALSE,
		      x + 1,
		      y,
		      6,
		      6,
		      0,
		      360*64);
	break;
      case GZW_BULLET_SQUARE:
	gdk_draw_rectangle (widget->window,
			    widget->style->fg_gc[widget->state],
			    FALSE,
			    x + 1,
			    y,
			    6,
			    6);
	break;
      }
      gzw_paint_finish_screen (paint);
    }
#ifdef VERBOSE
  g_print ("\n");
#endif
}

static void
gzw_bullet_destroy (Gzw *gzw)
{
  g_free (gzw);
}

static const GzwClass gzw_bullet_class =
{
  gzw_null_size_nego_x,
  gzw_bullet_size_nego_y,
  gzw_bullet_paint,
  gzw_null_handle_event,
  gzw_null_gtk_foreach,
  gzw_bullet_destroy,
  gzw_null_request_resize
};

Gzw *
gzw_bullet_new (GzwBulletType type)
{
  GzwBullet *gzw_bullet;
  GzwRect null_rect = {0, 0, 0, 0};

  gzw_bullet = g_new (GzwBullet, 1);
  gzw_bullet->gzw.klass = &gzw_bullet_class;
  gzw_bullet->gzw.allocation = null_rect;
  gzw_bullet->gzw.req_width_min = 8;
  gzw_bullet->gzw.req_width_max = 8;

  gzw_bullet->gzw.req_height = 8;
  gzw_bullet->gzw.req_ascent = 0;

  gzw_bullet->gzw.flags = 0;
  gzw_bullet->gzw.parent = NULL;
  gzw_bullet->gzw.container = NULL;

  gzw_bullet->type = type;

  return (Gzw *) gzw_bullet;
}
