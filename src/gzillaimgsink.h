#ifndef __GZILLA_IMG_SINK_H__
#define __GZILLA_IMG_SINK_H__

/* The GzillaImgSink data structure and methods. */

#include "gzillastatus.h"

typedef struct _GzillaImgSink GzillaImgSink;

typedef enum {
  GZILLA_IMG_TYPE_INDEXED,
  GZILLA_IMG_TYPE_RGB,
  GZILLA_IMG_TYPE_GRAY
} GzillaImgType;

struct _GzillaImgSink {
  void (* write) (GzillaImgSink *imgsink,
		  char *buf,
		  gint x,
		  gint y,
		  gint stride);
  void (* close) (GzillaImgSink *imgsink);
  void (* status) (GzillaImgSink *imgsink,
		   GzillaStatusDir dir,
		   gboolean abort,
		   GzillaStatusMeaning meaning,
		   const char *text);
  void (* set_parms) (GzillaImgSink *imgsink,
		      gint width,
		      gint height,
		      GzillaImgType type);
  void (* set_cmap) (GzillaImgSink *imgsink,
		     guchar *cmap,
		     gint num_colors,
		     gint bg_index);

  void (*close_handler) (void *data, GzillaImgSink *imgsink);
  void *close_data;

  void (*status_handler) (void *data, GzillaImgSink *imgsink,
			  GzillaStatusDir dir,
			  gboolean abort,
			  GzillaStatusMeaning meaning,
			  const char *text);
  void *status_data;

  void (*status_handler2) (void *data, GzillaImgSink *imgsink,
			   GzillaStatusDir dir,
			   gboolean abort,
			   GzillaStatusMeaning meaning,
			   const char *text);
  void *status_data2;
};

void gzilla_imgsink_init      (GzillaImgSink *imgsink);
gint gzilla_imgsink_bpp       (GzillaImgType type);
void gzilla_imgsink_write     (GzillaImgSink *imgsink,
			       char *buf,
			       gint x,
			       gint y,
			       gint stride);
void gzilla_imgsink_close     (GzillaImgSink *imgsink);
void gzilla_imgsink_status    (GzillaImgSink *imgsink,
			       GzillaStatusDir dir,
			       gboolean abort,
			       GzillaStatusMeaning meaning,
			       const char *text);
void gzilla_imgsink_set_parms (GzillaImgSink *imgsink,
			       gint width,
			       gint height,
			       GzillaImgType type);
void gzilla_imgsink_set_cmap  (GzillaImgSink *imgsink,
			       guchar *cmap,
			       gint num_colors,
			       gint bg_index);

/* low-tech signal handlers for close and abort signals */
void gzilla_imgsink_set_close_handler (GzillaImgSink *imgsink,
				       void (*close_handler) (void *data, GzillaImgSink *imgsink),
				       void *data);
/* this is a good argument for making a status handler a separate typedef,
   isn't it? */
void gzilla_imgsink_set_status_handler (GzillaImgSink *imgsink,
					void (*status_handler) (void *data,
								GzillaImgSink *imgsink,
								GzillaStatusDir dir,
								gboolean abort,
								GzillaStatusMeaning meaning,
								const char *text),
					void *data);

void gzilla_imgsink_set_status_handler2 (GzillaImgSink *imgsink,
					 void (*status_handler) (void *data,
								 GzillaImgSink *imgsink,
								 GzillaStatusDir dir,
								 gboolean abort,
								 GzillaStatusMeaning meaning,
								 const char *text),
					 void *data);

/* Note: the imgsink methods are always called in the following order:
   set_parms
   set_cmap, iff type in set_parms call was GZILLA_IMG_TYPE_INDEXED
   write, any number of times
   close | abort

   The close and abort methods are responsible for freeing all
   internal state and the imgsink itself. */

#endif /* __GZILLA_IMG_SINK__ */
