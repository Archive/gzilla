/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */


/* This module provides an API for asynchrnously retrieving URLs from
   the network and feeding them to bytesinks. */

#include <stdio.h> /* for sprintf */
#include <string.h> /* for strcmp */
#include <gtk/gtk.h>

#include "gzillaurl.h"
#include "gzillabytesink.h"
#include "gzillahttp.h"
#include "gzillafile.h"
#include "gzillaproto.h"

void
gzilla_about_get (const char *url,
		  GzillaByteSink *bytesink,
		  void (*status_callback) (const char *status, void *data),
		  void *data)
{
  char str[1024];
  char *loc;
  const char *tail;

  tail = url + 6; /* strlen ("about:") */
  loc = "http://www.gzilla.com/";
  if (!strcmp (tail, "jwz"))
    loc = "http://www.jwz.org/";
  else if (!strcmp (tail, "raph"))
    loc = "http://www.levien.com/";
  else if (!strcmp (tail, "yosh"))
    loc = "http://yosh.gimp.org/";
  else if (!strcmp (tail, "snorfle"))
    loc = "http://www.snorfle.net/";
  sprintf (str,
	   "HTTP/1.0 301 Moved\n"
	   "Location: %s\n"
	   "Content-Type: text-plain\n"
	   "\n"
	   "gzilla_about_get: this should never be displayed.\n",
	   loc);
  gzilla_bytesink_write (bytesink, str, strlen (str));
  return;
}

/* Create a new network connection for URL url, and asynchronously
   feed the bytes that come back to bytesink. */
void
gzilla_get_url (const char *url,
		GzillaByteSink *bytesink,
		void (*status_callback) (const char *status, void *data),
		void *data)
{
  gint proto_num;

  proto_num = gzilla_proto_find (url);
  if (proto_num >= 0) {
    gzilla_proto_get_url (url, proto_num, bytesink, status_callback, data);
  } else if (gzilla_url_match_method (url, "file")) {
    gzilla_file_get (url, bytesink, status_callback, data);
  } else if (gzilla_url_match_method (url, "http")) {
    gzilla_http_get (url, bytesink, status_callback, data);
  } else if (gzilla_url_match_method (url, "about")) {
    gzilla_about_get (url, bytesink, status_callback, data);
  }
  /* todo: error message for unknown method. */
}
