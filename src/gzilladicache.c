 /*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

/* The decompressed image cache for Gzilla. This code has been reviewed
   28 Nov 1997 by RLL. */

#include <gtk/gtk.h>
#include <string.h>         /* for memset */

#include "gzillabytesink.h"
#include "gzillaimgsink.h"
#include "gzilla.h"         /* for open_url_raw */
#include "gzillaweb.h"      /* for gzilla_web_new_img_decoder */
#include "gzilladicache.h"

/* This was a list of things to be done, but now it's all done.

   register imgsinks in the browser_window structure. I think
   that can happen outside this particular module (I'm thinking
   request_url_img) and also (hopefully) move the existing
   BrowserWindow stuff out.

   Once imgsinks are registered, bytesinks feeding image decoders will
   _not_ be registered with the browser window.

   abort_all will go; instead, gzilla_bw_abort_all will simply call
   abort for each imgsink in the browser window.

   I'll have to add a second callback for the "close" method so that
   closing an imgsink causes the interface to remove it from the
   imgsinks list. This will require a "baby" version of the GTK+
   signal mechanism.

   These changes are needed to make multiple browser windows work.

   This interface no longer involves the browser window. */

/* Bit vector stuff - split into another file? */
typedef char GzillaBitVec;


/* Create a new bit vector, with each bit set to FALSE. */

GzillaBitVec*
gzilla_bitvec_new (gint size) {
  GzillaBitVec *bitvec;

  bitvec = g_malloc ((size + 7) >> 3);
  memset (bitvec, 0, (size + 7) >> 3);
  return bitvec;
}

void
gzilla_bitvec_free (GzillaBitVec *bitvec)
{
  g_free (bitvec);
}

void
gzilla_bitvec_set (GzillaBitVec *bitvec, gint pos, gboolean val)
{
  char mask;
  gint index;

  mask = 1 << (pos & 7);
  index = pos >> 3;
  if (val)
    bitvec[index] |= mask;
  else
    bitvec[index] &= ~mask;
}

gboolean
gzilla_bitvec_get (GzillaBitVec *bitvec, gint pos)
{
  char mask;
  gint index;

  mask = 1 << (pos & 7);
  index = pos >> 3;
  return (bitvec[index] & mask) != 0;
}

/* Return the pos of the first bit set in the range [0..max), or max if
   all clear. */

gint
gzilla_bitvec_scan (GzillaBitVec *bitvec, gint start, gint max)
{
  gint i;

  /* A more efficient implementation based on byte (or even word)
     scanning is possible. I'll do it if profiling tells me I need to. */

  for (i = start; i < max; i++) {
    if (gzilla_bitvec_get (bitvec, i))
      break;
  }
  return i;
}

GzillaBitVec*
gzilla_bitvec_dup (GzillaBitVec *bitvec, gint size)
{
  GzillaBitVec *new_bitvec;

  new_bitvec = g_malloc ((size + 7) >> 3);
  memcpy (new_bitvec, bitvec, (size + 7) >> 3);
  return new_bitvec;
}

void
gzilla_bitvec_print (GzillaBitVec *bitvec, gint size)
{
  gint i;

  for (i = 0; i < size; i++)
    {
      g_print (gzilla_bitvec_get (bitvec, i) ? "*" : ".");
    }
  g_print ("\n");
}

typedef struct _GzillaDICacheLine GzillaDICacheLine;
typedef struct _GzillaDICacheCon GzillaDICacheCon;

struct _GzillaDICacheLine {
  char *url;
  gint width;
  gint height;
  GzillaImgType type;
  char *buf;
  GzillaBitVec *bitvec; /* bit=TRUE says line is present */
  guchar *cmap;         /* these three fields are only valid if */
  gint num_colors;      /*  type == GZILLA_IMG_TYPE_INDEXED     */
  gint bg_index;        /*                                      */
  gint last_use;
  gboolean eof;
  GzillaImgSink *imgsink;
};

struct _GzillaDICacheCon {
  GzillaDICacheLine *line;
  GzillaImgSink *imgsink;
  GzillaBitVec *bitvec; /* bit=TRUE says line needs to get written */
  gint start; /* invariant: bitvec[0..start) are all FALSE */
};

typedef struct _GzillaImgSinkDIC GzillaImgSinkDIC;

struct _GzillaImgSinkDIC {
  GzillaImgSink imgsink;
  GzillaDICacheLine *line;
};

#define DICACHE_MAX 2000000

GzillaDICacheLine **dicache;
gint dicache_size;
gint dicache_size_max;

GzillaDICacheCon **diccons;
gint num_diccons;
gint num_diccons_max;

gint dicache_size_total;	/* invariant: dicache_size_total is
				   the sum of the image sizes of all
				   cache lines in dicache. */
gint dicache_idle_tag;
gint dicache_counter;

void
gzilla_dicache_init (void)
{
  dicache_size = 0;
  dicache_size_max = 16;
  dicache = g_new(GzillaDICacheLine *, dicache_size_max);
	
  num_diccons = 0;
  num_diccons_max = 16;
  diccons = g_new(GzillaDICacheCon *, dicache_size_max);
	
  dicache_size_total = 0;
  dicache_idle_tag = 0;
  dicache_counter = 0;
}

/* Write a scan line into the cached image. This is only called from
   gzilla_dicache_imgsink_write, so maybe the two should be combined. */

static void
gzilla_dicache_write (GzillaDICacheLine *line,
		      char *buf,
		      gint x,
		      gint y,
		      gint stride)
{
  gint bpp, bpl;
  gint i;

  bpp = gzilla_imgsink_bpp (line->type);
  bpl = line->width * bpp;
  if (stride == 1)
    memcpy (line->buf + y * bpl, buf, bpl);
  else
    g_print ("gzilla_dicache_write: stride > 1 not implemented\n");
  gzilla_bitvec_set (line->bitvec, y, TRUE);
  for (i = 0; i < num_diccons; i++)
    {
      if (diccons[i]->line == line)
	{

	  /* Another possibility would be to write from the cached
	     image, guaranteeing a stride of 1 for downstream
	     imagesinks. Well, we can worry about that when we start
	     doing progressive png's. -Raph */

	  gzilla_imgsink_write (diccons[i]->imgsink, buf, x, y, stride);
	  gzilla_bitvec_set (diccons[i]->bitvec, y, FALSE);
	}
    }
}

/* Implement the write method of the cache line's image sink */
static void
gzilla_dicache_imgsink_write (GzillaImgSink *imgsink,
			      char *buf,
			      gint x,
			      gint y,
			      gint stride) {
  gzilla_dicache_write (((GzillaImgSinkDIC *)imgsink)->line,
			buf, x, y, stride);
}

/* Remove the dicache connection from the diccons table. */

static void
gzilla_dicache_delete_diccon (gint diccon_index)
{
  if (diccons[diccon_index]->bitvec)
    gzilla_bitvec_free (diccons[diccon_index]->bitvec);
  g_free (diccons[diccon_index]);
  diccons[diccon_index] = diccons[--num_diccons];
}

/* This is only called from gzilla_dicache_imgsink_write, so maybe the
   two should be combined. */
static void
gzilla_dicache_close (GzillaDICacheLine *line)
{
  gint i;

  line->eof = TRUE;
  for (i = 0; i < num_diccons; i++)
    {
      if (diccons[i]->line == line &&
	  gzilla_bitvec_scan (diccons[i]->bitvec,
			      diccons[i]->start, 
			      line->height) == line->height)
	{
	  gzilla_imgsink_close (diccons[i]->imgsink);
	  gzilla_dicache_delete_diccon (i--);
	}
    }
  line->imgsink = NULL;
}

/* Implement the close method of the cache line's image sink */
static void
gzilla_dicache_imgsink_close (GzillaImgSink *imgsink)
{
  gzilla_dicache_close (((GzillaImgSinkDIC *)imgsink)->line);
  g_free (imgsink);
}

/* Remove an image from the cache */
static void
gzilla_dicache_remove (gint dicache_index) {
  gint bpp, bpl;

  if (dicache[dicache_index]->buf)
    {
      g_free (dicache[dicache_index]->buf);
      bpp = gzilla_imgsink_bpp (dicache[dicache_index]->type);
      bpl = dicache[dicache_index]->width * bpp;
      dicache_size_total -= bpl * dicache[dicache_index]->height;
    }
  if (dicache[dicache_index]->bitvec)
    g_free (dicache[dicache_index]->bitvec);
  if (dicache[dicache_index]->cmap)
    g_free (dicache[dicache_index]->cmap);
  g_free (dicache[dicache_index]);
  dicache[dicache_index] = dicache[--dicache_size];
}

/* Expand the cache, throwing out old items to enforce the size limit. */

static void
gzilla_dicache_expand (gint size)
{
  gint dicache_index, diccon_index;
  gint best;
	
  dicache_size_total += size;
#ifdef VERBOSE
  g_print("dicache_size_total = %d\n", dicache_size_total);
#endif
	
  /* I know the algorithm is inefficient. I don't really think it matters. */
  while (dicache_size_total > DICACHE_MAX)
    {
      best = -1;
      for (dicache_index = 0; dicache_index < dicache_size; dicache_index++)
	if (dicache[dicache_index]->eof)
	  {
	    for (diccon_index = 0; diccon_index < num_diccons; diccon_index++)
	      if (diccons[diccon_index]->line == dicache[dicache_index])
		break;

	    if (diccon_index == num_diccons &&
		(best == -1 ||
		 dicache[dicache_index]->last_use < dicache[best]->last_use))
	      best = dicache_index;
	  }
      if (best == -1)
	return;
      gzilla_dicache_remove (best);
    }
}

/* Implement the status method of the cache line's image sink */
static void
gzilla_dicache_imgsink_status (GzillaImgSink *imgsink,
			       GzillaStatusDir dir,
			       gboolean abort,
			       GzillaStatusMeaning meaning,
			       const char *text)
{
  gint i;
  GzillaDICacheLine *line;

  line = ((GzillaImgSinkDIC *)imgsink)->line;

  if (dir == GZILLA_STATUS_DIR_UP)
    return;

  for (i = 0; i < num_diccons; i++)
    {
      if (diccons[i]->line == line)
	{
	  gzilla_imgsink_status (diccons[i]->imgsink,
				 GZILLA_STATUS_DIR_DOWN,
				 abort,
				 meaning,
				 text);
	  if (abort)
	    {
	      g_free (diccons[i]);
	      diccons[i] = diccons[--num_diccons];
	      i--;
	    }
	}
    }

  if (abort)
    {
      g_free (imgsink);
      for (i = 0; i < dicache_size; i++)
	if (dicache[i] == line)
	  break;
      if (i < dicache_size)
	gzilla_dicache_remove (i);
    }
}

/* Implement the set_parms method of the cache line's image sink */
static void
gzilla_dicache_imgsink_set_parms (GzillaImgSink *imgsink,
				  gint width,
				  gint height,
				  GzillaImgType type)
{
  GzillaDICacheLine *line;
  gint bpp, bpl;
  gint i;

  line = ((GzillaImgSinkDIC *)imgsink)->line;
  bpp = gzilla_imgsink_bpp (type);
  bpl = width * bpp;
  gzilla_dicache_expand (height * bpl);
  line->buf = g_malloc (height * bpl);
  line->bitvec = gzilla_bitvec_new (height);
  line->width = width;
  line->height = height;
  line->type = type;

  for (i = 0; i < num_diccons; i++)
    {
      if (diccons[i]->line == line)
	{
	  gzilla_imgsink_set_parms (diccons[i]->imgsink,
				    width, height, type);
	  diccons[i]->bitvec = gzilla_bitvec_new (height);
	}
    }
}

/* Implement the set_parms method of the cache line's image sink */
void
gzilla_dicache_imgsink_set_cmap (GzillaImgSink *imgsink,
				 guchar *cmap,
				 gint num_colors,
				 gint bg_index)
{
  GzillaDICacheLine *line;
  guchar *new_cmap;
  gint i;

  line = ((GzillaImgSinkDIC *)imgsink)->line;
  new_cmap = g_malloc (3 * num_colors);
  memcpy (new_cmap, cmap, 3 * num_colors);
  line->cmap = new_cmap;
  line->num_colors = num_colors;
  line->bg_index = bg_index;

  for (i = 0; i < num_diccons; i++)
    {
      if (diccons[i]->line == line)
	gzilla_imgsink_set_cmap (diccons[i]->imgsink,
				 cmap, num_colors, bg_index);
    }
}

/* Number of lines to write per idle cycle */
#define BLOCK_SIZE 100

gint
gzilla_dicache_idle_func (void)
{
  gint i;
  gint num_active;
  gint y;
  gint bpp, bpl;
  gint num_lines;

  num_active = 0;
  for (i = 0; i < num_diccons; i++)
    {
      /* Find out if there is a scanline in the cached image that hasn't
	 been written to the imgsink.

	 Note: maybe there should be a flag for an all-empty bitvec.
	 */
      if (diccons[i]->bitvec)
	{
	  num_active++;
	  for (num_lines = 0; num_lines < BLOCK_SIZE; num_lines++)
	    {
	      y = gzilla_bitvec_scan (diccons[i]->bitvec,
				      diccons[i]->start,
				      diccons[i]->line->height);
	      if (y < diccons[i]->line->height)
		{
		  bpp = gzilla_imgsink_bpp (diccons[i]->line->type);
		  bpl = diccons[i]->line->width * bpp;
		  gzilla_imgsink_write (diccons[i]->imgsink,
					diccons[i]->line->buf + y * bpl,
					0,
					y,
					1);
		  gzilla_bitvec_set (diccons[i]->bitvec, y, FALSE);
		  diccons[i]->start = y + 1;
		}
	      else 
		{
		  num_active--;
		  if (diccons[i]->line->eof)
		    {
		      /* all done writing cached image to imgsink */
		      gzilla_imgsink_close (diccons[i]->imgsink);
		      gzilla_dicache_delete_diccon (i--);
		    }
		  break;
		}
	    }
	}
    }
  if (num_active == 0)
    {
      dicache_idle_tag = 0;
#ifdef VERBOSE
      g_print ("removing dicache idle function\n");
#endif
      return FALSE;
    }
  else
    {
      return TRUE;
    }
}

/* This status handler is attached to all downstream imgsinks */
static void
gzilla_dicache_status_handler (void *data, GzillaImgSink *imgsink,
			       GzillaStatusDir dir,
			       gboolean abort,
			       GzillaStatusMeaning meaning,
			       const char *text)
{
  gint i;
  gint num_active;
  GzillaDICacheLine *line = data;

#ifdef VERBOSE
  g_print ("gzilla_dicache_status_handler: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (dir == GZILLA_STATUS_DIR_DOWN || !abort)
    return;

  for (i = 0; i < num_diccons; i++)
      if (diccons[i]->imgsink == imgsink)
	break;

  if (i == num_diccons)
    {
      g_warning ("gzilla_dicache_status_handler: imgsink not in the cache");
      return;
    }

  if (line != diccons[i]->line)
    g_warning ("gzilla_dicache_status_handler: line is inconsistent with diccons[i]->line!\n");

  g_free (diccons[i]);
  diccons[i] = diccons[--num_diccons];
	
  num_active = 0;
  for (i = 0; i < num_diccons; i++) {
    if (diccons[i]->line == line)
      num_active++;
  }

  if (num_active == 0 && line->imgsink != NULL)
    {
      gzilla_imgsink_status (line->imgsink,
			     GZILLA_STATUS_DIR_UP,
			     abort,
			     meaning,
			     text);
      for (i = 0; i < dicache_size; i++)
	{
	  if (dicache[i] == line)
	    break;
	}
      if (i < dicache_size)
	gzilla_dicache_remove (i);
      else
	g_warning ("gzilla_dicache_status_handler: can't find line to remove");
    }
}

void
gzilla_dicache_hit (GzillaImgSink *imgsink, GzillaDICacheLine *line)
{
  GzillaDICacheCon *diccon;

  if (num_diccons == num_diccons_max)
    {
      num_diccons_max <<= 1;
      diccons = g_realloc (diccons, num_diccons_max *
			   sizeof(GzillaDICacheCon *));
    }
  diccon = g_new (GzillaDICacheCon, 1);
  diccon->line = line;
  diccon->imgsink = imgsink;
  diccons[num_diccons++] = diccon;

  line->last_use = dicache_counter++;

  if (line->buf)
    {
      /* take this branch if gzilla_imgsink_set_parms () has already
	 been called on the cache line, so we know the image size */
      diccon->bitvec = gzilla_bitvec_dup (line->bitvec, line->height);
      diccon->start = gzilla_bitvec_scan (line->bitvec, 0, line->height);
      gzilla_imgsink_set_parms (imgsink,
				line->width, line->height, line->type);
      if (line->cmap)
	gzilla_imgsink_set_cmap (imgsink,
				 line->cmap, line->num_colors, line->bg_index);

      /* Add a new cache idle process if there isn't already one
	 going, and if the cache image has something in it. */
      if (dicache_idle_tag == 0 &&
	  (line->eof || diccon->start < line->height))
	{
#ifdef VERBOSE
	  g_print ("adding dicache idle function\n");
#endif
	  dicache_idle_tag = gtk_idle_add ((GtkFunction) gzilla_dicache_idle_func,
					   NULL);
	}
    }
  else
    {
      diccon->start = 0;
      diccon->bitvec = NULL;
    }

  gzilla_imgsink_set_status_handler (imgsink,
				     gzilla_dicache_status_handler, line);
}

/* The url missed in the cache.

   1. Create a new imgsink that will be used to feed the decompressed
      image data into the cache.
   2. Create a new image decoder bytesink.
   3. Request that the URL be opened and start feeding the new imgsink.
   4. Make a new cache line.
   5. Do a gzilla_dicache_hit to connect the cache line to the argument
      imgsink.
*/

void
gzilla_dicache_miss (GzillaImgSink *imgsink, const char *url,
		     BrowserWindow *bw)
{
  GzillaImgSink *new_imgsink;
  GzillaImgSinkDIC *new_imgsinkdic;
  GzillaDICacheLine *line;
  GzillaByteSink *bytesink;

  /* Step 1 */
  line = g_new (GzillaDICacheLine, 1);
  new_imgsinkdic = g_new (GzillaImgSinkDIC, 1);
  new_imgsink = &new_imgsinkdic->imgsink;
  gzilla_imgsink_init (new_imgsink);
  new_imgsink->write = gzilla_dicache_imgsink_write;
  new_imgsink->close = gzilla_dicache_imgsink_close;
  new_imgsink->status = gzilla_dicache_imgsink_status;
  new_imgsink->set_parms = gzilla_dicache_imgsink_set_parms;
  new_imgsink->set_cmap = gzilla_dicache_imgsink_set_cmap;
  new_imgsinkdic->line = line;

  /* Step 2 */
  bytesink = gzilla_web_new_img_decoder (new_imgsink);

  /* Step 4 */
  line->url = g_strdup (url);
  line->buf = NULL;
  line->bitvec = NULL;
  line->cmap = NULL;
  line->last_use = dicache_counter++;
  line->eof = FALSE;
  line->imgsink = new_imgsink;

  if (dicache_size == dicache_size_max)
    {
      dicache_size_max <<= 1;
      dicache = g_realloc(dicache, dicache_size_max *
			  sizeof(GzillaDICacheLine *));
    }
  dicache[dicache_size++] = line;

  /* Step 5 */
  gzilla_dicache_hit (imgsink, line);

  /* Step 3 */
  /* This is done later so that any status signals generated by the
     open_url_raw get propagated. */
  open_url_raw (bytesink, url);
}

void
gzilla_dicache_open (GzillaImgSink *imgsink, const char *url,
		     BrowserWindow *bw)
{
  gint i;

#undef DISABLE_DICACHE
#ifdef DISABLE_DICACHE
  {
    GzillaByteSink *bytesink;
    bytesink = gzilla_web_new_img_decoder (imgsink);
    open_url_raw (bytesink, url);
  }
#else
  for (i = 0; i < dicache_size; i++)
    {
      if (!strcmp (url, dicache[i]->url)) {
	gzilla_dicache_hit (imgsink, dicache[i]);
	return;
      }
    }
  gzilla_dicache_miss (imgsink, url, bw);
#endif
}


