/* Some simple data structures and utilities for working with rectangles. */

#ifndef __GZW_RECT_H__
#define __GZW_RECT_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GzwPoint GzwPoint;
typedef struct _GzwRect GzwRect;

struct _GzwPoint {
  gint x, y;
};

struct _GzwRect {
  gint x0, y0, x1, y1;
};

/* Since rectangles are of fixed size, it is expected that they are
   statically allocated (i.e. on the stack). Functions on rectangles,
   however, will be defined in terms of pointers to rectangles.

   Functions with a rectangle return will have a "destination"
   rectangle pointer as the first argument. */

void gzw_rect_copy (GzwRect *dest, const GzwRect *src);
void gzw_rect_union (GzwRect *dest, const GzwRect *src1, const GzwRect *src2);
void gzw_rect_intersect (GzwRect *dest,
			 const GzwRect *src1, const GzwRect *src2);
gboolean gzw_rect_empty (const GzwRect *src);
gboolean gzw_point_inside (GzwRect *rect, GzwPoint *point);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZW_RECT_H__ */
