#ifndef __GZILLA_MISC_H__
#define __GZILLA_MISC_H__

char *gzilla_misc_user_home(void);
char *gzilla_misc_prepend_user_home(char *file);
char *gzilla_misc_file(char *file);

#endif /* __GZILLA_MISC_H__ */

