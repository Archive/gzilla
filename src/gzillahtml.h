#ifndef __GTK_HTML_H__
#define __GTK_HTML_H__

#include <gdk/gdk.h>
#include <gtk/gtkcontainer.h>
#include "gzillabytesink.h"
#include "gzillalinkblock.h"

#include "gzw.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* First, the html linkblock. For now, this mostly has forms, although
   pointers to actual links will go here soon, if for no other reason
   than to implement history-sensitive link colors. Also, it seems
   likely that imagemaps will go here. */

#define GZILLA_HTML_LB(obj)           GTK_CHECK_CAST (obj, gzilla_html_lb_get_type (), GzillaHtmlLB)
#define GZILLA_HTML_LB_CLASS(klass)   GTK_CHECK_CLASS_CAST (klass, gzilla_html_get_lb_type (), GzillaHtmlLBClass)
#define GZILLA_IS_HTML_LB(obj)        GTK_CHECK_TYPE (obj, gzilla_html_lb_get_type ())

typedef struct _GzillaHtmlLB      GzillaHtmlLB;
typedef struct _GzillaHtmlLBClass GzillaHtmlLBClass;

typedef struct _GzillaHtml        GzillaHtml;
typedef struct _GzillaHtmlClass   GzillaHtmlClass;
typedef struct _GzillaHtmlState   GzillaHtmlState;
typedef struct _GzillaHtmlForm    GzillaHtmlForm;
typedef struct _GzillaHtmlOption  GzillaHtmlOption;
typedef struct _GzillaHtmlSelect  GzillaHtmlSelect;
typedef struct _GzillaHtmlInput   GzillaHtmlInput;


struct _GzillaHtmlLB {
  GzillaLinkBlock linkblock;

  GzillaHtmlForm *forms;
  gint num_forms;
  gint num_forms_max;
};

struct _GzillaHtmlLBClass {
  GzillaLinkBlockClass  parent_class;
};

/* Now, the html bytesink object. */


#define GZILLA_HTML(obj)           GTK_CHECK_CAST (obj, gzilla_html_get_type (), GzillaHtml)
#define GZILLA_HTML_CLASS(klass)   GTK_CHECK_CLASS_CAST (klass, gzilla_html_get_type (), GzillaHtmlClass)
#define GZILLA_IS_HTML(obj)        GTK_CHECK_TYPE (obj, gzilla_html_get_type ())

typedef enum {
  GZILLA_HTML_PARSE_MODE_INIT,
  GZILLA_HTML_PARSE_MODE_STASH,
  GZILLA_HTML_PARSE_MODE_BODY,
  GZILLA_HTML_PARSE_MODE_PRE
} GzillaHtmlParseMode;

struct _GzillaHtmlState {
  char *tag;
  gint attr;
  GzillaHtmlParseMode parse_mode;
  gint list_level;
  gint form;
};

typedef enum {
  GZILLA_HTML_METHOD_GET,
  GZILLA_HTML_METHOD_POST
} GzillaHtmlMethod;

typedef enum {
  GZILLA_HTML_ENC_URLENCODING
} GzillaHtmlEnc;

struct _GzillaHtmlForm {
  GzillaHtmlMethod method;
  char *action;
  GzillaHtmlEnc enc;

  GzillaHtmlInput *inputs;
  gint num_inputs;
  gint num_inputs_max;
};

struct _GzillaHtmlOption {
  GtkWidget *menuitem;
  char *value;
  gboolean init_val;
};

struct _GzillaHtmlSelect {
  GtkWidget *menu;
  gint size;

  GzillaHtmlOption *options;
  gint num_options;
  gint num_options_max;
};

typedef enum {
  GZILLA_HTML_INPUT_TEXT,
  GZILLA_HTML_INPUT_PASSWORD,
  GZILLA_HTML_INPUT_CHECKBOX,
  GZILLA_HTML_INPUT_RADIO,
  GZILLA_HTML_INPUT_IMAGE,
  GZILLA_HTML_INPUT_HIDDEN,
  GZILLA_HTML_INPUT_SUBMIT,
  GZILLA_HTML_INPUT_RESET,
  GZILLA_HTML_INPUT_SELECT,
  GZILLA_HTML_INPUT_SEL_LIST,
  GZILLA_HTML_INPUT_TEXTAREA
} GzillaHtmlInputType;

struct _GzillaHtmlInput {
  GzillaHtmlInputType type;
  GtkWidget *widget;
  char *name;
  char *init_str;    /* note: some overloading - for buttons, init_str
			is simply the value of the button; for text
			entries, it is the initial value */
  GzillaHtmlSelect *select;
  gboolean init_val; /* only meaningful for buttons */
};

struct _GzillaHtml {
  GzillaByteSink bytesink;

  Gzw *gzw;

  GzillaHtmlLB *linkblock;

  char *inbuf;        /* a partial token of input */
  gint size_inbuf;
  gint size_inbuf_max;

  GzillaHtmlState *stack;
  gint num_states;
  gint num_states_max;

  char *base_url;

  char *stash_buf;
  gint stash_size;
  gint stash_size_max;
  gboolean stash_space;
};

struct _GzillaHtmlClass {
  GzillaByteSinkClass  parent_class;
};

guint           gzilla_html_get_type (void);
guint           gzilla_html_lb_get_type (void);
GzillaByteSink *gzilla_html_new (void);
GzillaLinkBlock *gzilla_html_get_linkblock (GzillaHtml *html);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_HTML_H__ */
