/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

/* The gif decoder for gzilla. It is responsible for decoding GIF data
   in a bytesink and transferring it to an imgsink.

   Adapted by Raph Levien from giftopnm.c as found in the
   netpbm-1mar1994 release. The copyright notice for giftopnm.c
   follows: */

/* +-------------------------------------------------------------------+ */
/* | Copyright 1990, 1991, 1993, David Koblas.  (koblas@netcom.com)    | */
/* |   Permission to use, copy, modify, and distribute this software   | */
/* |   and its documentation for any purpose and without fee is hereby | */
/* |   granted, provided that the above copyright notice appear in all | */
/* |   copies and that both that copyright notice and this permission  | */
/* |   notice appear in supporting documentation.  This software is    | */
/* |   provided "as is" without express or implied warranty.           | */
/* +-------------------------------------------------------------------+ */


/* Notes 13 Oct 1997

   Today, just for the hell of it, I implemented a new decoder from
   scratch. It's oriented around pushing bytes, while the old decoder
   was based around reads which may suspend. There were basically
   three motivations.

   1. To increase the speed.

   2. To fix some bugs I had seen, most likely due to suspension.

   3. To make sure that the code had no buffer overruns or the like.

   4. So that the code could be released under a freer license.

   Let's see how we did on speed. I used a large image for testing
   (fvwm95-2.gif).

   The old decoder spent a total of about 1.04 seconds decoding the
   image. Another .58 seconds went into gzilla_image_line, almost
   entirely conversion from colormap to RGB.

   The new decoder spent a total of 0.46 seconds decoding the image.
   However, the time for gzilla_image_line went up to 1.01 seconds.
   Thus, even though the decoder seems to be about twice as fast,
   the net gain is pretty minimal. Could this be because of cache
   effects?

   Lessons learned: The first, which I keep learning over and over, is
   not to try to optimize too much. It doesn't work. Just keep things
   simple. 

   Second, it seems that the colormap to RGB conversion is really a
   significant part of the overall time. It's possible that going
   directly to 16 bits would help, but that's optimization again :)

*/

/* Todos:

 + Make sure to handle error cases gracefully (including aborting the
   imgsink, if necessary).

 */

#undef VERBOSE
#define NEW_DECODER

#include <stdio.h>  /* for sprintf */
#include <string.h> /* for memcpy and memmove */

#include <gtk/gtk.h>
#include "gzillabytesink.h"
#include "gzillaimgsink.h"
#include "gzillagif.h"

static void gzilla_gif_class_init (GzillaGifClass *klass);
static void gzilla_gif_init       (GzillaGif      *gif);
static void gzilla_gif_destroy    (GtkObject      *object);
static void gzilla_gif_write      (GzillaByteSink *bytesink,
				   char *bytes,
				   gint num);
static void gzilla_gif_close      (GzillaByteSink *bytesink);
static void gzilla_gif_status     (GzillaByteSink *bytesink,
				   GzillaStatusDir dir,
				   gboolean abort,
				   GzillaStatusMeaning meaning,
				   const char *text);

static GzillaByteSinkClass *parent_class = NULL;



#define INTERLACE              0x40
#define LOCALCOLORMAP  0x80

#define LM_to_uint(a,b)                        (((b)<<8)|(a))

#define GZILLA_GIF_CLEAN 0
#define GZILLA_GIF_DIRTY 1
#define GZILLA_GIF_SIZECHANGE 2

guint
gzilla_gif_get_type ()
{
  static guint gif_type = 0;

  if (!gif_type)
    {
      GtkTypeInfo gif_info =
      {
	"GzillaGif",
	sizeof (GzillaGif),
	sizeof (GzillaGifClass),
	(GtkClassInitFunc) gzilla_gif_class_init,
	(GtkObjectInitFunc) gzilla_gif_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      gif_type = gtk_type_unique (gzilla_bytesink_get_type (), &gif_info);
    }

  return gif_type;
}

void
gzilla_gif_class_init (GzillaGifClass *class)
{
  GtkObjectClass      *object_class;
  GzillaByteSinkClass *bytesink_class;

  parent_class = gtk_type_class (gzilla_bytesink_get_type ());

  object_class = (GtkObjectClass*) class;
  bytesink_class = (GzillaByteSinkClass*) class;

  object_class->destroy = gzilla_gif_destroy;

  bytesink_class->write = gzilla_gif_write;
  bytesink_class->close = gzilla_gif_close;
  bytesink_class->status = gzilla_gif_status;
}

void gzilla_gif_init (GzillaGif *gif) {
  gif->state = 0;
  gif->size_inbuf = 0;
  gif->size_inbuf_max = 1024;
  gif->inbuf = g_new (char, gif->size_inbuf_max);
  gif->linebuf = NULL;
  gif->fresh = FALSE;
  gif->transparent = -1;
  gif->num_spill_lines_max = 0;
  gif->spill_lines = 0;
}

static gint process_bytes (GzillaGif *gif, char *buf, gint bufsize);

static void lwz_init (GzillaGif *gif);
static void get_code_init (GzillaGif *gif);
static int
lwz_read_byte (GzillaGif *gif, char **pbuf, gint *pbufsize);
static int
get_code (GzillaGif *gif, char **pbuf, gint *pbufsize);
static int
get_data_block (char **pbuf, gint *pbufsize, char *buf);

static void
gzilla_gif_status_handler (void *data,
			   GzillaImgSink *imgsink,
			   GzillaStatusDir dir,
			   gboolean abort,
			   GzillaStatusMeaning meaning,
			   const char *text)
{
  GzillaGif *gif = data;
 
#ifdef VERBOSE
  g_print ("gzilla_gif_status_handler: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (dir != GZILLA_STATUS_DIR_DOWN)
    gzilla_bytesink_status (&gif->bytesink, GZILLA_STATUS_DIR_UP,
			    abort, meaning, text);
}

GzillaByteSink*
gzilla_gif_new (GzillaImgSink *imgsink)
{
  GzillaGif *gif;

  gif = gtk_type_new (gzilla_gif_get_type ());

  gif->imgsink = imgsink;

  gzilla_imgsink_set_status_handler (imgsink, gzilla_gif_status_handler,
				     gif);

#ifdef VERBOSE
  g_print ("new gif %x\n", gif);
#endif

  return &gif->bytesink;
}

static void gzilla_gif_write (GzillaByteSink *bytesink,
			      char *bytes,
			      gint num) {
  GzillaGif *gif;
  char *buf;
  gint bufsize, bytes_consumed;

  gif = (GzillaGif *)bytesink;

#ifdef VERBOSE
  g_print ("gzilla_gif_write: %d bytes\n", num);
#endif
  /* Concatenate with the partial input, if any. */
  if (gif->size_inbuf) {
    bufsize = gif->size_inbuf + num;
    while (gif->size_inbuf_max < bufsize)
      gif->size_inbuf_max <<= 1;
    gif->inbuf = g_realloc (gif->inbuf, gif->size_inbuf_max);
    buf = gif->inbuf;
    memcpy (buf + gif->size_inbuf, bytes, num);
  } else {
    buf = bytes;
    bufsize = num;
  }

  /* Process the bytes in the input buffer. */
  do {
    if (bufsize > 0) {
      bytes_consumed = process_bytes (gif, buf, bufsize);
      buf += bytes_consumed;
      bufsize -= bytes_consumed;
    } else {
      bytes_consumed = 0;
    }
  } while (bytes_consumed > 0);

  /* Put unprocessed input in inbuf, if any. */
  gif->size_inbuf = bufsize;
  if (gif->size_inbuf > 0) {
    /* todo: check that inbuf is big enough */
    memmove (gif->inbuf, buf, gif->size_inbuf);
  }

#ifdef VERBOSE
  g_print ("exit gzilla_gif_write, bufsize=%d\n", bufsize);
#endif
}

static void
gzilla_gif_close (GzillaByteSink *bytesink)
{
#ifdef VERBOSE
  g_print ("gzilla_gif_close: %x\n", bytesink);
#endif

  if ((GZILLA_GIF (bytesink))->imgsink != NULL)
    gzilla_imgsink_status ((GZILLA_GIF (bytesink))->imgsink,
			   GZILLA_STATUS_DIR_DOWN, TRUE,
			   GZILLA_STATUS_MEANING_DEBUG,
			   "truncated gif");

  gtk_object_destroy (GTK_OBJECT (bytesink));
}

static void
gzilla_gif_status (GzillaByteSink *bytesink,
		   GzillaStatusDir dir,
		   gboolean abort,
		   GzillaStatusMeaning meaning,
		   const char *text)
{
#ifdef VERBOSE
  g_print ("gzilla_gif_status: %x %d %d %d %s\n", bytesink,
	   dir, abort, meaning, text);
#endif

  if (dir != GZILLA_STATUS_DIR_UP)
    gzilla_imgsink_status ((GZILLA_GIF (bytesink))->imgsink,
			   GZILLA_STATUS_DIR_DOWN, abort, meaning, text);

  if (abort)
    gtk_object_destroy (GTK_OBJECT (bytesink));
}

static void gzilla_gif_destroy (GtkObject *object) {
  GzillaGif *gif;
  gint i;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GZILLA_IS_GIF (object));

  gif = GZILLA_GIF (object);

#ifdef VERBOSE
  g_print ("destroy gif %x\n", gif);
#endif

  if (gif->linebuf != NULL)
    g_free (gif->linebuf);
  g_free (gif->inbuf);

  if (gif->spill_lines != NULL)
    {
      for (i = 0; i < gif->num_spill_lines_max; i++)
	g_free (gif->spill_lines[i]);
      g_free (gif->spill_lines);
    }

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static gboolean read_ok (char **pbuf, gint *pbufsize, char *obuf, gint size) {
  if (size > *pbufsize)
    return FALSE;
  memcpy (obuf, *pbuf, size);
  *pbuf += size;
  *pbufsize -= size;
  return TRUE;
}

/* Return TRUE when the extension is over */
static gboolean do_extension (GzillaGif *gif, char **pbuf, gint *pbufsize) {
  gint size;
  unsigned char buf[260];

  size = get_data_block (pbuf, pbufsize, buf);
  if (size == -1)
    return FALSE;
  else if (size == 0)
    return TRUE;
  if (gif->label == 0xf9) {
    /* Graphic Control Extension */
    gif->disposal = (buf[0] >> 2) & 0x7;
    gif->inputFlag = (buf[0] >> 1) & 0x1;
    gif->delayTime = LM_to_uint (buf[1], buf[2]);
    if (buf[0] & 0x1)
      gif->transparent = buf[3];
  }
  return FALSE;
}

/* Here begins the new push-oriented decoder. It should be quite a bit
   faster than the old code, which was adapted to be asynchronous from
   David Koblas's original giftopnm code.

 */

static void
gzilla_gif_lwz_init (GzillaGif *gif)
{
  gif->num_spill_lines_max = 1;
  gif->spill_lines = g_new (unsigned char *, gif->num_spill_lines_max);
  gif->spill_lines[0] = g_new (unsigned char, gif->Width);
  gif->bits_in_window = 0;
  gif->last_code = (1 << gif->input_code_size) + 1;
  gif->code_size = gif->input_code_size + 1;
  gif->line_index = 0;
}

/* Send the image line to the imgsink, also handling the interlacing. */
static void
gzilla_gif_emit_line (GzillaGif *gif, unsigned char *linebuf)
{
  gzilla_imgsink_write (gif->imgsink, linebuf, 0, gif->y, 1);
  if (gif->interlace) {
    switch (gif->pass) {
    case 0:
    case 1:
      gif->y += 8;
      break;
    case 2:
      gif->y += 4;
      break;
    case 3:
      gif->y += 2;
      break;
    }
    if (gif->y >= gif->Height) {
      gif->pass++;
      switch (gif->pass) {
      case 1:
	gif->y = 4;
	break;
      case 2:
	gif->y = 2;
	break;
      case 3:
	gif->y = 1;
	break;
      default:
	/* arriving here is an error in the input image. */
	gif->y = 0;
	break;
      }
    }
    
  } else {
    if (gif->y < gif->Height)
      gif->y++;
  }
}

/* Decode the packetized lwz bytes into the image sink.

   Return codes:

    0 = working
    1 = successful eof code (but does not consume zero size packet)
   -1 = error

   I apologize for the large size of this routine and the goto error
   construct - I almost _never_ do that. I offer the excuse of
   optimizing for speed. */

static gint
gzilla_gif_decode (GzillaGif *gif, unsigned char **pbuf, gint *pbufsize)
{
  gint lwz_bytes;
  unsigned char *buf;
  gint bufsize;
  gint packet_size;
  gint clear_code;
  guint window;
  gint bits_in_window;
  gint code;
  gint last_code;
  gint code_size;
  gint code_mask;
  gint width;
  unsigned char *linebuf;
  gint line_index;
  gint sequence_length;
  gint num_spill_lines;
  unsigned char *obuf;
  gint o_index;
  gint o_size;
  gint spill_line_index;
  gint code_and_byte;
  gint num_lines_emit;

  gint orig_code;
  unsigned char *last_byte_ptr;

  buf = *pbuf; 
  bufsize = *pbufsize;
  *pbuf += bufsize; /* assumes all bytes will be consumed - will be fixed
		       up in case of sudden exit */

  /* Want to get all inner loop state into local variables. */
  packet_size = gif->packet_size;
  window = gif->window;
  bits_in_window = gif->bits_in_window;
  last_code = gif->last_code;
  code_size = gif->code_size;
  code_mask = (1 << code_size) - 1;
  line_index = gif->line_index;

  clear_code = 1 << gif->input_code_size;
  width = gif->Width;
  linebuf = gif->linebuf;

  while (bufsize > 0)
    {
      /* lwz_bytes is the number of remaining lwz bytes in the packet. */
      lwz_bytes = MIN (packet_size, bufsize);
      bufsize -= lwz_bytes;
      packet_size -= lwz_bytes;
      for (; lwz_bytes > 0; lwz_bytes--)
	{
	  /* printf ("%d ", *buf) would print the depacketized lwz stream. */
	  window = (window >> 8) | (*buf++ << 24);
	  bits_in_window += 8;
	  while (bits_in_window >= code_size)
	    {
	      code = (window >> (32 - bits_in_window)) & code_mask;
	      /* printf ("%d\n", code); would print the code stream. */
	      /*
	      printf ("cs = %d, last_code = %d, code = %d, biw = %d, w = %x\n",
		      code_size, last_code, code, bits_in_window, window);
		      */
#ifdef VERBOSE
	      printf ("last_code = %d, code = %d, ",
		      last_code, code);
#endif
	      bits_in_window -= code_size;
	      if (code < clear_code)
		{
		  /* a literal code. */
#ifdef VERBOSE
		  printf ("literal\n");
#endif
		  linebuf[line_index++] = code;
		  gif->length[last_code + 1] = 2;
		  gif->code_and_byte[last_code + 1] = (code << 8);
		  gif->code_and_byte[last_code] |= code;
		  last_code++;
		  if ((last_code & code_mask) == 0)
		    {
		      if (last_code == (1 << MAX_LWZ_BITS))
			last_code--;
		      else
			{
			  code_size++;
			  code_mask = (1 << code_size) - 1;
			}
		    }
		  if (line_index == width)
		    {
		      gzilla_gif_emit_line (gif, gif->linebuf);
		      line_index = 0;
		    }
		}
	      else if (code >= clear_code + 2)
		{
		  /* a sequence code. */
		  if (code > last_code) goto error;
		  sequence_length = gif->length[code];
		  gif->length[last_code + 1] = sequence_length + 1;
		  gif->code_and_byte[last_code + 1] = (code << 8);
		  /* We're going to traverse the sequence backwards. Thus,
		     we need to allocate spill lines if the sequence won't
		     fit entirely within the present scan line. */
		  if (line_index + sequence_length <= width)
		    {
		      num_spill_lines = 0;
		      obuf = linebuf;
		      o_index = line_index + sequence_length;
		      o_size = sequence_length - 1;
		    }
		  else
		    {
		      num_spill_lines = (line_index + sequence_length - 1) /
			width;
		      o_index = (line_index + sequence_length - 1) % width + 1;
		      if (num_spill_lines > gif->num_spill_lines_max)
			{
			  /* Allocate more spill lines. */
			  spill_line_index = gif->num_spill_lines_max;
			  do
			    gif->num_spill_lines_max <<= 1;
			  while (num_spill_lines > gif->num_spill_lines_max);
			  gif->spill_lines =
			    g_realloc (gif->spill_lines,
				       gif->num_spill_lines_max *
				       sizeof (unsigned char *));
			  for (; spill_line_index < gif->num_spill_lines_max;
			       spill_line_index++)
			    gif->spill_lines[spill_line_index] =
			      g_new (unsigned char, width);
			}
		      spill_line_index = num_spill_lines - 1;
		      obuf = gif->spill_lines[spill_line_index];
		      o_size = o_index;
		    }
		  line_index = o_index; /* for afterwards */

		  /* for fixing up later if last_code == code */
		  orig_code = code;
		  last_byte_ptr = obuf + o_index - 1;

		  /* spill lines are allocated, and we are clear to
		     write. This loop does not write the first byte of
		     the sequence, however (last byte traversed). */
		  while (sequence_length > 1)
		    {
		      sequence_length -= o_size;
		      /* Write o_size bytes to
			 obuf[o_index - o_size..o_index). */
		      for (; o_size > 0; o_size--)
			{
#ifdef VERBOSE
			  printf ("%d ", gif->code_and_byte[code] & 255);
#endif
			  code_and_byte = gif->code_and_byte[code];
			  obuf[--o_index] = code_and_byte & 255;
			  code = code_and_byte >> 8;
			}
		      /* Prepare for writing to next line. */
		      if (o_index == 0)
			{
			  if (spill_line_index > 0)
			    {
			      spill_line_index--;
			      obuf = gif->spill_lines[spill_line_index];
			      o_size = width;
			    }
			  else
			    {
			      obuf = linebuf;
			  o_size = sequence_length - 1;
			    }
			  o_index = width;
			}
		    }
		  /* Ok, now we write the first byte of the sequence. */
		  /* We are sure that the code is literal. */
#ifdef VERBOSE
		  printf ("%d", code);
#endif
		  obuf[--o_index] = code;
		  gif->code_and_byte[last_code] |= code;

		  /* Fix up the output if the original code was last_code. */
		  if (orig_code == last_code) {
		    *last_byte_ptr = code;
#ifdef VERBOSE
		    printf (" fixed (%d)!", code);
#endif
		  }

#ifdef VERBOSE
		  printf ("\n");
#endif

		  /* Output any full lines. */
		  num_lines_emit = num_spill_lines;
		  if (line_index == width)
		    {
		      num_lines_emit++;
		      line_index = 0;
		    }
		  if (num_lines_emit)
		    {
		      gzilla_gif_emit_line (gif, gif->linebuf);
		      for (spill_line_index = 0;
			   spill_line_index < num_lines_emit - 1;
			   spill_line_index++)
			gzilla_gif_emit_line
			  (gif, gif->spill_lines[spill_line_index]);
		    }

		  if (num_spill_lines > 0)
		    {
		      /* Swap the last spill line with the gif line, using
			 linebuf as the swap temporary. */
		      linebuf = gif->spill_lines[num_spill_lines - 1];
		      gif->spill_lines[num_spill_lines - 1] = gif->linebuf;
		      gif->linebuf = linebuf;
		    }

		  last_code++;
		  if ((last_code & code_mask) == 0)
		    {
		      if (last_code == (1 << MAX_LWZ_BITS))
			last_code--;
		      else
			{
			  code_size++;
			  code_mask = (1 << code_size) - 1;
			}
		    }
		}
	      else if (code == clear_code)
		{
		  /* clear code. */
#ifdef VERBOSE
		  printf ("clear\n");
#endif
		  code_size = gif->input_code_size + 1;
		  code_mask = (1 << code_size) - 1;
		  last_code = clear_code + 1;
		}
	      else
		{
		  /* end code. */
#ifdef VERBOSE
		  printf ("end\n");
#endif
		  *pbufsize -= bufsize;
		  gzilla_imgsink_close (gif->imgsink);
		  gif->imgsink = NULL;
		  return 1;
		}
	    }
	}
      if (bufsize > 0)
	{
	  packet_size = *buf++;
	  if (packet_size == 0) goto error;
	  bufsize--;
	}
    }

  gif->packet_size = packet_size;
  gif->window = window;
  gif->bits_in_window = bits_in_window;
  gif->last_code = last_code;
  gif->code_size = code_size;
  gif->line_index = line_index;
  *pbufsize = 0;
  return 0;

error:
  printf ("gzilla_gif_decode: error!\n");
  *pbufsize = bufsize;
  *pbuf -= bufsize;
  return -1;
}

/* Process some bytes from the input gif stream. It's a state machine. */
gint process_bytes (GzillaGif *gif, char *ibuf, gint bufsize) {
  gchar *tmp_buf = ibuf;
  gint tmp_bufsize = bufsize;
  unsigned char buf[16];
  char c;
  int byte;
  gint initial_state;
  unsigned char *cmap;

  initial_state = gif->state;

  switch (gif->state) {

  case 0:
    /* at beginning of file - read magic number */
    if (!read_ok (&tmp_buf, &tmp_bufsize, buf, 6))
      break;
    if (strncmp ((char *)buf, "GIF", 3) != 0) {
      gif->state = 999;
      break;
    }
    if (strncmp ((char *)buf + 3, "87a", 3) != 0 &&
	strncmp ((char *)buf + 3, "89a", 3) != 0) {
      gif->state = 999;
      break;
    }
    gif->state = 1;
    break;

  case 1:
    /* screen descriptor */
    if (!read_ok (&tmp_buf, &tmp_bufsize, buf, 7))
      break;
    gif->Width = LM_to_uint(buf[0],buf[1]);
    gif->Height = LM_to_uint(buf[2],buf[3]);
    gif->BitPixel = 2<<(buf[4]&0x07);
    gif->ColorResolution = (((buf[4]&0x70)>>3)+1);
    gif->Background      = buf[5];
    gif->AspectRatio     = buf[6];

    gif->linebuf = g_new (char, gif->Width);
    gzilla_imgsink_set_parms (gif->imgsink, gif->Width, gif->Height,
			      GZILLA_IMG_TYPE_INDEXED);

    if (buf[4] & LOCALCOLORMAP) {
      gif->state = 2;
    } else {
      gif->state = 3;
    }
    break;

  case 2:
    /* read colormap */
    if (!read_ok (&tmp_buf, &tmp_bufsize,
		  gif->ColorMap,
		  gif->BitPixel * 3))
      break;
    gif->state = 3;
    break;

  case 3:
    /* next character is one of:
       ';' = GIF terminator
       '!' = extension
       ',' = start character
       */
    if (!read_ok (&tmp_buf, &tmp_bufsize, &c, 1))
      break;
#ifdef VERBOSE
    g_print ("c = %c\n", c);
#endif
    if (c == ',') {
      gif->state = 4;
    } else if (c == '!') {
      gif->state = 7;
    } else {
      gif->state = 999;
    }
    break;

  case 4:
    /* right after the start character */
    if (!read_ok (&tmp_buf, &tmp_bufsize, buf, 9))
      break;
    /* we should probably just check that the local stuff is consistent
       with the stuff at the header. For now, we punt... */
    gif->interlace = (buf[8] & INTERLACE) != 0;
    gif->pass = 0;
    if (buf[8] & LOCALCOLORMAP)
      gif->state = 41;
    else
      gif->state = 5;
    break;

  case 41:
    /* read the local color map */
    if (!read_ok (&tmp_buf, &tmp_bufsize,
		  gif->ColorMap,
		  gif->BitPixel * 3))
      break;
    gif->state = 5;
    break;

  case 5:
    /* corresponds to ReadImage */
    if (!read_ok (&tmp_buf, &tmp_bufsize, &gif->input_code_size, 1))
      break;
    if (gif->input_code_size > 8)
      {
	gif->state = 999;
	break;
      }
    gif->x = 0;
    gif->y = 0;
#ifdef NEW_DECODER
    gzilla_gif_lwz_init (gif);
#else
    lwz_init (gif);
#endif
    if (gif->imgsink)
      gzilla_imgsink_set_cmap (gif->imgsink, gif->ColorMap, 256,
			       gif->transparent);
    gif->state = 6;
    break;

  case 6:
    /* get an image byte */
#ifdef NEW_DECODER
    if (gzilla_gif_decode (gif, &tmp_buf, &tmp_bufsize))
      gif->state = 999;
    break;
#else
    cmap = gif->ColorMap;
    do {
      byte = lwz_read_byte (gif, &tmp_buf, &tmp_bufsize);
      if (byte < 0)
	break;
      if (gif->imgsink) {
	gif->linebuf[gif->x] = byte;
      } else {
	gif->linebuf[gif->x * 3] = cmap[byte * 3];
	gif->linebuf[gif->x * 3 + 1] = cmap[byte * 3 + 1];
	gif->linebuf[gif->x * 3 + 2] = cmap[byte * 3 + 2];
      }
      gif->x++;
      if (gif->x == gif->Width) {
	gzilla_imgsink_write (gif->imgsink, gif->linebuf, 0, gif->y, 1);

	gif->x = 0;
	if (gif->interlace) {
	  switch (gif->pass) {
	  case 0:
	  case 1:
	    gif->y += 8;
	    break;
	  case 2:
	    gif->y += 4;
	    break;
	  case 3:
	    gif->y += 2;
	    break;
	  }
	  if (gif->y >= gif->Height) {
	    gif->pass++;
	    switch (gif->pass) {
	    case 1:
	      gif->y = 4;
	      break;
	    case 2:
	      gif->y = 2;
	      break;
	    case 3:
	      gif->y = 1;
	      break;
	    }
	  }

	} else {
	  gif->y++;
	}
      }
    } while (c >= 0 && gif->y < gif->Height);
    if (gif->y >= gif->Height) {
      gzilla_imgsink_close (gif->imgsink);
      gif->imgsink = NULL;
      gif->state = 999;
    }
    break;
#endif

  case 7:
    /* get the extension type */
    if (!read_ok (&tmp_buf, &tmp_bufsize, &gif->label, 1))
      break;
    gif->state = 8;
    break;

  case 8:
    /* corresponds to DoExtension */
    if (do_extension (gif, &tmp_buf, &tmp_bufsize)) {
      gif->state = 3;
    }
    break;

  default:
    /* error - just consume all input */
    tmp_bufsize = 0;
  }
#ifdef VERBOSE
  printf ("process_bytes: initial state %d, final state %d, %d bytes consumed\n",
	  initial_state, gif->state, bufsize - tmp_bufsize);
#endif
  return bufsize - tmp_bufsize;
}

static void
lwz_init (GzillaGif *gif) {
  int i;

  gif->set_code_size = gif->input_code_size;
  gif->code_size = gif->set_code_size+1;
  gif->clear_code = 1 << gif->set_code_size;
  gif->end_code = gif->clear_code + 1;
  gif->max_code_size = 2*gif->clear_code;
  gif->max_code = gif->clear_code+2;

  get_code_init (gif);

  gif->fresh = TRUE;

  for (i = 0; i < gif->clear_code; ++i) {
    gif->table[0][i] = 0;
    gif->table[1][i] = i;
  }
  for (; i < (1<<MAX_LWZ_BITS); ++i)
    gif->table[0][i] = gif->table[1][0] = 0;

  gif->sp = gif->stack;
}

static int
lwz_read_byte (GzillaGif *gif, char **pbuf, gint *pbufsize) {
  int    code, incode;
  int    i;

  if (gif->fresh) {
    do {
      gif->firstcode = gif->oldcode =
	get_code(gif, pbuf, pbufsize);
    } while (gif->firstcode == gif->clear_code);
    if (gif->firstcode >= 0)
      gif->fresh = FALSE;
    return gif->firstcode;
  }

  if (gif->sp > gif->stack)
    return *--gif->sp;

  while ((code = get_code(gif, pbuf, pbufsize)) >= 0) {
    if (gif->firstcode < 0) {
      gif->firstcode = gif->oldcode = code;
      return gif->firstcode;
    } else if (code == gif->clear_code) {
      for (i = 0; i < gif->clear_code; ++i) {
	gif->table[0][i] = 0;
	gif->table[1][i] = i;
      }
      for (; i < (1<<MAX_LWZ_BITS); ++i)
	gif->table[0][i] = gif->table[1][i] = 0;
      gif->code_size = gif->set_code_size+1;
      gif->max_code_size = 2*gif->clear_code;
      gif->max_code = gif->clear_code+2;
      gif->sp = gif->stack;
      gif->firstcode = gif->oldcode =
	get_code(gif, pbuf, pbufsize);
      return gif->firstcode;
    } else if (code == gif->end_code) {
      int             count;
      unsigned char   buf[260];

      if (gif->done)
	return -2;

      while ((count = get_data_block(pbuf, pbufsize, buf)) > 0)
	;

#if 0
      if (count != 0)
	pm_message("missing EOD in data stream (common occurence)");
      /* of course, the other possibility is that the EOD
	 spans a block boundary */
#endif
      return -2;
    }

    incode = code;

    if (code >= gif->max_code) {
      *gif->sp++ = gif->firstcode;
      code = gif->oldcode;
    }

    while (code >= gif->clear_code) {
      *gif->sp++ = gif->table[1][code];
#if 0
      if (code == gif->table[0][code])
	pm_error("circular table entry BIG ERROR");
#endif
      code = gif->table[0][code];
    }

    *gif->sp++ = gif->firstcode = gif->table[1][code];

    if ((code = gif->max_code) <(1<<MAX_LWZ_BITS)) {
      gif->table[0][code] = gif->oldcode;
      gif->table[1][code] = gif->firstcode;
      ++gif->max_code;
      if ((gif->max_code >= gif->max_code_size) &&
	  (gif->max_code_size < (1<<MAX_LWZ_BITS))) {
	gif->max_code_size *= 2;
	++gif->code_size;
      }
    }

    gif->oldcode = incode;

    if (gif->sp > gif->stack)
      return *--gif->sp;
  }
  return code;
}

static void get_code_init (GzillaGif *gif) {
  gif->curbit = 0;
  gif->lastbit = 0;
  gif->done = FALSE;
}

static int
get_code (GzillaGif *gif, char **pbuf, gint *pbufsize) {
       int           i, j, ret;
       int           count;
       int           code_size;
       unsigned char *buf;

       code_size = gif->code_size;
       buf = gif->buf;

       /* Why is this inequality nonstrict? */
       if ( (gif->curbit + code_size) >= gif->lastbit) {
               if (gif->done) {
#if 0
		 /* should probably handle this error condition */
                       if (curbit >= lastbit)
                               pm_error("ran off the end of my bits" );
#endif
                       return -1;
               }

	       count = get_data_block(pbuf, pbufsize, &buf[2]);
	       if (count < 0)
		 return -1;
               else if (count == 0)
		 gif->done = TRUE;

               gif->last_byte = 2 + count;
               gif->curbit = (gif->curbit - gif->lastbit) + 16;
               gif->lastbit = (2+count)*8 ;
       }

       ret = 0;
       for (i = gif->curbit, j = 0; j < code_size; ++i, ++j)
	 ret |= ((buf[i >> 3] & (1 << (i & 7))) != 0) << j;

       gif->curbit += code_size;

       if ( (gif->curbit + code_size) >= gif->lastbit) {
	 buf[0] = buf[gif->last_byte-2];
	 buf[1] = buf[gif->last_byte-1];
       }

#ifdef VERBOSE
       printf ("code = %d\n", ret);
#endif
       return ret;
}

/* Return number of bytes in block or -1 if no data present. */
static int
get_data_block (char **pbuf, gint *pbufsize, char *buf) {
  int size;

  if (*pbufsize > 0) {
    size = *(unsigned char *)*pbuf;
    if (*pbufsize >= 1 + size) {
      memcpy (buf, *pbuf + 1, size);
      *pbuf += 1 + size;
      *pbufsize -= 1 + size;
      return size;
    } else {
      return -1;
    }
  } else {
    return -1;
  }
}

