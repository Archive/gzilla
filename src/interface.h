#ifndef __INTERFACE_H__
#define __INTERFACE_H__

#include "gzillabrowser.h"
#include "gzillabytesink.h"

void gzilla_bw_init (void);
void gzilla_bw_abort_all (BrowserWindow *bw);
void gzilla_bw_add_bytesink (BrowserWindow *bw,
			     GzillaByteSink *bytesink);
void gzilla_bw_add_imgsink (BrowserWindow *bw,
			    GzillaImgSink *imgsink);
void gzilla_bw_connect_linkblock (BrowserWindow *bw,
				  GzillaLinkBlock *linkblock);
void gzilla_bw_connect_linkblock_loading (BrowserWindow *bw,
					  GzillaLinkBlock *linkblock);
void gzilla_quit_all (void);

void gzilla_status(const char *string, BrowserWindow *bw);

void create_openfile_dialog(BrowserWindow *bw);
void create_open_dialog(GtkWidget *widget, BrowserWindow *bw);
void create_quit_dialog(BrowserWindow *bw);

void gzilla_set_page_title(BrowserWindow *bw, char *title);
void entry_open_url(GtkWidget *widget, BrowserWindow *bw);
BrowserWindow *gzilla_new_browser_window(void);

void gzilla_bw_set_button_sens (BrowserWindow *bw);

GtkWidget *gzilla_xpm_label_box(GtkWidget *parent, gchar **data, gchar *lbltxt);

#endif /* __INTERFACE_H__ */
