#ifndef __GZILLADICACHE_H__
#define __GZILLADICACHE_H__

#include "gzillaimgsink.h"

void gzilla_dicache_init (void);
void gzilla_dicache_open (GzillaImgSink *imgsink, const char *url,
			  BrowserWindow *bw);

#endif /* __GZILLADICACHE_H__ */
