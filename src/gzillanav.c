/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

#include <gtk/gtk.h>
#include "gzillabytesink.h"
#include "gzillalinkblock.h"
#include "gzillacache.h"
#include "gzillabrowser.h"
#include "menus.h"
#include "gzilla.h"
#include "interface.h"

#include "gzw.h"
#include "gzillaweb.h"
#include "gzilladicache.h"

/* Support for a navigation stack */

/* The other main thing to do is make the gzilla_nav structure a per-window
 *  thing rather than a global. This would be needed if we ever get multiple
 *  browser windows going.
 *
 * Done -Ian
 */

/* todo: be smarter about aborting "loading" connections.

   This probably means making a distinction between documents that
   have successfully been loaded (and are on the nav stack) and
   documents that aren't. Right now, the predicate (ptr = size - 1
   and loading_ptr = size) is used as a stand-in, but it isn't quite
   right, because it's possible to load the document, go back, then
   go forward.

   Documents that have been succesfully loaded should also have
   slightly different navigation behavior: the url (and maybe title)
   should be displayed immediately. */


/* Not sure this should really be separate from the routine that creates
   bytesinks. */
void
gzilla_nav_init (BrowserWindow *bw)
{
    bw->size_gzilla_nav = 0;
    bw->size_gzilla_nav_max = 16;
    bw->gzilla_nav = g_new(GzillaNav, bw->size_gzilla_nav_max);

    bw->gzilla_nav_ptr = 0;
    bw->gzilla_nav_loading_ptr = 0;

    bw->last_waiting = FALSE;
}

static void
gzilla_nav_open_url (BrowserWindow *bw, char *url)
{
  GzillaByteSink *bytesink;
  GzillaLinkBlock *linkblock;

  gzilla_bw_set_button_sens (bw);

  gzilla_bw_abort_all (bw);

  bytesink = gzilla_web_new ();
  linkblock = gzilla_web_get_linkblock (GZILLA_WEB (bytesink));

  gzilla_bw_connect_linkblock_loading (bw, linkblock);

  if (!bw->last_waiting)
    {
      gtk_entry_set_text (GTK_ENTRY(bw->location),
			  bw->gzilla_nav[bw->gzilla_nav_ptr - 1].url);
    }

  open_url (bytesink, linkblock, url, bw);
}

/* Cancel the last waiting nav structure if present. The responsibility
   for actually aborting the bytesink remains with the caller. */
static void
gzilla_nav_cancel_last_waiting (BrowserWindow *bw)
{
  gint i;

  if (bw->last_waiting)
    {
      i = bw->gzilla_nav_loading_ptr - 1;
      g_free (bw->gzilla_nav[i].url);
      if (bw->gzilla_nav[i].title != NULL)
	g_free (bw->gzilla_nav[i].title);
      bw->size_gzilla_nav--;
      bw->gzilla_nav_loading_ptr--;
      if (bw->linkblock_loading != NULL)
	{
	  gtk_object_destroy (GTK_OBJECT (bw->linkblock_loading));
	  bw->linkblock_loading = NULL;
	}
      bw->last_waiting = FALSE;
    }

  if (bw->gzilla_nav_loading_ptr != bw->gzilla_nav_ptr)
    g_warning ("gzilla_nav_cancel_last_waiting: invariants broken");
}

void
gzilla_nav_push (char *url, BrowserWindow *bw)
{
  gint i;
    
  for (i = bw->gzilla_nav_ptr; i < bw->size_gzilla_nav; i++)
    {
      g_free (bw->gzilla_nav[i].url);
      if (bw->gzilla_nav[i].title != NULL)
	g_free (bw->gzilla_nav[i].title);
    }
  if (bw->gzilla_nav_ptr == bw->size_gzilla_nav_max)
    {
      bw->size_gzilla_nav_max <<= 1;
      bw->gzilla_nav = g_realloc(bw->gzilla_nav,
				 bw->size_gzilla_nav_max * sizeof(GzillaNav));
    }
  bw->gzilla_nav[bw->gzilla_nav_ptr].url = g_strdup(url);
  bw->gzilla_nav[bw->gzilla_nav_ptr].title = NULL;
  bw->size_gzilla_nav = bw->gzilla_nav_ptr + 1;
  bw->gzilla_nav_loading_ptr = bw->gzilla_nav_ptr + 1;
  bw->last_waiting = TRUE;

  gzilla_nav_open_url (bw, url);
}

void
gzilla_nav_back (BrowserWindow *bw)
{
  gzilla_nav_cancel_last_waiting (bw);
  if (bw->gzilla_nav_ptr > 1)
    {
      bw->gzilla_nav_ptr--;
      bw->gzilla_nav_loading_ptr = bw->gzilla_nav_ptr;
      gzilla_nav_open_url (bw, bw->gzilla_nav[bw->gzilla_nav_ptr - 1].url);
    }
}

void
gzilla_nav_forw (BrowserWindow *bw)
{
  gzilla_nav_cancel_last_waiting (bw);
  if (bw->gzilla_nav_ptr < bw->size_gzilla_nav)
    {
      bw->gzilla_nav_ptr++;
      bw->gzilla_nav_loading_ptr = bw->gzilla_nav_ptr;
      gzilla_nav_open_url (bw, bw->gzilla_nav[bw->gzilla_nav_ptr - 1].url);
    }
}

void
gzilla_nav_home (BrowserWindow *bw)
{
  /* todo: make this configurable */
  /* bah! we'll make them come to us!! moohahaha! :) */
  gzilla_nav_push ("http://www.gzilla.com/", bw);
}

void gzilla_nav_reload(BrowserWindow *bw)
{
  gzilla_nav_cancel_last_waiting (bw);
  if (bw->size_gzilla_nav > 0)
    {
      gzilla_cache_remove_url (bw->gzilla_nav[bw->gzilla_nav_ptr - 1].url);
      /* todo: should remove url from dicache too, so images can be reloaded. */
      gzilla_nav_open_url (bw, bw->gzilla_nav[bw->gzilla_nav_ptr - 1].url);
    }
}

void
gzilla_nav_stop (BrowserWindow *bw)
{
  gzilla_nav_cancel_last_waiting (bw);
  gzilla_bw_abort_all (bw);
}

void gzilla_nav_redirect (char *url, BrowserWindow *bw)
{
  /* warning: this assumes that the loading pointer is one ahead of the
     nav pointer. This assumption is accurate for HTTP redirects, but
     fails for client pulls. When we add client pulls, the logic will
     need to be changed. */
  gzilla_nav_push (url, bw);
}

/* End navigation stack. */

