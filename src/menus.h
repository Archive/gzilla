/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __MENUS_H__
#define __MENUS_H__

/* Always include this file after gzillabrowser.h */

#include <gtk/gtk.h>


#ifdef GTK_HAVE_FEATURES_1_1_0
	void menus_get_gzilla_menubar (GtkWidget **menubar,
		GtkAccelGroup **group);
#else
	void menus_get_gzilla_menubar (GtkWidget **menubar,
		GtkAcceleratorTable **table);
#endif

void menus_create (GtkMenuEntry *entries, int nmenu_entries);
void menus_set_sensitive (char *path, int sensitive);
void menus_add_path (char *path, char *accelerator);
void menus_quit (void);

GtkWidget *gzilla_menu_mainbar_new (BrowserWindow *bw);


#endif /* MENUS_H */
