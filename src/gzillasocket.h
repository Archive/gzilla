#ifndef __GZILLA_SOCKET_H__
#define __GZILLA_SOCKET_H__

/* This module provides an API for asynchronously opening a client
   socket, using the GTK framework.
   */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Initialize the socket data structures. */
void
gzilla_socket_init (void);

/* Create a new connection to (hostname, port). When the socket is
   created, call the callback with the file descriptor of the socket,
   or -1 on failure. The callback may be called inside the call to
   this function, or later, from a GTK input handler. */
gint
gzilla_socket_new (const char *hostname,
		   gint port,
		   void (* callback) (gint fd, void *callback_data),
		   void *callback_data);

/* Abort the connection. After an abort call, the callback associated
   with the tag will not be called. Also frees resources used in
   setting up the connection (aborting a pending dns request, closing
   the socket, and removing an input handler, depending on the state
   of the connection). */
void
gzilla_socket_abort (gint tag);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_SOCKET_H__ */
