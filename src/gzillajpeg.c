/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

/* The jpeg decoder for gzilla. It is responsible for decoding JPEG data
   in a bytesink and transferring it to an imgsink. It uses libjpeg to
   do the actual decoding. */

/* Todo: install an error handler that doesn't exit.*/

#undef VERBOSE

#include <stdio.h>
#include <string.h> /* for memcpy and memmove */

#include <gtk/gtk.h>

#include <jpeglib.h>

#include "gzillabytesink.h"
#include "gzillaimgsink.h"
#include "gzillajpeg.h"

static void gzilla_jpeg_class_init (GzillaJpegClass *klass);
static void gzilla_jpeg_init       (GzillaJpeg      *jpeg);
static void gzilla_jpeg_destroy    (GtkObject      *object);
static void gzilla_jpeg_write      (GzillaByteSink *bytesink,
				    char *bytes,
				    gint num);
static void gzilla_jpeg_close      (GzillaByteSink *bytesink);
static void gzilla_jpeg_status     (GzillaByteSink *bytesink,
				    GzillaStatusDir dir,
				    gboolean abort,
				    GzillaStatusMeaning meaning,
				    const char *text);

static GzillaByteSinkClass *parent_class = NULL;

guint
gzilla_jpeg_get_type ()
{
  static guint jpeg_type = 0;

  if (!jpeg_type)
    {
      GtkTypeInfo jpeg_info =
      {
	"GzillaJpeg",
	sizeof (GzillaJpeg),
	sizeof (GzillaJpegClass),
	(GtkClassInitFunc) gzilla_jpeg_class_init,
	(GtkObjectInitFunc) gzilla_jpeg_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      jpeg_type = gtk_type_unique (gzilla_bytesink_get_type (), &jpeg_info);
    }

  return jpeg_type;
}

void
gzilla_jpeg_class_init (GzillaJpegClass *class)
{
  GtkObjectClass      *object_class;
  GzillaByteSinkClass *bytesink_class;

  parent_class = gtk_type_class (gzilla_bytesink_get_type ());

  object_class = (GtkObjectClass*) class;
  bytesink_class = (GzillaByteSinkClass*) class;

  object_class->destroy = gzilla_jpeg_destroy;

  bytesink_class->write = gzilla_jpeg_write;
  bytesink_class->close = gzilla_jpeg_close;
  bytesink_class->status = gzilla_jpeg_status;
}

void gzilla_jpeg_init (GzillaJpeg *jpeg) {
  jpeg->state = GZILLA_JPEG_INIT;
  jpeg->size_inbuf = 0;
  jpeg->size_inbuf_max = 1024;
  jpeg->inbuf = g_new (char, jpeg->size_inbuf_max);
  jpeg->num_skip = 0;
}

static void gzilla_jpeg_destroy (GtkObject *object) {
  GzillaJpeg *jpeg;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GZILLA_IS_JPEG (object));

  jpeg = GZILLA_JPEG (object);

  g_free (jpeg->inbuf);

  if (jpeg->state != GZILLA_JPEG_DONE)
    jpeg_destroy_decompress (&(jpeg->cinfo));

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

/* An implementation of a suspending source manager */

typedef struct {
  struct jpeg_source_mgr pub;   /* public fields */

  GzillaJpeg *jpeg;             /* a pointer back to the jpeg object */
} my_source_mgr;

void
init_source (j_decompress_ptr cinfo) {
}

boolean
fill_input_buffer (j_decompress_ptr cinfo) {
  GzillaJpeg *jpeg;

  jpeg = ((my_source_mgr *)cinfo->src)->jpeg;

#ifdef VERBOSE
  g_print ("fill_input_buffer: bufsize = %d, num_skip = %d\n",
	   jpeg->bufsize, jpeg->num_skip);
#endif
  if (jpeg->num_skip >= jpeg->bufsize)
    {
      jpeg->num_skip -= jpeg->bufsize;
#ifdef VERBOSE
      g_print ("suspending with %d bytes left (%x)\n",
	       cinfo->src->bytes_in_buffer,
	       cinfo->src->next_input_byte);
#endif
      jpeg->bufsize = cinfo->src->bytes_in_buffer;
      jpeg->bufptr = (char *)cinfo->src->next_input_byte; /* discards const */
      cinfo->src->bytes_in_buffer = 0;
      return FALSE;
    }
  cinfo->src->next_input_byte = jpeg->bufptr + jpeg->num_skip;
  cinfo->src->bytes_in_buffer = jpeg->bufsize - jpeg->num_skip;
  jpeg->num_skip = 0;
  jpeg->bufptr += jpeg->bufsize;
  jpeg->bufsize = 0;
  return TRUE;
}

void
skip_input_data (j_decompress_ptr cinfo, long num_bytes) {
  GzillaJpeg *jpeg;

  jpeg = ((my_source_mgr *)cinfo->src)->jpeg;

#ifdef VERBOSE
  g_print ("skip_input_data: bufsize = %d, num_skip = %d, num_bytes = %d, %d bytes in buffer\n",
	   jpeg->bufsize, jpeg->num_skip, num_bytes,
	   cinfo->src->bytes_in_buffer);
#endif

  if (num_bytes <= cinfo->src->bytes_in_buffer)
    {
      cinfo->src->bytes_in_buffer -= num_bytes;
      cinfo->src->next_input_byte += num_bytes;
      num_bytes = 0;
    } else {
      num_bytes -= cinfo->src->bytes_in_buffer;
      cinfo->src->bytes_in_buffer = 0;
    }
  jpeg->num_skip += num_bytes;
}

void
term_source (j_decompress_ptr cinfo) {
}

static void
gzilla_jpeg_status_handler (void *data,
			    GzillaImgSink *imgsink,
			    GzillaStatusDir dir,
			    gboolean abort,
			    GzillaStatusMeaning meaning,
			    const char *text)
{
  GzillaJpeg *jpeg = data;
 
  if (dir != GZILLA_STATUS_DIR_DOWN)
    gzilla_bytesink_status (&jpeg->bytesink, GZILLA_STATUS_DIR_UP,
			    abort, meaning, text);
}

GzillaByteSink*
gzilla_jpeg_new (GzillaImgSink *imgsink)
{
  GzillaJpeg *jpeg;
  my_source_mgr *src;
  
  jpeg = gtk_type_new (gzilla_jpeg_get_type ());

  jpeg->imgsink = imgsink;

  /* decompression step 1 (see libjpeg.doc) */
  jpeg->cinfo.err = jpeg_std_error (&(jpeg->jerr));
  jpeg_create_decompress (&(jpeg->cinfo));

  /* decompression step 2 (see libjpeg.doc) */
  jpeg->cinfo.src = (struct jpeg_source_mgr *)
    g_malloc (sizeof (my_source_mgr));

  src = (my_source_mgr *) jpeg->cinfo.src;
  src->pub.init_source = init_source;
  src->pub.fill_input_buffer = fill_input_buffer;
  src->pub.skip_input_data = skip_input_data;
  src->pub.resync_to_restart = jpeg_resync_to_restart; /* use default method */
  src->pub.term_source = term_source;
  src->pub.bytes_in_buffer = 0; /* forces fill_input_buffer on first read */
  src->pub.next_input_byte = NULL; /* until buffer loaded */

  src->jpeg = jpeg;

  gzilla_imgsink_set_status_handler (imgsink, gzilla_jpeg_status_handler,
				     jpeg);

  /* decompression steps continue in write method */

  return GZILLA_BYTESINK (jpeg);
}

static void gzilla_jpeg_write (GzillaByteSink *bytesink,
			      char *bytes,
			      gint num) {
  GzillaJpeg *jpeg;
  GzillaImgType type;
  char *linebuf;
  JSAMPLE *array[1];
  gint num_read;

  jpeg = (GzillaJpeg *)bytesink;

  /* Concatenate with the partial input, if any. */
  if (jpeg->size_inbuf) {
    jpeg->bufsize = jpeg->size_inbuf + num;
    while (jpeg->size_inbuf_max < jpeg->bufsize)
      jpeg->size_inbuf_max <<= 1;
    jpeg->inbuf = g_realloc (jpeg->inbuf, jpeg->size_inbuf_max);
    jpeg->bufptr = jpeg->inbuf;
    memcpy (jpeg->bufptr + jpeg->size_inbuf, bytes, num);
  } else {
    jpeg->bufptr = bytes;
    jpeg->bufsize = num;
  }

  /* Process the bytes in the input buffer. */
  if (jpeg->state == GZILLA_JPEG_INIT)
    {

      /* decompression step 3 (see libjpeg.doc) */
      if (jpeg_read_header (&(jpeg->cinfo), TRUE) != JPEG_SUSPENDED)
	{
	  type = GZILLA_IMG_TYPE_GRAY;
	  if (jpeg->cinfo.num_components == 1)
	    type = GZILLA_IMG_TYPE_GRAY;
	  else if (jpeg->cinfo.num_components == 3)
	    type = GZILLA_IMG_TYPE_RGB;
	  else
	    g_print ("jpeg: can't handle %d component images\n",
		     jpeg->cinfo.num_components);
	  gzilla_imgsink_set_parms (jpeg->imgsink,
				    jpeg->cinfo.image_width,
				    jpeg->cinfo.image_height,
				    type);

	  /* decompression step 4 (see libjpeg.doc) */

	  jpeg->state = GZILLA_JPEG_STARTING;

	}
    }

  if (jpeg->state == GZILLA_JPEG_STARTING)
    {
      /* decompression step 5 (see libjpeg.doc) */
      if (jpeg_start_decompress (&(jpeg->cinfo)))
	{
	  jpeg->y = 0;
	  jpeg->state = GZILLA_JPEG_READING;
	}
    }

  if (jpeg->state == GZILLA_JPEG_READING)
    {
      linebuf = g_malloc (jpeg->cinfo.image_width *
			  jpeg->cinfo.num_components);
      array[0] = linebuf;
      while (jpeg->y < jpeg->cinfo.image_height)
	{
	  num_read = jpeg_read_scanlines (&(jpeg->cinfo),
				   array,
				   1);
	  if (num_read == 0)
	    break;
	  gzilla_imgsink_write (jpeg->imgsink,
				linebuf,
				0,
				jpeg->y,
				1);
	  jpeg->y++;
	}
      if (jpeg->y == jpeg->cinfo.image_height)
	{
	  jpeg_destroy_decompress (&(jpeg->cinfo));
	  jpeg->state = GZILLA_JPEG_DONE;
	}
      g_free (linebuf);
    }

  if (jpeg->state == GZILLA_JPEG_DONE)
    {
      gzilla_imgsink_close (jpeg->imgsink);
      jpeg->imgsink = NULL;
      jpeg->bufsize = 0;
    }

  /* Put unprocessed input in inbuf, if any. */
  jpeg->size_inbuf = jpeg->bufsize; /* any reason to maintain two variables? */
  if (jpeg->size_inbuf > 0) {
    memmove (jpeg->inbuf, jpeg->bufptr, jpeg->size_inbuf);
  }
}

static void
gzilla_jpeg_close (GzillaByteSink *bytesink)
{
  GzillaJpeg *jpeg;

  jpeg = GZILLA_JPEG (bytesink);
  if (jpeg->imgsink != NULL)
    gzilla_imgsink_status (jpeg->imgsink,
			   GZILLA_STATUS_DIR_DOWN, TRUE,
			   GZILLA_STATUS_MEANING_DEBUG,
			   "truncated gif");

  gtk_object_destroy (GTK_OBJECT (bytesink));
}

static void
gzilla_jpeg_status (GzillaByteSink *bytesink,
		    GzillaStatusDir dir,
		    gboolean abort,
		    GzillaStatusMeaning meaning,
		    const char *text)
{
  if (dir != GZILLA_STATUS_DIR_UP)
    gzilla_imgsink_status ((GZILLA_JPEG (bytesink))->imgsink,
			   GZILLA_STATUS_DIR_DOWN, abort, meaning, text);

  if (abort)
    gtk_object_destroy (GTK_OBJECT (bytesink));
}
