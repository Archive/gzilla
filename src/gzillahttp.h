#ifndef __GZILLA_HTTP_H__
#define __GZILLA_HTTP_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* Initialize the http data structures, making gzilla_http_get ready
   to use. */
void gzilla_http_init (void);

/* Create a new http connection for URL url, and asynchronously feed
   the bytes that come back to bytesink. */
void gzilla_http_get (const char *url,
		      GzillaByteSink *bytesink,
		      void (*status_callback) (const char *status, void *data),
		      void *data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_HTTP_H__ */
