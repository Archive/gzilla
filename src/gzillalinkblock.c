/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

#include <gtk/gtk.h>
#include "gzillabytesink.h"
#include "gzillaimgsink.h"
#include "gzillalinkblock.h"

enum {
  REQUEST_URL,
  REQUEST_URL_IMG,
  LINK,
  STATUS,
  REDIRECT,
  TITLE,
  HAVE_GZW,
  SET_BASE_URL,
  LAST_SIGNAL
};

typedef void (*GzillaLinkBlockSignal1) (GtkObject *object,
				       gpointer  arg1,
				       gpointer  data);

typedef void (*GzillaLinkBlockSignal2) (GtkObject *object,
				       gpointer  arg1,
				       gpointer  arg2,
				       gpointer  data);

typedef void (*GzillaLinkBlockSignal3) (GtkObject *object,
				       gint      arg1,
				       gboolean  arg2,
				       gint      arg3,
				       gpointer  arg4,
				       gpointer  data);

static void gzilla_linkblock_marshal_signal_1 (GtkObject     *object,
					      GtkSignalFunc  func,
					      gpointer       func_data,
					      GtkArg        *args);
static void gzilla_linkblock_marshal_signal_2 (GtkObject     *object,
					      GtkSignalFunc  func,
					      gpointer       func_data,
					      GtkArg        *args);
static void gzilla_linkblock_marshal_signal_3 (GtkObject     *object,
					      GtkSignalFunc  func,
					      gpointer       func_data,
					      GtkArg        *args);

static void gzilla_linkblock_class_init  (GzillaLinkBlockClass  *klass);
static void gzilla_linkblock_init        (GzillaLinkBlock       *widget);

static GtkObjectClass *parent_class = NULL;
static gint linkblock_signals[LAST_SIGNAL] = { 0 };

guint
gzilla_linkblock_get_type () {
  static guint linkblock_type = 0;

  if (!linkblock_type)
    {
      GtkTypeInfo linkblock_info =
      {
	"GzillaLinkBlock",
	sizeof (GzillaLinkBlock),
	sizeof (GzillaLinkBlockClass),
	(GtkClassInitFunc) gzilla_linkblock_class_init,
	(GtkObjectInitFunc) gzilla_linkblock_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      linkblock_type = gtk_type_unique (gtk_object_get_type (), &linkblock_info);
    }

  return linkblock_type;
}

static void
gzilla_linkblock_class_init (GzillaLinkBlockClass *klass)
{
  GtkObjectClass *object_class;
  gint i;

  object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (gtk_object_get_type ());

  i = 0;
  linkblock_signals[i++] =
    gtk_signal_new ("request_url",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaLinkBlockClass, request_url),
		    gzilla_linkblock_marshal_signal_2, GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  linkblock_signals[i++] =
    gtk_signal_new ("request_url_img",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaLinkBlockClass, request_url_img),
		    gzilla_linkblock_marshal_signal_2, GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  linkblock_signals[i++] =
    gtk_signal_new ("link",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaLinkBlockClass, link),
		    gzilla_linkblock_marshal_signal_1, GTK_TYPE_NONE, 1,
		    GTK_TYPE_POINTER);
  linkblock_signals[i++] =
    gtk_signal_new ("status",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaLinkBlockClass, status),
		    gzilla_linkblock_marshal_signal_3, GTK_TYPE_NONE, 4,
		    GTK_TYPE_ENUM,
		    GTK_TYPE_BOOL,
		    GTK_TYPE_ENUM,
		    GTK_TYPE_STRING);
  linkblock_signals[i++] =
    gtk_signal_new ("redirect",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaLinkBlockClass, redirect),
		    gzilla_linkblock_marshal_signal_1, GTK_TYPE_NONE, 1,
		    GTK_TYPE_POINTER);
  linkblock_signals[i++] =
    gtk_signal_new ("title",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaLinkBlockClass, title),
		    gzilla_linkblock_marshal_signal_1, GTK_TYPE_NONE, 1,
		    GTK_TYPE_POINTER);
  linkblock_signals[i++] =
    gtk_signal_new ("have_gzw",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaLinkBlockClass, have_gzw),
		    gzilla_linkblock_marshal_signal_1, GTK_TYPE_NONE, 1,
		    GTK_TYPE_POINTER);
  linkblock_signals[i++] =
    gtk_signal_new ("set_base_url",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaLinkBlockClass, set_base_url),
		    gzilla_linkblock_marshal_signal_1, GTK_TYPE_NONE, 1,
		    GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, linkblock_signals, LAST_SIGNAL);

  klass->request_url = NULL;
  klass->request_url_img = NULL;
  klass->link = NULL;
  klass->status = NULL;
  klass->redirect = NULL;
  klass->title = NULL;
  klass->have_gzw = NULL;
  klass->set_base_url = NULL;
}

static void
gzilla_linkblock_init (GzillaLinkBlock *linkblock)
{
}

GzillaLinkBlock *
gzilla_linkblock_new (void)
{
  return gtk_type_new (gzilla_linkblock_get_type ());
}

void
gzilla_linkblock_request_url (GzillaLinkBlock *linkblock,
			      GzillaByteSink *bytesink,
			      const char *url)
{
  gtk_signal_emit (GTK_OBJECT (linkblock), linkblock_signals[REQUEST_URL],
		   bytesink, url);
}

void
gzilla_linkblock_request_url_img (GzillaLinkBlock *linkblock,
				 GzillaImgSink *imgsink,
				 const char *url)
{
  gtk_signal_emit (GTK_OBJECT (linkblock), linkblock_signals[REQUEST_URL_IMG],
		   imgsink, url);
}

void
gzilla_linkblock_link (GzillaLinkBlock *linkblock,
			   const char *url)
{
  gtk_signal_emit (GTK_OBJECT (linkblock), linkblock_signals[LINK],
		   url);
}

void
gzilla_linkblock_status (GzillaLinkBlock *linkblock,
			GzillaStatusDir dir,
			gboolean abort,
			GzillaStatusMeaning meaning,
			const char *text)
{
  gtk_signal_emit (GTK_OBJECT (linkblock), linkblock_signals[STATUS],
		   dir, abort, meaning, text);
}

void
gzilla_linkblock_redirect (GzillaLinkBlock *linkblock,
			  const char *url)
{
  gtk_signal_emit (GTK_OBJECT (linkblock), linkblock_signals[REDIRECT],
		   url);
}

void
gzilla_linkblock_title (GzillaLinkBlock *linkblock,
		       const char *title)
{
  gtk_signal_emit (GTK_OBJECT (linkblock), linkblock_signals[TITLE],
		   title);
}

void
gzilla_linkblock_have_gzw (GzillaLinkBlock *linkblock,
			   Gzw *gzw)
{
  gtk_signal_emit (GTK_OBJECT (linkblock), linkblock_signals[HAVE_GZW],
		   gzw);
}

void
gzilla_linkblock_set_base_url (GzillaLinkBlock *linkblock,
			      const char *url)
{
  gtk_signal_emit (GTK_OBJECT (linkblock), linkblock_signals[SET_BASE_URL],
		   url);
}

static void
gzilla_linkblock_marshal_signal_1 (GtkObject      *object,
				   GtkSignalFunc   func,
				   gpointer        func_data,
				   GtkArg         *args)
{
  GzillaLinkBlockSignal1 rfunc;

  rfunc = (GzillaLinkBlockSignal1) func;

  (* rfunc) (object, GTK_VALUE_POINTER (args[0]), func_data);
}

static void
gzilla_linkblock_marshal_signal_2 (GtkObject      *object,
				  GtkSignalFunc   func,
				  gpointer        func_data,
				  GtkArg         *args)
{
  GzillaLinkBlockSignal2 rfunc;

  rfunc = (GzillaLinkBlockSignal2) func;

  (* rfunc) (object, GTK_VALUE_POINTER (args[0]),
	     GTK_VALUE_POINTER (args[1]), func_data);
}

static void
gzilla_linkblock_marshal_signal_3 (GtkObject      *object,
				  GtkSignalFunc   func,
				  gpointer        func_data,
				  GtkArg         *args)
{
  GzillaLinkBlockSignal3 rfunc;

  rfunc = (GzillaLinkBlockSignal3) func;

  (* rfunc) (object,
	     GTK_VALUE_INT (args[0]),
	     GTK_VALUE_BOOL (args[1]),
	     GTK_VALUE_INT (args[2]),
	     GTK_VALUE_STRING (args[3]), func_data);
}
