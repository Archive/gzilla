#ifndef __GTK_PLAIN_H__
#define __GTK_PLAIN_H__

#include <gdk/gdk.h>
#include <gtk/gtkcontainer.h>
#include "gzillabytesink.h"

#include "gzw.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GZILLA_PLAIN(obj)           GTK_CHECK_CAST (obj, gzilla_plain_get_type (), GzillaPlain)
#define GZILLA_PLAIN_CLASS(klass)   GTK_CHECK_CLASS_CAST (klass, gzilla_plain_get_type (), GzillaPlainClass)
#define GZILLA_IS_PLAIN(obj)        GTK_CHECK_TYPE (obj, gzilla_plain_get_type ())

typedef struct _GzillaPlain        GzillaPlain;
typedef struct _GzillaPlainClass   GzillaPlainClass;

struct _GzillaPlain {
  GzillaByteSink bytesink;

  Gzw *gzw;

  char *inbuf;        /* a partial token of input */
  gint size_inbuf;
  gint size_inbuf_max;

  gint attr;
};

struct _GzillaPlainClass {
  GzillaByteSinkClass  parent_class;
};

guint           gzilla_plain_get_type (void);
GzillaByteSink *gzilla_plain_new (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_PLAIN_H__ */
