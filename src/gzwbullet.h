#ifndef __GZW_BULLET_H__
#define __GZW_BULLET_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GzwBullet GzwBullet;

typedef enum {
  GZW_BULLET_DISC,
  GZW_BULLET_CIRCLE,
  GZW_BULLET_SQUARE
} GzwBulletType;

struct _GzwBullet {
  Gzw gzw;

  GzwBulletType type;
};

Gzw *gzw_bullet_new (GzwBulletType type);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZW_BULLET_H__ */
