#ifndef __GZILLA_NAV_H__
#define __GZILLA_NAV_H__


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* always include gzillanav.c before this file. */

void gzilla_nav_push(char *url, BrowserWindow *bw);
void gzilla_nav_back(BrowserWindow *bw);
void gzilla_nav_forw(BrowserWindow *bw);
void gzilla_nav_home(BrowserWindow *bw);
void gzilla_nav_reload(BrowserWindow *bw);
void gzilla_nav_init(BrowserWindow *bw);
void gzilla_nav_stop (BrowserWindow *bw);
void gzilla_nav_redirect(char *url, BrowserWindow *bw);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_NAV_H__ */

					
