/* This module contains the gzw_page widget, which is the "back end" to
   Web text widgets including html. */

#ifndef __GZW_PAGE_H__
#define __GZW_PAGE_H__

#undef USE_TYPE1

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GzwPage        GzwPage;
typedef struct _GzwPageClass   GzwPageClass;

/* Internal data structures (maybe shouldn't be in public .h file? */
typedef struct _GzwPageFont    GzwPageFont;
typedef struct _GzwPageLink    GzwPageLink;
typedef struct _GzwPageAttr    GzwPageAttr;
typedef struct _GzwPageLine    GzwPageLine;
typedef struct _GzwPageWord    GzwPageWord;

struct _GzwPageFont {
  char *name;
  gint size;
  gboolean bold;
  gboolean italic;

#ifdef USE_TYPE1
  gint t1fontid;
#else
  GdkFont *font;
#endif
  gint space_width; /* only valid if font != NULL */
};

struct _GzwPageLink {
  char *url;
};

struct _GzwPageAttr {
  gint font;
  gint link;
  gint32 color;

  gint left_indent_first;
  gint left_indent_rest;
  gint right_indent;

  gint align;
};

#define GZW_PAGE_ALIGN_LEFT 0
#define GZW_PAGE_ALIGN_CENTER 1
#define GZW_PAGE_ALIGN_RIGHT 2

struct _GzwPageLine {
  gint num_words;
  gint num_words_max; /* number allocated */
  gint y_top;
  gint x_size, y_ascent, y_descent, y_space;
  gboolean hard;      /* false = soft break, true = hard break */
  gboolean first;     /* true = first line in paragraph */
  GzwPageWord *words;
};

struct _GzwPageWord {
  /* At some point, this is going to become a variant record (i.e. it
     could point to a widget instead of just being text). */

  gint x_size, y_ascent, y_descent;
  gint x_space; /* space after the word, only if it's not a break */
  gint content_type;
  union {
    char *text;
    Gzw *widget;
  } content;

  gint attr;
};

#define GZW_PAGE_CONTENT_TEXT 0
#define GZW_PAGE_CONTENT_WIDGET 1

struct _GzwPage {
  Gzw gzw;

  GdkGC *gc;

  GzwPageLine *lines;
  gint num_lines;
  gint num_lines_max; /* number allocated */

  gint width;         /* the width (not including pad) at which line wrap
			 was calculated. If this changes, then the whole
			 thing should get re-wrapped. */

  gint last_line_max_width; /* the maximum width of the last line (assuming
			       no word wrap) */

  GzwPageFont *fonts;
  gint num_fonts;
  gint num_fonts_max;

  GzwPageLink *links;
  gint num_links;
  gint num_links_max;

  GdkColor *colors;
  gint num_colors;
  gint num_colors_max;

  GzwPageAttr *attrs;
  gint num_attrs;
  gint num_attrs_max;

  /* Stuff for doing redraws */

  GdkRectangle clear;  /* Rectangle that must be cleared. */
  GdkRectangle redraw; /* Rectangle that must be redrawn. */

  gint redraw_start_line;

  /* The link under a button press */
  gint link_pressed;

  /* The link under the button */
  gint hover_link;

  void (*link) (void *data, char *url);
  void *link_data;

  void (*status) (void *data, char *url);
  void *status_data;
};

Gzw *gzw_page_new       (void);

void
gzw_page_set_callbacks (GzwPage *page,
			void (*link) (void *data, char *url),
			void *link_data,
			void (*status) (void *data, char *url),
			void *status_data);

void gzw_page_set_width (GzwPage *page, gint width);

void gzw_page_update_begin (GzwPage *page);
void gzw_page_update_end (GzwPage *page);

void gzw_page_init_attr (GzwPage *page, GzwPageAttr *attr);
gint gzw_page_find_font (GzwPage *page, const GzwPageFont *font);
gint gzw_page_new_link (GzwPage *page, const char *url);
gint gzw_page_find_color (GzwPage *page, gint32 color);
gint gzw_page_find_attr (GzwPage *page, const GzwPageAttr *attr);
void gzw_page_add_text (GzwPage *page, char *text, gint attr);
void gzw_page_add_widget (GzwPage *page, Gzw *widget, gint attr);
void gzw_page_add_space (GzwPage *page, gint attr);
void gzw_page_linebreak (GzwPage *page);
void gzw_page_parbreak (GzwPage *page, gint space);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GZW_PAGE_H__ */
