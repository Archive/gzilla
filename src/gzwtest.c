/* A Gzw test widget. */

#include <stdio.h>	/* for sprintf */
#include <gtk/gtk.h>
#include "gzw.h"
#include "gzwtest.h"

static void
gzw_test_timeout (Gzw *gzw)
{
  GzwRect rect;
  ((GzwTest *)gzw)->counter++;

  rect.x0 = 20;
  rect.x1 = 40;
  rect.y0 = 65;
  rect.y1 = 85;
  gzw_request_parent_resize (gzw);
  gzw_request_paint (gzw, &rect);
}

static void
gzw_test_size_nego_x (Gzw *gzw, gint width)
{
  gzw->req_height = 100 + ((GzwTest *)gzw)->counter;
}

static void
gzw_test_size_nego_y (Gzw *gzw, GzwRect *allocation)
{
#ifdef VERBOSE
  g_print ("gzw_test_size_nego_y (%d, %d) - (%d, %d)\n",
	   allocation->x0, allocation->y0, allocation->x1, allocation->y1);
#endif
  gzw->allocation = *allocation;
}

static void
gzw_test_paint (Gzw *gzw, GzwRect *rect, GzwPaint *paint)
{
  GtkWidget *widget;
  GzwContainer *container;
  char str[80];
  gint x, y;

#ifdef VERBOSE
  g_print ("gzw_test_paint (%d, %d) - (%d, %d)",
	   rect->x0, rect->y0, rect->x1, rect->y1);
#endif
  container = gzw_find_container (gzw);
  if (container != NULL)
    {
#ifdef VERBOSE
      g_print (", visible = (%d, %d) - (%d, %d)",
	       container->visible.x0, container->visible.y0,
	       container->visible.x1, container->visible.y1);
#endif
      widget = container->widget;
      gzw_paint_to_screen (widget, container, rect, paint);
      x = gzw->allocation.x0 + container->x_offset;
      y = gzw->allocation.x0 + container->y_offset;
      gdk_draw_line (widget->window,
		     widget->style->fg_gc[GTK_STATE_NORMAL],
		     x,
		     y,
		     x + 99,
		     y + 99);
      sprintf (str, "%d", ((GzwTest *)gzw)->counter);
      gdk_draw_string (widget->window,
		       widget->style->font,
		       widget->style->fg_gc[GTK_STATE_NORMAL],
		       x + 20, x + 80,
		       str);
      gzw_paint_finish_screen (paint);
    }
#ifdef VERBOSE
  g_print ("\n");
#endif
}

static void
gzw_test_destroy (Gzw *gzw)
{
  g_free (gzw);
}

static const GzwClass gzw_test_class =
{
  gzw_test_size_nego_x,
  gzw_test_size_nego_y,
  gzw_test_paint,
  gzw_null_handle_event,
  gzw_null_gtk_foreach,
  gzw_test_destroy,
  gzw_null_request_resize
};

Gzw *
gzw_test_new (void)
{
  GzwTest *gzw_test;

  gzw_test = g_new (GzwTest, 1);
  gzw_test->gzw.klass = &gzw_test_class;
  gzw_test->gzw.req_width_min = 100;
  gzw_test->gzw.req_width_max = 100;
  /*
  gzw_test->req_height = 100;
  gzw_test->req_ascent = 0;
  */
  gzw_test->gzw.flags = 0;
  gzw_test->gzw.parent = NULL;
  gzw_test->gzw.container = NULL;

  gzw_test->counter = 0;

  gtk_timeout_add (1000, (GtkFunction) gzw_test_timeout, gzw_test);

  return (Gzw *) gzw_test;
}
