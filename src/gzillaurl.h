#ifndef __GTK_URL_H__
#define __GTK_URL_H__

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


gboolean gzilla_url_is_absolute (const char *url);
gboolean gzilla_url_match_method (const char *url, const char *method);
gboolean gzilla_url_relative (const char *base_url,
			  const char *relative_url,
			  char *new_url,
			  gint size_new_url);
char *gzilla_url_parse (char *url,
			char *hostname,
			gint hostname_size,
			int *port);
void gzilla_url_parse_hash (char **p_head, char **p_tail, const char *url);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_WEB_H__ */
