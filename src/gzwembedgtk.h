#ifndef __GZW_EMBED_GTK_H__
#define __GZW_EMBED_GTK_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GzwEmbedGtk GzwEmbedGtk;

struct _GzwEmbedGtk {
  Gzw gzw;

  GtkWidget *widget;
};

Gzw *gzw_embed_gtk_new (GtkWidget *widget);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZW_EMBED_GTK_H__ */
