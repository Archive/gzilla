#ifndef __GZW_REGION_H__
#define __GZW_REGION_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "gzwrect.h"

typedef struct _GzwRegion GzwRegion;

/* Regions are dynamically allocated, and reference counted. Most
   functions for manipulating regions will decrement the reference
   count of the arguments, and create a new region with a reference
   count of 1. */

GzwRegion *gzw_region_pin (GzwRegion *region);
void gzw_region_drop (GzwRegion *region);
GzwRegion *gzw_region_from_rect (const GzwRect *src);
gboolean gzw_region_empty (const GzwRegion *region);

/* There should probably also be a gzw_region_sanitycheck for testing
   purposes. */

/* Standard set operations on regions. */
GzwRegion *gzw_region_union (GzwRegion *src1, GzwRegion *src2);
GzwRegion *gzw_region_intersect (GzwRegion *src1, GzwRegion *src2);
GzwRegion *gzw_region_minus (GzwRegion *src1, GzwRegion *src2);

void gzw_region_tile (GzwRect *dst, GzwRegion *src, gint max_xs, gint max_ys,
		      gint waste);

void gzw_region_to_bitmap (GzwRegion *src, guchar *buf, gint xs, gint ys);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZW_REGION_H__ */
