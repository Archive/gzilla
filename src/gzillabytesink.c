/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

/* todo: move the widget into the subclasses (not all bytesinks have
   widgets)? */

#include <gtk/gtk.h>
#include "gzillabytesink.h"

enum {
  WRITE,
  CLOSE,
  STATUS,
  LAST_SIGNAL
};

typedef void (*GzillaByteSinkSignal1) (GtkObject *object,
				       gpointer  arg1,
				       gint      arg2,
				       gpointer  data);

typedef void (*GzillaByteSinkSignal2) (GtkObject *object,
				       gint      arg1,
				       gboolean  arg2,
				       gint      arg3,
				       gpointer  arg4,
				       gpointer  data);

static void gzilla_bytesink_marshal_signal_1 (GtkObject     *object,
					      GtkSignalFunc  func,
					      gpointer       func_data,
					      GtkArg        *args);
static void gzilla_bytesink_marshal_signal_2 (GtkObject     *object,
					      GtkSignalFunc  func,
					      gpointer       func_data,
					      GtkArg        *args);

static void gzilla_bytesink_class_init  (GzillaByteSinkClass  *klass);
static void gzilla_bytesink_init        (GzillaByteSink       *widget);

static GtkObjectClass *parent_class = NULL;
static gint bytesink_signals[LAST_SIGNAL] = { 0 };

guint gzilla_bytesink_get_type () {
  static guint bytesink_type = 0;

  if (!bytesink_type)
    {
      GtkTypeInfo bytesink_info =
      {
	"GzillaByteSink",
	sizeof (GzillaByteSink),
	sizeof (GzillaByteSinkClass),
	(GtkClassInitFunc) gzilla_bytesink_class_init,
	(GtkObjectInitFunc) gzilla_bytesink_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      bytesink_type = gtk_type_unique (gtk_object_get_type (), &bytesink_info);
    }

  return bytesink_type;
}

static void gzilla_bytesink_class_init (GzillaByteSinkClass *klass)
{
  GtkObjectClass *object_class;
  gint i;

  object_class = (GtkObjectClass *)klass;

  parent_class = gtk_type_class (gtk_object_get_type ());

  i = 0;
  bytesink_signals[i++] =
    gtk_signal_new ("write",
		    GTK_RUN_FIRST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaByteSinkClass, write),
		    gzilla_bytesink_marshal_signal_1, GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_INT);
  /* close and status are RUN_LAST because the method may destroy the
     object, so by running it last we ensure that the signal handlers
     attached to the object all get run. */
  bytesink_signals[i++] =
    gtk_signal_new ("close",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaByteSinkClass, close),
		    gtk_signal_default_marshaller, GTK_TYPE_NONE, 0);
  bytesink_signals[i++] =
    gtk_signal_new ("status",
		    GTK_RUN_LAST,
		    object_class->type,
		    GTK_SIGNAL_OFFSET (GzillaByteSinkClass, status),
		    gzilla_bytesink_marshal_signal_2, GTK_TYPE_NONE, 4,
		    GTK_TYPE_ENUM,
		    GTK_TYPE_BOOL,
		    GTK_TYPE_ENUM,
		    GTK_TYPE_STRING);

  gtk_object_class_add_signals (object_class, bytesink_signals, LAST_SIGNAL);

#if 0
  /* we should probably destroy the associated widget */
  object->class_destroy = gzilla_bytesink_real_bytesink_destroy;
#endif

  klass->write = NULL;
  klass->close = NULL;
  klass->status = NULL;
}

static void gzilla_bytesink_init (GzillaByteSink *bytesink) {
}

void gzilla_bytesink_write (GzillaByteSink *bytesink,
			    char *buf,
			    gint bufsize) {
  gtk_signal_emit (GTK_OBJECT (bytesink), bytesink_signals[WRITE],
		   buf, bufsize);
}

void gzilla_bytesink_close (GzillaByteSink *bytesink) {
  gtk_signal_emit (GTK_OBJECT (bytesink), bytesink_signals[CLOSE]);
}

void
gzilla_bytesink_status (GzillaByteSink *bytesink,
			GzillaStatusDir dir,
			gboolean abort,
			GzillaStatusMeaning meaning,
			const char *text)
{
  gtk_signal_emit (GTK_OBJECT (bytesink), bytesink_signals[STATUS],
		   dir, abort, meaning, text);
}

static void
gzilla_bytesink_marshal_signal_1 (GtkObject      *object,
				  GtkSignalFunc   func,
				  gpointer        func_data,
				  GtkArg         *args)
{
  GzillaByteSinkSignal1 rfunc;

  rfunc = (GzillaByteSinkSignal1) func;

  (* rfunc) (object, GTK_VALUE_POINTER (args[0]),
	     GTK_VALUE_INT (args[1]), func_data);
}

static void
gzilla_bytesink_marshal_signal_2 (GtkObject      *object,
				  GtkSignalFunc   func,
				  gpointer        func_data,
				  GtkArg         *args)
{
  GzillaByteSinkSignal2 rfunc;

  rfunc = (GzillaByteSinkSignal2) func;

  (* rfunc) (object,
	     GTK_VALUE_INT (args[0]),
	     GTK_VALUE_BOOL (args[1]),
	     GTK_VALUE_INT (args[2]),
	     GTK_VALUE_POINTER (args[3]), func_data);
}
