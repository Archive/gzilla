/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */


/* todo: make error messages more descriptive. */

#include <stdio.h>	      /* for sprintf */
#include <stdlib.h>	      /* for getenv */
#include <unistd.h>

#include <errno.h>            /* for errno */
#include <string.h>           /* for strstr */

#include <gtk/gtk.h>

#include "gzillabytesink.h"
#include "gzillaurl.h"
#include "gzillasocket.h"
#include "config.h"

typedef struct _GzillaHttp GzillaHttp;

typedef enum {
  GZILLA_HTTP_INIT,
  GZILLA_HTTP_SOCKET_WAIT,
  GZILLA_HTTP_SENDING,
  GZILLA_HTTP_RECEIVING,
  GZILLA_HTTP_ERROR
} GzillaHttpState;

struct _GzillaHttp {
  gint tag;
  gint socket_tag;
  GzillaHttpState state;
  gint fd;
  gint wr_tag, rd_tag;
  char *query;
  gint query_size;
  gint query_index;
  gint status_id;
  GzillaByteSink *bytesink;
};

static gint num_http;
static gint num_http_max;
static GzillaHttp *http;

static gint http_tag = 0;

void
gzilla_http_init (void)
{
  num_http = 0;
  num_http_max = 16;
  http = g_new (GzillaHttp, num_http_max);
}


/* The input function for reading the reply back from the socket. This
   function gets called whenever the socket has bytes available for
   reading. Try reading a buffer and writing it out to the bytesink.
   If we reach eof, then close the fd and bytesink. */

static void
gzilla_http_input_func (gpointer data,
			gint source,
			GdkInputCondition condition)
{
  gint tag = (gint)data;
  gint i;
  char buf[8192];
  gint num_bytes;

  for (i = 0; i < num_http; i++)
    {
      if (http[i].tag == tag)
	break;
    }
  if (i < num_http)
    {
      num_bytes = read (http[i].fd, buf, sizeof (buf));
#ifdef VERBOSE
      g_print ("http %d read %d bytes\n", i, num_bytes);
#endif
      if (num_bytes < 0)
	{
	  if (errno == EINTR || errno == EAGAIN)
	    return;
	  num_bytes = 0;
	}
      if (num_bytes > 0)
	gzilla_bytesink_write (http[i].bytesink, buf, num_bytes);
      else
	{
#ifdef VERBOSE
	  g_print ("closing http %d fd %d\n", i, http[i].fd);
#endif
	  gtk_signal_disconnect (GTK_OBJECT (http[i].bytesink),
				 http[i].status_id);
	  gdk_input_remove (http[i].rd_tag);
	  close (http[i].fd);
	  gzilla_bytesink_close (http[i].bytesink);
	  http[i] = http[--num_http];
	}
    }
  else
    g_warning ("gzilla_http_input_function: trying to write into http that has already been removed\n");
}

/* The input function for writing the query out to the socket. This
   function gets called whenever the socket is ready for writing. Try
   writing the query string out to the socket. When all of the bytes
   have been written, remove this input function and install the input
   function for reading the response back. */

static void
gzilla_http_query_func (gpointer data,
			gint source,
			GdkInputCondition condition)
{
  gint tag = (gint)data;
  gint i;
  gint num_bytes;

  for (i = 0; i < num_http; i++)
    {
      if (http[i].tag == tag)
	break;
    }
  if (i < num_http)
    {
      num_bytes = write (http[i].fd,
			 http[i].query + http[i].query_index,
			 http[i].query_size - http[i].query_index);
      if (num_bytes < 0)
	{
	  /* todo: test for EAGAIN and EINTR */
	  num_bytes = 0;
	}
      http[i].query_index += num_bytes;
      if (http[i].query_index == http[i].query_size)
	{
	  gdk_input_remove (http[i].wr_tag);
	  http[i].wr_tag = 0;
	  g_free (http[i].query);
	  http[i].query = NULL;
#ifdef VERBOSE
	  g_print ("http %d sent query \n", i);
#endif
	  http[i].state = GZILLA_HTTP_RECEIVING;
	  http[i].rd_tag = 
	    gdk_input_add (http[i].fd,
			   GDK_INPUT_READ,
			   (GdkInputFunction) gzilla_http_input_func,
			   (void *)http[i].tag);
	}
    }
  else
    g_warning ("gzilla_http_query_function: trying to write query from http that has already been removed\n");
}

/* The callback for socket setup. This function gets called whenever
   the socket setup completes. Install the input handler for writing
   the query string to the socket. */

static void
gzilla_http_callback (gint fd,
		      void *callback_data)
{
  gint tag = (gint)callback_data;
  gint i;
  GzillaHttpState old_state;

  for (i = 0; i < num_http; i++)
    {
      if (http[i].tag == tag)
	break;
    }
  if (i < num_http)
    {
#ifdef VERBOSE
      g_print ("http %d got fd %d\n", i, fd);
#endif
      if (fd >= 0)
	{
	  http[i].fd = fd;
	  http[i].wr_tag =
	    gdk_input_add (fd,
			   GDK_INPUT_WRITE,
			   (GdkInputFunction) gzilla_http_query_func,
			   (void *)http[i].tag);
	  http[i].state = GZILLA_HTTP_SENDING;
	  gzilla_bytesink_status (http[i].bytesink,
				  GZILLA_STATUS_DIR_DOWN,
				  FALSE,
				  GZILLA_STATUS_MEANING_SHOW,
				  "Connected");
	}
      else
	{
	  gzilla_bytesink_status (http[i].bytesink,
				  GZILLA_STATUS_DIR_DOWN,
				  TRUE,
				  GZILLA_STATUS_MEANING_SHOW,
				  "Connect failed");
	  g_free (http[i].query);
	  old_state = http[i].state;
	  http[i].state = GZILLA_HTTP_ERROR;
	  if (old_state != GZILLA_HTTP_INIT)
	    http[i] = http[--num_http];
	}
    }
  else
    g_warning ("gzilla_http_callback: set up a socket for an http that has already been removed\n");
}

/* This function gets called when the bytesink we're writing into gets
   aborted. It shuts down the connection and deletes the http
   structure, freeing any resources that were taken. */

void
gzilla_http_status (GzillaByteSink *bytesink,
		    GzillaStatusDir dir,
		    gboolean abort,
		    GzillaStatusMeaning meaning,
		    const char *text,
		    void *data)
{
  gint tag = (gint)data;
  gint i;

#ifdef VERBOSE
  g_print ("gzilla_http_status: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (!abort)
    return;

  if (dir == GZILLA_STATUS_DIR_UP)
    {
      for (i = 0; i < num_http; i++)
	{
	  if (http[i].tag == tag)
	    break;
	}
      if (i < num_http)
	{
	  if (http[i].state == GZILLA_HTTP_SOCKET_WAIT)
	    {
	      gzilla_socket_abort (http[i].socket_tag);
	      g_free (http[i].query);
	    }
	  else if (http[i].state == GZILLA_HTTP_SENDING)
	    {
	      g_free (http[i].query);
	      close (http[i].fd);
	      gdk_input_remove (http[i].wr_tag);
	    }
	  else if (http[i].state == GZILLA_HTTP_RECEIVING)
	    {
	      close (http[i].fd);
	      gdk_input_remove (http[i].rd_tag);
	    }
	  else
	    g_warning ("gzilla_http_status: http state is inconsistent\n");
	  http[i] = http[--num_http];
	}
      else
	g_warning ("gzilla_http_status: trying to abort a nonexistent http\n");
    }
}

/* Create a new http connection for URL url, and asynchronously
   feed the bytes that come back to bytesink. */
void
gzilla_http_get (const char *url,
		 GzillaByteSink *bytesink,
		 void (*status_callback) (const char *status, void *data),
		 void *data)
{
  char hostname[256];
  int port;
  char *tail;
  char query[1024];
  gint tag;
  gint i;

  port = 80;

  /* hacked-in support for proxies, inspired by Olivier Aubert */
  if (getenv ("http_proxy") != NULL &&
      !(getenv ("no_proxy") != NULL &&
	strstr (url, getenv ("no_proxy")) != NULL))
    {
      gzilla_url_parse ((char *) getenv ("http_proxy"),
			hostname, sizeof(hostname), &port);
      tail = (char *)url;
    }
  else
    tail = gzilla_url_parse ((char *)url, hostname, sizeof(hostname), &port);

  if (tail != NULL)
    {
      if (num_http == num_http_max)
	{
	  num_http_max <<= 1;
	  http = g_realloc (http, num_http_max * sizeof(GzillaHttp));
	}
      tag = ++http_tag;
      i = num_http;
      num_http++;
      http[i].tag = tag;
      http[i].state = GZILLA_HTTP_INIT;
      http[i].fd = -1;
      http[i].bytesink = bytesink;
      /* todo: check size */
      sprintf (query,
	       "GET %s HTTP/1.0\r\n"
	       "User-Agent: gzilla %s\r\n"
	       "Host: %s:%d\r\n"
	       "\r\n",
	       tail,
	       VERSION,
	       hostname,
	       port);
      http[i].query = g_strdup (query);
      http[i].query_size = strlen (query);
      http[i].query_index = 0;
      http[i].rd_tag = 0;
      http[i].wr_tag = 0;
      http[i].socket_tag = gzilla_socket_new (hostname,
					      port,
					      gzilla_http_callback,
					      (void *)tag);
      if (http[i].state == GZILLA_HTTP_INIT)
	http[i].state = GZILLA_HTTP_SOCKET_WAIT;
      if (http[i].state == GZILLA_HTTP_ERROR)
	{
	  /* If error setting up socket, delete the http structure
	     immediately.

	     todo: shouldn't this send a signal down the bytesink, so
	     that the bytesink knows not to expect any more data? */
	  http[i] = http[--num_http];
	  gzilla_bytesink_status (bytesink,
				  GZILLA_STATUS_DIR_DOWN,
				  TRUE,
				  GZILLA_STATUS_MEANING_SHOW,
				  "Connect failed");
	}
      else
	{
	  /* hook up abort signal handler */
	  http[i].status_id =
	    gtk_signal_connect (GTK_OBJECT (bytesink),
				"status",
				(GtkSignalFunc) gzilla_http_status,
				(void *)tag);
	  if (port == 80)
	    sprintf (query, "Connecting to %s", hostname);
	  else
	    sprintf (query, "Connecting to %s:%d", hostname, port);
	  gzilla_bytesink_status (bytesink,
				  GZILLA_STATUS_DIR_DOWN,
				  FALSE,
				  GZILLA_STATUS_MEANING_SHOW,
				  query);
	}
    }
}
