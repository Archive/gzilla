#include <gtk/gtk.h>

#include "gzillaimgsink.h"
#include "gzillaimage.h"

/* This file implements a gzilla_image module. It handles the transfer
   of data from an ImgSink to a gzw_image widget. */

/* todo: performance seems rather poor :(

   Need to add support for RGBA images */

static void
gzilla_image_line (GzillaImgSinkImg *image,
		   char *buf,
		   gint y,
		   guchar *linebuf)
{
  gint width;
  gint in_width;
  guchar *cmap;
  gint x;
  gint byte;

  /* todo: handle image types other than indexed. */

  width = image->width;
  in_width = image->in_width;

  switch (image->in_type) {
  case GZILLA_IMG_TYPE_INDEXED:
    cmap = image->cmap;
    if (width == in_width) {
      for (x = 0; x < width; x++) {
	byte = ((unsigned char *)buf)[x];
	linebuf[x * 3] = cmap[byte * 3];
	linebuf[x * 3 + 1] = cmap[byte * 3 + 1];
	linebuf[x * 3 + 2] = cmap[byte * 3 + 2];
      }
    } else {
      /* could use DDA here if speed were an issue */
      for (x = 0; x < width; x++) {
	byte = ((unsigned char *)buf)[x * in_width / width];
	linebuf[x * 3] = cmap[byte * 3];
	linebuf[x * 3 + 1] = cmap[byte * 3 + 1];
	linebuf[x * 3 + 2] = cmap[byte * 3 + 2];
      }
    }
    break;
  case GZILLA_IMG_TYPE_GRAY:
    cmap = image->cmap;
    if (width == in_width) {
      for (x = 0; x < width; x++) {
	byte = ((unsigned char *)buf)[x];
	linebuf[x * 3] = byte;
	linebuf[x * 3 + 1] = byte;
	linebuf[x * 3 + 2] = byte;
      }
    } else {
      /* could use DDA here if speed were an issue */
      for (x = 0; x < width; x++) {
	byte = ((unsigned char *)buf)[x * in_width / width];
	linebuf[x * 3] = byte;
	linebuf[x * 3 + 1] = byte;
	linebuf[x * 3 + 2] = byte;
      }
    }
    break;
  case GZILLA_IMG_TYPE_RGB:
    cmap = image->cmap;
    if (width == in_width) {
      /* could avoid memcpy here, if clever. For example, return a boolean
	 to indicate whether to use linebuf or the input buf. */
      memcpy (linebuf, buf, in_width * 3);
    } else {
      /* could use DDA here if speed were an issue */
      for (x = 0; x < width; x++) {
	gint x0;

	x0 = (x * in_width / width) * 3;
	byte = ((unsigned char *)buf)[x * in_width / width];
	linebuf[x * 3] = ((unsigned char *)buf)[x0];
	linebuf[x * 3 + 1] = ((unsigned char *)buf)[x0 + 1];
	linebuf[x * 3 + 2] = ((unsigned char *)buf)[x0 + 2];
      }
    }
    break;
  }
}

void
gzilla_image_write (GzillaImgSink *imgsink,
		    char *buf,
		    gint x0,
		    gint y,
		    gint stride)
{
  GzillaImgSinkImg *image;
  gint y_new;
  gint y_begin, y_end;

  image = (GzillaImgSinkImg *)imgsink;
  y_begin = (y * image->height + image->in_height - 1) / image->in_height;
  y_end = ((y + 1) * image->height + image->in_height - 1) / image->in_height;
  if (y_end > y_begin) {
    gzilla_image_line (image, buf, y, image->linebuf);
    
    for (y_new = y_begin; y_new < y_end; y_new++)
      gzw_image_draw_row ((GzwImage *)image->gzw,
			  image->linebuf,
			  0,
			  y_new,
			  image->width);
  }
}

void
gzilla_image_close (GzillaImgSink *imgsink)
{
  GzillaImgSinkImg *image;

  image = (GzillaImgSinkImg *)imgsink;
  if (image->cmap)
    g_free (image->cmap);
  g_free (image->linebuf);
  g_free (image);
}

static void
gzilla_image_status (GzillaImgSink *imgsink,
		     GzillaStatusDir dir,
		     gboolean abort,
		     GzillaStatusMeaning meaning,
		     const char *text)
{
  /* todo */
}

/* Implement the set_parms method of the image sink */
void
gzilla_image_set_parms (GzillaImgSink *imgsink,
			gint width,
			gint height,
			GzillaImgType type)
{
  GzillaImgSinkImg *image;
  gboolean resize;

  image = (GzillaImgSinkImg *)imgsink;
  image->in_type = type;
  image->in_width = width;
  image->in_height = height;
  resize = (image->width == 0 || image->height == 0);
  if (image->width == 0) {
    image->width = width;
    image->linebuf = g_malloc (3 * width);
  }
  if (image->height == 0)
    image->height = height;
  if (resize) {
    gzw_image_size ((GzwImage *)image->gzw,
		    image->width, image->height);
  }
}

/* Implement the set_parms method of the image sink */
void
gzilla_image_set_cmap (GzillaImgSink *imgsink,
		       guchar *cmap,
		       gint num_colors,
		       gint bg_index)
{
  GzillaImgSinkImg *image;

  image = (GzillaImgSinkImg *)imgsink;
  image->cmap = g_malloc (3 * num_colors);
  memcpy (image->cmap, cmap, 3 * num_colors);
  image->num_colors = num_colors;
  if (bg_index >= 0 && bg_index < num_colors) {
    image->cmap[bg_index * 3] = (image->bg_color >> 16) & 0xff;
    image->cmap[bg_index * 3 + 1] = (image->bg_color >> 8) & 0xff;
    image->cmap[bg_index * 3 + 2] = (image->bg_color) & 0xff;
  }
}

GzillaImgSink*
gzilla_image_new (gint width,
		  gint height,
		  const char *alt,
		  gint32 bg_color) {
  GzillaImgSinkImg *image;
  Gzw *gzw;

  image = g_new (GzillaImgSinkImg, 1);
  gzilla_imgsink_init (&image->imgsink);
  gzw = gzw_image_new (GZW_IMAGE_RGB);
  gzw_image_size ((GzwImage *)gzw, width, height);
  image->gzw = gzw;
  image->width = width;
  image->height = height;
  /* image->alt = alt; */
  image->bg_color = bg_color;
  if (width == 0)
    image->linebuf = NULL;
  else
    image->linebuf = g_malloc (3 * width);

  image->cmap = NULL;

  image->imgsink.write = gzilla_image_write;
  image->imgsink.close = gzilla_image_close;
  image->imgsink.status = gzilla_image_status;
  image->imgsink.set_parms = gzilla_image_set_parms;
  image->imgsink.set_cmap = gzilla_image_set_cmap;

  return &(image->imgsink);
}


/* Return the gzw widget associated with this imgsink. */
Gzw *
gzilla_image_gzw (GzillaImgSink *imgsink)
{
  return ((GzillaImgSinkImg *)imgsink)->gzw;
}
