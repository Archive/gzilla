 /*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

/* Dispatch functions for the Gzw widget set. */

#include <gtk/gtk.h>

#include "gzwrect.h"
#include "gzw.h"

/* Here are the dispatch functions for Gzw methods. */

void
gzw_size_nego_x (Gzw *gzw, gint width)
{
  gzw->klass->size_nego_x (gzw, width);
}

/* Note: in addition to invoking the method, this routine also resets
   GZW_FLAG_REQ_RESIZE flag, because calling this method generally
   means that the resize request has been granted. */
void
gzw_size_nego_y (Gzw *gzw, GzwRect *allocation)
{
  gzw->flags &= ~GZW_FLAG_REQ_RESIZE;
  gzw->klass->size_nego_y (gzw, allocation);
}

void
gzw_paint (Gzw *gzw, GzwRect *rect, GzwPaint *paint)
{

  /* propagates container info down. */
  if (gzw->container == NULL && gzw->parent != NULL)
    gzw->container = gzw->parent->container;

  gzw->klass->paint (gzw, rect, paint);
}

void
gzw_handle_event (Gzw *gzw, GdkEvent *event)
{
  gzw->klass->handle_event (gzw, event);
}

void
gzw_gtk_foreach (Gzw *gzw, GzwCallback callback, gpointer callback_data,
		 GzwRect *plus, GzwRect *minus)
{
  gzw->klass->gtk_foreach (gzw, callback, callback_data, plus, minus);
}

void
gzw_destroy (Gzw *gzw)
{
  gzw->klass->destroy (gzw);
}

/* This routine is not a straightforward invocation of the widget
   method. Rather, it sets the REQ_RESIZE flag on the widget and calls
   the request_resize method on the widget's parent, if the flag is
   not already set. */
void
gzw_request_parent_resize (Gzw *gzw)
{
  if (!(gzw->flags & GZW_FLAG_REQ_RESIZE))
    {
      gzw->flags |= GZW_FLAG_REQ_RESIZE;
      if (gzw->parent != NULL)
	{
	  gzw->parent->klass->request_resize (gzw->parent, gzw);
	}
      else if (gzw->container != NULL)
	{
	  gzw->container->klass->request_resize (gzw->container);
	}
    }
}

void
gzw_request_paint (Gzw *gzw, GzwRect *rect)
{
  GzwContainer *container;

  container = gzw_find_container (gzw);
  if (container == NULL)
    return;
  container->klass->request_paint (container, rect);
}

/* Some utility functions. */


/* Find the container or return NULL if there isn't one. */
GzwContainer *
gzw_find_container (Gzw *gzw)
{
  while (gzw != NULL)
    {
      if (gzw->container != NULL)
	return gzw->container;
      gzw = gzw->parent;
    }
  return NULL;
}

/* Transfer the top layer to the screen if it's not already there. */
void
gzw_paint_to_screen (GtkWidget *widget, GzwContainer *container,
		     GzwRect *rect, GzwPaint *paint)
{
  if (!(paint->flags & GZW_PAINT_SCREEN_UNDER))
    {
      if (paint->flags & GZW_PAINT_BG_UNDER)
	{
	  gdk_window_clear_area (widget->window,

				 container->x_offset + rect->x0,
				 container->y_offset + rect->y0,
				 rect->x1 - rect->x0,
				 rect->y1 - rect->y0);
	  paint->flags |= GZW_PAINT_SCREEN_UNDER;
	}
      else
      /* here's where we'd paint the buf to the screen */
	{
	  /* inconsistent state */
	}
    }
}

/* Update paint flags to indicate we just painted to the screen. */
void
gzw_paint_finish_screen (GzwPaint *paint)
{
  paint->flags &= ~GZW_PAINT_BG_UNDER;
  paint->flags |= GZW_PAINT_SCREEN_UNDER;
}

/* Similarly, there will be a gzw_paint_finish_buf to indicate we just
   painted to the buf. */

/* Some null functions, useful in widgets that don't define all methods. */
void
gzw_null_size_nego_x (Gzw *gzw, gint width)
{
}

void
gzw_null_handle_event (Gzw *gzw, GdkEvent *event)
{
}

void
gzw_null_gtk_foreach (Gzw *gzw, GzwCallback callback, gpointer callback_data,
		      GzwRect *plus, GzwRect *minus)
{
}

void
gzw_null_request_resize (Gzw *gzw, Gzw *child)
{
}
