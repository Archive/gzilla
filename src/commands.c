#include <gtk/gtk.h>

#include "gzillabrowser.h"
#include "gzillalinkblock.h"

#include "gzillabookmark.h"
#include "interface.h"
#include "gzilla.h"
#include "gzillacache.h"
#include "gzillanav.h"


#include <stdio.h>    /* for sprintf */
#include <sys/time.h> /* for gettimeofday (testing gorp only) */
#include <unistd.h>

/* define pi */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif  /*  M_PI  */

/* GZILLA MENU */
void gzilla_new_cmd_callback(GtkWidget * widget, gpointer client_data)
{
	gzilla_new_browser_window ();
}

void gzilla_openfile_cmd_callback(GtkWidget * widget, gpointer client_data)
{
	BrowserWindow *bw = (BrowserWindow *)client_data;
	create_openfile_dialog(bw);
}

void gzilla_openurl_cmd_callback(GtkWidget * widget, gpointer client_data)
{
	BrowserWindow *bw = (BrowserWindow *)client_data;
	create_open_dialog(widget, bw);
}

void gzilla_prefs_cmd_callback(GtkWidget * widget, gpointer client_data)
{ }

void gzilla_close_cmd_callback(GtkWidget * widget, gpointer client_data)
{ }

void gzilla_exit_cmd_callback (GtkWidget *widget, gpointer client_data)
{
	#ifdef ASK_REALLY_QUIT
		BrowserWindow *bw = (BrowserWindow *)client_data;
		create_quit_dialog (bw);
	#else
		/* There should be a close command that does this: */
		/* gtk_widget_destroy (bw->main_window); */
		gzilla_quit_all ();
	#endif
}

/* DOCUMENT MENU */
void document_viewsource_cmd_callback(GtkWidget * widget, gpointer client_data)
{ }

void document_selectall_cmd_callback(GtkWidget * widget, gpointer client_data)
{ }

void document_findtext_cmd_callback(GtkWidget * widget, gpointer client_data)
{ }

void document_print_cmd_callback(GtkWidget * widget, gpointer client_data)
{ }

/* This is a hack and will be removed as soon as the Gzw stuff is integrated
   into the main gzilla. */
#include "gzw.h"
#include "gtkgzwscroller.h"
#include "gzwtest.h"
#include "gzwbullet.h"
#include "gzwpage.h"
#include "gzwborder.h"
#include "gzwembedgtk.h"
#include "gzwimage.h"
/* CP: Testing to see if this solves my bookmarking thing */
#include "gzillamisc.h"

gint test_attr;

#define TEST_IMAGE
#define IMAGE_SIZE 100

#ifdef TEST_PAGE
static void test_timeout (GzwPage *page)
{
	static int counter = 0;
	char *strings[5] = {"and", "another", "cup", "of", "flour,"};

	gzw_page_update_begin (page);
	gzw_page_add_text (page, g_strdup (strings[counter]), test_attr);
	gzw_page_add_space (page, test_attr);
	counter = (counter + 1) % 5;
	gzw_page_update_end (page);
}
#endif

#ifdef TEST_IMAGE
#include <math.h>
static gdouble calc (gint x, gint y, gdouble angle)
{
	gdouble s, c;

	s = sin (angle * M_PI / 180) * 256.0 / IMAGE_SIZE;
	c = cos (angle * M_PI / 180) * 256.0 / IMAGE_SIZE;
	return 128 + (x - (IMAGE_SIZE >> 1)) * c - (y - (IMAGE_SIZE >> 1)) * s;
}

gint timeout_tag;

static gint test_timeout (GzwImage *image)
{
	static gint y = 0;
	guchar buf[3 * IMAGE_SIZE];
	gint x;
	gdouble r, g, b;
	gdouble dr, dg, db;

	r = calc (0, y, 0);
	g = calc (0, y, 120);
	b = calc (0, y, 240);

	dr = calc (1, y, 0) - r;
	dg = calc (1, y, 120) - g;
	db = calc (1, y, 240) - b;

	for (x = 0; x < IMAGE_SIZE; x++)
	{
		buf[x * 3] = CLAMP ((gint)r, 0, 255);
		buf[x * 3 + 1] = CLAMP ((gint)g, 0, 255);
		buf[x * 3 + 2] = CLAMP ((gint)b, 0, 255);
		r += dr;
		g += dg;
		b += db;
	}

	gzw_image_draw_row (image, buf, 0, y, IMAGE_SIZE);
	y++;
	return (y != IMAGE_SIZE);
}
#endif

void document_testgzw_cmd_callback (GtkWidget *widget, gpointer client_data)
{
	GtkWidget *window;
	GtkWidget *gzw_scroller;
	Gzw *test;
	GzwPageFont font;
	GzwPageAttr attr;
	gint attr_index;
	gint i;

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gzw_scroller = gtk_gzw_scroller_new (NULL, NULL);
	#ifdef TEST_PAGE
		test = gzw_page_new ();
	#if 0
		font.font = NULL;
	#endif
	font.name = "times";
	font.size = 14;
	font.bold = FALSE;
	font.italic = FALSE;

	gzw_page_init_attr ((GzwPage *)test, &attr);
	attr.font = gzw_page_find_font ((GzwPage *)test, &font);
	attr_index = gzw_page_find_attr ((GzwPage *)test, &attr);
	test_attr = attr_index;
	gzw_page_update_begin ((GzwPage *)test);
	for (i = 0; i < 1000; i++)
	{
		char str[80];

		sprintf (str, "word%d", i);
		gzw_page_add_text ((GzwPage *)test, g_strdup (str), attr_index);
		gzw_page_add_space ((GzwPage *)test, attr_index);
	}
	gzw_page_parbreak ((GzwPage *)test, 10);
	font.name = "courier";
	attr.font = gzw_page_find_font ((GzwPage *)test, &font);
	attr_index = gzw_page_find_attr ((GzwPage *)test, &attr);
	gzw_page_add_text ((GzwPage *)test, g_strdup ("hello,"), attr_index);
	gzw_page_add_space ((GzwPage *)test, attr_index);
	gzw_page_add_widget ((GzwPage *)test,
		gzw_bullet_new (GZW_BULLET_DISC), attr_index);
	gzw_page_add_text ((GzwPage *)test, g_strdup ("world!"), attr_index);
	gzw_page_parbreak ((GzwPage *)test, 10);
	{
		GtkWidget *button;

		#if 1
			button = gtk_button_new_with_label ("press me");
		#else
			button = gtk_label_new ("don't press me");
		#endif
		gtk_widget_show (button);
		gzw_page_add_widget ((GzwPage *)test,
			gzw_embed_gtk_new (button), attr_index);
	}
	gzw_page_parbreak ((GzwPage *)test, 10);
	gzw_page_update_end ((GzwPage *)test);
	gtk_timeout_add (1000, (GtkFunction) test_timeout, test);
	#endif
	#ifdef TEST_BULLET
		test = gzw_bullet_new (GZW_BULLET_SQUARE);
	#endif
	#ifdef TEST_TEST
		test = gzw_test_new ();
	#endif
	#ifdef TEST_IMAGE
		test = gzw_image_new (GZW_IMAGE_RGB);
		gzw_image_size ((GzwImage *)test, IMAGE_SIZE, IMAGE_SIZE);
		timeout_tag = gtk_timeout_add (100, (GtkFunction) test_timeout, test);
	#endif

	test = gzw_border_new (test, 5);
	gtk_gzw_scroller_set_gzw (GTK_GZW_SCROLLER (gzw_scroller), test);
	gtk_container_add (GTK_CONTAINER (window), gzw_scroller);
	gtk_widget_show (gzw_scroller);
	gtk_widget_show (window);
	{
		struct timeval start_tv, end_tv;
		struct timezone tz;
		gettimeofday (&start_tv, &tz);
		for (i = 0; i < 1000; i++)
			gdk_flush ();
		gettimeofday (&end_tv, &tz);
		printf ("gdk_flush takes %fms.\n",
			(end_tv.tv_sec + 1e-6 * end_tv.tv_usec) -
			(start_tv.tv_sec + 1e-6 * start_tv.tv_usec));
	}
}

/* BROWSE MENU */
void browse_back_cmd_callback(GtkWidget * widget, gpointer client_data)
{
	BrowserWindow *bw = (BrowserWindow *)client_data;
	gzilla_nav_back(bw);
}

void browse_forw_cmd_callback(GtkWidget * widget, gpointer client_data)
{
	BrowserWindow *bw = (BrowserWindow *)client_data;
	gzilla_nav_forw(bw);
}

void browse_reload_cmd_callback(GtkWidget * widget, gpointer client_data)
{
	BrowserWindow *bw = (BrowserWindow *)client_data;
	gzilla_nav_reload(bw);
}

void browse_stop_cmd_callback(GtkWidget * widget, gpointer client_data)
{
	/* todo: move this into nav - it needs to cancel_last_waiting () */
	BrowserWindow *bw = (BrowserWindow *)client_data;
	gzilla_nav_stop (bw);
}

void browse_home_cmd_callback(GtkWidget * widget, gpointer client_data)
{
	BrowserWindow *bw = (BrowserWindow *)client_data;
	gzilla_nav_home(bw);
}

/* BOOKMARKS MENU */
void bookmark_add_cmd_callback (GtkWidget *widget, gpointer client_data)
{
	gzilla_bookmark_add (widget, client_data);
}

void bookmark_viewbm_cmd_callback (GtkWidget *widget, gpointer client_data)
{
	BrowserWindow *bw = (BrowserWindow *)client_data;
	int bmfile;
	bmfile = gzilla_misc_prepend_user_home(".gzilla/bookmarks.html");
	
	/* CP: Gzilla dies when you try to bookmark your own bookmark file.
	That shouldn't happen. */
	gzilla_nav_push (bmfile, bw);
}

/* HELP MENU */
void help_home_cmd_callback (GtkWidget *widget, gpointer client_data)
{
	BrowserWindow *bw = (BrowserWindow *)client_data;
	gzilla_nav_push ("http://www.gzilla.com/", bw);
}

void help_manual_cmd_callback (GtkWidget *widget, gpointer client_data)
{
	/* CP: Uncomment this when the feature is implemented. :)
	BrowserWindow *bw = (BrowserWindow *)client_data;
	gzilla_nav_push ("file:/usr/local/man/man1/gzilla.man | groff -man", bw);
	*/
}
