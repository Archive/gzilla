#ifndef __GZILLA_FILE_H__
#define __GZILLA_FILE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void gzilla_file_init (void);

/* Create a new file connection for URL url, and asyncrhonously feed
   the bytes that come back to bytesink. */
void gzilla_file_get (const char *url,
		      GzillaByteSink *bytesink,
		      void (*status_callback) (const char *status, void *data),
		      void *data);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_FILE_H__ */
