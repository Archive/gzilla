/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

#include <gtk/gtk.h>

#include <ctype.h>		/* for tolower */
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include <netinet/in.h>		/* needed before including gzillasocket.h */

#include "gzillabytesink.h"
#include "config.h"
#include "gzillaurl.h"

/* has definitions for gzillaCacheCon and gzillaCacheLine structures.*/ 
#include "gzillacache.h"

#include "gzillasocket.h"

#include "gzillageturl.h"

#define CACHE_MAX 2000000

GzillaCacheLine **cache;
gint cache_size;
gint cache_size_max;

GzillaCacheCon **ccons;
gint num_ccons;
gint num_ccons_max;

gint cache_size_total;
/* invariant: cache_size_total is the sum of the buf_size fields of
   all cache lines in cache. */

gint cache_idle_tag;
gint cache_counter;

static void gzilla_cache_write  (GzillaCacheLine *line,
				 char *buf,
				 gint bufsize);
static void gzilla_cache_close  (GzillaCacheLine *line);
static void gzilla_cache_status (GzillaByteSink *bytesink,
				 GzillaStatusDir dir,
				 gboolean abort,
				 GzillaStatusMeaning meaning,
				 const char *text,
				 GzillaCacheLine *line);
static void gzilla_cache_remove (gint cache_index);

/* Here's the bytesink that's used to feed a cache line. */

#define GZILLA_CACHE_BS(obj)           GTK_CHECK_CAST (obj, gzilla_cache_bs_get_type (), GzillaCacheBS)
#define GZILLA_CACHE_BS_CLASS(klass)   GTK_CHECK_CLASS_CAST (klass, gzilla_cache_bs_get_type (), GzillaCacheBSClass)
#define GZILLA_IS_CACHE_BS(obj)        GTK_CHECK_TYPE (obj, gzilla_cache_bs_get_type ())

typedef struct _GzillaCacheBS        GzillaCacheBS;
typedef struct _GzillaCacheBSClass   GzillaCacheBSClass;

struct _GzillaCacheBS {
  GzillaByteSink  bytesink;
  GzillaCacheLine *line;
};

struct _GzillaCacheBSClass {
  GzillaByteSinkClass  parent_class;
};

static void gzilla_cache_bs_class_init (GzillaCacheBSClass *klass);
static void gzilla_cache_bs_init       (GzillaCacheBS      *cache_bs);
static void gzilla_cache_bs_destroy    (GtkObject      *object);
static void gzilla_cache_bs_write      (GzillaByteSink *bytesink,
					char *bytes,
					gint num);
static void gzilla_cache_bs_close      (GzillaByteSink *bytesink);
static void gzilla_cache_bs_status     (GzillaByteSink *bytesink,
					GzillaStatusDir dir,
					gboolean abort,
					GzillaStatusMeaning meaning,
					const char *text);

static GzillaByteSinkClass *parent_class = NULL;

guint
gzilla_cache_bs_get_type ()
{
  static guint cache_bs_type = 0;

  if (!cache_bs_type)
    {
      GtkTypeInfo cache_bs_info =
      {
	"GzillaCacheBS",
	sizeof (GzillaCacheBS),
	sizeof (GzillaCacheBSClass),
	(GtkClassInitFunc) gzilla_cache_bs_class_init,
	(GtkObjectInitFunc) gzilla_cache_bs_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      cache_bs_type = gtk_type_unique (gzilla_bytesink_get_type (), &cache_bs_info);
    }

  return cache_bs_type;
}

static void
gzilla_cache_bs_class_init (GzillaCacheBSClass *class)
{
  GtkObjectClass      *object_class;
  GzillaByteSinkClass *bytesink_class;

  parent_class = gtk_type_class (gzilla_bytesink_get_type ());

  object_class = (GtkObjectClass*) class;
  bytesink_class = (GzillaByteSinkClass*) class;

  object_class->destroy = gzilla_cache_bs_destroy;

  bytesink_class->write = gzilla_cache_bs_write;
  bytesink_class->close = gzilla_cache_bs_close;
  bytesink_class->status = gzilla_cache_bs_status;
}

static void
gzilla_cache_bs_init (GzillaCacheBS *cache_bs)
{
  cache_bs->line = NULL;
}

static void
gzilla_cache_bs_destroy (GtkObject *object)
{
  GzillaCacheBS *cache_bs;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GZILLA_IS_CACHE_BS (object));

  cache_bs = GZILLA_CACHE_BS (object);
  /* todo: if the line field is still set (indicating that the bytesink
     is being destroyed before it was closed), then abort. */

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gzilla_cache_bs_write (GzillaByteSink *bytesink,
		       char *bytes,
		       gint num)
{
  gzilla_cache_write (GZILLA_CACHE_BS (bytesink)->line,
		      bytes,
		      num);
}

static void
gzilla_cache_bs_close (GzillaByteSink *bytesink)
{
  gzilla_cache_close (GZILLA_CACHE_BS (bytesink)->line);
  GZILLA_CACHE_BS (bytesink)->line = NULL;
}

/* propagate status and abort downstream */
static void
gzilla_cache_bs_status (GzillaByteSink *bytesink,
			GzillaStatusDir dir,
			gboolean abort,
			GzillaStatusMeaning meaning,
			const char *text)
{
  gint i;
  GzillaCacheLine *line;

#ifdef VERBOSE
  g_print ("gzilla_cache_bs_status: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (dir == GZILLA_STATUS_DIR_UP)
    return;

  line = GZILLA_CACHE_BS (bytesink)->line;
  line->eof = TRUE;
  for (i = 0; i < num_ccons; i++)
    {
      if (ccons[i]->line == line)
	{
	  /* todo: is it really necessary to disconnect the status
	     signal handler? */
	  if (abort)
	    gtk_signal_disconnect (GTK_OBJECT (ccons[i]->bytesink),
				   ccons[i]->status_id);

	  gzilla_bytesink_status (ccons[i]->bytesink,
				  GZILLA_STATUS_DIR_DOWN,
				  abort,
				  meaning,
				  text);
	  if (abort)
	    {
	      g_free (ccons[i]);
	      ccons[i] = ccons[--num_ccons];
	      i--;
	    }
	}
    }
  if (abort)
    {
      gtk_object_destroy (GTK_OBJECT (line->bytesink));

      for (i = 0; i < cache_size; i++)
	if (cache[i] == line)
	  break;
      if (i < cache_size)
	gzilla_cache_remove (i);
    }
}

GzillaByteSink*
gzilla_cache_bs_new (GzillaCacheLine *line)
{
  GzillaCacheBS *cache_bs;

  cache_bs = gtk_type_new (gzilla_cache_bs_get_type ());

  cache_bs->line = line;
  return GZILLA_BYTESINK (cache_bs);
}

/* End of cache bytesink */

void
gzilla_cache_init (void)
{
    cache_size = 0;
    cache_size_max = 16;
    cache = g_new(GzillaCacheLine *, cache_size_max);
	
    num_ccons = 0;
    num_ccons_max = 16;
    ccons = g_new(GzillaCacheCon *, cache_size_max);
	
    cache_size_total = 0;
    cache_idle_tag = 0;
    cache_counter = 0;
}

gint
gzilla_cache_idle_func (void)
{
  gint i;
  gint size;
  gint num_active;
	
  num_active = 0;
  for (i = 0; i < num_ccons; i++)
    {
      size = ccons[i]->line->size - ccons[i]->index;
      if (size != 0)
	{
	  size = MIN(size, 8192);
	  gzilla_bytesink_write(ccons[i]->bytesink,
				ccons[i]->line->buf + ccons[i]->index,
				size);
	  ccons[i]->index += size;
	  if (ccons[i]->index == ccons[i]->line->size)
	    {
	      if (ccons[i]->line->eof)
		{
		  gtk_signal_disconnect (GTK_OBJECT (ccons[i]->bytesink),
					 ccons[i]->status_id);
		  gzilla_bytesink_close(ccons[i]->bytesink);
		  g_free(ccons[i]);
		  ccons[i] = ccons[--num_ccons];
		  i--;
		}
	    }
	  else
	    {
	      num_active++;
	    }
	}
    }
  if (num_active == 0)
    {
      cache_idle_tag = 0;
      return FALSE;
    }
  else
    {
      return TRUE;
    }
}

/* Pipe the cache line contents to the bytesink. The implementation is
 to create a new cache connection, and start the cache idle process
 if needed. */
static void
gzilla_cache_hit (GzillaByteSink * bytesink, GzillaCacheLine * line)
{
    GzillaCacheCon *ccon;
	
    if (num_ccons == num_ccons_max) {
		num_ccons_max <<= 1;
		ccons = g_realloc(ccons, num_ccons_max * sizeof(GzillaCacheCon *));
    }
    ccon = g_new(GzillaCacheCon, 1);
    ccon->line = line;
    ccon->bytesink = bytesink;
    ccon->index = 0;
#ifdef VERBOSE
    g_print ("adding abort tag to bytesink %x, line is %x\n",
	     bytesink, line);
#endif
    ccon->status_id = gtk_signal_connect (GTK_OBJECT (bytesink),
					 "status",
					 (GtkSignalFunc) gzilla_cache_status,
					 line);
    ccons[num_ccons++] = ccon;
	
    line->last_use = cache_counter++;
	
    if ((line->eof || line->size > 0) && cache_idle_tag == 0) {
		cache_idle_tag = gtk_idle_add((GtkFunction) gzilla_cache_idle_func, NULL);
    }
}

/* Remove a line from the cache */
static void
gzilla_cache_remove (gint cache_index)
{
#ifdef VERBOSE
    g_print("removing %s from cache\n", cache[cache_index]->url);
#endif
	
    if (cache[cache_index]->buf)
			g_free(cache[cache_index]->buf);
    cache_size_total -= cache[cache_index]->buf_size;
    g_free(cache[cache_index]);
    cache[cache_index] = cache[--cache_size];
}

static void
gzilla_cache_expand (gint size)
{
  gint cache_index, ccon_index;
  gint best;
	
  cache_size_total += size;
#ifdef VERBOSE
  g_print("cache_size_total = %d\n", cache_size_total);
#endif
	
  /* I know the algorithm is inefficient. I don't really think it matters. */
  while (cache_size_total > CACHE_MAX) {
    best = -1;
    for (cache_index = 0; cache_index < cache_size; cache_index++)
      {
	if (cache[cache_index]->eof)
	  {
	    for (ccon_index = 0; ccon_index < num_ccons; ccon_index++)
	      {
		if (ccons[ccon_index]->line == cache[cache_index])
		  break;
	      }
	    if (ccon_index == num_ccons &&
		(best == -1 ||
		 cache[cache_index]->last_use < cache[best]->last_use))
	      {
		best = cache_index;
	      }
	  }
      }
    if (best == -1)
      return;
    gzilla_cache_remove (best);
  }
}

/* Create a new cache line. */
static void
gzilla_cache_miss (GzillaByteSink * bytesink, const char *url)
{
  GzillaCacheLine *line;
  GzillaByteSink *upstream_bytesink;
	
  line = g_new (GzillaCacheLine, 1);
	
  line->url = g_strdup (url);
  line->size = 0;
  line->buf_size = 1024;
  line->buf = g_new (char, line->buf_size);
  gzilla_cache_expand(line->buf_size);
  line->last_use = cache_counter++;
  line->eof = FALSE;

  if (cache_size == cache_size_max)
    {
      cache_size_max <<= 1;
      cache = g_realloc (cache, cache_size_max * sizeof(GzillaCacheLine *));
    }
  cache[cache_size++] = line;
	
  upstream_bytesink = gzilla_cache_bs_new (line);
  line->bytesink = upstream_bytesink;

  /* the cache hit is done first in case get_url throws a status signal. */

  gzilla_cache_hit (bytesink, line);

  gzilla_get_url (line->url, upstream_bytesink, NULL, NULL);
}

/* Try finding the url in the cache. If it hits, pipe the cache contents
 to the bytesink. If it misses, set up a connection. */
void
gzilla_cache_get_url (GzillaByteSink *bytesink, const char *url)
{
  gint i;
	
  for (i = 0; i < cache_size; i++)
    {
      /* maybe do some canonicalization of the url here? */
      if (!strcmp (url, cache[i]->url))
	{
	  gzilla_cache_hit (bytesink, cache[i]);
	  return;
	}
    }
  gzilla_cache_miss(bytesink, url);
}

static void
gzilla_cache_write (GzillaCacheLine * line, char *buf, gint bufsize)
{
  gint i;
	
  if (line->buf)
    {
      while (line->size + bufsize > line->buf_size)
	{
	  gzilla_cache_expand (line->buf_size);
	  line->buf_size <<= 1;
	  line->buf = g_realloc (line->buf, line->buf_size);
	}
      memcpy (line->buf + line->size, buf, bufsize);
    }
  for (i = 0; i < num_ccons; i++)
    {
      if (ccons[i]->line == line && ccons[i]->index == line->size)
	{
	  gzilla_bytesink_write(ccons[i]->bytesink, buf, bufsize);
	  ccons[i]->index += bufsize;
	}
    }
  line->size += bufsize;
}

static void
gzilla_cache_close (GzillaCacheLine *line)
{
  gint i;
	
  line->eof = TRUE;
  for (i = 0; i < num_ccons; i++)
    {
      if (ccons[i]->line == line &&
	  ccons[i]->index == line->size)
	{
	  gtk_signal_disconnect (GTK_OBJECT (ccons[i]->bytesink),
				 ccons[i]->status_id);
	  gzilla_bytesink_close(ccons[i]->bytesink);
	  g_free(ccons[i]);
	  ccons[i] = ccons[--num_ccons];
	  i--;
	}
    }
  /* It's ok to destroy an object in the middle of a method invocation on
     it - it actually gets destroyed when the method finishes. */
  gtk_object_destroy (GTK_OBJECT (line->bytesink));
  line->bytesink = NULL;
}

/* This function is called whenever a bytesink that we're writing into
   is aborted. It deletes the connection. It also propagates the abort
   signal upstream to the bytesink feeding the cache line, if that was
   the only connection left. */

static void
gzilla_cache_status (GzillaByteSink *bytesink,
		     GzillaStatusDir dir,
		     gboolean abort,
		     GzillaStatusMeaning meaning,
		     const char *text,
		     GzillaCacheLine *line)
{
  gint i;
  gint num_active;

#ifdef VERBOSE
  g_print ("gzilla_cache_status: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (dir == GZILLA_STATUS_DIR_DOWN || !abort)
    return;

  for (i = 0; i < num_ccons; i++)
    {
      if (ccons[i]->bytesink == bytesink)
	break;
    }
  if (i == num_ccons)
    {
      g_warning ("gzilla_cache_status: bytesink not in the cache");
      return;
    }

  if (line != ccons[i]->line)
    g_warning ("gzilla_cache_status: line is inconsistent with ccons[i]->line!\n");

  gtk_signal_disconnect (GTK_OBJECT (ccons[i]->bytesink),
			 ccons[i]->status_id);
  
  g_free(ccons[i]);
  ccons[i] = ccons[--num_ccons];
	
  num_active = 0;
  for (i = 0; i < num_ccons; i++) {
    if (ccons[i]->line == line)
      num_active++;
  }
    
  if (num_active == 0 && line->bytesink != NULL)
    {
#ifdef VERBOSE
      g_print ("gzilla_cache_abort: aborting bytesink\n");
#endif
      gzilla_bytesink_status (line->bytesink,
			      GZILLA_STATUS_DIR_UP,
			      abort,
			      meaning,
			      text);
      for (i = 0; i < cache_size; i++)
	{
	  if (cache[i] == line)
	    break;
	}
      if (i < cache_size)
	gzilla_cache_remove (i);
      else
	g_warning ("gzilla_cache_status: can't find line to remove");
    }
}

/* todo: the case where the URL is open in a different window is not
   handled well (I think we should abort the upstream bytesink if the
   cache line is not at eof). */
void
gzilla_cache_remove_url (char *url)
{
  gint i;

  for (i = 0; i < cache_size; i++)
    {
      if (!strcmp (cache[i]->url, url))
	{
	  gzilla_cache_remove (i);
	  return;
	}
    }
}

/* End RAM cache */
