#include <gtk/gtk.h>

#include "gzillaimgsink.h"

#undef DISABLE_ALL_IMGSINK

void
gzilla_imgsink_init (GzillaImgSink *imgsink)
{
  imgsink->close_handler = NULL;
  imgsink->status_handler = NULL;
  imgsink->status_handler2 = NULL;
}

gint
gzilla_imgsink_bpp (GzillaImgType type)
{
  switch (type) {
  case GZILLA_IMG_TYPE_INDEXED:
    return 1;
  case GZILLA_IMG_TYPE_RGB:
    return 3;
  case GZILLA_IMG_TYPE_GRAY:
    return 1;
  }
  return 0;
}

void
gzilla_imgsink_write (GzillaImgSink *imgsink,
		      char *buf,
		      gint x,
		      gint y,
		      gint stride)
{
#ifdef DISABLE_ALL_IMGSINK
  return;
#endif
  imgsink->write (imgsink, buf, x, y, stride);
}


void
gzilla_imgsink_close (GzillaImgSink *imgsink)
{
#ifdef DISABLE_ALL_IMGSINK
  return;
#endif
  if (imgsink->close_handler != NULL)
    (*imgsink->close_handler) (imgsink->close_data, imgsink);
  imgsink->close (imgsink);
}

/* This is the abort signal that propagates upstream. */
void
gzilla_imgsink_status (GzillaImgSink *imgsink,
		       GzillaStatusDir dir,
		       gboolean abort,
		       GzillaStatusMeaning meaning,
		       const char *text)
{
#ifdef DISABLE_ALL_IMGSINK
  return;
#endif
  if (imgsink->status_handler != NULL)
    (*imgsink->status_handler) (imgsink->status_data, imgsink,
				dir, abort, meaning, text);
  if (imgsink->status_handler2 != NULL)
    (*imgsink->status_handler2) (imgsink->status_data2, imgsink,
				dir, abort, meaning, text);
  imgsink->status (imgsink, dir, abort, meaning, text);
}

void
gzilla_imgsink_set_parms (GzillaImgSink *imgsink,
			  gint width,
			  gint height,
			  GzillaImgType type)
{
#ifdef DISABLE_ALL_IMGSINK
  return;
#endif
  imgsink->set_parms (imgsink, width, height, type);
}

void
gzilla_imgsink_set_cmap (GzillaImgSink *imgsink,
			 guchar *cmap,
			 gint num_colors,
			 gint bg_index)
{
#ifdef DISABLE_ALL_IMGSINK
  return;
#endif
  imgsink->set_cmap (imgsink, cmap, num_colors, bg_index);
}

void
gzilla_imgsink_set_close_handler (GzillaImgSink *imgsink,
				  void (*close_handler) (void *data, GzillaImgSink *imgsink),
				  void *data)
{
  imgsink->close_handler = close_handler;
  imgsink->close_data = data;
}

void
gzilla_imgsink_set_status_handler (GzillaImgSink *imgsink,
				   void (*status_handler) (void *data,
							   GzillaImgSink *imgsink,
							   GzillaStatusDir dir,
							   gboolean abort,
							   GzillaStatusMeaning meaning,
							   const char *text),
				   void *data)
{
  imgsink->status_handler = status_handler;
  imgsink->status_data = data;
}

void
gzilla_imgsink_set_status_handler2 (GzillaImgSink *imgsink,
				    void (*status_handler) (void *data,
							    GzillaImgSink *imgsink,
							    GzillaStatusDir dir,
							    gboolean abort,
							    GzillaStatusMeaning meaning,
							    const char *text),
				    void *data)
{
  imgsink->status_handler2 = status_handler;
  imgsink->status_data2 = data;
}
