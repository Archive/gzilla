/* Some common enums for status processing in Gzilla. See abort.html
   for more details. */

#ifndef __GZILLA_STATUS_H__
#define __GZILLA_STATUS_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef enum
{
  GZILLA_STATUS_DIR_ORIG,
  GZILLA_STATUS_DIR_UP,
  GZILLA_STATUS_DIR_DOWN
} GzillaStatusDir;

typedef enum
{
  /* show only in debugging mode */
  GZILLA_STATUS_MEANING_DEBUG,
  /* show in status label */
  GZILLA_STATUS_MEANING_SHOW
} GzillaStatusMeaning;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_STATUS_H__ */
