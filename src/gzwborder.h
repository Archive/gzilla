/* This module contains a simple gzw container that adds a border around
   the child widget. In addition to providing the border around the page
   widget in gzilla, it is also interesting to show how to make
   container widgets in gzw. */

#ifndef __GZW_BORDER_H__
#define __GZW_BORDER_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GzwBorder  GzwBorder;

struct _GzwBorder {
  Gzw gzw;

  Gzw *child;
  gint border;
};

Gzw *gzw_border_new (Gzw *child, gint border);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GZW_BORDER_H__ */
