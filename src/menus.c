/* The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gtk/gtk.h>

#include "commands.h"
#include "gzillabrowser.h"

/* TO DO:
   One menu per browser window. [CP: ?]
   Get bw as callback data for commands.
   CP: See TODO file.
*/

/* New menus stuff here (based on GTK+ menus and not menufactory. */

/* Taken from gtkmenufactory.c.
   Todo: make it much more general, so that it can handle keys outside
   the 8-bit ASCII set. This will require changes to GTK :-(
   CP: Why do we need non-ASCII hotkeys?
*/

static void gtk_menu_factory_parse_accelerator
	(char *accelerator, char *accelerator_key, guint8 *accelerator_mods)
{
	int done;

	g_return_if_fail (accelerator != NULL);
	g_return_if_fail (accelerator_key != NULL);
	g_return_if_fail (accelerator_mods != NULL);

	*accelerator_key = 0;
	*accelerator_mods = 0;

	done = FALSE;
	while (!done)
 	{
		if (strncmp (accelerator, "<shift>", 7) == 0)
		{
			accelerator += 7;
			*accelerator_mods |= GDK_SHIFT_MASK;
		}
		else if (strncmp (accelerator, "<ctrl>", 5) == 0)
		{
			accelerator += 5;
			*accelerator_mods |= GDK_MOD1_MASK;
		}
		else if (strncmp (accelerator, "<control>", 9) == 0)
		{
			accelerator += 9;
			*accelerator_mods |= GDK_CONTROL_MASK;
		}
		else
		{
			done = TRUE;
			*accelerator_key = accelerator[0];
		}
    }
}


/* Make a new menu, insert it into the menu bar, and return it. */
GtkWidget * gzilla_menu_new
	(GtkWidget *menubar, const char *name,
	gboolean right_justify, BrowserWindow *bw)
{
	GtkWidget *menu;
	GtkWidget *menuitem;

	menu = gtk_menu_new ();
	menuitem = gtk_menu_item_new_with_label ((char *)name);
	if (right_justify)
		gtk_menu_item_right_justify (GTK_MENU_ITEM (menuitem));
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem), menu);
	gtk_menu_bar_append (GTK_MENU_BAR (menubar), menuitem);
	gtk_widget_show (menuitem);

	#ifdef GTK_HAVE_FEATURES_1_1_0
		gtk_menu_set_accel_group (GTK_MENU (menu), bw->accel_group);
	#else
		gtk_menu_set_accelerator_table (GTK_MENU (menu), bw->accel_table);
	#endif

	return menu;
}

/* Add an item to a menu, including the name, an accelerator (not yet
   implemented), and a callback function for activation. */
static GtkWidget * gzilla_menu_add
	(GtkWidget *menu, const char *name, const char *accel,
	void *data, BrowserWindow *bw, void (*callback)
	(GtkWidget *widget, void *data))
{
	GtkWidget *menuitem;
	guint accel_key;
	guint accel_mods;

	menuitem = gtk_menu_item_new_with_label ((char *)name);
	gtk_menu_append (GTK_MENU (menu), menuitem);
	gtk_widget_show (menuitem);
	if (accel != NULL)
		{
		#ifdef GTK_HAVE_FEATURES_1_1_0
			gtk_accelerator_parse (accel, &accel_key, &accel_mods);
			gtk_widget_add_accelerator
				(menuitem, "activate", bw->accel_group,
				accel_key, accel_mods, GTK_ACCEL_VISIBLE);
		#else
			gtk_menu_factory_parse_accelerator
				((char *)accel, &accel_key, &accel_mods);
			gtk_widget_install_accelerator
				(menuitem, bw->accel_table, "activate",
				accel_key, accel_mods);
		#endif
		}

	if (callback != NULL)
		gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
			(GtkSignalFunc) callback, data);

	return menuitem;
}

/* Add a separator into the menu. */
static void gzilla_menu_sep (GtkWidget *menu)
{
	GtkWidget *widget;

	widget = gtk_menu_item_new ();
	gtk_menu_append (GTK_MENU (menu), widget);
	gtk_widget_show (widget);
}

/* Make up a new menubar for a main browser window. The accelerator table
   is stored in bw->accel_table.
   Currently does not deal with dynamic menu items (bookmarks and history).
   CP: It *seems* to handle dynamic menu items...
*/
GtkWidget * gzilla_menu_mainbar_new (BrowserWindow *bw)
{
	GtkWidget *menubar;
	GtkWidget *gzilla_menu;
	GtkWidget *document_menu;
	GtkWidget *browse_menu;
	GtkWidget *bookmarks_menu;
	GtkWidget *help_menu;

	menubar = gtk_menu_bar_new ();

	#ifdef GTK_HAVE_FEATURES_1_1_0
		bw->accel_group = gtk_accel_group_new ();
	#else
		bw->accel_table = gtk_accelerator_table_new ();
		gtk_accelerator_table_ref (bw->accel_table);
	#endif

	/* GZILLA (PROGRAM) MENU */
	gzilla_menu = gzilla_menu_new (menubar, "Gzilla", FALSE, bw);
	
	gzilla_menu_add (gzilla_menu, "New Browser", "<ctrl>N", bw, bw,
		gzilla_new_cmd_callback);
	gzilla_menu_add (gzilla_menu, "Open File...", "<ctrl>O", bw, bw,
		gzilla_openfile_cmd_callback);
	gzilla_menu_add (gzilla_menu, "Open URL...", "<ctrl>L", bw, bw,
		gzilla_openurl_cmd_callback);
	gzilla_menu_add (gzilla_menu, "Preferences", "<ctrl>E", bw, bw,
		gzilla_prefs_cmd_callback);
	gzilla_menu_add (gzilla_menu, "Close Window", "<ctrl>W", bw, bw,
		gzilla_close_cmd_callback);
	gzilla_menu_sep (gzilla_menu);
	gzilla_menu_add (gzilla_menu, "Exit Gzilla", "<ctrl>X", bw, bw,
		gzilla_exit_cmd_callback);
	

	/* DOCUMENT MENU */
	document_menu = gzilla_menu_new (menubar, "Document", FALSE, bw);

	gzilla_menu_add (document_menu, "View Source", "<ctrl>V", bw, bw,
		document_viewsource_cmd_callback);
	gzilla_menu_add (document_menu, "Select All", "<ctrl>A", bw, bw,
		document_selectall_cmd_callback);
	gzilla_menu_add (document_menu, "Find Text...", "<ctrl>F", bw, bw,
		document_findtext_cmd_callback);
	gzilla_menu_add (document_menu, "Print...", "<ctrl>P", bw, bw,
		document_print_cmd_callback);
	gzilla_menu_add (document_menu, "Test Gzw", "<ctrl>Z", bw, bw,
		document_testgzw_cmd_callback);


	/* BROWSE MENU */
	browse_menu = gzilla_menu_new (menubar, "Browse", FALSE, bw);

	bw->back_menuitem = gzilla_menu_add (browse_menu, "Back", NULL, bw, bw,
		browse_back_cmd_callback);
	bw->forw_menuitem = gzilla_menu_add (browse_menu, "Forward", NULL, bw, bw,
		browse_forw_cmd_callback);
	gzilla_menu_add (browse_menu, "Reload", "<ctrl>R", bw, bw,
		browse_reload_cmd_callback);
	bw->stop_menuitem = gzilla_menu_add (browse_menu, "Stop Loading", NULL,
		bw, bw, browse_stop_cmd_callback);
	gzilla_menu_add (browse_menu, "Home", NULL, bw, bw,
		browse_home_cmd_callback);

	/* BOOKMARKS MENU */
	bookmarks_menu = gzilla_menu_new (menubar, "Bookmarks", FALSE, bw);
	bw->bookmarks_menu = bookmarks_menu;

	gzilla_menu_add (bookmarks_menu, "Add Bookmark", "<ctrl>A", bw, bw,
		bookmark_add_cmd_callback);
	gzilla_menu_add (bookmarks_menu, "View Bookmarks", NULL, bw, bw,
		bookmark_viewbm_cmd_callback);
	gzilla_menu_sep (bookmarks_menu);

	/* HELP MENU */
	help_menu = gzilla_menu_new (menubar, "Help", TRUE, bw);

	gzilla_menu_add (help_menu, "Gzilla Home", NULL, bw, bw,
		help_home_cmd_callback);
	gzilla_menu_add (help_menu, "Gzilla Manual", NULL, bw, bw,
		help_manual_cmd_callback);

	return menubar;
}
