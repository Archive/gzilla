/* This is a Gzw widget for displaying images. It is adapted directly
   from gzw_preview in the Gzw+ distribution, but will eventuall diverge
   from it in some important details.

   The main reason to implement images as Gzw widgets rather than
   embedded gzw_previews (as was the case in all Gzilla versions up to
   0.1.4) is to avoid the overhead of creating a new window for each
   embedded image. For pages with lots of small images, this overhead
   was significant.

   I have other reasons for diverging, as well. At first, this widget
   will only accept 24-bit color images. Eventually, it will also
   handle 8-bit indexed and 32-bit RGBA images as well. Both modes
   will allow transparency support, not feasible using plain
   gzw_preview. In addition, the 8-bit indexed mode will allow a
   substantial speed improvement on 16-bit visuals (what I currently
   run) - instead of converting each pixel from 8 to 24 bit color,
   then back to 16, it will precompute the colormap to go straight
   from 8 to 16 bits.

   So here goes! RLL

   We need to implement _new, _draw_row, and _size externally.

   Some more notes (2 Jan 1998 RLL):

   Apparently, the colormap allocation and dithering should be redone
   to use Gdk color contexts instead of what it's doing now. I've
   ignored the GTK_PREVIEW_INFO property altogether. Accept that this
   won't always work well on 8-bit displays.

   I'll want to move some of the color stuff anyway into the Gzw
   infrastructure (for example, to be able to set background colors
   and text colors). Perhaps this will be done at the same time that
   it's changed to color contexts.

   Consider this module to be in a state of flux until the colormap
   stuff is all worked out, alpha compositing is implemented, and the
   drawing functions are optimized.

   */
   

/* GZW - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. */

/* todo: trim */
#include <gtk/gtk.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>

/* This #include seems to conflict with glib.h's definition of MIN and
   MAX. Do we really need it? */
#if NEED_SYS_PARAM_H
#include <sys/param.h>
#endif

#include <netinet/in.h>
#include <gdk/gdkx.h>

#include "gzw.h"

#include "gzwimage.h"

/* Some static data. It is basically invalid until init is set. */
static gboolean init = FALSE;
static GzwImageInfo *image_info = NULL;
static GdkImage *static_image = NULL;
static gint install_cmap = FALSE;

#define IMAGE_SIZE            256
#define COLOR_COMPOSE(r,g,b)  (lookup_red[r] | lookup_green[g] | lookup_blue[b])
typedef void (*GzwTransferFunc) (guchar *dest, guchar *src, gint count);

#if 0
#define IMAGE_SIZE            256
#define IMAGE_CLASS(w)      GZW_IMAGE_CLASS (GZW_OBJECT (w)->klass)
#define COLOR_COMPOSE(r,g,b)  (lookup_red[r] | lookup_green[g] | lookup_blue[b])


typedef struct _GzwImageProp  GzwImageProp;
typedef void (*GzwTransferFunc) (guchar *dest, guchar *src, gint count);

struct _GzwImageProp
{
  guint16 ref_count;
  guint16 nred_shades;
  guint16 ngreen_shades;
  guint16 nblue_shades;
  guint16 ngray_shades;
};


static void   gzw_image_class_init    (GzwImageClass  *klass);
static void   gzw_image_init          (GzwImage       *image);
static void   gzw_image_destroy       (GzwObject        *object);
static void   gzw_image_realize       (GzwWidget        *widget);
static void   gzw_image_unrealize     (GzwWidget        *widget);
static gint   gzw_image_expose        (GzwWidget        *widget,
				         GdkEventExpose   *event);
static void   gzw_image_make_buffer   (GzwImage       *image);
static void   gzw_image_get_visuals   (GzwImageClass  *klass);
static void   gzw_image_get_cmaps     (GzwImageClass  *klass);
static void   gzw_image_dither_init   (GzwImageClass  *klass);
static void   gzw_fill_lookup_array     (gulong           *array,
					 int               depth,
					 int               shift,
					 int               prec);
static void   gzw_trim_cmap             (GzwImageClass  *klass);
static void   gzw_create_8_bit          (GzwImageClass  *klass);

static void   gzw_color_8               (guchar           *src,
				         guchar           *data,
				         gint              x,
				         gint              y,
				         gulong            width);
static void   gzw_color_16              (guchar           *src,
				         guchar           *data,
				         gulong            width);
static void   gzw_color_24              (guchar           *src,
				         guchar           *data,
				         gulong            width);
static void   gzw_grayscale_8           (guchar           *src,
				         guchar           *data,
				         gint              x,
				         gint              y,
				         gulong            width);
static void   gzw_grayscale_16          (guchar           *src,
				         guchar           *data,
				         gulong            width);
static void   gzw_grayscale_24          (guchar           *src,
				         guchar           *data,
				         gulong            width);

static gint   gzw_get_image_prop      (guint            *nred,
					 guint            *nblue,
					 guint            *ngreen,
					 guint            *ngray);
static void   gzw_set_image_prop      (guint             nred,
					 guint             ngreen,
					 guint             nblue,
					 guint             ngray);

/* transfer functions:
 *  destination byte order/source bpp/destination bpp
 */
static void   gzw_lsbmsb_1_1            (guchar           *dest,
					 guchar           *src,
					 gint              count);
static void   gzw_lsb_2_2               (guchar           *dest,
					 guchar           *src,
					 gint              count);
static void   gzw_msb_2_2               (guchar           *dest,
					 guchar           *src,
					 gint              count);
static void   gzw_lsb_3_3               (guchar           *dest,
					 guchar           *src,
					 gint              count);
static void   gzw_msb_3_3               (guchar           *dest,
					 guchar           *src,
					 gint              count);
static void   gzw_lsb_3_4               (guchar           *dest,
					 guchar           *src,
					 gint              count);
static void   gzw_msb_3_4               (guchar           *dest,
					 guchar           *src,
					 gint              count);

static GzwImageInfo *image_info = NULL;
static gint install_cmap = FALSE;


  if (image_info)
    klass->info = *image_info;
  else
    {
      klass->info.visual = NULL;
      klass->info.cmap = NULL;

      klass->info.color_pixels = NULL;
      klass->info.gray_pixels = NULL;
      klass->info.reserved_pixels = NULL;

      klass->info.lookup_red = NULL;
      klass->info.lookup_green = NULL;
      klass->info.lookup_blue = NULL;

      klass->info.dither_red = NULL;
      klass->info.dither_green = NULL;
      klass->info.dither_blue = NULL;
      klass->info.dither_gray = NULL;
      klass->info.dither_matrix = NULL;

      klass->info.nred_shades = 6;
      klass->info.ngreen_shades = 6;
      klass->info.nblue_shades = 4;
      klass->info.ngray_shades = 24;
      klass->info.nreserved = 0;

      klass->info.bpp = 0;
      klass->info.cmap_alloced = FALSE;
      klass->info.gamma = 1.0;
    }

  klass->image = NULL;

  gzw_image_get_visuals (klass);
  gzw_image_get_cmaps (klass);
  gzw_image_dither_init (klass);
}

static void
gzw_image_init (GzwImage *image)
{
  GZW_WIDGET_SET_FLAGS (image, GZW_BASIC);

  image->buffer = NULL;
  image->buffer_width = 0;
  image->buffer_height = 0;
  image->expand = FALSE;
}

void
gzw_image_uninit ()
{
  GzwImageProp *prop;
  GdkAtom property;

  if (image_class && !install_cmap &&
      (image_info->visual->type != GDK_VISUAL_TRUE_COLOR) &&
      (image_info->visual->type != GDK_VISUAL_DIRECT_COLOR))
    {
      property = gdk_atom_intern ("GZW_IMAGE_INFO", FALSE);

      if (gdk_property_get (NULL, property, property,
			    0, sizeof (GzwImageProp), FALSE,
			    NULL, NULL, NULL, (guchar**) &prop))
	{
	  prop->ref_count = ntohs (prop->ref_count) - 1;
	  if (prop->ref_count == 0)
	    {
	      gdk_property_delete (NULL, property);
	    }
	  else
	    {
	      prop->ref_count = htons (prop->ref_count);
	      gdk_property_change (NULL, property, property, 16,
				   GDK_PROP_MODE_REPLACE,
				   (guchar*) prop, 5);
	    }
	}
    }
}

#endif

static void
gzw_image_size_nego_y (Gzw *gzw, GzwRect *allocation)
{
  gzw->allocation = *allocation;
}

static void
gzw_lsbmsb_1_1 (guchar *dest,
		guchar *src,
		gint    count)
{
  memcpy (dest, src, count);
}

static void
gzw_lsb_2_2 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  memcpy (dest, src, count * 2);
}

static void
gzw_msb_2_2 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  while (count--)
    {
      dest[0] = src[1];
      dest[1] = src[0];
      dest += 2;
      src += 2;
    }
}

static void
gzw_lsb_3_3 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  memcpy (dest, src, count * 3);
}

static void
gzw_msb_3_3 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  while (count--)
    {
      dest[0] = src[2];
      dest[1] = src[1];
      dest[2] = src[0];
      dest += 3;
      src += 3;
    }
}

static void
gzw_lsb_3_4 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  while (count--)
    {
      dest[0] = src[0];
      dest[1] = src[1];
      dest[2] = src[2];
      dest += 4;
      src += 3;
    }
}

static void
gzw_msb_3_4 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  while (count--)
    {
      dest[1] = src[2];
      dest[2] = src[1];
      dest[3] = src[0];
      dest += 4;
      src += 3;
    }
}

/* destx, desty are coordinates where image is drawn relative to X
   window. */
static void
gzw_image_put (GzwImage   *image,
	       GdkWindow    *window,
	       GdkGC        *gc,
	       gint          srcx,
	       gint          srcy,
	       gint          destx,
	       gint          desty,
	       gint          width,
	       gint          height)
{
  GdkRectangle r1, r2, r3;
  GzwTransferFunc transfer_func;
  guchar *image_mem;
  guchar *src, *dest;
  gint x, xe, x2;
  gint y, ye, y2;
  guint dest_rowstride;
  guint src_bpp;
  guint dest_bpp;
  gint i;

  g_return_if_fail (image != NULL);
  g_return_if_fail (window != NULL);

  if (!image->buffer)
    return;

  r1.x = srcx;
  r1.y = srcy;
  r1.width = image->buffer_width;
  r1.height = image->buffer_height;

  r2.x = destx;
  r2.y = desty;
  r2.width = width;
  r2.height = height;

  if (!gdk_rectangle_intersect (&r1, &r2, &r3))
    return;

#ifdef VERBOSE
  g_print ("r1 = (%d, %d) (%d x %d)\n", r1.x, r1.y, r1.width, r1.height);
  g_print ("r2 = (%d, %d) (%d x %d)\n", r2.x, r2.y, r2.width, r2.height);
  g_print ("r3 = (%d, %d) (%d x %d)\n", r3.x, r3.y, r3.width, r3.height);
#endif

  x2 = r3.x + r3.width;
  y2 = r3.y + r3.height;

  if (!static_image)
    static_image = gdk_image_new (GDK_IMAGE_FASTEST,
					image_info->visual,
					IMAGE_SIZE, IMAGE_SIZE);
  src_bpp = image_info->bpp;

  image_mem = static_image->mem;
  dest_bpp = static_image->bpp;
  dest_rowstride = static_image->bpl;

  transfer_func = NULL;

  switch (dest_bpp)
    {
    case 1:
      switch (src_bpp)
	{
	case 1:
	  transfer_func = gzw_lsbmsb_1_1;
	  break;
	}
      break;
    case 2:
      switch (src_bpp)
	{
	case 2:
	  if (static_image->byte_order == GDK_MSB_FIRST)
	    transfer_func = gzw_msb_2_2;
	  else
	    transfer_func = gzw_lsb_2_2;
	  break;
	case 3:
	  break;
	}
      break;
    case 3:
      switch (src_bpp)
	{
	case 3:
	  if (static_image->byte_order == GDK_MSB_FIRST)
	    transfer_func = gzw_msb_3_3;
	  else
	    transfer_func = gzw_lsb_3_3;
	  break;
	}
      break;
    case 4:
      switch (src_bpp)
	{
	case 3:
	  if (static_image->byte_order == GDK_MSB_FIRST)
	    transfer_func = gzw_msb_3_4;
	  else
	    transfer_func = gzw_lsb_3_4;
	  break;
	}
      break;
    }

  if (!transfer_func)
    {
      g_warning ("unsupported byte order/src bpp/dest bpp combination: %s:%d:%d",
		 (static_image->byte_order == GDK_MSB_FIRST) ? "msb" : "lsb", src_bpp, dest_bpp);
      return;
    }

  for (y = r3.y; y < y2; y += IMAGE_SIZE)
    {
      for (x = r3.x; x < x2; x += IMAGE_SIZE)
	{
	  xe = x + IMAGE_SIZE;
	  if (xe > x2)
	    xe = x2;

	  ye = y + IMAGE_SIZE;
	  if (ye > y2)
	    ye = y2;

	  for (i = y; i < ye; i++)
	    {
	      src = image->buffer + (((gulong) (i - r1.y) * (gulong) image->buffer_width) +
				       (x - r1.x)) * (gulong) src_bpp;
	      dest = image_mem + ((gulong) (i - y) * dest_rowstride);

	      if (xe > x)
		(* transfer_func) (dest, src, xe - x);
	    }

	  gdk_draw_image (window, gc,
			  static_image, 0, 0, x, y,
			  xe - x, ye - y);
	  gdk_flush ();
	}
    }
}

static void
gzw_image_paint (Gzw *gzw, GzwRect *rect, GzwPaint *paint)
{
  GtkWidget *widget;
  GzwContainer *container;
  GzwRect image_rect, paint_rect;
  gint x, y;

#ifdef VERBOSE
  g_print ("gzw_image_paint (%d, %d) - (%d, %d)",
	   rect->x0, rect->y0, rect->x1, rect->y1);
#endif
  container = gzw_find_container (gzw);
  if (container != NULL)
    {
#ifdef VERBOSE
      g_print (", visible = (%d, %d) - (%d, %d)",
	       container->visible.x0, container->visible.y0,
	       container->visible.x1, container->visible.y1);
#endif
      widget = container->widget;

      /* Even if the requisition and allocation don't match, it's
	 still possible to optimize this call - we only need to
	 paint to screen the area outside the image. */
      if (gzw->req_width_min !=
	  gzw->allocation.x1 - gzw->allocation.x0 ||
	  gzw->req_height !=
	  gzw->allocation.y1 - gzw->allocation.y0)
	gzw_paint_to_screen (widget, container, rect, paint);

      x = gzw->allocation.x0 + container->x_offset;
      y = gzw->allocation.y0 + container->y_offset;
      image_rect.x0 = gzw->allocation.x0;
      image_rect.y0 = gzw->allocation.y0;
      image_rect.x1 = gzw->allocation.x0 + gzw->req_width_min;
      image_rect.y1 = gzw->allocation.y0 + gzw->req_height;
      gzw_rect_intersect (&paint_rect, &image_rect, rect);

      if (!gzw_rect_empty (&paint_rect))
	gzw_image_put ((GzwImage *)gzw,
		       widget->window,
		       widget->style->fg_gc[widget->state],
		       image_rect.x0,
		       image_rect.y0,
		       paint_rect.x0,
		       paint_rect.y0,
		       paint_rect.x1 - paint_rect.x0,
		       paint_rect.y1 - paint_rect.y0);
      gzw_paint_finish_screen (paint);
    }
#ifdef VERBOSE
  g_print ("\n");
#endif
}

static void
gzw_image_destroy (Gzw *gzw)
{
  GzwImage *gzw_image;

  gzw_image = (GzwImage *)gzw;
  if (gzw_image->buffer != NULL)
    g_free (gzw_image->buffer);
  g_free (gzw);
}

static const GzwClass gzw_image_class =
{
  gzw_null_size_nego_x,
  gzw_image_size_nego_y,
  gzw_image_paint,
  gzw_null_handle_event,
  gzw_null_gtk_foreach,
  gzw_image_destroy,
  gzw_null_request_resize
};

static void
gzw_fill_lookup_array (gulong *array,
		       int     depth,
		       int     shift,
		       int     prec)
{
  double one_over_gamma;
  double ind;
  int val;
  int i;

  if (image_info->gamma != 0.0)
    one_over_gamma = 1.0 / image_info->gamma;
  else
    one_over_gamma = 1.0;

  for (i = 0; i < 256; i++)
    {
      if (one_over_gamma == 1.0)
        array[i] = ((i >> prec) << shift);
      else
        {
          ind = (double) i / 255.0;
          val = (int) (255 * pow (ind, one_over_gamma));
          array[i] = ((val >> prec) << shift);
        }
    }
}

static void
gzw_image_get_visuals (void)
{
  static GdkVisualType types[] =
  {
    GDK_VISUAL_TRUE_COLOR,
    GDK_VISUAL_DIRECT_COLOR,
    GDK_VISUAL_TRUE_COLOR,
    GDK_VISUAL_DIRECT_COLOR,
    GDK_VISUAL_TRUE_COLOR,
    GDK_VISUAL_DIRECT_COLOR,
    GDK_VISUAL_TRUE_COLOR,
    GDK_VISUAL_DIRECT_COLOR,
    GDK_VISUAL_PSEUDO_COLOR
  };
  static gint depths[] = { 24, 24, 32, 32, 16, 16, 15, 15, 8 };
  static gint nvisual_types = sizeof (types) / sizeof (types[0]);

  int i;

  if (!image_info->visual)
    for (i = 0; i < nvisual_types; i++)
      if ((image_info->visual = gdk_visual_get_best_with_both (depths[i], types[i])))
	{
	  if ((image_info->visual->type == GDK_VISUAL_TRUE_COLOR) ||
	      (image_info->visual->type == GDK_VISUAL_DIRECT_COLOR))
	    {
	      image_info->lookup_red = g_new (gulong, 256);
	      image_info->lookup_green = g_new (gulong, 256);
	      image_info->lookup_blue = g_new (gulong, 256);

	      gzw_fill_lookup_array (image_info->lookup_red,
				     image_info->visual->depth,
				     image_info->visual->red_shift,
				     8 - image_info->visual->red_prec);
	      gzw_fill_lookup_array (image_info->lookup_green,
				     image_info->visual->depth,
				     image_info->visual->green_shift,
				     8 - image_info->visual->green_prec);
	      gzw_fill_lookup_array (image_info->lookup_blue,
				     image_info->visual->depth,
				     image_info->visual->blue_shift,
				     8 - image_info->visual->blue_prec);
	    }
	  break;
	}

  if (!image_info->visual)
    {
      g_warning ("unable to find a suitable visual for color image display.\n");
      return;
    }

  switch (image_info->visual->depth)
    {
    case 8:
      image_info->bpp = 1;
      break;
    case 15:
    case 16:
      image_info->bpp = 2;
      break;
    case 24:
    case 32:
      image_info->bpp = 3;
      break;
    }
}

static void
gzw_trim_cmap (void)
{
  gulong pixels[256];
  guint nred;
  guint ngreen;
  guint nblue;
  guint ngray;
  guint nreserved;
  guint total;
  guint tmp;
  gint success;

  nred = image_info->nred_shades;
  ngreen = image_info->ngreen_shades;
  nblue = image_info->nblue_shades;
  ngray = image_info->ngray_shades;
  nreserved = image_info->nreserved;

  success = FALSE;
  while (!success)
    {
      total = nred * ngreen * nblue + ngray + nreserved;

      if (total <= 256)
	{
	  if ((nred < 2) || (ngreen < 2) || (nblue < 2) || (ngray < 2))
	    success = TRUE;
	  else
	    {
	      success = gdk_colors_alloc (image_info->cmap, 0, NULL, 0, pixels, total);
	      if (success)
		{
		  if (nreserved > 0)
		    {
		      image_info->reserved_pixels = g_new (gulong, nreserved);
		      memcpy (image_info->reserved_pixels, pixels, sizeof (gulong) * nreserved);
		      gdk_colors_free (image_info->cmap, &pixels[nreserved],
				       total - nreserved, 0);
		    }
		  else
		    {
		      gdk_colors_free (image_info->cmap, pixels, total, 0);
		    }
		}
	    }
	}

      if (!success)
	{
	  if ((nblue >= nred) && (nblue >= ngreen))
	    nblue = nblue - 1;
	  else if ((nred >= ngreen) && (nred >= nblue))
	    nred = nred - 1;
	  else
	    {
	      tmp = log (ngray) / log (2);

	      if (ngreen >= tmp)
		ngreen = ngreen - 1;
	      else
		ngray -= 1;
	    }
	}
    }

  if ((nred < 2) || (ngreen < 2) || (nblue < 2) || (ngray < 2))
    {
      g_print ("Unable to allocate sufficient colormap entries.\n");
      g_print ("Try exiting other color intensive applications.\n");
      return;
    }

  /*  If any of the shade values has changed, issue a warning  */
  if ((nred != image_info->nred_shades) ||
      (ngreen != image_info->ngreen_shades) ||
      (nblue != image_info->nblue_shades) ||
      (ngray != image_info->ngray_shades))
    {
      g_print ("Not enough colors to satisfy requested color cube.\n");
      g_print ("Reduced color cube shades from\n");
      g_print ("[%d of Red, %d of Green, %d of Blue, %d of Gray] ==> [%d of Red, %d of Green, %d of Blue, %d of Gray]\n",
               image_info->nred_shades, image_info->ngreen_shades,
	       image_info->nblue_shades, image_info->ngray_shades,
	       nred, ngreen, nblue, ngray);
    }

  image_info->nred_shades = nred;
  image_info->ngreen_shades = ngreen;
  image_info->nblue_shades = nblue;
  image_info->ngray_shades = ngray;
}

static void
gzw_create_8_bit (void)
{
  unsigned int r, g, b;
  unsigned int rv, gv, bv;
  unsigned int dr, dg, db, dgray;
  GdkColor color;
  gulong *pixels;
  double one_over_gamma;
  int i;

  if (!image_info->color_pixels)
    image_info->color_pixels = g_new (gulong, 256);

  if (!image_info->gray_pixels)
    image_info->gray_pixels = g_new (gulong, 256);

  if (image_info->gamma != 0.0)
    one_over_gamma = 1.0 / image_info->gamma;
  else
    one_over_gamma = 1.0;

  dr = image_info->nred_shades - 1;
  dg = image_info->ngreen_shades - 1;
  db = image_info->nblue_shades - 1;
  dgray = image_info->ngray_shades - 1;

  pixels = image_info->color_pixels;

  for (r = 0, i = 0; r <= dr; r++)
    for (g = 0; g <= dg; g++)
      for (b = 0; b <= db; b++, i++)
        {
          rv = (unsigned int) ((r * image_info->visual->colormap_size) / dr);
          gv = (unsigned int) ((g * image_info->visual->colormap_size) / dg);
          bv = (unsigned int) ((b * image_info->visual->colormap_size) / db);
          color.red = ((int) (255 * pow ((double) rv / 256.0, one_over_gamma))) * 257;
          color.green = ((int) (255 * pow ((double) gv / 256.0, one_over_gamma))) * 257;
          color.blue = ((int) (255 * pow ((double) bv / 256.0, one_over_gamma))) * 257;

	  if (!gdk_color_alloc (image_info->cmap, &color))
	    {
	      g_error ("could not initialize 8-bit combined colormap");
	      return;
	    }

	  pixels[i] = color.pixel;
        }

  pixels = image_info->gray_pixels;

  for (i = 0; i < (int) image_info->ngray_shades; i++)
    {
      color.red = (i * image_info->visual->colormap_size) / dgray;
      color.red = ((int) (255 * pow ((double) color.red / 256.0, one_over_gamma))) * 257;
      color.green = color.red;
      color.blue = color.red;

      if (!gdk_color_alloc (image_info->cmap, &color))
	{
	  g_error ("could not initialize 8-bit combined colormap");
	  return;
	}

      pixels[i] = color.pixel;
    }
}

static void
gzw_image_get_cmaps (void)
{
  g_return_if_fail (image_info->visual != NULL);

  if ((image_info->visual->type != GDK_VISUAL_TRUE_COLOR) &&
      (image_info->visual->type != GDK_VISUAL_DIRECT_COLOR))
    {
      if (install_cmap)
	{
	  image_info->cmap = gdk_colormap_new (image_info->visual, FALSE);
	  image_info->cmap_alloced = install_cmap;

	  gzw_trim_cmap ();
	  gzw_create_8_bit ();
	}
      else
	{
	  guint nred;
	  guint ngreen;
	  guint nblue;
	  guint ngray;
	  gint set_prop;

	  image_info->cmap = gdk_colormap_get_system ();

#if 0
	  set_prop = TRUE;
	  if (gzw_get_image_prop (&nred, &ngreen, &nblue, &ngray))
	    {
	      set_prop = FALSE;

	      image_info->nred_shades = nred;
	      image_info->ngreen_shades = ngreen;
	      image_info->nblue_shades = nblue;
	      image_info->ngray_shades = ngray;

	      if (image_info->nreserved)
		{
		  image_info->reserved_pixels = g_new (gulong, image_info->nreserved);
		  if (!gdk_colors_alloc (image_info->cmap, 0, NULL, 0,
					 image_info->reserved_pixels,
					 image_info->nreserved))
		    {
		      g_free (image_info->reserved_pixels);
		      image_info->reserved_pixels = NULL;
		    }
		}
	    }
	  else
	    {
	      gzw_trim_cmap ();
	    }
#endif

	  gzw_create_8_bit ();

#if 0
	  if (set_prop)
	    gzw_set_image_prop (image_info->nred_shades,
				  image_info->ngreen_shades,
				  image_info->nblue_shades,
				  image_info->ngray_shades);
#endif
	}
    }
  else
    {
      if (image_info->visual == gdk_visual_get_system ())
	image_info->cmap = gdk_colormap_get_system ();
      else
	image_info->cmap = gdk_colormap_new (image_info->visual, FALSE);
      image_info->cmap_alloced = TRUE;

      image_info->nred_shades = 0;
      image_info->ngreen_shades = 0;
      image_info->nblue_shades = 0;
      image_info->ngray_shades = 0;
    }
}

static void
gzw_image_dither_init (void)
{
  int i, j, k;
  unsigned char low_shade, high_shade;
  unsigned short index;
  long red_mult, green_mult;
  double red_matrix_width;
  double green_matrix_width;
  double blue_matrix_width;
  double gray_matrix_width;
  double red_colors_per_shade;
  double green_colors_per_shade;
  double blue_colors_per_shade;
  double gray_colors_per_shade;
  gulong *gray_pixels;
  gint shades_r, shades_g, shades_b, shades_gray;
  GzwDitherInfo *red_ordered_dither;
  GzwDitherInfo *green_ordered_dither;
  GzwDitherInfo *blue_ordered_dither;
  GzwDitherInfo *gray_ordered_dither;
  guchar ***dither_matrix;
  guchar DM[8][8] =
  {
    { 0,  32, 8,  40, 2,  34, 10, 42 },
    { 48, 16, 56, 24, 50, 18, 58, 26 },
    { 12, 44, 4,  36, 14, 46, 6,  38 },
    { 60, 28, 52, 20, 62, 30, 54, 22 },
    { 3,  35, 11, 43, 1,  33, 9,  41 },
    { 51, 19, 59, 27, 49, 17, 57, 25 },
    { 15, 47, 7,  39, 13, 45, 5,  37 },
    { 63, 31, 55, 23, 61, 29, 53, 21 }
  };

  if (image_info->visual->type != GDK_VISUAL_PSEUDO_COLOR)
    return;

  shades_r = image_info->nred_shades;
  shades_g = image_info->ngreen_shades;
  shades_b = image_info->nblue_shades;
  shades_gray = image_info->ngray_shades;

  red_mult = shades_g * shades_b;
  green_mult = shades_b;

  red_colors_per_shade = 255.0 / (shades_r - 1);
  red_matrix_width = red_colors_per_shade / 64;

  green_colors_per_shade = 255.0 / (shades_g - 1);
  green_matrix_width = green_colors_per_shade / 64;

  blue_colors_per_shade = 255.0 / (shades_b - 1);
  blue_matrix_width = blue_colors_per_shade / 64;

  gray_colors_per_shade = 255.0 / (shades_gray - 1);
  gray_matrix_width = gray_colors_per_shade / 64;

  /*  alloc the ordered dither arrays for accelerated dithering  */

  image_info->dither_red = g_new (GzwDitherInfo, 256);
  image_info->dither_green = g_new (GzwDitherInfo, 256);
  image_info->dither_blue = g_new (GzwDitherInfo, 256);
  image_info->dither_gray = g_new (GzwDitherInfo, 256);

  red_ordered_dither = image_info->dither_red;
  green_ordered_dither = image_info->dither_green;
  blue_ordered_dither = image_info->dither_blue;
  gray_ordered_dither = image_info->dither_gray;

  dither_matrix = g_new (guchar**, 8);
  for (i = 0; i < 8; i++)
    {
      dither_matrix[i] = g_new (guchar*, 8);
      for (j = 0; j < 8; j++)
	dither_matrix[i][j] = g_new (guchar, 65);
    }

  image_info->dither_matrix = dither_matrix;

  /*  setup the ordered_dither_matrices  */

  for (i = 0; i < 8; i++)
    for (j = 0; j < 8; j++)
      for (k = 0; k <= 64; k++)
	dither_matrix[i][j][k] = (DM[i][j] < k) ? 1 : 0;

  /*  setup arrays containing three bytes of information for red, green, & blue  */
  /*  the arrays contain :
   *    1st byte:    low end shade value
   *    2nd byte:    high end shade value
   *    3rd & 4th bytes:    ordered dither matrix index
   */

  gray_pixels = image_info->gray_pixels;

  for (i = 0; i < 256; i++)
    {

      /*  setup the red information  */
      {
	low_shade = (unsigned char) (i / red_colors_per_shade);
	if (low_shade == (shades_r - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short)
	  (((double) i - low_shade * red_colors_per_shade) /
	   red_matrix_width);

	low_shade *= red_mult;
	high_shade *= red_mult;

	red_ordered_dither[i].s[1] = index;
	red_ordered_dither[i].c[0] = low_shade;
	red_ordered_dither[i].c[1] = high_shade;
      }


      /*  setup the green information  */
      {
	low_shade = (unsigned char) (i / green_colors_per_shade);
	if (low_shade == (shades_g - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short)
	  (((double) i - low_shade * green_colors_per_shade) /
	   green_matrix_width);

	low_shade *= green_mult;
	high_shade *= green_mult;

	green_ordered_dither[i].s[1] = index;
	green_ordered_dither[i].c[0] = low_shade;
	green_ordered_dither[i].c[1] = high_shade;
      }


      /*  setup the blue information  */
      {
	low_shade = (unsigned char) (i / blue_colors_per_shade);
	if (low_shade == (shades_b - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short)
	  (((double) i - low_shade * blue_colors_per_shade) /
	   blue_matrix_width);

	blue_ordered_dither[i].s[1] = index;
	blue_ordered_dither[i].c[0] = low_shade;
	blue_ordered_dither[i].c[1] = high_shade;
      }


      /*  setup the gray information */
      {
	low_shade = (unsigned char) (i / gray_colors_per_shade);
	if (low_shade == (shades_gray - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short)
	  (((double) i - low_shade * gray_colors_per_shade) /
	   gray_matrix_width);

	gray_ordered_dither[i].s[1] = index;
	gray_ordered_dither[i].c[0] = gray_pixels[low_shade];
	gray_ordered_dither[i].c[1] = gray_pixels[high_shade];
      }
    }
}

static void
gzw_image_init (void)
{

  image_info = g_new0 (GzwImageInfo, 1);

  image_info->visual = NULL;
  image_info->cmap = NULL;

  image_info->color_pixels = NULL;
  image_info->gray_pixels = NULL;
  image_info->reserved_pixels = NULL;

  image_info->lookup_red = NULL;
  image_info->lookup_green = NULL;
  image_info->lookup_blue = NULL;

  image_info->dither_red = NULL;
  image_info->dither_green = NULL;
  image_info->dither_blue = NULL;
  image_info->dither_gray = NULL;
  image_info->dither_matrix = NULL;

  image_info->nred_shades = 6;
  image_info->ngreen_shades = 6;
  image_info->nblue_shades = 4;
  image_info->ngray_shades = 24;
  image_info->nreserved = 0;

  image_info->bpp = 0;
  image_info->cmap_alloced = FALSE;
  image_info->gamma = 1.0;

  gzw_image_get_visuals ();
  gzw_image_get_cmaps ();
  gzw_image_dither_init ();

  init = TRUE;
}

Gzw *
gzw_image_new (GzwImageType type)
{
  GzwImage *image;
  GzwRect null_rect = {0, 0, 0, 0};

  if (!init)
    gzw_image_init ();

  image = g_new (GzwImage, 1);
  image->gzw.klass = &gzw_image_class;
  image->gzw.allocation = null_rect;
  image->gzw.req_width_min = 0;
  image->gzw.req_width_max = 0;
  image->gzw.req_height = 0;
  image->gzw.req_ascent = 0;

  image->gzw.flags = 0;
  image->gzw.parent = NULL;
  image->gzw.container = NULL;

  image->type = type;

  image->buffer = NULL;

  return (Gzw *) image;
}

void
gzw_image_size (GzwImage *image,
		gint     width,
		gint     height)
{
  g_return_if_fail (image != NULL);

  if ((width != image->gzw.req_width_min) ||
      (height != image->gzw.req_height))
    {
      image->gzw.req_width_min = width;
      image->gzw.req_width_max = width;
      image->gzw.req_height = height;

      if (image->buffer)
	g_free (image->buffer);
      image->buffer = NULL;
      
      gzw_request_parent_resize (&image->gzw);
    }
}

static void
gzw_image_make_buffer (GzwImage *image)
{
  gint width;
  gint height;

  width = image->gzw.req_width_min;
  height = image->gzw.req_height;

  if (!image->buffer ||
      (image->buffer_width != width) ||
      (image->buffer_height != height))
    {
      if (image->buffer)
	g_free (image->buffer);

      image->buffer_width = width;
      image->buffer_height = height;

      image->buffer = g_new0 (guchar,
			      image->buffer_width *
			      image->buffer_height *
			      image_info->bpp);
    }
}

static void
gzw_color_8 (guchar *src,
	     guchar *dest,
	     gint    x,
	     gint    y,
	     gulong  width)
{
  gulong *colors;
  GzwDitherInfo *dither_red;
  GzwDitherInfo *dither_green;
  GzwDitherInfo *dither_blue;
  GzwDitherInfo r, g, b;
  guchar **dither_matrix;
  guchar *matrix;

  colors = image_info->color_pixels;
  dither_red = image_info->dither_red;
  dither_green = image_info->dither_green;
  dither_blue = image_info->dither_blue;
  dither_matrix = image_info->dither_matrix[y & 0x7];

  while (width--)
    {
      r = dither_red[src[0]];
      g = dither_green[src[1]];
      b = dither_blue[src[2]];
      src += 3;

      matrix = dither_matrix[x++ & 0x7];
      *dest++ = colors[(r.c[matrix[r.s[1]]] +
			g.c[matrix[g.s[1]]] +
			b.c[matrix[b.s[1]]])];
    }
}

static void
gzw_color_16 (guchar *src,
	      guchar *dest,
	      gulong  width)
{
  gulong *lookup_red;
  gulong *lookup_green;
  gulong *lookup_blue;
  gulong val;

  lookup_red = image_info->lookup_red;
  lookup_green = image_info->lookup_green;
  lookup_blue = image_info->lookup_blue;

  while (width--)
    {
      val = COLOR_COMPOSE (src[0], src[1], src[2]);
      dest[0] = val;
      dest[1] = val >> 8;
      dest += 2;
      src += 3;
    }
}

static void
gzw_color_24 (guchar *src,
	      guchar *dest,
	      gulong  width)
{
  gulong *lookup_red;
  gulong *lookup_green;
  gulong *lookup_blue;
  gulong val;

  lookup_red = image_info->lookup_red;
  lookup_green = image_info->lookup_green;
  lookup_blue = image_info->lookup_blue;

  while (width--)
    {
      val = COLOR_COMPOSE (src[0], src[1], src[2]);
      dest[0] = val;
      dest[1] = val >> 8;
      dest[2] = val >> 16;
      dest += 3;
      src += 3;
    }
}

void
gzw_image_draw_row (GzwImage *image, guchar *data, gint x, gint y, gint w)
{
  guchar *dest;
  GzwRect rect;

  g_return_if_fail (image != NULL);
  g_return_if_fail (data != NULL);

  if ((w <= 0) || (y < 0))
    return;

  if (!image->buffer)
    {
      gzw_image_make_buffer (image);
      rect.x0 = image->gzw.allocation.x0;
      rect.x1 = image->gzw.allocation.x0 + image->gzw.req_width_min;
      rect.y0 = image->gzw.allocation.y0;
      rect.y1 = image->gzw.allocation.y0 + image->gzw.req_height;
      gzw_request_paint (&image->gzw, &rect);
    }

  switch (image_info->visual->depth)
    {
    case 8:
      dest = image->buffer + ((gulong) y * (gulong) image->buffer_width + (gulong) x);
      gzw_color_8 (data, dest, x, y, w);
      break;
    case 15:
    case 16:
      dest = image->buffer + ((gulong) y * (gulong) image->buffer_width + (gulong) x) * 2;
      gzw_color_16 (data, dest, w);
      break;
    case 24:
    case 32:
      dest = image->buffer + ((gulong) y * (gulong) image->buffer_width + (gulong) x) * 3;
      gzw_color_24 (data, dest, w);
      break;
    }
  rect.x0 = image->gzw.allocation.x0 + x;
  rect.x1 = image->gzw.allocation.x0 + x + w;
  rect.y0 = image->gzw.allocation.y0 + y;
  rect.y1 = image->gzw.allocation.y0 + y + 1;
  gzw_request_paint (&image->gzw, &rect);
}

#if 0
void
gzw_image_put (GzwImage   *image,
		 GdkWindow    *window,
		 GdkGC        *gc,
		 gint          srcx,
		 gint          srcy,
		 gint          destx,
		 gint          desty,
		 gint          width,
		 gint          height)
{
  GzwWidget *widget;
  GdkImage *image;
  GdkRectangle r1, r2, r3;
  GzwTransferFunc transfer_func;
  guchar *image_mem;
  guchar *src, *dest;
  gint x, xe, x2;
  gint y, ye, y2;
  guint dest_rowstride;
  guint src_bpp;
  guint dest_bpp;
  gint i;

  g_return_if_fail (image != NULL);
  g_return_if_fail (GZW_IS_IMAGE (image));
  g_return_if_fail (window != NULL);

  if (!image->buffer)
    return;

  widget = GZW_WIDGET (image);

  r1.x = srcx;
  r1.y = srcy;
  r1.width = image->buffer_width;
  r1.height = image->buffer_height;

  r2.x = destx;
  r2.y = desty;
  r2.width = width;
  r2.height = height;

  if (!gdk_rectangle_intersect (&r1, &r2, &r3))
    return;

  x2 = r3.x + r3.width;
  y2 = r3.y + r3.height;

  if (!image_class->image)
    image_class->image = gdk_image_new (GDK_IMAGE_FASTEST,
					  image_info->visual,
					  IMAGE_SIZE, IMAGE_SIZE);
  image = image_class->image;
  src_bpp = image_info->bpp;

  image_mem = image->mem;
  dest_bpp = image->bpp;
  dest_rowstride = image->bpl;

  transfer_func = NULL;

  switch (dest_bpp)
    {
    case 1:
      switch (src_bpp)
	{
	case 1:
	  transfer_func = gzw_lsbmsb_1_1;
	  break;
	}
      break;
    case 2:
      switch (src_bpp)
	{
	case 2:
	  if (image->byte_order == GDK_MSB_FIRST)
	    transfer_func = gzw_msb_2_2;
	  else
	    transfer_func = gzw_lsb_2_2;
	  break;
	case 3:
	  break;
	}
      break;
    case 3:
      switch (src_bpp)
	{
	case 3:
	  if (image->byte_order == GDK_MSB_FIRST)
	    transfer_func = gzw_msb_3_3;
	  else
	    transfer_func = gzw_lsb_3_3;
	  break;
	}
      break;
    case 4:
      switch (src_bpp)
	{
	case 3:
	  if (image->byte_order == GDK_MSB_FIRST)
	    transfer_func = gzw_msb_3_4;
	  else
	    transfer_func = gzw_lsb_3_4;
	  break;
	}
      break;
    }

  if (!transfer_func)
    {
      g_warning ("unsupported byte order/src bpp/dest bpp combination: %s:%d:%d",
		 (image->byte_order == GDK_MSB_FIRST) ? "msb" : "lsb", src_bpp, dest_bpp);
      return;
    }

  for (y = r3.y; y < y2; y += IMAGE_SIZE)
    {
      for (x = r3.x; x < x2; x += IMAGE_SIZE)
	{
	  xe = x + IMAGE_SIZE;
	  if (xe > x2)
	    xe = x2;

	  ye = y + IMAGE_SIZE;
	  if (ye > y2)
	    ye = y2;

	  for (i = y; i < ye; i++)
	    {
	      src = image->buffer + (((gulong) (i - r1.y) * (gulong) image->buffer_width) +
				       (x - r1.x)) * (gulong) src_bpp;
	      dest = image_mem + ((gulong) (i - y) * dest_rowstride);

	      if (xe > x)
		(* transfer_func) (dest, src, xe - x);
	    }

	  gdk_draw_image (window, gc,
			  image, 0, 0, x, y,
			  xe - x, ye - y);
	  gdk_flush ();
	}
    }
}

void
gzw_image_put_row (GzwImage *image,
		     guchar     *src,
		     guchar     *dest,
		     gint        x,
		     gint        y,
		     gint        w)
{
  g_return_if_fail (image != NULL);
  g_return_if_fail (GZW_IS_IMAGE (image));
  g_return_if_fail (src != NULL);
  g_return_if_fail (dest != NULL);

  switch (image->type)
    {
    case GZW_IMAGE_COLOR:
      switch (image_info->visual->depth)
	{
	case 8:
	  gzw_color_8 (src, dest, x, y, w);
	  break;
	case 15:
	case 16:
	  gzw_color_16 (src, dest, w);
	  break;
	case 24:
	case 32:
	  gzw_color_24 (src, dest, w);
	  break;
	}
      break;
    case GZW_IMAGE_GRAYSCALE:
      switch (image_info->visual->depth)
	{
	case 8:
	  gzw_grayscale_8 (src, dest, x, y, w);
	  break;
	case 15:
	case 16:
	  gzw_grayscale_16 (src, dest, w);
	  break;
	case 24:
	case 32:
	  gzw_grayscale_24 (src, dest, w);
	  break;
	}
      break;
    }
}

void
gzw_image_draw_row (GzwImage *image,
		      guchar     *data,
		      gint        x,
		      gint        y,
		      gint        w)
{
  guchar *dest;

  g_return_if_fail (image != NULL);
  g_return_if_fail (GZW_IS_IMAGE (image));
  g_return_if_fail (data != NULL);

  if ((w <= 0) || (y < 0))
    return;

  g_return_if_fail (data != NULL);

  gzw_image_make_buffer (image);

  if (y >= image->buffer_height)
    return;

  switch (image->type)
    {
    case GZW_IMAGE_COLOR:
      switch (image_info->visual->depth)
	{
	case 8:
	  dest = image->buffer + ((gulong) y * (gulong) image->buffer_width + (gulong) x);
	  gzw_color_8 (data, dest, x, y, w);
	  break;
	case 15:
	case 16:
	  dest = image->buffer + ((gulong) y * (gulong) image->buffer_width + (gulong) x) * 2;
	  gzw_color_16 (data, dest, w);
	  break;
	case 24:
	case 32:
	  dest = image->buffer + ((gulong) y * (gulong) image->buffer_width + (gulong) x) * 3;
	  gzw_color_24 (data, dest, w);
	  break;
	}
      break;
    case GZW_IMAGE_GRAYSCALE:
      switch (image_info->visual->depth)
	{
	case 8:
	  dest = image->buffer + ((gulong) y * (gulong) image->buffer_width + (gulong) x);
	  gzw_grayscale_8 (data, dest, x, y, w);
	  break;
	case 15:
	case 16:
	  dest = image->buffer + ((gulong) y * (gulong) image->buffer_width + (gulong) x) * 2;
	  gzw_grayscale_16 (data, dest, w);
	  break;
	case 24:
	case 32:
	  dest = image->buffer + ((gulong) y * (gulong) image->buffer_width + (gulong) x) * 3;
	  gzw_grayscale_24 (data, dest, w);
	  break;
	}
      break;
    }
}

void
gzw_image_set_expand (GzwImage *image,
			gint        expand)
{
  g_return_if_fail (image != NULL);
  g_return_if_fail (GZW_IS_IMAGE (image));

  image->expand = (expand != FALSE);
}

void
gzw_image_set_gamma (double _gamma)
{
  g_return_if_fail (image_class == NULL);

  if (!image_info)
    {
      image_info = g_new0 (GzwImageInfo, 1);
      image_info->nred_shades = 6;
      image_info->ngreen_shades = 6;
      image_info->nblue_shades = 4;
      image_info->ngray_shades = 24;
    }

  image_info->gamma = _gamma;
}

void
gzw_image_set_color_cube (guint nred_shades,
			    guint ngreen_shades,
			    guint nblue_shades,
			    guint ngray_shades)
{
  g_return_if_fail (image_class == NULL);

  if (!image_info)
    {
      image_info = g_new0 (GzwImageInfo, 1);
      image_info->gamma = 1.0;
    }

  image_info->nred_shades = nred_shades;
  image_info->ngreen_shades = ngreen_shades;
  image_info->nblue_shades = nblue_shades;
  image_info->ngray_shades = ngray_shades;
}

void
gzw_image_set_install_cmap (gint _install_cmap)
{
  /* g_return_if_fail (image_class == NULL); */

  install_cmap = _install_cmap;
}

void
gzw_image_set_reserved (gint nreserved)
{
  if (!image_info)
    image_info = g_new0 (GzwImageInfo, 1);

  image_info->nreserved = nreserved;
}

GdkVisual*
gzw_image_get_visual ()
{
  if (!image_class)
    image_class = gzw_type_class (gzw_image_get_type ());

  return image_info->visual;
}

GdkColormap*
gzw_image_get_cmap ()
{
  if (!image_class)
    image_class = gzw_type_class (gzw_image_get_type ());

  return image_info->cmap;
}

GzwImageInfo*
gzw_image_get_info ()
{
  if (!image_class)
    image_class = gzw_type_class (gzw_image_get_type ());

  return &image_class->info;
}


static void
gzw_image_destroy (GzwObject *object)
{
  GzwImage *image;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GZW_IS_IMAGE (object));

  image = GZW_IMAGE (object);
  if (image->buffer)
    g_free (image->buffer);
  image->type = (GzwImageType) -1;

  if (GZW_OBJECT_CLASS (parent_class)->destroy)
    (* GZW_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gzw_image_realize (GzwWidget *widget)
{
  GzwImage *image;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GZW_IS_IMAGE (widget));

  GZW_WIDGET_SET_FLAGS (widget, GZW_REALIZED);
  image = GZW_IMAGE (widget);

  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gzw_widget_get_visual (widget);
  attributes.colormap = gzw_widget_get_colormap (widget);
  attributes.event_mask = gzw_widget_get_events (widget) | GDK_EXPOSURE_MASK;
  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  widget->window = gdk_window_new (widget->parent->window, &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, widget);

  widget->style = gzw_style_attach (widget->style, widget->window);
  gzw_style_set_background (widget->style, widget->window, GZW_STATE_NORMAL);
}

static void
gzw_image_unrealize (GzwWidget *widget)
{
  GzwImage *image;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GZW_IS_IMAGE (widget));

  image = GZW_IMAGE (widget);

  if (GZW_WIDGET_CLASS (parent_class)->unrealize)
    (* GZW_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static gint
gzw_image_expose (GzwWidget      *widget,
		    GdkEventExpose *event)
{
  GzwImage *image;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GZW_IS_IMAGE (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  if (GZW_WIDGET_DRAWABLE (widget))
    {
      image = GZW_IMAGE (widget);

      gzw_image_put (GZW_IMAGE (widget),
		       widget->window, widget->style->black_gc,
		       (widget->allocation.width - image->buffer_width) / 2,
		       (widget->allocation.height - image->buffer_height) / 2,
		       event->area.x, event->area.y,
		       event->area.width, event->area.height);
    }

  return FALSE;
}

static void
gzw_image_make_buffer (GzwImage *image)
{
  GzwWidget *widget;
  gint width;
  gint height;

  g_return_if_fail (image != NULL);
  g_return_if_fail (GZW_IS_IMAGE (image));

  widget = GZW_WIDGET (image);

  if (image->expand &&
      (widget->allocation.width != 0) &&
      (widget->allocation.height != 0))
    {
      width = widget->allocation.width;
      height = widget->allocation.height;
    }
  else
    {
      width = widget->requisition.width;
      height = widget->requisition.height;
    }

  if (!image->buffer ||
      (image->buffer_width != width) ||
      (image->buffer_height != height))
    {
      if (image->buffer)
	g_free (image->buffer);

      image->buffer_width = width;
      image->buffer_height = height;

      image->buffer = g_new0 (guchar,
				image->buffer_width *
				image->buffer_height *
				image_info->bpp);
    }
}

static void
gzw_image_get_visuals (GzwImageClass *klass)
{
  static GdkVisualType types[] =
  {
    GDK_VISUAL_TRUE_COLOR,
    GDK_VISUAL_DIRECT_COLOR,
    GDK_VISUAL_TRUE_COLOR,
    GDK_VISUAL_DIRECT_COLOR,
    GDK_VISUAL_TRUE_COLOR,
    GDK_VISUAL_DIRECT_COLOR,
    GDK_VISUAL_TRUE_COLOR,
    GDK_VISUAL_DIRECT_COLOR,
    GDK_VISUAL_PSEUDO_COLOR
  };
  static gint depths[] = { 24, 24, 32, 32, 16, 16, 15, 15, 8 };
  static gint nvisual_types = sizeof (types) / sizeof (types[0]);

  int i;

  g_return_if_fail (klass != NULL);

  if (!image_info->visual)
    for (i = 0; i < nvisual_types; i++)
      if ((image_info->visual = gdk_visual_get_best_with_both (depths[i], types[i])))
	{
	  if ((image_info->visual->type == GDK_VISUAL_TRUE_COLOR) ||
	      (image_info->visual->type == GDK_VISUAL_DIRECT_COLOR))
	    {
	      image_info->lookup_red = g_new (gulong, 256);
	      image_info->lookup_green = g_new (gulong, 256);
	      image_info->lookup_blue = g_new (gulong, 256);

	      gzw_fill_lookup_array (image_info->lookup_red,
				     image_info->visual->depth,
				     image_info->visual->red_shift,
				     8 - image_info->visual->red_prec);
	      gzw_fill_lookup_array (image_info->lookup_green,
				     image_info->visual->depth,
				     image_info->visual->green_shift,
				     8 - image_info->visual->green_prec);
	      gzw_fill_lookup_array (image_info->lookup_blue,
				     image_info->visual->depth,
				     image_info->visual->blue_shift,
				     8 - image_info->visual->blue_prec);
	    }
	  break;
	}

  if (!image_info->visual)
    {
      g_warning ("unable to find a suitable visual for color image display.\n");
      return;
    }

  switch (image_info->visual->depth)
    {
    case 8:
      image_info->bpp = 1;
      break;
    case 15:
    case 16:
      image_info->bpp = 2;
      break;
    case 24:
    case 32:
      image_info->bpp = 3;
      break;
    }
}

static void
gzw_image_get_cmaps (GzwImageClass *klass)
{
  g_return_if_fail (klass != NULL);
  g_return_if_fail (image_info->visual != NULL);

  if ((image_info->visual->type != GDK_VISUAL_TRUE_COLOR) &&
      (image_info->visual->type != GDK_VISUAL_DIRECT_COLOR))
    {
      if (install_cmap)
	{
	  image_info->cmap = gdk_colormap_new (image_info->visual, FALSE);
	  image_info->cmap_alloced = install_cmap;

	  gzw_trim_cmap (klass);
	  gzw_create_8_bit (klass);
	}
      else
	{
	  guint nred;
	  guint ngreen;
	  guint nblue;
	  guint ngray;
	  gint set_prop;

	  image_info->cmap = gdk_colormap_get_system ();

	  set_prop = TRUE;
	  if (gzw_get_image_prop (&nred, &ngreen, &nblue, &ngray))
	    {
	      set_prop = FALSE;

	      image_info->nred_shades = nred;
	      image_info->ngreen_shades = ngreen;
	      image_info->nblue_shades = nblue;
	      image_info->ngray_shades = ngray;

	      if (image_info->nreserved)
		{
		  image_info->reserved_pixels = g_new (gulong, image_info->nreserved);
		  if (!gdk_colors_alloc (image_info->cmap, 0, NULL, 0,
					 image_info->reserved_pixels,
					 image_info->nreserved))
		    {
		      g_free (image_info->reserved_pixels);
		      image_info->reserved_pixels = NULL;
		    }
		}
	    }
	  else
	    {
	      gzw_trim_cmap (klass);
	    }

	  gzw_create_8_bit (klass);

	  if (set_prop)
	    gzw_set_image_prop (image_info->nred_shades,
				  image_info->ngreen_shades,
				  image_info->nblue_shades,
				  image_info->ngray_shades);
	}
    }
  else
    {
      if (image_info->visual == gdk_visual_get_system ())
	image_info->cmap = gdk_colormap_get_system ();
      else
	image_info->cmap = gdk_colormap_new (image_info->visual, FALSE);
      image_info->cmap_alloced = TRUE;

      image_info->nred_shades = 0;
      image_info->ngreen_shades = 0;
      image_info->nblue_shades = 0;
      image_info->ngray_shades = 0;
    }
}

static void
gzw_image_dither_init (GzwImageClass *klass)
{
  int i, j, k;
  unsigned char low_shade, high_shade;
  unsigned short index;
  long red_mult, green_mult;
  double red_matrix_width;
  double green_matrix_width;
  double blue_matrix_width;
  double gray_matrix_width;
  double red_colors_per_shade;
  double green_colors_per_shade;
  double blue_colors_per_shade;
  double gray_colors_per_shade;
  gulong *gray_pixels;
  gint shades_r, shades_g, shades_b, shades_gray;
  GzwDitherInfo *red_ordered_dither;
  GzwDitherInfo *green_ordered_dither;
  GzwDitherInfo *blue_ordered_dither;
  GzwDitherInfo *gray_ordered_dither;
  guchar ***dither_matrix;
  guchar DM[8][8] =
  {
    { 0,  32, 8,  40, 2,  34, 10, 42 },
    { 48, 16, 56, 24, 50, 18, 58, 26 },
    { 12, 44, 4,  36, 14, 46, 6,  38 },
    { 60, 28, 52, 20, 62, 30, 54, 22 },
    { 3,  35, 11, 43, 1,  33, 9,  41 },
    { 51, 19, 59, 27, 49, 17, 57, 25 },
    { 15, 47, 7,  39, 13, 45, 5,  37 },
    { 63, 31, 55, 23, 61, 29, 53, 21 }
  };

  if (image_info->visual->type != GDK_VISUAL_PSEUDO_COLOR)
    return;

  shades_r = image_info->nred_shades;
  shades_g = image_info->ngreen_shades;
  shades_b = image_info->nblue_shades;
  shades_gray = image_info->ngray_shades;

  red_mult = shades_g * shades_b;
  green_mult = shades_b;

  red_colors_per_shade = 255.0 / (shades_r - 1);
  red_matrix_width = red_colors_per_shade / 64;

  green_colors_per_shade = 255.0 / (shades_g - 1);
  green_matrix_width = green_colors_per_shade / 64;

  blue_colors_per_shade = 255.0 / (shades_b - 1);
  blue_matrix_width = blue_colors_per_shade / 64;

  gray_colors_per_shade = 255.0 / (shades_gray - 1);
  gray_matrix_width = gray_colors_per_shade / 64;

  /*  alloc the ordered dither arrays for accelerated dithering  */

  image_info->dither_red = g_new (GzwDitherInfo, 256);
  image_info->dither_green = g_new (GzwDitherInfo, 256);
  image_info->dither_blue = g_new (GzwDitherInfo, 256);
  image_info->dither_gray = g_new (GzwDitherInfo, 256);

  red_ordered_dither = image_info->dither_red;
  green_ordered_dither = image_info->dither_green;
  blue_ordered_dither = image_info->dither_blue;
  gray_ordered_dither = image_info->dither_gray;

  dither_matrix = g_new (guchar**, 8);
  for (i = 0; i < 8; i++)
    {
      dither_matrix[i] = g_new (guchar*, 8);
      for (j = 0; j < 8; j++)
	dither_matrix[i][j] = g_new (guchar, 65);
    }

  image_info->dither_matrix = dither_matrix;

  /*  setup the ordered_dither_matrices  */

  for (i = 0; i < 8; i++)
    for (j = 0; j < 8; j++)
      for (k = 0; k <= 64; k++)
	dither_matrix[i][j][k] = (DM[i][j] < k) ? 1 : 0;

  /*  setup arrays containing three bytes of information for red, green, & blue  */
  /*  the arrays contain :
   *    1st byte:    low end shade value
   *    2nd byte:    high end shade value
   *    3rd & 4th bytes:    ordered dither matrix index
   */

  gray_pixels = image_info->gray_pixels;

  for (i = 0; i < 256; i++)
    {

      /*  setup the red information  */
      {
	low_shade = (unsigned char) (i / red_colors_per_shade);
	if (low_shade == (shades_r - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short)
	  (((double) i - low_shade * red_colors_per_shade) /
	   red_matrix_width);

	low_shade *= red_mult;
	high_shade *= red_mult;

	red_ordered_dither[i].s[1] = index;
	red_ordered_dither[i].c[0] = low_shade;
	red_ordered_dither[i].c[1] = high_shade;
      }


      /*  setup the green information  */
      {
	low_shade = (unsigned char) (i / green_colors_per_shade);
	if (low_shade == (shades_g - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short)
	  (((double) i - low_shade * green_colors_per_shade) /
	   green_matrix_width);

	low_shade *= green_mult;
	high_shade *= green_mult;

	green_ordered_dither[i].s[1] = index;
	green_ordered_dither[i].c[0] = low_shade;
	green_ordered_dither[i].c[1] = high_shade;
      }


      /*  setup the blue information  */
      {
	low_shade = (unsigned char) (i / blue_colors_per_shade);
	if (low_shade == (shades_b - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short)
	  (((double) i - low_shade * blue_colors_per_shade) /
	   blue_matrix_width);

	blue_ordered_dither[i].s[1] = index;
	blue_ordered_dither[i].c[0] = low_shade;
	blue_ordered_dither[i].c[1] = high_shade;
      }


      /*  setup the gray information */
      {
	low_shade = (unsigned char) (i / gray_colors_per_shade);
	if (low_shade == (shades_gray - 1))
	  low_shade--;
	high_shade = low_shade + 1;

	index = (unsigned short)
	  (((double) i - low_shade * gray_colors_per_shade) /
	   gray_matrix_width);

	gray_ordered_dither[i].s[1] = index;
	gray_ordered_dither[i].c[0] = gray_pixels[low_shade];
	gray_ordered_dither[i].c[1] = gray_pixels[high_shade];
      }
    }
}

static void
gzw_fill_lookup_array (gulong *array,
		       int     depth,
		       int     shift,
		       int     prec)
{
  double one_over_gamma;
  double ind;
  int val;
  int i;

  if (image_info->gamma != 0.0)
    one_over_gamma = 1.0 / image_info->gamma;
  else
    one_over_gamma = 1.0;

  for (i = 0; i < 256; i++)
    {
      if (one_over_gamma == 1.0)
        array[i] = ((i >> prec) << shift);
      else
        {
          ind = (double) i / 255.0;
          val = (int) (255 * pow (ind, one_over_gamma));
          array[i] = ((val >> prec) << shift);
        }
    }
}

static void
gzw_trim_cmap (GzwImageClass *klass)
{
  gulong pixels[256];
  guint nred;
  guint ngreen;
  guint nblue;
  guint ngray;
  guint nreserved;
  guint total;
  guint tmp;
  gint success;

  nred = image_info->nred_shades;
  ngreen = image_info->ngreen_shades;
  nblue = image_info->nblue_shades;
  ngray = image_info->ngray_shades;
  nreserved = image_info->nreserved;

  success = FALSE;
  while (!success)
    {
      total = nred * ngreen * nblue + ngray + nreserved;

      if (total <= 256)
	{
	  if ((nred < 2) || (ngreen < 2) || (nblue < 2) || (ngray < 2))
	    success = TRUE;
	  else
	    {
	      success = gdk_colors_alloc (image_info->cmap, 0, NULL, 0, pixels, total);
	      if (success)
		{
		  if (nreserved > 0)
		    {
		      image_info->reserved_pixels = g_new (gulong, nreserved);
		      memcpy (image_info->reserved_pixels, pixels, sizeof (gulong) * nreserved);
		      gdk_colors_free (image_info->cmap, &pixels[nreserved],
				       total - nreserved, 0);
		    }
		  else
		    {
		      gdk_colors_free (image_info->cmap, pixels, total, 0);
		    }
		}
	    }
	}

      if (!success)
	{
	  if ((nblue >= nred) && (nblue >= ngreen))
	    nblue = nblue - 1;
	  else if ((nred >= ngreen) && (nred >= nblue))
	    nred = nred - 1;
	  else
	    {
	      tmp = log (ngray) / log (2);

	      if (ngreen >= tmp)
		ngreen = ngreen - 1;
	      else
		ngray -= 1;
	    }
	}
    }

  if ((nred < 2) || (ngreen < 2) || (nblue < 2) || (ngray < 2))
    {
      g_print ("Unable to allocate sufficient colormap entries.\n");
      g_print ("Try exiting other color intensive applications.\n");
      return;
    }

  /*  If any of the shade values has changed, issue a warning  */
  if ((nred != image_info->nred_shades) ||
      (ngreen != image_info->ngreen_shades) ||
      (nblue != image_info->nblue_shades) ||
      (ngray != image_info->ngray_shades))
    {
      g_print ("Not enough colors to satisfy requested color cube.\n");
      g_print ("Reduced color cube shades from\n");
      g_print ("[%d of Red, %d of Green, %d of Blue, %d of Gray] ==> [%d of Red, %d of Green, %d of Blue, %d of Gray]\n",
               image_info->nred_shades, image_info->ngreen_shades,
	       image_info->nblue_shades, image_info->ngray_shades,
	       nred, ngreen, nblue, ngray);
    }

  image_info->nred_shades = nred;
  image_info->ngreen_shades = ngreen;
  image_info->nblue_shades = nblue;
  image_info->ngray_shades = ngray;
}

static void
gzw_create_8_bit (GzwImageClass *klass)
{
  unsigned int r, g, b;
  unsigned int rv, gv, bv;
  unsigned int dr, dg, db, dgray;
  GdkColor color;
  gulong *pixels;
  double one_over_gamma;
  int i;

  if (!image_info->color_pixels)
    image_info->color_pixels = g_new (gulong, 256);

  if (!image_info->gray_pixels)
    image_info->gray_pixels = g_new (gulong, 256);

  if (image_info->gamma != 0.0)
    one_over_gamma = 1.0 / image_info->gamma;
  else
    one_over_gamma = 1.0;

  dr = image_info->nred_shades - 1;
  dg = image_info->ngreen_shades - 1;
  db = image_info->nblue_shades - 1;
  dgray = image_info->ngray_shades - 1;

  pixels = image_info->color_pixels;

  for (r = 0, i = 0; r <= dr; r++)
    for (g = 0; g <= dg; g++)
      for (b = 0; b <= db; b++, i++)
        {
          rv = (unsigned int) ((r * image_info->visual->colormap_size) / dr);
          gv = (unsigned int) ((g * image_info->visual->colormap_size) / dg);
          bv = (unsigned int) ((b * image_info->visual->colormap_size) / db);
          color.red = ((int) (255 * pow ((double) rv / 256.0, one_over_gamma))) * 257;
          color.green = ((int) (255 * pow ((double) gv / 256.0, one_over_gamma))) * 257;
          color.blue = ((int) (255 * pow ((double) bv / 256.0, one_over_gamma))) * 257;

	  if (!gdk_color_alloc (image_info->cmap, &color))
	    {
	      g_error ("could not initialize 8-bit combined colormap");
	      return;
	    }

	  pixels[i] = color.pixel;
        }

  pixels = image_info->gray_pixels;

  for (i = 0; i < (int) image_info->ngray_shades; i++)
    {
      color.red = (i * image_info->visual->colormap_size) / dgray;
      color.red = ((int) (255 * pow ((double) color.red / 256.0, one_over_gamma))) * 257;
      color.green = color.red;
      color.blue = color.red;

      if (!gdk_color_alloc (image_info->cmap, &color))
	{
	  g_error ("could not initialize 8-bit combined colormap");
	  return;
	}

      pixels[i] = color.pixel;
    }
}


static void
gzw_color_8 (guchar *src,
	     guchar *dest,
	     gint    x,
	     gint    y,
	     gulong  width)
{
  gulong *colors;
  GzwDitherInfo *dither_red;
  GzwDitherInfo *dither_green;
  GzwDitherInfo *dither_blue;
  GzwDitherInfo r, g, b;
  guchar **dither_matrix;
  guchar *matrix;

  colors = image_info->color_pixels;
  dither_red = image_info->dither_red;
  dither_green = image_info->dither_green;
  dither_blue = image_info->dither_blue;
  dither_matrix = image_info->dither_matrix[y & 0x7];

  while (width--)
    {
      r = dither_red[src[0]];
      g = dither_green[src[1]];
      b = dither_blue[src[2]];
      src += 3;

      matrix = dither_matrix[x++ & 0x7];
      *dest++ = colors[(r.c[matrix[r.s[1]]] +
			g.c[matrix[g.s[1]]] +
			b.c[matrix[b.s[1]]])];
    }
}

static void
gzw_color_16 (guchar *src,
	      guchar *dest,
	      gulong  width)
{
  gulong *lookup_red;
  gulong *lookup_green;
  gulong *lookup_blue;
  gulong val;

  lookup_red = image_info->lookup_red;
  lookup_green = image_info->lookup_green;
  lookup_blue = image_info->lookup_blue;

  while (width--)
    {
      val = COLOR_COMPOSE (src[0], src[1], src[2]);
      dest[0] = val;
      dest[1] = val >> 8;
      dest += 2;
      src += 3;
    }
}

static void
gzw_color_24 (guchar *src,
	      guchar *dest,
	      gulong  width)
{
  gulong *lookup_red;
  gulong *lookup_green;
  gulong *lookup_blue;
  gulong val;

  lookup_red = image_info->lookup_red;
  lookup_green = image_info->lookup_green;
  lookup_blue = image_info->lookup_blue;

  while (width--)
    {
      val = COLOR_COMPOSE (src[0], src[1], src[2]);
      dest[0] = val;
      dest[1] = val >> 8;
      dest[2] = val >> 16;
      dest += 3;
      src += 3;
    }
}

static void
gzw_grayscale_8 (guchar *src,
		 guchar *dest,
		 gint    x,
		 gint    y,
		 gulong  width)
{
  GzwDitherInfo *dither_gray;
  GzwDitherInfo gray;
  guchar **dither_matrix;
  guchar *matrix;

  dither_gray = image_info->dither_gray;
  dither_matrix = image_info->dither_matrix[y & 0x7];

  while (width--)
    {
      gray = dither_gray[*src++];
      matrix = dither_matrix[x++ & 0x7];
      *dest++ = gray.c[matrix[gray.s[1]]];
    }
}

static void
gzw_grayscale_16 (guchar *src,
		  guchar *dest,
		  gulong  width)
{
  gulong *lookup_red;
  gulong *lookup_green;
  gulong *lookup_blue;
  gulong val;

  lookup_red = image_info->lookup_red;
  lookup_green = image_info->lookup_green;
  lookup_blue = image_info->lookup_blue;

  while (width--)
    {
      val = COLOR_COMPOSE (*src, *src, *src);
      dest[0] = val;
      dest[1] = val >> 8;
      dest += 2;
      src += 1;
    }
}

static void
gzw_grayscale_24 (guchar  *src,
		  guchar  *dest,
		  gulong   width)
{
  gulong *lookup_red;
  gulong *lookup_green;
  gulong *lookup_blue;
  gulong val;

  lookup_red = image_info->lookup_red;
  lookup_green = image_info->lookup_green;
  lookup_blue = image_info->lookup_blue;

  while (width--)
    {
      val = COLOR_COMPOSE (*src, *src, *src);
      dest[0] = val;
      dest[1] = val >> 8;
      dest[2] = val >> 16;
      dest += 3;
      src += 1;
    }
}


static gint
gzw_get_image_prop (guint *nred,
		      guint *ngreen,
		      guint *nblue,
		      guint *ngray)
{
  GzwImageProp *prop;
  GdkAtom property;

  property = gdk_atom_intern ("GZW_IMAGE_INFO", FALSE);

  if (gdk_property_get (NULL, property, property,
			0, sizeof (GzwImageProp), FALSE,
			NULL, NULL, NULL, (guchar**) &prop))
    {
      *nred = ntohs (prop->nred_shades);
      *ngreen = ntohs (prop->ngreen_shades);
      *nblue = ntohs (prop->nblue_shades);
      *ngray = ntohs (prop->ngray_shades);

      prop->ref_count = htons (ntohs (prop->ref_count) + 1);
      gdk_property_change (NULL, property, property, 16,
			   GDK_PROP_MODE_REPLACE,
			   (guchar*) prop, 5);

      return TRUE;
    }

  return FALSE;
}

static void
gzw_set_image_prop (guint nred,
		      guint ngreen,
		      guint nblue,
		      guint ngray)
{
  GzwImageProp prop;
  GdkAtom property;

  property = gdk_atom_intern ("GZW_IMAGE_INFO", FALSE);

  prop.ref_count = htons (1);
  prop.nred_shades = htons (nred);
  prop.ngreen_shades = htons (ngreen);
  prop.nblue_shades = htons (nblue);
  prop.ngray_shades = htons (ngray);

  gdk_property_change (NULL, property, property, 16,
		       GDK_PROP_MODE_REPLACE,
		       (guchar*) &prop, 5);
}


static void
gzw_lsbmsb_1_1 (guchar *dest,
		guchar *src,
		gint    count)
{
  memcpy (dest, src, count);
}

static void
gzw_lsb_2_2 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  memcpy (dest, src, count * 2);
}

static void
gzw_msb_2_2 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  while (count--)
    {
      dest[0] = src[1];
      dest[1] = src[0];
      dest += 2;
      src += 2;
    }
}

static void
gzw_lsb_3_3 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  memcpy (dest, src, count * 3);
}

static void
gzw_msb_3_3 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  while (count--)
    {
      dest[0] = src[2];
      dest[1] = src[1];
      dest[2] = src[0];
      dest += 3;
      src += 3;
    }
}

static void
gzw_lsb_3_4 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  while (count--)
    {
      dest[0] = src[0];
      dest[1] = src[1];
      dest[2] = src[2];
      dest += 4;
      src += 3;
    }
}

static void
gzw_msb_3_4 (guchar *dest,
	     guchar *src,
	     gint    count)
{
  while (count--)
    {
      dest[1] = src[2];
      dest[2] = src[1];
      dest[3] = src[0];
      dest += 4;
      src += 3;
    }
}

#endif
