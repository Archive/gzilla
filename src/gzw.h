#ifndef __GZW_H__
#define __GZW_H__

/* The main header file for the Gzilla Widget set. At its heart is the
   Gzw data structure, and its "helper" data structures, including
   GzwRect and GzwClass.

   It also includes definitions for the dispatch functions.
*/

#define USE_GZW

#include "gzwrect.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _Gzw Gzw;
typedef struct _GzwClass GzwClass;
typedef struct _GzwPaint GzwPaint;
typedef struct _GzwContainerClass GzwContainerClass;
typedef struct _GzwContainer GzwContainer;

/* The main Gzw widget structure. Most widgets will extend it (i.e.
   allocate a structure that includes a Gzw as its first element). */

struct _Gzw {
  const GzwClass *klass;
  gint req_width_min;     /* the minimum comfortable width */
  gint req_width_max;     /* the maximum comfortable width */

   /* If the widget is not to be baseline aligned, it sets its
      req_height to the height, and req_ascent zero. Conversely, if it
      wants to be baseline aligned, it sets req_height and
      req_ascent. The descent is the height minus the ascent.
   */
   gint req_height;
   gint req_ascent;

   /* Allocation is similar to GTK.

      x, y coordinates are relative to the toplevel Gzilla widget.
   */
   GzwRect allocation;

   /* Flags include:

      + alignment options (baseline, vertical, horizontal?)

      + information about what's currently displayed, one of:
        + valid data (no need for repaint)
        + previously valid data (can incrementally repaint)
        + window background (don't need to clear before drawing)
        + good data underneath (don't clear before drawing; for layers)
        + unknown (better clear and repaint)

      + widget needs resize

      + widget contains embedded GTK widget (used to prune propagation
        of gtk_foreach calls)

   */
   gint flags;

   Gzw *parent;

   /* Information about the enclosing gtk container. */
   GzwContainer *container;
};

/* Gzw flags. Should these be an enum rather than #defines? */

/* BASELINE is set if req_ascent is valid. */
#define GZW_FLAG_BASELINE   0x0001

/* REQ_RESIZE is set if the widget (or, in the case of a container, any
   descendant widgets) have requested resize). */
#define GZW_FLAG_REQ_RESIZE 0x0002

/* GTK_EMBED is set if the widget contains any embedded GTK widgets.
   This stuff probably needs some changes. */
#define GZW_FLAG_GTK_EMBED  0x0004

/* The methods defined for a Gzw. */

typedef void (*GzwCallback) (GtkWidget *widget,
			     gpointer   data,
			     GtkAllocation *allocation);

struct _GzwClass {

  void (*size_nego_x) (Gzw *gzw, gint width);

  void (*size_nego_y) (Gzw *gzw, GzwRect *allocation);

  void (*paint) (Gzw *gzw, GzwRect *rect, GzwPaint *paint);

  void (*handle_event) (Gzw *gzw, GdkEvent *event);

  /* todo: define the active rectangle more precisely (I think that
     widgets intersecting rect1 minus widgets intersecting rect2
     might be what we want. */
  void (*gtk_foreach) (Gzw *gzw, GzwCallback callback, gpointer callback_data,
		       GzwRect *plus, GzwRect *minus);

  void (*destroy) (Gzw *gzw);

  /* The following method is supported only by containers: */

  void (*request_resize) (Gzw *gzw, Gzw *child);

};

/* A few invariants for these methods:

   The paint method is only called if the widget has a toplevel container,
   and that container is mapped and visible.

   */

struct _GzwContainer {
  const GzwContainerClass *klass;

  /* The GTK widget that contains the toplevel Gzw widget. */
  GtkWidget *widget;

  /* The visible rectangle, in coordinates relative to the toplevel
     Gzw. */
  GzwRect visible;

  /* You get X11 window coordinates by adding these to toplevel Gzw
     coordinates. */
  gint x_offset, y_offset;
};

/* These are methods on the toplevel Gzw widget, for communicating to
   the enclosing Gtk widget. */
struct _GzwContainerClass {

  /* todo: it should probably take a flag to indicate whether
     the previous data is considered valid. */
  /* rect is in toplevel coordinates. */
  void (*request_paint) (GzwContainer *container, GzwRect *rect);

  /* request a resize of the contained gzw. */
  void (*request_resize) (GzwContainer *container);
};

struct _GzwPaint {
  gint flags;
  gint32 bg_color;
  /* may want to put the paint rect in here. */
};

/* GzwPaint flags: */

/* An invariant: at least one of the UNDER flags is set. */

/* The layer under us is the background color. */
#define GZW_PAINT_BG_UNDER         0x0001

/* The layer under us is on the screen. */
#define GZW_PAINT_SCREEN_UNDER     0x0002

/* GZW_PAINT_BUF_UNDER would indicate that the layer under us is in a
   buffer. */

/* The screen is the background color. */
#define GZW_PAINT_BG_SCREEN        0x0100

/* Good data from the last repaint is on the screen. */
#define GZW_PAINT_TOP_SCREEN       0x0200

GzwContainer *gzw_find_container (Gzw *gzw);

void gzw_size_nego_x (Gzw *gzw, gint width);
void gzw_size_nego_y (Gzw *gzw, GzwRect *allocation);
void gzw_paint (Gzw *gzw, GzwRect *rect, GzwPaint *paint);
void gzw_handle_event (Gzw *gzw, GdkEvent *event);
void gzw_gtk_foreach (Gzw *gzw, GzwCallback callback, gpointer callback_data,
		      GzwRect *plus, GzwRect *minus);
void gzw_destroy (Gzw *gzw);
void gzw_request_parent_resize (Gzw *gzw);
void gzw_request_paint (Gzw *gzw, GzwRect *rect);

GzwContainer *gzw_find_container (Gzw *gzw);
void gzw_paint_to_screen (GtkWidget *widget, GzwContainer *container,
			  GzwRect *rect, GzwPaint *paint);
void gzw_paint_finish_screen (GzwPaint *paint);

void gzw_null_size_nego_x (Gzw *gzw, gint width);
void gzw_null_handle_event (Gzw *gzw, GdkEvent *event);
void gzw_null_gtk_foreach (Gzw *gzw,
			   GzwCallback callback,
			   gpointer callback_data,
			   GzwRect *plus,
			   GzwRect *minus);
void gzw_null_request_resize (Gzw *gzw, Gzw *child);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZW_H__ */
