/* This module contains the gzw_page widget, which is the "back end" to
   Web text widgets including html. 

   This widget is currently in transition. The current goal (as of 28
   Oct 1997) is to transition from the gzw_page design into the Gzw
   widget framework, preserving most of the interface and operation.
   In the future, an important goal will be to improve the layout
   capabilities, at least to support floating elements (a must), but
   possibly to meet most of the CSS1 layout model.

   */

#define TRACE_GTK_METHODS
#undef NO_WINDOW
#define HUGE_WINDOW
#define GZW_PAGE_MAX_WINDOW_SIZE 32760

#include <ctype.h> /* for isspace */
#include <string.h> /* for memcpy and memmove */
#include <stdlib.h> /* for atoi */
#include <stdio.h> /* for sprintf */

#include <gtk/gtk.h>
#include "gzw.h"
#include "gzwpage.h"



/* todo:

   + do request_repaint for added stuff. But what level of
   granularity? reinstate the update_begin/update_end cycle? Do at
   line granularity when making a new line (but risk paint
   inconsistency)? Do at word granularity but take on region overhead
   on every word (optimize away if outside visible rectangle)?

   + better formatting, including justification & floats

   */


/* Returns the x offset (the indentation plus any offset needed for
   centering or right justification) for the line. The offset returned
   is relative to the page (i.e. add allocation.x0 to get the gzw
   toplevel coordinate. */
static gint
gzw_page_line_x_offset (GzwPage *page, GzwPageLine *line)
{
  gint x;

  if (line->first)
    x = page->attrs[line->words[0].attr].left_indent_first;
  else
    x = page->attrs[line->words[0].attr].left_indent_rest;
  return x;
}

/* gzw_page_finish_line "finishes" the last line in the page. At
   present, the only function is to size allocate the child widgets.
   Later, it will be responsible for some justification code.

   Note: there is no need to request paint for the child widget,
   because this is (presumably) done by update_end.

   */

static void
gzw_page_finish_line (GzwPage *page)
{
  GzwPageLine *line;
  GzwPageWord *word;
  GzwRect rect;
  gint word_index;
  gint x_cursor, y_cursor;

  line = &page->lines[page->num_lines - 1];
  x_cursor = page->gzw.allocation.x0 + gzw_page_line_x_offset (page, line);
  y_cursor = page->gzw.allocation.y0 + line->y_top + line->y_ascent;
  for (word_index = 0; word_index < line->num_words; word_index++)
    {
      word = &line->words[word_index];
      if (word->content_type == GZW_PAGE_CONTENT_WIDGET)
	{
	  rect.x0 = x_cursor;
	  rect.y0 = y_cursor - word->y_ascent;
	  rect.x1 = x_cursor + word->x_size;
	  rect.y1 = y_cursor + word->y_descent;
	  gzw_size_nego_y (word->content.widget, &rect);
	}
      x_cursor += word->x_size + word->x_space;
    }
}

static void
gzw_page_add_line (GzwPage *page) {
  gint num_lines;
  GzwPageLine *line;

  num_lines = page->num_lines;
  if (num_lines > 0)
    gzw_page_finish_line (page);
  if (num_lines == page->num_lines_max) {
    page->num_lines_max <<= 1;
    page->lines = g_realloc (page->lines,
			     page->num_lines_max * sizeof (GzwPageLine));
  }
  line = &(page->lines[num_lines]);
  line->num_words = 0;
  line->num_words_max = 16;
  line->words = g_new (GzwPageWord, line->num_words_max);
  if (num_lines == 0) {
    line->y_top = 0;
  } else {
    line->y_top =
      page->lines[num_lines - 1].y_top + page->lines[num_lines - 1].y_ascent +
      page->lines[num_lines - 1].y_descent +
      page->lines[num_lines - 1].y_space;
  }
  line->x_size = 0;
  line->y_ascent = 6;
  line->y_descent = 3;
  line->y_space = 0;
  line->hard = FALSE;
  line->first = (num_lines == 0 || (page->lines[num_lines - 1].hard &&
				    page->lines[num_lines - 1].y_space > 0));
  page->num_lines++;
}

/* Allocate a new word in a page structure. This routine is where word
   wrapping happens. The newly allocated word has the x_size, x_space,
   y_ascent, and y_descent fields filled in, but no others.

*/

static GzwPageWord *gzw_page_new_word (GzwPage *page,
				       gint x_size,
				       gint y_ascent,
				       gint y_descent,
				       gint attr) {
  GzwPageLine *line;
  GzwPageWord *word;
  gboolean new_line;
  gint width;
  gint set_width;
  gint j;
  gint num_lines;

  new_line = FALSE;
  num_lines = page->num_lines;
  if (num_lines == 0) {
    new_line = TRUE;
  } else {
    line = &(page->lines[num_lines - 1]);
    if (line->hard) {
      new_line = TRUE;
    } else {
      /* Calclulate width of new line and see if it exceeds page width. */
      j = line->num_words;
      if (j > 0) {
	set_width = page->width - page->attrs[attr].right_indent;
	if (line->first)
	  set_width -= page->attrs[attr].left_indent_first;
	else
	  set_width -= page->attrs[attr].left_indent_rest;
	if (set_width < 100) set_width = 100;
	width = line->x_size + line->words[j - 1].x_space + x_size;
	if (width > set_width)
	  new_line = TRUE;
      }
    }
  }

  if (new_line) {
    gzw_page_add_line (page);
    line = &(page->lines[num_lines]);
  }

  if (line->num_words == line->num_words_max) {
    line->num_words_max <<= 1;
    line->words = g_realloc (line->words,
			     line->num_words_max * sizeof (GzwPageWord));
  }
  j = line->num_words;
  line->num_words++;
  word = &(line->words[j]);
  word->x_size = x_size;
  word->x_space = 0;
  word->y_ascent = y_ascent;
  word->y_descent = y_descent;
  if (j)
    line->x_size += line->words[j - 1].x_space;
  line->x_size += x_size;
  line->y_ascent = MAX (line->y_ascent, y_ascent);
  line->y_descent = MAX (line->y_descent, y_descent);

  /* update the width requisitions */
  if (j == 0 && line->first)
    page->last_line_max_width = x_size + page->attrs[attr].left_indent_first;
  else if (j == 0)
    /* this is kind of ugly, I know */
    page->last_line_max_width +=
      page->lines[num_lines - 1].words[page->lines[num_lines - 1].num_words - 1].x_space + x_size;
  else
    page->last_line_max_width += line->words[j - 1].x_space + x_size;
  page->gzw.req_width_max = MAX (page->gzw.req_width_max,
				 page->last_line_max_width);
  page->gzw.req_width_min = MAX (page->gzw.req_width_min, x_size);

  return word;
}

/* Rewrap the page. There are basically two times we'll want to do this:
   either when the viewport is resized, or when the size changes on one
   of the child widgets. */

/* Performance is not as good as it could be - it could only rewrap if
   it needs to (should be a cheap test: the line is ok if it fits
   within the page->width and either it's hard or the width plus the
   width of the first word on the next line (plus the spacing of the
   last word) exceed the page->width). This would save on memory
   allocation too.

   But hey, it works!

*/

static void gzw_page_rewrap (GzwPage *page) {
  GzwPageLine *old_lines;
  gint old_num_lines;

  gint line_index, word_index;
  GzwPageLine *old_line;
  GzwPageWord *old_word, *word;

  old_lines = page->lines;
  old_num_lines = page->num_lines;

  /* Initialize the lines structure of the page. */
  page->num_lines = 0;
  page->num_lines_max = 16;
  page->lines = g_new (GzwPageLine, page->num_lines_max);

  /* todo: reset width requisitions... they will be rebuilt by
     gzw_page_new_word. */

  /* Iterate over the old lines, adding the words to the page structure.
   */
  for (line_index = 0; line_index < old_num_lines; line_index++) {
    old_line = &(old_lines[line_index]);
    /* Here, we're actually relying on the invariant that a soft line
       is never followed by an empty hard line. */
    if (old_line->num_words == 0 && old_line->hard) {
      gzw_page_add_line (page);
    }
    for (word_index = 0; word_index < old_line->num_words; word_index++) {
      old_word = &(old_line->words[word_index]);
      /* todo: Here is probably where we should check the word for a
	 resize request. */
      word = gzw_page_new_word (page,
				old_word->x_size,
				old_word->y_ascent, old_word->y_descent,
				old_word->attr);
      word->x_space      =  old_word->x_space;
      word->content_type =  old_word->content_type;
      word->content      =  old_word->content;
      word->attr         =  old_word->attr;
    }

    if (old_line->hard) {
      gzw_page_linebreak (page);
      page->lines[page->num_lines - 1].y_space = old_line->y_space;
    }

    g_free (old_line->words);
  }
  g_free (old_lines);

  gzw_request_parent_resize (&page->gzw);
  /* Request that the entire visible area be repainted. After some
     optimization, it may be possible to restrict the repainted area
     to being much smaller. */
  gzw_request_paint (&page->gzw, &page->gzw.allocation);

}

static void
gzw_page_size_nego_x (Gzw *gzw, gint width)
{
  GzwPage *page;

  page = (GzwPage *)gzw;
  if (width != page->width)
    {
      page->width = width;
      gzw_page_rewrap (page);
    }
  if (page->num_lines > 0)
    gzw->req_height = page->lines[page->num_lines - 1].y_top +
      page->lines[page->num_lines - 1].y_ascent +
      page->lines[page->num_lines - 1].y_descent;
#ifdef VERBOSE
  g_print ("gzw_page_size_nego_x: requisition = %d - %d x %d\n",
	   page->gzw.req_width_min, page->gzw.req_width_max,
	   page->gzw.req_height);
#endif
}

static void
gzw_page_size_nego_y (Gzw *gzw, GzwRect *allocation)
{
  /* todo: handle a rewrap request generated by a child's resize
     request. */

  /* todo: if the allocation offset changes, we have to reallocate all
     child widgets. */
  gzw->allocation = *allocation;
#ifdef VERBOSE
  g_print ("gzw_page_size_nego_y: allocation = (%d, %d) - (%d, %d)\n",
	   gzw->allocation.x0,
	   gzw->allocation.x1,
	   gzw->allocation.y0,
	   gzw->allocation.y1);
#endif
}

/* x and y are toplevel gzw coordinates */
static void
gzw_page_expose_line (GzwPage *page,
		      GzwRect *rect,
		      GzwPaint *paint,
		      GzwPageLine *line,
		      gint x,
		      gint y) {
  GtkWidget *widget;
  GzwContainer *container;
  GzwPageWord *word;
  gint word_index;
  gint x_cursor, y_cursor;
  gint last_font = -1;
  gint font;
  gint32 last_color = -1;
  gint32 color;
  GdkGC *gc;
  GdkColor *gdk_color;
  GdkColormap *colormap;
  gint uline_width;
  GzwRect child_rect;

  /* Here's an idea on how to optimize this routine to minimize the number
     of calls to gdk_draw_string:

     Copy the text from the words into a buffer, adding a new word
     only if: the attributes match, and the spacing is either zero or
     equal to the width of ' '. In the latter case, copy a " " into
     the buffer. Then draw the buffer. */

  container = gzw_find_container (&page->gzw);
  widget = container->widget;

  /* A gtk question: should we be creating our own gc? */

  colormap = gtk_widget_get_colormap (widget);
  x_cursor = x + container->x_offset;
  y_cursor = y + line->y_ascent + container->y_offset;
  gc = page->gc;
  for (word_index = 0; word_index < line->num_words; word_index++) {
    word = &(line->words[word_index]);
    switch (word->content_type) {
    case GZW_PAGE_CONTENT_TEXT:
      font = page->attrs[word->attr].font;
#ifndef USE_TYPE1
      if (font != last_font) {
	gdk_gc_set_font (gc, page->fonts[font].font);
	last_font = font;
      }
#endif
      color = page->attrs[word->attr].color;
      if (color != last_color) {
	gdk_color = &(page->colors[color]);
	if (gdk_color->pixel == -1) {
	  gdk_color_alloc (colormap, gdk_color);
	}
	gdk_gc_set_foreground (gc, gdk_color);
	last_color = color;
      }
      gdk_draw_string (widget->window,
		       page->fonts[font].font,
		       gc,
		       x_cursor,
		       y_cursor,
		       word->content.text);
      if (page->attrs[word->attr].link >= 0) {
	uline_width = word->x_size;
	if (word_index + 1 < line->num_words &&
	    page->attrs[word->attr].link ==
	    page->attrs[line->words[word_index + 1].attr].link)
	  uline_width += word->x_space;
	gdk_draw_line (widget->window,
		       gc,
		       x_cursor,
		       y_cursor + 1,
		       x_cursor + uline_width - 1,
		       y_cursor + 1);
      }
      break;
    case GZW_PAGE_CONTENT_WIDGET:
      gzw_rect_intersect (&child_rect, rect,
			  &word->content.widget->allocation);
      if (!gzw_rect_empty (&child_rect))
	{
	  gzw_paint (word->content.widget, &child_rect, paint);
	  gzw_paint_to_screen (widget, container, &child_rect, paint);
	}
      break;
    }
    x_cursor += word->x_size + word->x_space;
  }
}


/* Find the first line index that includes y, relative to top of widget. */

static gint gzw_page_find_line_index (GzwPage *page, gint y) {
  gint line_index;

  /* This could be a faster algorithm, for example a binary search. */

  for (line_index = 0; line_index < page->num_lines; line_index++) {
    if (page->lines[line_index].y_top +
	page->lines[line_index].y_ascent +
	page->lines[line_index].y_descent > y)
      break;
  }

  return line_index;
}

/* Draw the actual lines, starting at (x, y) in toplevel Gzw coords. */

static void
gzw_page_expose_lines (GzwPage *page,
		       GzwRect *rect,
		       GzwPaint *paint,
		       gint x,
		       gint y) {
  gint line_index;
  GzwPageLine *line;
  gint x1;


  line_index = gzw_page_find_line_index (page, rect->y0 - y);
  for (; line_index < page->num_lines; line_index++) {
    line = &(page->lines[line_index]);
    if (y + line->y_top >= rect->y1)
      break;
    /* Maybe should move this into expose_line */
    x1 = x + gzw_page_line_x_offset (page, line);
    gzw_page_expose_line (page, rect, paint,
			  &(page->lines[line_index]),
			  x1,
			  y + page->lines[line_index].y_top);
  }
}

static void
gzw_page_paint (Gzw *gzw, GzwRect *rect, GzwPaint *paint)
{
  GtkWidget *widget;
  GzwContainer *container;
  GzwPage *page;
  gint x, y;

#ifdef VERBOSE
  g_print ("gzw_page_paint (%d, %d) - (%d, %d)",
	   rect->x0, rect->y0, rect->x1, rect->y1);
#endif
  container = gzw_find_container (gzw);
  if (container != NULL)
    { 
#ifdef VERBOSE
      g_print (", visible = (%d, %d) - (%d, %d)\n",
	       container->visible.x0, container->visible.y0,
	       container->visible.x1, container->visible.y1);
#endif
      widget = container->widget;
      gzw_paint_to_screen (widget, container, rect, paint);
      page = (GzwPage *)gzw;

      /* todo: this should probably be in the realize method, not here. */
      if (page->gc == NULL) {
	page->gc = gdk_gc_new (widget->window);
      }

      /* get x and y from gzw's allocation */
      x = page->gzw.allocation.x0;
      y = page->gzw.allocation.y0;

      gzw_page_expose_lines (page, rect, paint, x, y);
      gzw_paint_finish_screen (paint);
    }
#ifdef VERBOSE
  else
    g_print ("\n");
#endif
}

/* Find a link given a coordinate location relative to the window */

static gint
gzw_page_find_link (GzwPage *page, gint x, gint y)
{
  gint line_index;
  gint word_index;
  gint x_cursor;
  gint x1, y1; /* coordinates relative to page */
  GzwPageLine *line;
  GzwPageWord *word;
  GzwContainer *container;

  container = gzw_find_container (&page->gzw);
  if (container == NULL)
    return -1;

  x1 = x - (container->x_offset + page->gzw.allocation.x0);
  y1 = y - (container->y_offset + page->gzw.allocation.y0);

  line_index = gzw_page_find_line_index (page, y1);
  if (line_index >= page->num_lines) return -1;
  line = &page->lines[line_index];
  x_cursor = gzw_page_line_x_offset (page, line);
  for (word_index = 0; word_index < line->num_words; word_index++)
    {
      word = &line->words[word_index];
      if (x_cursor <= x1 && x_cursor + word->x_size + word->x_space > x1)
	return page->attrs[word->attr].link;
      x_cursor += word->x_size + word->x_space;
    }
  return -1;
}

static void
gzw_page_handle_event (Gzw *gzw, GdkEvent *event)
{
  GzwPage *page = (GzwPage *)gzw;
  GdkEventButton *button;
  GdkEventMotion *motion;
  gint hover_link;

  switch (event->type)
    {
    case GDK_BUTTON_PRESS:
      button = (GdkEventButton *)event;

#ifdef VERBOSE
      g_print ("gzw_page_handle_event: button (%g, %g) +%d\n",
	       button->x, button->y, button->button);
#endif
      
      /* todo: I think we need to do a grab. Do we do it here, or in
	 the gtk_gzw_view? */
      
      if (button->button == 1)
	{
	  page->link_pressed = gzw_page_find_link (page,
						   button->x, button->y);
#ifdef VERBOSE
	  g_print ("link pressed = %d\n",
		   page->link_pressed);
#endif
	}
      break;
    case GDK_BUTTON_RELEASE:
      button = (GdkEventButton *)event;
      
#ifdef VERBOSE
      g_print ("gzw_page_handle_event: button (%g, %g) -%d\n",
	       button->x, button->y, button->button);
#endif

      if (button->button == 1)
	{
	  if (page->link_pressed >= 0 &&
	      page->link_pressed == gzw_page_find_link (page,
							button->x, button->y))
	    {
#ifdef VERBOSE
	      g_print ("gzw_page_handle_event: link %s\n",
		       page->links[page->link_pressed].url);
#endif
	      if (page->link != NULL)
		(*page->link) (page->link_data,
			       page->links[page->link_pressed].url);
	    }
	  page->link_pressed = -1;
	}
      break;
    case GDK_MOTION_NOTIFY:
      motion = (GdkEventMotion *)event;


      hover_link = gzw_page_find_link (page, motion->x, motion->y);
      if (page->status != NULL && page->hover_link != hover_link)
	{
	  if (hover_link >= 0)
	    (*page->status) (page->status_data,
			     page->links[hover_link].url);
	  else
	    (*page->status) (page->status_data, "");
	  page->hover_link = hover_link;
	}
      
      break;
    default:
#ifdef VERBOSE
      g_print ("gzw_page_handle_event: unknown event (%d)\n",
	       event->type);
#endif
      break;
    }
}

static void
gzw_page_gtk_foreach (Gzw *gzw, GzwCallback callback, gpointer callback_data,
		      GzwRect *plus, GzwRect *minus)
{
  /* outline: find the first line overlapping the plus rectangle. iterate
     through until the bottom of the plus rectangle is reached. Simply
     pass the minus rectangle to the children.

     Only call child if GZW_FLAG_GTK_EMBED is set? (this would require
     doing a resize_request when an embedded widget got added -- is it
     really an important optimization considering we're already
     pruning by geometry?) */
  GzwPageLine *line;
  GzwPageWord *word;
  gint line_index;
  gint word_index;
  GzwRect child_rect;

  line_index = gzw_page_find_line_index ((GzwPage *)gzw,
					 plus->y0 -
					 gzw->allocation.y0);
  for (; line_index < ((GzwPage *)gzw)->num_lines; line_index++)
    {
      line = &((GzwPage *)gzw)->lines[line_index];
      if (gzw->allocation.y0 + line->y_top >= plus->y1)
	break;
      for (word_index = 0; word_index < line->num_words; word_index++)
	{
	  word = &line->words[word_index];
	  if (word->content_type == GZW_PAGE_CONTENT_WIDGET)
	    {
	      gzw_rect_intersect (&child_rect, plus,
				  &word->content.widget->allocation);
	      if (!gzw_rect_empty (&child_rect))
		gzw_gtk_foreach (word->content.widget,
				      callback, callback_data,
				      plus, minus);
	    }
	}
    }
}

static void
gzw_page_destroy (Gzw *gzw)
{
  GzwPage *page;
  GzwPageLine *line;
  GzwPageWord *word;
  gint line_index;
  gint word_index;
  gint font_index;
  gint link_index;

  page = (GzwPage *)gzw;
  for (line_index = 0; line_index < page->num_lines; line_index++)
    {
      line = &page->lines[line_index];
      for (word_index = 0; word_index < line->num_words; word_index++)
	{
	  word = &line->words[word_index];
	  if (word->content_type == GZW_PAGE_CONTENT_WIDGET)
	    gzw_destroy (word->content.widget);
	  else if (word->content_type == GZW_PAGE_CONTENT_TEXT)
	    g_free (word->content.text);
	}
      g_free (line->words);
    }
  g_free (page->lines);

#ifndef USE_TYPE1
  for (font_index = 0; font_index < page->num_fonts; font_index++)
    {
      g_free (page->fonts[font_index].name);
      if (page->fonts[font_index].font != NULL)
	gdk_font_unref (page->fonts[font_index].font);
    }
#endif
  g_free (page->fonts);
  g_free (page->colors);
  g_free (page->attrs);
  for (link_index = 0; link_index < page->num_links; link_index++)
    g_free (page->links[link_index].url);
  g_free (page->links);

  g_free (gzw);
}

static void
gzw_page_request_resize (Gzw *gzw, Gzw *child)
{
  /* ok, here's roughly how the resize request will go.

     We find the line that the child belongs to, and recalculate the
     width requisitions for the whole widget. If the child is
     shrinking and the child's old req_width_min is equal to the parent's,
     then we may have to rescan the whole widget. Otherwise, it should
     be possible to skip that step. Similarly, if the line's old width
     is equal to the parent's, that's also reason for a rescan.

     Then, set a flag saying that there will be child widget resizes.
     size_nego_x can find the child widgets and recalculate the
     height of the appropriate line. How to find the child widget?
     Keep a list of children with pending resize requests?

     For now, we could just rewrap the whole page, which is what
     gtk_page used to do.

     */

  gzw_request_parent_resize (gzw);
}

static const GzwClass gzw_page_class =
{
  gzw_page_size_nego_x,
  gzw_page_size_nego_y,
  gzw_page_paint,
  gzw_page_handle_event,
  gzw_page_gtk_foreach,
  gzw_page_destroy,
  gzw_page_request_resize
};

Gzw *
gzw_page_new (void)
{
  GzwPage *gzw_page;
  GzwRect null_rect = {0, 0, 0, 0};

  gzw_page = g_new (GzwPage, 1);
  gzw_page->gzw.klass = &gzw_page_class;
  gzw_page->gzw.allocation = null_rect;
  gzw_page->gzw.req_width_min = 0;
  gzw_page->gzw.req_width_max = 0;
  gzw_page->gzw.req_height = 0;

  gzw_page->gzw.flags = 0;
  gzw_page->gzw.parent = NULL;
  gzw_page->gzw.container = NULL;

  /* stuff from gtk_page_init. */

  gzw_page->gc = NULL;

  gzw_page->num_lines_max = 16;
  gzw_page->num_lines = 0;
  gzw_page->lines = g_new (GzwPageLine, gzw_page->num_lines_max);

  gzw_page->num_fonts_max = 16;
  gzw_page->num_fonts = 0;
  gzw_page->fonts = g_new (GzwPageFont, gzw_page->num_fonts_max);

  gzw_page->num_links_max = 16;
  gzw_page->num_links = 0;
  gzw_page->links = g_new (GzwPageLink, gzw_page->num_links_max);

  gzw_page->num_colors_max = 16;
  gzw_page->num_colors = 0;
  gzw_page->colors = g_new (GdkColor, gzw_page->num_colors_max);

  gzw_page->num_attrs_max = 16;
  gzw_page->num_attrs = 0;
  gzw_page->attrs = g_new (GzwPageAttr, gzw_page->num_attrs_max);

  gzw_page->width = 100;

  gzw_page->last_line_max_width = 0;

  gzw_page->hover_link = -1;
  /* end stuff from gtk_page_init. */

  gzw_page->link = NULL;
  gzw_page->status = NULL;

  return (Gzw *)gzw_page;
}

void
gzw_page_set_callbacks (GzwPage *page,
			void (*link) (void *data, char *url),
			void *link_data,
			void (*status) (void *data, char *url),
			void *status_data)
{
  page->link = link;
  page->link_data = link_data;
  page->status = status;
  page->status_data = status_data;
}


/* Find the color in the page structure, or add a new one if it doesn't
   already exist. In either case, return the index. */

gint gzw_page_find_color (GzwPage *page, gint32 color) {
  gint color_index;
  gint red, green, blue;

  red = (color >> 16) & 255;
  red |= red << 8;
  green = (color >> 8) & 255;
  green |= green << 8;
  blue = color & 255;
  blue |= blue << 8;
  for (color_index = 0; color_index < page->num_colors; color_index++) {
    if (page->colors[color_index].red == red &&
	page->colors[color_index].green == green &&
	page->colors[color_index].blue == blue) {
      return color_index;
    }
  }
  if (page->num_colors == page->num_colors_max ) {
    page->num_colors_max <<= 1;
    page->colors = g_realloc (page->colors,
			      page->num_colors_max * sizeof (GdkColor));
  }
  page->colors[color_index].pixel = -1;
  page->colors[color_index].red = red;
  page->colors[color_index].green = green;
  page->colors[color_index].blue = blue;
  page->num_colors++;
  return color_index;
}

/* Set all attribute fields except font to reasonable defaults. */

void gzw_page_init_attr (GzwPage *page, GzwPageAttr *attr) {
  attr->link = -1;
  attr->color = gzw_page_find_color (page, 0);
  attr->left_indent_first = 0;
  attr->left_indent_rest = 0;
  attr->right_indent = 0;
  attr->align = GZW_PAGE_ALIGN_LEFT;
}

/* Find the font in the page structure, or add a new one if it doesn't
   already exist. In either case, return the index. */

gint gzw_page_find_font (GzwPage *page, const GzwPageFont *font) {
  gint font_index;

  for (font_index = 0; font_index < page->num_fonts; font_index++) {
    if (page->fonts[font_index].size == font->size &&
	page->fonts[font_index].bold == font->bold &&
	page->fonts[font_index].italic == font->italic &&
	!strcmp (page->fonts[font_index].name, font->name)) {
      return font_index;
    }
  }
  if (page->num_fonts == page->num_fonts_max ) {
    page->num_fonts_max <<= 1;
    page->fonts = g_realloc (page->fonts,
			     page->num_fonts_max * sizeof (GzwPageFont));
  }
  page->fonts[font_index].name = g_strdup (font->name);
  page->fonts[font_index].size = font->size;
  page->fonts[font_index].bold = font->bold;
  page->fonts[font_index].italic = font->italic;
  page->fonts[font_index].font = NULL;
  page->fonts[font_index].space_width = 0;
  page->num_fonts++;
  return font_index;
}

/* Create a new link, return the index. */
gint gzw_page_new_link (GzwPage *page, const char *url) {
  if (page->num_links == page->num_links_max ) {
    page->num_links_max <<= 1;
    page->links = g_realloc (page->links,
			     page->num_links_max * sizeof (GzwPageLink));
  }
  page->links[page->num_links].url = g_strdup (url);
  return page->num_links++;
}

/* Find the attr in the page structure, or add a new one if it doesn't
   already exist. In either case, return the index. */

gint gzw_page_find_attr (GzwPage *page, const GzwPageAttr *attr) {
  gint attr_index;

  for (attr_index = 0; attr_index < page->num_attrs; attr_index++) {
    if (page->attrs[attr_index].font == attr->font &&
	page->attrs[attr_index].link == attr->link &&
	page->attrs[attr_index].color == attr->color &&
	page->attrs[attr_index].left_indent_first == attr->left_indent_first &&
	page->attrs[attr_index].left_indent_rest == attr->left_indent_rest &&
	page->attrs[attr_index].right_indent == attr->right_indent &&
	page->attrs[attr_index].align == attr->align) {
      return attr_index;
    }
  }
  if (page->num_attrs == page->num_attrs_max ) {
    page->num_attrs_max <<= 1;
    page->attrs = g_realloc (page->attrs,
			     page->num_attrs_max * sizeof (GzwPageAttr));
  }
  page->attrs[attr_index].font = attr->font;
  page->attrs[attr_index].link = attr->link;
  page->attrs[attr_index].color = attr->color;
  page->attrs[attr_index].left_indent_first = attr->left_indent_first;
  page->attrs[attr_index].left_indent_rest = attr->left_indent_rest;
  page->attrs[attr_index].right_indent = attr->right_indent;
  page->attrs[attr_index].align = attr->align;
  page->num_attrs++;
  return attr_index;
}


/* Load the gdk font for the font data structure if necessary. In either
   case, return the gdk font. (NULL if using t1lib fonts) */

static GdkFont *gzw_page_realize_font (GzwPageFont *font) {
  char fontname[256];
  if (font->font != NULL)
    return font->font;

  sprintf (fontname, "-*-%s-%s-%s-*-*-%d-*-75-75-*-*-*-*",
	   font->name,
	   font->bold ? "bold" : "medium",
	   font->italic ? "i" : "r",
	   font->size);
  font->font = gdk_font_load (fontname);

  if (font->font == NULL && font->italic) {
    /* Some italic fonts (e.g. Courier) use "o" instead of "i". */
    sprintf (fontname, "-*-%s-%s-o-*-*-%d-*-75-75-*-*-*-*",
	     font->name,
	     font->bold ? "bold" : "medium",
	     font->size);
    font->font = gdk_font_load (fontname);
  }

  if (font->font == NULL) {
    /* Can't load the font - substitute the default instead. */
    font->font = gdk_font_load ("-adobe-helvetica-medium-r-normal--*-100-*-*-*-*-*-*");
  }

  if (font->font == NULL) {
    g_print ("Can't load any fonts!\n");
    /* Should probably abort here. */
  }

  font->space_width = gdk_char_width (font->font, ' ');
  return font->font;
}



/* Add a word to the page structure. Stashes the argument pointer in
   the page data structure so that it will be deallocated on destroy.
   */

void gzw_page_add_text (GzwPage *page, char *text, gint attr) {
  GzwPageWord *word;
  gint x_size, y_ascent, y_descent;
  GzwPageFont *font;
  GdkFont *gdkfont;

  font = &(page->fonts[page->attrs[attr].font]);
  gdkfont = gzw_page_realize_font (font);

  x_size = gdk_string_width (gdkfont, text);
  y_ascent = gdkfont->ascent;
  y_descent = gdkfont->descent;

  word = gzw_page_new_word (page, x_size, y_ascent, y_descent, attr);
  word->content_type = GZW_PAGE_CONTENT_TEXT;
  word->content.text = text;
  word->attr = attr;
}

void
gzw_page_add_widget (GzwPage *page,
		     Gzw *gzw,
		     gint attr)
{
  GzwPageWord *word;
  gint x_size, y_ascent, y_descent;

  /* First, find the child widget's width. For now, we always
     give the minimum requisition, but that will change with table
     support (tables should probably get 100% of the set width - the
     margins, or the maximum requisition, whichever is smaller). */

  x_size = gzw->req_width_min;
  gzw_size_nego_x (gzw, x_size);
  if (gzw->flags & GZW_FLAG_BASELINE)
    {
      y_ascent = gzw->req_ascent;
      y_descent = gzw->req_height - y_ascent;
    }
  else
    {
      /* align bottom of widget to baseline for now */
      y_ascent = gzw->req_height;
      y_descent = 0;
    }
  word = gzw_page_new_word (page, x_size, y_ascent, y_descent, attr);

  word->content_type = GZW_PAGE_CONTENT_WIDGET;
  word->content.widget = gzw;
  word->attr = attr;

  gzw->parent = &page->gzw;

  /* The rest of the size negotiation continues in finish_line. */
}




void gzw_page_add_space (GzwPage *page, gint attr) {
  gint i, j;
  gint space;
  GzwPageFont *font;
  GdkFont *gdkfont;

  font = &(page->fonts[page->attrs[attr].font]);
  gdkfont = gzw_page_realize_font (font);

  i = page->num_lines - 1;
  if (i >= 0) {
    j = page->lines[i].num_words - 1;
    if (j >= 0) {
      space = font->space_width;
      page->lines[i].words[j].x_space = space;
    }
  }
}

/* Cause a line break */

void gzw_page_linebreak (GzwPage *page) {
  int i;

  i = page->num_lines;
  if (i == 0) return;
  page->lines[i - 1].hard = TRUE;
}

/* Cause a paragraph break */
void gzw_page_parbreak (GzwPage *page, gint space) {
  int i;

  i = page->num_lines;
  if (i == 0) return;
  page->lines[i - 1].hard = TRUE;
  if (space > page->lines[i - 1].y_space)
    page->lines[i - 1].y_space = space;
}

/* Call this routine before updating the state. By wrapping all state
   changes between these two routines, the internal state of the widget
   always matches the display, without requiring each state change
   operation to calculate updates on a fine-grained basis. */

void
gzw_page_update_begin (GzwPage *page)
{
  page->redraw_start_line = page->num_lines;
  if (page->num_lines > 0 && !page->lines[page->num_lines - 1].hard)
    page->redraw_start_line--;
}

/* Call this routine after updating the state. */
void
gzw_page_update_end (GzwPage *page)
{
  GzwRect repaint;
  gint last_line;

  if (page->num_lines > 0)
    gzw_page_finish_line (page);

  /* Request repaint of all lines from redraw_start_line to the last
     line in the page (inclusive). */
  last_line = page->num_lines - 1;
  if (page->redraw_start_line <= last_line)
    {
      repaint.x0 = page->gzw.allocation.x0;
      repaint.y0 = page->gzw.allocation.y0 +
	page->lines[page->redraw_start_line].y_top;
      repaint.x1 = page->gzw.allocation.x1;
      repaint.y1 = page->gzw.allocation.y0 + page->lines[last_line].y_top +
	page->lines[last_line].y_ascent + page->lines[last_line].y_descent;
      gzw_request_paint (&page->gzw, &repaint);
    }
  gzw_request_parent_resize (&page->gzw);
}




/* Set the width. Cause a rewrap and check_resize if necessary. */

void gzw_page_set_width (GzwPage *page, gint width) {
  gint new_width;

  new_width = width - 2 * GTK_CONTAINER (page)->border_width;
  if (new_width != page->width) {
    page->width = new_width;
    gzw_page_rewrap (page);

  }
}


