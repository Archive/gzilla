/* A gzw border. */

#include <gtk/gtk.h>
#include "gzw.h"
#include "gzwborder.h"

static void
gzw_border_size_nego_x (Gzw *gzw, gint width)
{
  gint border;

  border = ((GzwBorder *)gzw)->border;
  gzw_size_nego_x (((GzwBorder *)gzw)->child, width - border * 2);
  gzw->req_height = ((GzwBorder *)gzw)->child->req_height + border * 2;
  gzw->req_ascent = ((GzwBorder *)gzw)->child->req_ascent + border * 2;
}

static void
gzw_border_size_nego_y (Gzw *gzw, GzwRect *allocation)
{
  GzwRect child_allocation;
  gint border;
  GzwRect repaint;

  border = ((GzwBorder *)gzw)->border;

  /* Request incremental repaints. */
  if (allocation->x0 == gzw->allocation.x0 &&
      allocation->y0 == gzw->allocation.y0)
    {
      if (allocation->x1 != gzw->allocation.x1)
	{
	  repaint.x0 = MIN (allocation->x1, gzw->allocation.x1) - border;
	  repaint.y0 = allocation->y0;
	  repaint.x1 = allocation->x1;
	  repaint.y1 = allocation->y1;
	  gzw_request_paint (gzw, &repaint);
	}
      if (allocation->y1 != gzw->allocation.y1)
	{
	  repaint.x0 = allocation->x0;
	  repaint.y0 = MIN (allocation->y1, gzw->allocation.y1) - border;
	  repaint.x1 = allocation->x1;
	  repaint.y1 = allocation->y1;
#ifdef VERBOSE
	  g_print ("gzw_border_size_nego_y: request (%d, %d) - (%d, %d)\n",
		   repaint.x0,
		   repaint.y0,
		   repaint.x1,
		   repaint.y1);
#endif
	  gzw_request_paint (gzw, &repaint);
	}
    }

  gzw->allocation = *allocation;
  child_allocation.x0 = allocation->x0 + border;
  child_allocation.x1 = allocation->x1 - border;
  child_allocation.y0 = allocation->y0 + border;
  child_allocation.y1 = allocation->y1 - border;
  gzw_size_nego_y (((GzwBorder *)gzw)->child, &child_allocation);
}

static void
gzw_border_paint (Gzw *gzw, GzwRect *rect, GzwPaint *paint)
{
  GtkWidget *widget;
  GzwContainer *container;
  GzwRect child_rect;

#ifdef VERBOSE
  g_print ("gzw_border_paint (%d, %d) - (%d, %d)",
	   rect->x0, rect->y0, rect->x1, rect->y1);
#endif
  container = gzw_find_container (gzw);
  if (container != NULL)
    {
#ifdef VERBOSE
      g_print (", visible = (%d, %d) - (%d, %d)\n",
	       container->visible.x0, container->visible.y0,
	       container->visible.x1, container->visible.y1);
#endif
      widget = container->widget;
      gzw_paint_to_screen (widget, container, rect, paint);
      gzw_rect_intersect (&child_rect, rect,
			  &((GzwBorder *)gzw)->child->allocation);
      if (!gzw_rect_empty (&child_rect))
	{
	  gzw_paint (((GzwBorder *)gzw)->child, &child_rect, paint);
	  gzw_paint_to_screen (widget, container, &child_rect, paint);
	}
      gzw_paint_finish_screen (paint);
    }
#ifdef VERBOSE
  else
    g_print ("\n");
#endif
}

static void
gzw_border_handle_event (Gzw *gzw, GdkEvent *event)
{
  gzw_handle_event (((GzwBorder *)gzw)->child, event);
}

static void
gzw_border_gtk_foreach (Gzw *gzw, GzwCallback callback, gpointer callback_data,
			GzwRect *plus, GzwRect *minus)
{
  gzw_gtk_foreach (((GzwBorder *)gzw)->child, callback, callback_data,
		   plus, minus);
}

static void
gzw_border_destroy (Gzw *gzw)
{
  gzw_destroy (((GzwBorder *)gzw)->child);
  g_free (gzw);
}

static void
gzw_border_request_resize (Gzw *gzw, Gzw *child)
{
  gint border;

  border = ((GzwBorder *)gzw)->border;
  gzw->req_width_min = ((GzwBorder *)gzw)->child->req_width_min + border * 2;
  gzw->req_width_max = ((GzwBorder *)gzw)->child->req_width_max + border * 2;
  gzw_request_parent_resize (gzw);
}

static const GzwClass gzw_border_class =
{
  gzw_border_size_nego_x,
  gzw_border_size_nego_y,
  gzw_border_paint,
  gzw_border_handle_event,
  gzw_border_gtk_foreach,
  gzw_border_destroy,
  gzw_border_request_resize
};

Gzw *
gzw_border_new (Gzw *child, gint border)
{
  GzwBorder *gzw_border;
  GzwRect null_rect = {0, 0, 0, 0};

  gzw_border = g_new (GzwBorder, 1);
  gzw_border->gzw.klass = &gzw_border_class;
  gzw_border->gzw.allocation = null_rect;
  gzw_border->gzw.req_width_min = child->req_width_min + border * 2;
  gzw_border->gzw.req_width_max = child->req_width_max + border * 2;
  gzw_border->gzw.flags = child->flags &
    (GZW_FLAG_BASELINE | GZW_FLAG_GTK_EMBED);
  gzw_border->gzw.parent = NULL;
  gzw_border->gzw.container = NULL;

  gzw_border->child = child;
  gzw_border->border = border;

  child->parent = (Gzw *)gzw_border;

  return (Gzw *)gzw_border;
}
