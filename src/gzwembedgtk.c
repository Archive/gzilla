/* A Gzw widget that embeds gtk widgets inside. */

/* todo: deal with the child widget resizing. */

#include <gtk/gtk.h>
#include "gzw.h"
#include "gzwembedgtk.h"

static void
gzw_embed_gtk_size_nego_y (Gzw *gzw, GzwRect *allocation)
{
  GzwContainer *container;
  GtkAllocation child_allocation;

  gzw->allocation = *allocation;
  container = gzw_find_container (gzw);
  if (container != NULL)
    {
      child_allocation.x = container->x_offset + gzw->allocation.x0;
      child_allocation.y = container->y_offset + gzw->allocation.y0;
      child_allocation.width = gzw->allocation.x1 - gzw->allocation.x0;
      child_allocation.height = gzw->allocation.y1 - gzw->allocation.y0;
      gtk_widget_size_allocate (((GzwEmbedGtk *)gzw)->widget,
				&child_allocation);
    }
}

static void
gzw_embed_gtk_paint (Gzw *gzw, GzwRect *rect, GzwPaint *paint)
{
  GzwContainer *container;
  GtkAllocation child_allocation;
  GtkWidget *child;
  GdkEventExpose child_event;

#ifdef VERBOSE
  g_print ("gzw_embed_gtk_paint (%d, %d) - (%d, %d)\n",
	   rect->x0, rect->y0, rect->x1, rect->y1);
#endif
  container = gzw_find_container (gzw);
  g_return_if_fail (container != NULL);
  child = ((GzwEmbedGtk *)gzw)->widget;
  if (!GTK_WIDGET_REALIZED (child))
    {
      /* Need to size allocate and realize the child widget. */
      gtk_widget_set_parent (child, container->widget);
      child_allocation.x = container->x_offset + gzw->allocation.x0;
      child_allocation.y = container->y_offset + gzw->allocation.y0;
      child_allocation.width = gzw->allocation.x1 - gzw->allocation.x0;
      child_allocation.height = gzw->allocation.y1 - gzw->allocation.y0;
      gtk_widget_size_allocate (child, &child_allocation);
      gtk_widget_realize (child);
    }
  if (!GTK_WIDGET_MAPPED (child))
    gtk_widget_map (child);
  if (GTK_WIDGET_NO_WINDOW (child))
    {
      /* this call may not be necessary - it may be ok to call
	 expose of the child widget with the screen in a "don't care"
	 state. */
      gzw_paint_to_screen (container->widget, container, rect, paint);

      /* create a child expose event */
      child_event.type = GDK_EXPOSE;
      child_event.window = container->widget->window;
      child_event.area.x = container->x_offset + rect->x0;
      child_event.area.y = container->y_offset + rect->x0;
      child_event.area.width = rect->x1 - rect->x0;
      child_event.area.height = rect->y1 - rect->y0;
      child_event.count = 0; /* this cheats slightly */

      gtk_widget_event (child, (GdkEvent *) &child_event);

      gzw_paint_finish_screen (paint);
    }
}

static void
gzw_embed_gtk_foreach (Gzw *gzw, GzwCallback callback, gpointer callback_data,
		       GzwRect *plus, GzwRect *minus)
{
  GzwContainer *container;
  GzwRect test_rect;
  GtkAllocation child_allocation;

  gzw_rect_intersect (&test_rect, plus, &gzw->allocation);
  if (gzw_rect_empty (&test_rect))
    return;
  gzw_rect_intersect (&test_rect, minus, &gzw->allocation);
  if (!gzw_rect_empty (&test_rect))
    return;
  container = gzw_find_container (gzw);
  child_allocation.x = container->x_offset + gzw->allocation.x0;
  child_allocation.y = container->y_offset + gzw->allocation.y0;
  child_allocation.width = gzw->allocation.x1 - gzw->allocation.x0;
  child_allocation.height = gzw->allocation.y1 - gzw->allocation.y0;
  (*callback) (((GzwEmbedGtk *) gzw)->widget, callback_data,
	       &child_allocation);
}

static void
gzw_embed_gtk_destroy (Gzw *gzw)
{
#if 0
  gtk_widget_unparent (((GzwEmbedGtk *)gzw)->widget);
#endif
  gtk_widget_destroy (((GzwEmbedGtk *)gzw)->widget);
  g_free (gzw);
}

static const GzwClass gzw_embed_gtk_class =
{
  gzw_null_size_nego_x,
  gzw_embed_gtk_size_nego_y,
  gzw_embed_gtk_paint,
  gzw_null_handle_event,
  gzw_embed_gtk_foreach,
  gzw_embed_gtk_destroy,
  gzw_null_request_resize
};

Gzw *
gzw_embed_gtk_new (GtkWidget *widget)
{
  GzwEmbedGtk *gzw_embed_gtk;
  GzwRect null_rect = {0, 0, 0, 0};

  gzw_embed_gtk = g_new (GzwEmbedGtk, 1);
  gzw_embed_gtk->gzw.klass = &gzw_embed_gtk_class;
  gzw_embed_gtk->gzw.allocation = null_rect;

  /* We start the size negotiation now, so that we can fill in the
     requisition. */

  /* todo: we're not handling the case well where the widget is not
     visible. */

  gtk_widget_size_request (widget, &widget->requisition);

  gzw_embed_gtk->gzw.req_width_min = widget->requisition.width;
  gzw_embed_gtk->gzw.req_width_max = widget->requisition.width;

  gzw_embed_gtk->gzw.req_height = widget->requisition.height;
  gzw_embed_gtk->gzw.req_ascent = 0;

  gzw_embed_gtk->gzw.flags = 0;
  gzw_embed_gtk->gzw.parent = NULL;
  gzw_embed_gtk->gzw.container = NULL;

  gzw_embed_gtk->widget = widget;

  /* todo: how to handle the widget's request_resize operation? It is
     generally handled in the toplevel Gtk container, but that leaves
     no way to resolve from the Gtk widget to "us", i.e. the specific
     Gzw widget that embeds the Gtk widget.

     It may be that the only real solution is for the container to keep
     a list of Gtk widgets it contains. If this is done, it may obviate
     the need for the whole gtk_foreach mechanism.

     A totally reasonable data structure for holding the list would be
     sorted by y coordinate.
     */

  return &gzw_embed_gtk->gzw;
}
