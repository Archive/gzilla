#ifndef __GZILLA_CACHE_H__
#define __GZILLA_CACHE_H__

struct _GzillaCacheLine {
  char *url;
  gint size;
  gint buf_size;
  char *buf;
  gint last_use;
  GzillaByteSink *bytesink;    /* the bytesink that feeds us */
  /* it's possible we should put a list of connections here */
  gboolean eof;
};

typedef struct _GzillaCacheLine GzillaCacheLine;

struct _GzillaCacheCon {
  GzillaCacheLine *line;
  GzillaByteSink *bytesink;   /* The bytesink we're feeding */
  /* status_id is so we can disconnect the status signal. Do we still
     need to do this? I don't think so. */
  gint status_id;
  gint index;
  /* invariant: line->eof implies index < line->size */
};

typedef struct _GzillaCacheCon GzillaCacheCon;

void gzilla_cache_init (void);
void gzilla_cache_get_url (GzillaByteSink *bytesink, const char *url);
void gzilla_cache_remove_url (char *url);

#endif /* __GZILLA_CACHE_H__ */
