
#ifndef __GZILLA_BROWSER_H__
#define __GZILLA_BROWSER_H__

#include "gzillabytesink.h"
#include "gzillalinkblock.h"

typedef struct _GzillaBookmark GzillaBookmark;

typedef struct 
{
    char *url;
    char *title;
    /* maybe put some more stuff here:
     * + cached widgets 
     * + form data
     * + scroll position
     * what about char *page_title ?  
     */
} GzillaNav;

struct _GzillaBookmark {
  char *title;
  char *url;
  GtkWidget *menuitem;
};

/* browswer_window contains all widgets to create a single window */
typedef struct
{
    /* widgets for the main window */
    GtkWidget *main_window;
    GtkWidget *back_button;
    GtkWidget *forw_button;
    GtkWidget *stop_button;
    GtkWidget *location;
    GtkWidget *status;
     
  /* the keyboard accelerator table */
#ifdef GTK_HAVE_FEATURES_1_1_0
  GtkAccelGroup *accel_group;
#else
  GtkAcceleratorTable *accel_table;
#endif

  /* individual menuitems so their sensitivity can be set. */
  GtkWidget *back_menuitem;
  GtkWidget *forw_menuitem;
  GtkWidget *stop_menuitem;

  /* the bookmarks menu so that we can add things to it. */
  GtkWidget *bookmarks_menu;

  GzillaBookmark *bookmarks;
  gint num_bookmarks;
  gint num_bookmarks_max;

  /* umm.. heh.. */
  GtkWidget *docwin;
#if 0
    GzillaByteSink *doc;
    GtkContainer *doc_widget;
#endif

  /* pointers to bytesinks for all active connections in the window */
  GzillaByteSink **bytesinks;
  gint num_bytesinks;
  gint num_bytesinks_max;

  /* pointers to imgsinks for all active connections in the window */
  GzillaImgSink **imgsinks;
  gint num_imgsinks;
  gint num_imgsinks_max;

  /* widgets for dialog boxes off main window */
  GtkWidget *open_dialog_window;
  GtkWidget *open_dialog_entry;
  GtkWidget *openfile_dialog_window;
  GtkWidget *quit_dialog_window;

  /* Note: the url duplicates information that belongs to gzilla_nav. It is
   * likely that the page title should be moved there too. -Raph */
  /* I put the page title in the GzillaNav struct above. -Ian */
  GzillaNav *gzilla_nav;
  gint size_gzilla_nav_max;
  gint size_gzilla_nav;
  gint gzilla_nav_ptr;

  /* The gzilla_nav_ptr refers to what's being displayed. When the
     user clicks a link, that one doesn't change, but this one gets
     set ahead. When the document finally loads, the gzilla_nav_ptr is
     set to reflect this one. */
  gint gzilla_nav_loading_ptr;

  /* last_waiting is true if the last gzilla_nav is being loaded for
     the first time and has not gotten the gzw.

     last_waiting implies loading_ptr = size and ptr = size - 1.
     not last_waiting implies loading_ptr = ptr. */
  gboolean last_waiting;

  char *redir_url;

  /* The tag for the idle function that sets button sensitivity. */
  gint sens_idle_tag;

  /* This is the linkblock of the page currently being displayed. */
  GzillaLinkBlock *linkblock;

  /* This is the linkblock of the page we're waiting on a have_gzw. */
  GzillaLinkBlock *linkblock_loading;
} BrowserWindow;
    
/* typedef struct BrowserWindow _BrowserWindow; */

extern BrowserWindow **browser_window;
extern gint num_bw;

#endif /* __GZILLA_BROWSER_H__ */

