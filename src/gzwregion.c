 /*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

/* An implementation of the region abstract data type.

   This code is adapted from xink.

   */

#include <gtk/gtk.h>
#include "gzwregion.h"

#ifndef INF
#define INF 0x7fffffff
#endif

typedef struct range {
  int x0, x1;
} range;

typedef struct band {
  int refcount;
  int y0, y1;
  int n_ranges;
  range range[1];
} band;

/* An invariant: band[i]->y1 <= band[i+1]->y0 */
struct _GzwRegion {
  int refcount;
  int n_bands;
  band *band[1];
};

/* These two routines might get moved into a common "util" file. */
static int
min (int a, int b)
{
  return (a > b) ? b : a;
}

static int
max (int a, int b)
{
  return (a > b) ? a : b;
}

static band *
band_pin (band *band)
{
  band->refcount++;
  return band;
}

static void
band_drop (band *band)
{
  if (--(band->refcount) == 0)
    g_free (band);
}

GzwRegion *
gzw_region_pin (GzwRegion *region)
{
  region->refcount++;
  return region;
}

void
gzw_region_drop (GzwRegion *region)
{
  gint i;

  if (--(region->refcount) == 0) {
    for (i = 0; i < region->n_bands; i++)
      band_drop (region->band[i]);
    g_free (region);
  }
}

GzwRegion *
gzw_region_from_rect (const GzwRect *src)
{
  GzwRegion *r;
  band *b;

  if (gzw_rect_empty (src)) {
    /* Create the empty region. */
    r = (GzwRegion *)g_malloc (sizeof(GzwRegion) - sizeof(band *));
    r->refcount = 1;
    r->n_bands = 0;
  } else {
    r = (GzwRegion *)g_malloc (sizeof(GzwRegion));
    r->refcount = 1;
    r->n_bands = 1;
    b = (band *)g_malloc (sizeof(band));
    r->band[0] = b;
    b->refcount = 1;
    b->y0 = src->y0;
    b->y1 = src->y1;
    b->n_ranges = 1;
    b->range[0].x0 = src->x0;
    b->range[0].x1 = src->x1;
  }
  return r;
}

gboolean
gzw_region_empty (const GzwRegion *region)
{
  return (region->n_bands == 0);
}

/* Determine whether the x projections of two bands are equal.
   Does not drop its arguments. */
static gboolean
band_eq (const band *b1, const band *b2) {
  gint i, n;

  n = b1->n_ranges;
  if (n != b2->n_ranges) return FALSE;
  for (i = 0; i < n; i++)
    if (b1->range[i].x0 != b2->range[i].x0 ||
	b1->range[i].x1 != b2->range[i].x1)
      return FALSE;
  return TRUE;
}

/* Change the band's y projection while maintaining its x projection. */
static band *
band_resize (band *b1, int y0, int y1)
{
  if (b1->refcount != 1) {
    /* Might split this out into a band_dup function if it might get
       used elsewhere. */
    band *b;
    gint i, n;

    if (y0 == b1->y0 && y1 == b1->y1) return b1;
    n = b1->n_ranges;
    b = (band *)g_malloc (sizeof(band) + (n - 1) * sizeof(range));
    b->refcount = 1;
    b->n_ranges = b1->n_ranges;
    for (i = 0; i < n; i++) {
      b->range[i].x0 = b1->range[i].x0;
      b->range[i].x1 = b1->range[i].x1;
    }
    band_drop (b1);
    b1 = b;
  }
  b1->y0 = y0;
  b1->y1 = y1;
  return b1;
}


/* Combine two bands. The x projection of the resulting band is the
   union of the x projections of the two arguments, and the y
   projection is the intersection.

   Doesn't drop the arguments. */
static band *
band_union_combine (band *b1, band *b2)
{
  band *b;
  gint i, i1, i2;
  gint x_last;

  b = (struct band *)g_malloc (sizeof(band) +
			       ((b1->n_ranges + b2->n_ranges) - 1) *
			       sizeof(range));
  b->refcount = 1;
  b->y0 = max (b1->y0, b2->y0);
  b->y1 = min (b1->y1, b2->y1);
  i = 0;
  i1 = 0;
  i2 = 0;
  /* Invariant: b->range[i - 1].x0 < b1->range[i1].x0,
     and similarly for b2->range[i2]. */
  /* Invariant: x_last = b->range[i - 1].x1, or -1 if not defined. */
  x_last = -INF;
  while (i1 < b1->n_ranges || i2 < b2->n_ranges) {
    /* Perhaps the code duplication of these two branches could be
       eliminated. */
    if (i2 == b2->n_ranges ||
	(i1 < b1->n_ranges && b1->range[i1].x0 < b2->range[i2].x0)) {
      /* Add in the b1->range[i1]. */
      if (b1->range[i1].x0 <= x_last) {
	/* New range overlaps with last one. */
	if (b1->range[i1].x1 > x_last)
	  x_last = b->range[i - 1].x1 = b1->range[i1].x1;
      } else {
	b->range[i].x0 = b1->range[i1].x0;
	x_last = b->range[i++].x1 = b1->range[i1].x1;
      }
      i1++;
    } else {
      /* Add in the b2->range[i2]. */
      if (b2->range[i2].x0 <= x_last) {
	/* New range overlaps with last one. */
	if (b2->range[i2].x1 > x_last)
	  x_last = b->range[i - 1].x1 = b2->range[i2].x1;
      } else {
	b->range[i].x0 = b2->range[i2].x0;
	x_last = b->range[i++].x1 = b2->range[i2].x1;
      }
      i2++;
    }
  }
  b->n_ranges = i;
  return b;
}

/* Add a band to a region, simplifying. The psrc and src arguments
   allow the band equality check to be simplified in cases where
   band information is derived from already simplified regions. Codes
   1 and 2 refer to existing regions, and 3 to combination data. */
static void
add_band (GzwRegion *r, band *b, gint *psrc, gint src)
{
  gint n;

  /* Note: this test isn't necessary when being called from region_union,
     but is included here anyway to avoid having two separate versions
     of add_band. */
  if (b->n_ranges == 0) {
    band_drop (b);
    return;
  }
  n = r->n_bands;
  if (n == 0 ||
      (src != 3 && src == *psrc) ||
      !band_eq (r->band[n - 1], b)) {
    r->band[n] = b;
    r->n_bands++;
  } else {
    r->band[n - 1] =
      band_resize (r->band[n - 1], r->band[n - 1]->y0, b->y1);
    band_drop (b);
  }
  *psrc = src;
}

/* Return the union of two regions. */
GzwRegion *
gzw_region_union (GzwRegion *r1, GzwRegion *r2)
{
  GzwRegion *r;
  gint i1, i2;
  gint y;
  gint src;

  /* This bound may not be strict. */
  r = (GzwRegion *)g_malloc (sizeof(GzwRegion) +
			     ((r1->n_bands + r2->n_bands) * 2 - 1) *
			     sizeof(band *));
  r->refcount = 1;
  r->n_bands = 0;
  i1 = 0;
  i2 = 0;
  src = 0;
  /* Invariant: y = r->band[i - 1].y1, or 0 if not defined */
  y = -INF;
  while (i1 < r1->n_bands || i2 < r2->n_bands) {
    if (i2 == r2->n_bands ||
	(i1 < r1->n_bands && r1->band[i1]->y1 <= r2->band[i2]->y0)) {
      add_band (r, band_resize (band_pin (r1->band[i1]),
				 max (y, r1->band[i1]->y0),
				 r1->band[i1]->y1),
		&src, 1);
      y = r1->band[i1++]->y1;
    } else if (i1 == r1->n_bands ||
	       (i2 < r2->n_bands && r2->band[i2]->y1 <= r1->band[i1]->y0)) {
      add_band (r, band_resize (band_pin (r2->band[i2]),
				max (y, r2->band[i2]->y0),
				r2->band[i2]->y1),
		&src, 2);
      y = r2->band[i2++]->y1;
    } else {
      /* Combine two bands - output the next band beginning with y or
	 greater. */

      /* Advance y to y0 of actual next band. */
      y = max (y, min (r1->band[i1]->y0, r2->band[i2]->y0));

      /* Output the part where there is a deficiency in the y
	 projection of the other region. */
      if (r2->band[i2]->y0 > y) {	
        add_band (r, band_resize (band_pin (r1->band[i1]),
				  y,
				  r2->band[i2]->y0),
		  &src, 1);
	y = r2->band[i2]->y0;
      } else if (r1->band[i1]->y0 > y) {
	add_band (r, band_resize (band_pin (r2->band[i2]),
				    y,
				    r1->band[i1]->y0),
		  &src, 2);
	y = r1->band[i1]->y0;
      }

      /* Output the part where the y projections of the two bands
	 overlap. */
      add_band (r, band_union_combine (r1->band[i1], r2->band[i2]), &src, 3);

      /* Advance y to the next band to output. */
      if (r1->band[i1]->y1 < r2->band[i2]->y1)
	y = r1->band[i1++]->y1;
      else if (r1->band[i1]->y1 > r2->band[i2]->y1)
	y = r2->band[i2++]->y1;
      else {
	i1++;
	i2++;
      }
    }
  }
  gzw_region_drop (r1);
  gzw_region_drop (r2);
  return r;
}

/* Combine two bands. The x projection of the resulting band is the
   difference of the x projections of the two arguments, and the y
   projection is the intersection.

   Doesn't drop the arguments. */
static band *
band_minus_combine (band *b1, band *b2) {
  band *b;
  gint i, i1, i2;
  gint x_suppress;

  b = (struct band *)g_malloc (sizeof(band) +
			       ((b1->n_ranges + b2->n_ranges) - 1) *
			       sizeof(range));
  b->refcount = 1;
  b->y0 = max (b1->y0, b2->y0);
  b->y1 = min (b1->y1, b2->y1);
  i = 0;
  i1 = 0;
  i2 = 0;
  /* Invariant: b->range[i - 1].x0 < b1->range[i1].x0,
     and similarly for b2->range[i2]. */
  x_suppress = -INF;
  while (i1 < b1->n_ranges || i2 < b2->n_ranges) {
    if (i2 == b2->n_ranges ||
	(i1 < b1->n_ranges && b1->range[i1].x0 < b2->range[i2].x0)) {
      /* Add in the b1->range[i1]. */
      if (b1->range[i1].x1 > x_suppress) {
	b->range[i].x0 = max (b1->range[i1].x0, x_suppress);
	b->range[i++].x1 = b1->range[i1].x1;
      }
      i1++;
    } else {
      /* Subtract b2->range[i2] from last range. */
      if (i > 0 && b->range[i - 1].x1 > b2->range[i2].x0) {
	/* b2 range overlaps with last range */
	if (b->range[i - 1].x1 > b2->range[i2].x1) {
	  /* Need to split last range in two. */
	  b->range[i].x0 = b2->range[i2].x1;
	  b->range[i].x1 = b->range[i - 1].x1;
	  b->range[i - 1].x1 = b2->range[i2].x0;
	  i++;
	} else {
	  /* Just chop the last range. */
	  b->range[i - 1].x1 = b2->range[i2].x0;
	}
      }
      x_suppress = b2->range[i2].x1;
      i2++;
    }
  }
  b->n_ranges = i;
  return b;
}

/* Return the difference of two regions. */
GzwRegion *
gzw_region_minus (GzwRegion *r1, GzwRegion *r2)
{
  GzwRegion *r;
  gint i1, i2;
  gint y;
  gint src;

  /* This routine has very analogous structure to region_union. The
     main difference is that bands from r2 are skipped over in the
     absence of any band from r1, whereas in region_union, they are
     transferred to the output. */

  /* This bound may not be strict. */
  r = (GzwRegion *)g_malloc (sizeof(GzwRegion) +
			     ((r1->n_bands + r2->n_bands) * 2 - 1) *
			     sizeof(band *));
  r->refcount = 1;
  r->n_bands = 0;
  i1 = 0;
  i2 = 0;
  src = 0;
  /* Invariant: y = r->band[i - 1].y1, or 0 if not defined */
  y = -INF;
  while (i1 < r1->n_bands || i2 < r2->n_bands) {
    if (i2 == r2->n_bands ||
	(i1 < r1->n_bands && r1->band[i1]->y1 <= r2->band[i2]->y0)) {
      add_band (r, band_resize (band_pin (r1->band[i1]),
				 max (y, r1->band[i1]->y0),
				 r1->band[i1]->y1),
		&src, 1);
      y = r1->band[i1++]->y1;
    } else if (i1 == r1->n_bands ||
	       (i2 < r2->n_bands && r2->band[i2]->y1 <= r1->band[i1]->y0)) {
      y = r2->band[i2++]->y1;
    } else {
      /* Combine two bands - output the next band beginning with y or
	 greater. */

      /* Advance y to y0 of actual next band. */
      y = max (y, min (r1->band[i1]->y0, r2->band[i2]->y0));

      /* Output the part where there is a deficiency in the y
	 projection of the other region. */
      if (r2->band[i2]->y0 > y) {	
        add_band (r, band_resize (band_pin (r1->band[i1]),
				  y,
				  r2->band[i2]->y0),
		  &src, 1);
	y = r2->band[i2]->y0;
      } else if (r1->band[i1]->y0 > y) {
	y = r1->band[i1]->y0;
      }

      /* Output the part where the y projections of the two bands
	 overlap. */
      add_band (r, band_minus_combine (r1->band[i1], r2->band[i2]), &src, 3);

      /* Advance y to the next band to output. */
      if (r1->band[i1]->y1 < r2->band[i2]->y1)
	y = r1->band[i1++]->y1;
      else if (r1->band[i1]->y1 > r2->band[i2]->y1)
	y = r2->band[i2++]->y1;
      else {
	i1++;
	i2++;
      }
    }
  }
  gzw_region_drop (r1);
  gzw_region_drop (r2);
  return r;
}

#if 0
/* Return the number of pixels enclosed in the rectangle */
static int
compute_weight (GzwRegion *reg, GzwRect *rec) {
  gint i, j;
  gint band_height;
  gint weight, width;
  band *b;

  weight = 0;
  for (i = 0; i < reg->n_bands && reg->band[i]->y1 >= rec->y0; i++);
  for (; i < reg->n_bands && reg->band[i]->y0 < rec->y1; i++) {
    b = reg->band[i];
    band_height = min (b->y1, rec->y1) - max (b->y0, rec->y0);
    if (band_height > 0) { /* can we prove it will always be positive? */
      for (j = 0; j < b->n_ranges && b->range[j].x1 >= rec->x0; j++);
      for (; j < b->n_ranges && b->range[j].x0 < rec->x1; j++) {
	width = min (b->range[j].x0, rec->x0) -
	  max (b->range[j].x1, rec->x1);
	/* again, it may be possible to prove that width > 0 */
	if (width > 0) weight += width * band_height;
      }
    }
  }
  return weight;
}
#endif

/* Find a tile from the region, satisfying the following constraints:

   + it includes the leftmost pixel of the topmost scan line in the region

   + the size is no greater than (max_xs, max_ys)

   + the ratio of actual pixels to (waste + wasted pixels) is maximized

   The last constraint may be satisfied only approximately, in the interest
   of speed.

   This routine is useful for decomposing a region into tiles. In general,

   while (!region_empty (reg)) {
     region_tile (&rec, reg, ...);
     ...process rec...
     reg = region_minus (reg, region_from_rect (&rec));
   }

   */
void
gzw_region_tile (GzwRect *dst, GzwRegion *src, gint max_xs, gint max_ys,
		 gint waste)
{

  /* This implementation doesn't even try to optimize for waste. */

  if (gzw_region_empty (src)) {
    dst->x0 = 0;
    dst->x1 = 0;
    dst->y0 = 0;
    dst->y1 = 0;
    return;
  }
  dst->x0 = src->band[0]->range[0].x0;
  dst->x1 = min (src->band[0]->range[0].x1, dst->x0 + max_xs);
  dst->y0 = src->band[0]->y0;
  dst->y1 = min (src->band[0]->y1, dst->y0 + max_ys);
}

/* Render a region into a bitmap. This is 8bpp, just to keep things simple.
   (I may borrow some code from the alpha xink to render 1bpp bitmaps,
   but this is only for testing, so I may not bother). */
void
gzw_region_to_bitmap (GzwRegion *r, guchar *buf, gint xs, gint ys)
{
  gint i, j;
  gint x, y;
  band *b;
  gint n;

  i = 0;
  for (y = 0; y < ys; y++) {
    if (i == r->n_bands ||
	y < r->band[i]->y0 ||
	y >= r->band[i]->y1) {
      /* an empty scan line */
      for (x = 0; x < xs; x++)
	buf[y * xs + x] = 0;
    } else {
      /* a filled scan line */
      b = r->band[i];
      n = b->n_ranges;
      j = 0;
      while (j != n && b->range[j].x0 < 0) j++;
      x = 0;
      while (j != n && x < xs) {
	while (x < xs && x < b->range[j].x0)
	  buf[y * xs + x++] = 0;
	while (x < xs && x < b->range[j].x1)
	  buf[y * xs + x++] = 255;
	j++;
      }
      while (x < xs)
	buf[y * xs + x++] = 0;
      if (y + 1 == b->y1) i++;
    }
  }
}
