
/* cruelty :) */

/* Copyright (C) 1997 Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <gtk/gtk.h>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gzillabytesink.h"
#include "gzillaimgsink.h"
#include "gzillalinkblock.h"
#include "interface.h"
#include "gzilla.h"
#include "gzillanav.h"
#include "gzillabrowser.h"
#include "menus.h"

#define LOAD_BOOKMARKS 1
#define SAVE_BOOKMARK 2
#define CLOSE_BOOKMARKS 3

/* double quote */
#define D_QUOTE 0x22

static void add_bookmarks_to_menu(FILE *fp);
static void bookmarks_file_op(gint operation, char *title, char *url);
static void save_bookmark_to_file(FILE *fp, char *title, char *url);
static char *search_line(char *line, char *start_text, char *end_text);

static void gzilla_goto_bookmark (GtkWidget *widget, BrowserWindow *bw)
{
	gint i;

	for (i = 0; i < bw->num_bookmarks; i++)
		if (widget == bw->bookmarks[i].menuitem)
			break;
	if (i == bw->num_bookmarks)
	{
		g_warning ("bookmark not found!\n");
		return;
	}
	gzilla_nav_push (bw->bookmarks[i].url, bw);
}

/* Add a bookmark to the bookmarks menu of a particular browser window. */
/* CP: I can't find anything in here that would explain the crash that happens when I bookmark my bookmarks file (~/.gzilla/bookmarks.html). Gzilla also crashes when I bookmark my Netscape bookmarks file. This made me think that I had broken bookmarking somehow, but not so. I can bookmark any other page without a crash. */
void gzilla_bookmark_add_to_menu (BrowserWindow *bw, const char *title,
	const char *url)
{
	GtkWidget *menuitem;

	menuitem = gtk_menu_item_new_with_label (title);
	gtk_menu_append (GTK_MENU (bw->bookmarks_menu), menuitem);
	gtk_widget_show (menuitem);

	if (bw->num_bookmarks == bw->num_bookmarks_max)
	{
		bw->num_bookmarks <<= 1;
		bw->bookmarks = g_realloc (bw->bookmarks,
			bw->num_bookmarks_max * sizeof(GzillaBookmark));
	}
	bw->bookmarks[bw->num_bookmarks].title = g_strdup (title);
	bw->bookmarks[bw->num_bookmarks].url = g_strdup (url);
	bw->bookmarks[bw->num_bookmarks].menuitem = menuitem;
	bw->num_bookmarks++;

	/* accelerator goes here */

	gtk_signal_connect (GTK_OBJECT (menuitem), "activate",
		(GtkSignalFunc) gzilla_goto_bookmark, bw);
}

#if 1
void gzilla_bookmark_add (GtkWidget *widget, gpointer client_data)
{
	BrowserWindow *bw = (BrowserWindow *)client_data;
	gint i;
	char *title, *url;

	/* find title and url */
	if (bw->size_gzilla_nav == 0)
		return;

	title = bw->gzilla_nav[bw->gzilla_nav_ptr - 1].title;
	url = bw->gzilla_nav[bw->gzilla_nav_ptr - 1].url;

	#ifdef VERBOSE
		g_print ("add bookmark: %s %s\n", title, url);
	#endif

	/* add bookmark to bookmarks menu of _all_ browser windows. */

	for (i = 0; i < num_bw; i++)
		gzilla_bookmark_add_to_menu (browser_window[i], title, url);

	/* write bookmark to file */
	bookmarks_file_op (SAVE_BOOKMARK, title, url);
    
}

#else
void gzilla_bookmark_add (GtkWidget *widget, gpointer client_data)
{
	char buf[128];
	GtkMenuEntry bookmark_menu_item;
	BrowserWindow *bw = (BrowserWindow *)client_data;

	/* todo: doesn't index gzilla_nav correctly. */
    
	/* Hmm.. the dang /'s are making submenues :-/ 
	there a way around this ? */
    
	/* set menu path - menu item name */
	if (strlen(bw->gzilla_nav->title) < 1) {
	/* this page has no title, set the menu item to the page's url */
	bookmark_menu_item.path = g_malloc(strlen("No Title") + 64);
	sprintf(bookmark_menu_item.path, "<Gzilla>/Bookmarks/%s", "No Title");
	} else {
	/* set path to the page's title */
	bookmark_menu_item.path = g_malloc(strlen(bw->gzilla_nav->title) + 64);
	sprintf(bookmark_menu_item.path, "<Gzilla>/Bookmarks/%s", 
		bw->gzilla_nav->title);
	}
    
	/* set accelerator key - the first 10 bookmarks get them */
    if (bookmark_num < 10) {
	sprintf(buf, "<alt>%d", bookmark_num);
	bookmark_menu_item.accelerator = g_malloc(strlen(buf) + 2);
	strcpy(bookmark_menu_item.accelerator, buf);
    }
    
	/* set callback function */
	bookmark_menu_item.callback = gzilla_goto_bookmark;
    
	/* set the callback_data to the url */
	bookmark_menu_item.callback_data =
		g_malloc(strlen(bw->gzilla_nav->url) + 2);
	strcpy((char *)bookmark_menu_item.callback_data, bw->gzilla_nav->url);
    
	/* add entry to menu */
	menus_create(&bookmark_menu_item, 1);
    
	/* write bookmark file */
	printf("writing to bookmark file\n");
	bookmarks_file_op(SAVE_BOOKMARK, bw->gzilla_nav->title, 
		bw->gzilla_nav->url);
    
	bookmark_num++;
}
#endif

void gzilla_bookmarks_close(void)
{
    bookmarks_file_op(CLOSE_BOOKMARKS, NULL, NULL);
}

void gzilla_bookmarks_load(char *file)
{
    bookmarks_file_op(LOAD_BOOKMARKS, file, NULL);
}

/* performs operations on the bookmark file.. for first call, title is the filename */
void bookmarks_file_op(gint operation, char *title, char *url)
{
	static FILE *fp;
	static gint init = 1;
    
	if (init) {
		if (operation == LOAD_BOOKMARKS) {
			if ( (fp = fopen(title, "a+")) == NULL) 
				fprintf(stderr, "gzilla: opening bookmark file %s: %s\n",
					title, strerror(errno));
			} else {
				printf("erroneous call to bookmark_file_op - init but not LOAD_BOOKMARKS\n");
			}
		init = 0;
	}
    
	switch (operation)
	{
		case LOAD_BOOKMARKS :
			add_bookmarks_to_menu(fp);
			break;
		
		case SAVE_BOOKMARK :
			save_bookmark_to_file(fp, title, url);
			break;
	
		case CLOSE_BOOKMARKS :
			fclose(fp);
			break;
	
		default : break;
	}
}

/* CP: Maybe the crash is in here? */
void save_bookmark_to_file(FILE *fp, char *title, char *url)
{
    char *buf;
    fseek(fp, 0L, SEEK_END);
    
    buf = g_malloc(strlen(title) + strlen(url) + 128);
    
	/* CP: Adding <li>...</li> in prep for future structuring of file */
    sprintf(buf, "<li><A HREF=%c%s%c>%s</A></li>\n", D_QUOTE, url,
		D_QUOTE, title);
    
	fwrite(buf, strlen(buf), 1, fp);
}

void add_bookmarks_to_menu(FILE *fp)
{
    char *title;
    char *url;
    char buf[4096];
    gint i;
#if 0
    GtkMenuEntry bookmark_menu_item[1024];
#endif
    
    rewind(fp);
    
    g_print("Loading bookmarks...\n");
    while(1) {
	
	if ((fgets(buf, 4096, fp)) == NULL)
		break;
	
	/* get url from line */
	url = search_line(buf, "=\"", "\">");
	
	/* get title from line */
	title = search_line(buf, "\">", "</");
	
	if( (url == NULL) || (title == NULL))
		continue;
	
	

#if 1
	for (i = 0; i < num_bw; i++)
	  gzilla_bookmark_add_to_menu (browser_window[i], title, url);
#else
	/* set menu path - menu item name */
	bookmark_menu_item[bookmark_num].path = g_malloc(strlen(title) + 64);
	sprintf(bookmark_menu_item[bookmark_num].path, "<Gzilla>/Bookmarks/%s", title);
	
	/*
	 * printf("parsed --> %s %s - bookmark_num %d\n", title, url, bookmark_num);
	 */
	
	/* set accelerator key - the first 10 bookmarks get them */
	if (bookmark_num < 10) {
	    sprintf(buf, "<alt>%d", bookmark_num);
	    bookmark_menu_item[bookmark_num].accelerator = g_malloc(strlen(buf) + 2);
	    strcpy(bookmark_menu_item[bookmark_num].accelerator, buf);
	}
	
	/* set callback function */
	bookmark_menu_item[bookmark_num].callback = gzilla_goto_bookmark;
	
	/* set the callback_data to the url */
	bookmark_menu_item[bookmark_num].callback_data = g_malloc(strlen(url) + 2);
	strcpy((char *)bookmark_menu_item[bookmark_num].callback_data, url);
	
	/* free memory from call to search_line() */
	g_free(url);
	g_free(title);
	
	bookmark_num++;
#endif
    }

#if 0    
    /* add entries to menu */
    if (bookmark_num > 0)
	    menus_create(bookmark_menu_item, bookmark_num);
#endif
}

/* this call returns the text between start_text and end_text in line. 
 * ** this function allocates memory for text_return which must be freed! **
 *
 * On error, returns NULL.
 * 
 * I really hope there isn't a gtk or one of your html functions that does this :) 
 * I'm sure there must be an html one already..
 */

char *search_line(char *line, char *start_text, char *end_text)
{
    gint segment_length;
    char *start_index;
    char *end_index;
    char *text_return;
    
    /* if string is not found, return NULL */
    if ( (start_index = strstr(line, start_text)) == NULL)
	    return(NULL);
    
    if ( (end_index = strstr(line, end_text)) == NULL) 
	    return(NULL);
    
    /* adjustment cause strstr returns the start of the text */
    start_index += strlen(start_text);
    
    /* find length of text segment */
    segment_length = end_index - start_index;
    
    text_return = g_malloc( (sizeof(char) * segment_length) + 2);
    
    strncpy(text_return, start_index, segment_length);
    text_return[segment_length] = '\0';
    
    return(text_return);
}

void
gzilla_bookmarks_init (BrowserWindow *bw)
{
  gint i;

  bw->num_bookmarks = 0;
  bw->num_bookmarks_max = 16;
  bw->bookmarks = g_new (GzillaBookmark, bw->num_bookmarks_max);

  if (num_bw > 0)
    {
      /* copy bookmarks from existing browser window to this one. */
      for (i = 0; i < browser_window[0]->num_bookmarks; i++)
	gzilla_bookmark_add_to_menu (bw, 
				     browser_window[0]->bookmarks[i].title, 
				     browser_window[0]->bookmarks[i].url);
    }
}
