/* Header file for "byte sink" abstraction within gzilla. */

#ifndef __GZILLA_BYTESINK_H__
#define __GZILLA_BYTESINK_H__

#include <gtk/gtkobject.h>
#include "gzillaimgsink.h"
#include "gzillastatus.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GZILLA_BYTESINK(obj) GTK_CHECK_CAST (obj, gzilla_bytesink_get_type (), GzillaByteSink)
#define GZILLA_IS_BYTESINK(obj) GTK_CHECK_TYPE (obj, gzilla_bytesink_get_type ())

typedef struct _GzillaByteSink GzillaByteSink;
typedef struct _GzillaByteSinkClass GzillaByteSinkClass;

struct _GzillaByteSink {
  GtkObject object;
};

#define GZILLA_BYTE_SINK_OK 0
#define GZILLA_BYTE_SINK_EOF 1
#define GZILLA_BYTE_SINK_ABORT 2

struct _GzillaByteSinkClass {
  GtkObjectClass parent_class;

  /* of these, only write, close, reset, and set_base_url are expected
     to be implemented by the bytesink. The rest are just signals
     thrown by the bytesink, and maybe shouldn't be implemented here
     at all. */

  /* status too - RLL 25 Nov 1997 */

  void (* write)           (GzillaByteSink *bytesink,
			    char *bytes,
			    gint num);
  void (* close)           (GzillaByteSink *bytesink);
  void (* status)          (GzillaByteSink *bytesink,
			    GzillaStatusDir direction,
			    gboolean abort,
			    GzillaStatusMeaning meaning,
			    const char *text);
};

guint  gzilla_bytesink_get_type        (void);
void   gzilla_bytesink_write           (GzillaByteSink *bytesink,
					char *buf,
					gint bufsize);
void   gzilla_bytesink_close           (GzillaByteSink *bytesink);
void   gzilla_bytesink_status          (GzillaByteSink *bytesink,
					GzillaStatusDir direction,
					gboolean abort,
					GzillaStatusMeaning meaning,
					const char *text);

/* A little taste of documentation (written 7 May and 8 May 1997):

   The byte sink is the primary module interface within gzilla. Each
   byte sink is tightly coupled to widget. The widget may either be
   designed specially to be a web widget (as is the case with the html
   widget), or may be a generic gtk widget, in which case the byte
   sink's more_bytes method will call functions exported by the widget
   to change the widget's state (as is the case with images).

   In general, the creation function for a web widget creates both a
   gtk widget and an associated byte sink, and returns the byte sink.
   The caller of the creation function will generally pack and show the
   widget using bytesink->widget.

   The application will then fill the widget by repeatedly calling the
   more_bytes method. On the last call, the eof argument should be
   GZILLA_BYTE_SINK_EOF. If the display is aborted (for example, by
   the user clicking "stop" before all the bytes have been received),
   then the eof arg should be GZILLA_BYTE_SINK_ABORT. In either of these
   cases, the byte sink deallocates all of its internal state. Thus,
   a correctness condition for the caller is never to call any of the
   byte sink's methods or access any of its fields after the terminal
   more_bytes call. Similarly, to prevent memory leaks, there should
   eventually be a terminal call for each byte sink.

   This architecture basically implements double inheritance of a web
   widget using single inheritance mechanisms, by splitting the object
   into two pieces: one to handle progressive update, and one to
   handle UI events and display.

   One area I'm still a little unsure about is whether to make the
   byte sink side into a real class hierarchy. For example, the alt
   field (and the method to set it) is relevant for images but not for
   HTML. For the time being, I'll forgo the added complexity.

   Another related thing to think about is whether to put the byte
   sink in the gtk object hierarchy. It _would_ help to make things
   more consistent, but would cost a small amount of added boilerplate
   code.

   More thoughts (9 May 1997)

   One of the features I'd like to have is the ability to reset and
   restart a Web widget. This is entirely to avoid the overhead of
   destroying the widget, creating a new one, and going through the
   whole rigamarole about the container, size allocation, etc.

   But this means that the byte sink state should not get deallocated
   on "terminal call" (i.e. eof or abort). Instead, the deallocation
   should get moved to another method. This gives two new methods:
   reset and destroy.

   I'll think about it.

   11 May 1997

   I've decided to go ahead and put the bytesink into the gtk object
   hierarchy, to make it able to smoothly send signals for things
   like request_url, etc.

   12 May 1997

   more_bytes has split into write and close, just to be more consistent
   with Unix-style semantics.

*/

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_BYTESINK_H__ */
