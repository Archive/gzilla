#include <gtk/gtk.h>

#include "gzillafd.h"

int *gzilla_fd_list = NULL;
int gzilla_fd_list_size = 0;
int gzilla_fd_list_size_max;


void
gzilla_fd_add (int fd)
{
  gint i;

#ifdef VERBOSE
  g_print ("gzilla_fd_add (%d)\n", fd);
#endif
  if (gzilla_fd_list == NULL)
    {
      gzilla_fd_list_size = 0;
      gzilla_fd_list_size_max = 16;
      gzilla_fd_list = g_new (int, gzilla_fd_list_size_max);
    }

  for (i = 0; i < gzilla_fd_list_size; i++)
    if (gzilla_fd_list[i] == fd)
      {
	g_warning ("gzilla_fd_add: duplicate fd entry");
	return;
      }

  if (gzilla_fd_list_size == gzilla_fd_list_size_max)
    gzilla_fd_list = g_realloc (gzilla_fd_list, sizeof(int) *
				(gzilla_fd_list_size_max <<= 1));
  gzilla_fd_list[gzilla_fd_list_size++] = fd;
}

void
gzilla_fd_remove (int fd)
{
  gint i;

#ifdef VERBOSE
  g_print ("gzilla_fd_remove (%d)\n", fd);
#endif

  if (gzilla_fd_list == NULL)
    {
      g_warning ("gzilla_fd_remove: removing nonexistent fd (list is empty)");
      return;
    }

  for (i = 0; i < gzilla_fd_list_size; i++)
    if (gzilla_fd_list[i] == fd)
      {
	gzilla_fd_list[i] = gzilla_fd_list[--gzilla_fd_list_size];
	return;
      }

  g_warning ("gzilla_fd_remove: removing nonexistent fd (list is empty)");
}

void
gzilla_fd_print (void)
{
  gint i;

  g_print ("fd list:");
  if (gzilla_fd_list != NULL)
    for (i = 0; i < gzilla_fd_list_size; i++)
      g_print (" %d", gzilla_fd_list[i]);
  g_print ("\n");
}
