#ifndef __GZILLA_JPEG_H__
#define __GZILLA_JPEG_H__

/* you have to #include <jpeglib.h> before including this file, so
   that struct cinfo and struct jerr are defined. */

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GZILLA_JPEG(obj)           GTK_CHECK_CAST (obj, gzilla_jpeg_get_type (), GzillaJpeg)
#define GZILLA_JPEG_CLASS(klass)   GTK_CHECK_CLASS_CAST (klass, gzilla_jpeg_get_type (), GzillaJpegClass)
#define GZILLA_IS_JPEG(obj)        GTK_CHECK_TYPE (obj, gzilla_jpeg_get_type ())

typedef struct _GzillaJpeg       GzillaJpeg;
typedef struct _GzillaJpegClass  GzillaJpegClass;

typedef enum {
  GZILLA_JPEG_INIT,
  GZILLA_JPEG_STARTING,
  GZILLA_JPEG_READING,
  GZILLA_JPEG_DONE
} GzillaJpegState;

struct _GzillaJpeg {
  GzillaByteSink  bytesink;

  GzillaImgSink  *imgsink;

  GzillaJpegState state;

  guchar           *inbuf;
  gint            size_inbuf;
  gint            size_inbuf_max;

  char *bufptr;
  gint bufsize;
  gint num_skip;

  gint y;

  struct jpeg_decompress_struct cinfo;
  struct jpeg_error_mgr jerr;
};

struct _GzillaJpegClass {
  GzillaByteSinkClass  parent_class;
};

GzillaByteSink*
gzilla_jpeg_new (GzillaImgSink *imgsink);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_JPEG_H__ */
