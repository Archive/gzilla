#ifndef __GZILLA_H__
#define __GZILLA_H__

#include "gzillabrowser.h"

/* This header file describes stuff that's defined in gzilla.c but is
 * needed in other parts of the program. Probably, most of it should get
 * moved out into other modules. */


/* These functions are signal handlers, which interface.c attaches
   to newly created main web pages. Nothing really wrong with that,
   but perhaps these should be moved out.
   */
void request_url (GzillaByteSink *bytesink,
		  GzillaByteSink *sink2,
		  char *url,
		  BrowserWindow *bw);
void follow_link (GzillaByteSink *bytesink, char *url, BrowserWindow *bw);
void show_page_status (GzillaByteSink *bytesink,
		       GzillaStatusDir dir,
		       gboolean abort,
		       GzillaStatusMeaning meaning,
		       const char *text,
		       BrowserWindow *bw);
void redirect (GzillaByteSink *bytesink, char *url, BrowserWindow *bw);
void size_allocate (GtkWidget *widget, GtkAllocation * allocation);
void title (GzillaByteSink *bytesink, char *title, BrowserWindow *bw);
void request_url_img (GzillaByteSink *bytesink,
		      GzillaImgSink *imgsink,
		      char *url,
		      BrowserWindow *bw);

/* exported to gzilladicache.c */
void open_url (GzillaByteSink *bytesink, GzillaLinkBlock *linkblock,
	       const char *url, BrowserWindow *bw);
void open_url_raw (GzillaByteSink *bytesink, const char *url);

char *gzilla_user_home(void);
char *gzilla_prepend_user_home(char *file);

#endif /* __GZILLA_H__ */
