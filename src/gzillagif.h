/* Header file for the gif web widget */

#ifndef __GZILLA_GIF_H__
#define __GZILLA_GIF_H__

#include <gtk/gtk.h>
#include "gzillabytesink.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* The private, internal state of the gif byte sink. */
#define        MAXCOLORMAPSIZE         256
#define        MAX_LWZ_BITS            12


#define GZILLA_GIF(obj)           GTK_CHECK_CAST (obj, gzilla_gif_get_type (), GzillaGif)
#define GZILLA_GIF_CLASS(klass)   GTK_CHECK_CLASS_CAST (klass, gzilla_gif_get_type (), GzillaGifClass)
#define GZILLA_IS_GIF(obj)        GTK_CHECK_TYPE (obj, gzilla_gif_get_type ())

typedef struct _GzillaGif        GzillaGif;
typedef struct _GzillaGifClass   GzillaGifClass;

struct _GzillaGif {
  GzillaByteSink  bytesink;

  GzillaImgSink  *imgsink;

  gint            state;
  char           *inbuf;
  gint            size_inbuf;
  gint            size_inbuf_max;

  unsigned char   input_code_size;
  unsigned char   label;

  gchar          *linebuf;
  gint            x, y;
  gboolean        interlace;
  gint            pass;

  /* state for lwz_read_byte */
  int             fresh;
  int             code_size, set_code_size;
  int             max_code, max_code_size;
  int             firstcode, oldcode;
  int             clear_code, end_code;
  int             table[2][(1 << MAX_LWZ_BITS)];
  int             stack[(1<< MAX_LWZ_BITS)* 2], *sp;

  /* state for get_code */
  unsigned char   buf[280];
  int             curbit, lastbit, done, last_byte;

  /* The original GifScreen from giftopnm */
  unsigned int    Width;
  unsigned int    Height;
  unsigned char   ColorMap[MAXCOLORMAPSIZE * 3];
  unsigned int    BitPixel;
  unsigned int    ColorResolution;
  unsigned int    Background;
  unsigned int    AspectRatio;

  /* Gif89 extensions */
  int             transparent;
  int             delayTime;
  int             inputFlag;
  int             disposal;

  /* state for the new push-oriented decoder */
  gint            packet_size;
  guint           window;
  gint            bits_in_window;
  gint            last_code;
  /*   gint            code_size; */
  gint            line_index;
  unsigned char   **spill_lines;
  gint            num_spill_lines_max;
  gint            length[(1 << MAX_LWZ_BITS) + 1];
  gint            code_and_byte[(1 << MAX_LWZ_BITS) + 1];
};

/* Some invariants:

   last_code <= code_mask

   code_and_byte is stored packed: (code << 8) | byte
   */

struct _GzillaGifClass {
  GzillaByteSinkClass  parent_class;
};

GzillaByteSink*
gzilla_gif_new (GzillaImgSink *imgsink);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_GIF_H */
