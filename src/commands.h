#ifndef __COMMANDS_H__
#define __COMMANDS_H__


void gzilla_new_cmd_callback(GtkWidget * widget, gpointer client_data);
void gzilla_openfile_cmd_callback (GtkWidget *widget, gpointer client_data);
void gzilla_openurl_cmd_callback (GtkWidget *widget, gpointer client_data);
void gzilla_prefs_cmd_callback(GtkWidget * widget, gpointer client_data);
void gzilla_close_cmd_callback(GtkWidget * widget, gpointer client_data);
void gzilla_exit_cmd_callback (GtkWidget *widget, gpointer client_data);

void document_viewsource_cmd_callback (GtkWidget *widget, gpointer client_data);
void document_selectall_cmd_callback (GtkWidget *widget, gpointer client_data);
void document_findtext_cmd_callback (GtkWidget *widget, gpointer client_data);
void document_print_cmd_callback (GtkWidget *widget, gpointer client_data);
void document_testgzw_cmd_callback (GtkWidget *widget, gpointer client_data);

void browse_back_cmd_callback (GtkWidget *widget, gpointer client_data);
void browse_forw_cmd_callback (GtkWidget *widget, gpointer client_data);
void browse_reload_cmd_callback (GtkWidget *widget, gpointer client_data);
void browse_stop_cmd_callback (GtkWidget *widget, gpointer client_data);
void browse_home_cmd_callback (GtkWidget *widget, gpointer client_data);

void bookmark_add_cmd_callback (GtkWidget *widget, gpointer client_data);
void bookmark_viewbm_cmd_callback (GtkWidget *widget, gpointer client_data);

void help_home_cmd_callback (GtkWidget *widget, gpointer client_data);
void help_manual_cmd_callback (GtkWidget *widget, gpointer client_data);

#endif /* __COMMANDS_H__ */
