#ifndef __GTK_WEB_H__
#define __GTK_WEB_H__

#include <gtk/gtk.h>
#include "gzillabytesink.h"
#include "gzillaimgsink.h"
#include "gzillalinkblock.h"

#include "gzw.h"
#include "gtkgzwscroller.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

  /* This file defines _two_ objects. We're going to split them apart, but
     for now, we'll do the bytesink first and the widget second. */

#define GZILLA_WEB(obj)           GTK_CHECK_CAST (obj, gzilla_web_get_type (), GzillaWeb)
#define GZILLA_WEB_CLASS(klass)   GTK_CHECK_CLASS_CAST (klass, gzilla_web_get_type (), GzillaWebClass)
#define GZILLA_IS_WEB(obj)        GTK_CHECK_TYPE (obj, gzilla_web_get_type ())

typedef struct _GzillaWeb        GzillaWeb;
typedef struct _GzillaWebClass   GzillaWebClass;

struct _GzillaWeb {
  GzillaByteSink bytesink;

  GzillaImgSink *imgsink;

  Gzw *gzw;

  char *header;        /* the (partial) HTTP response. */
  gint size_header;
  gint size_header_max;

  gint state;

  GzillaByteSink *child;

  GtkWidget *child_widget;

  char *base_url;

  gint width, height;
  char *alt;
  gint32 bg_color;

  GzillaLinkBlock *linkblock;
  GzillaLinkBlock *child_linkblock;
};

struct _GzillaWebClass {
  GzillaByteSinkClass  parent_class;
};


guint      gzilla_web_get_type (void);
GzillaByteSink* gzilla_web_new (void);
GzillaByteSink* gzilla_web_new_gzw (GtkGzwScroller *scroller);
GzillaByteSink* gzilla_web_new_image (gint width, gint height, char *alt,
				      gint32 bg_color);
GzillaByteSink* gzilla_web_new_img_decoder (GzillaImgSink *imgsink);
GzillaLinkBlock *gzilla_web_get_linkblock (GzillaWeb *web);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_WEB_H__ */
