#ifndef __GZILLA_IMAGE_H__
#define __GZILLA_IMAGE_H__

#include "gzillaimgsink.h"
#include "gzw.h"
#include "gzwimage.h"

typedef struct _GzillaImgSinkImg GzillaImgSinkImg;

struct _GzillaImgSinkImg {
  GzillaImgSink imgsink;
  Gzw *gzw;

  /* parameters of the preview widget */
  gint width;
  gint height;
  /* alt goes here if we implement it */
  gint32 bg_color;

  guchar *linebuf;

  /* parameters of the input imgsink */
  gint in_width;
  gint in_height;
  GzillaImgType in_type;
  guchar *cmap;
  gint num_colors;
  /* don't need bg_index because the cmap already has the background
     color substituted. */
};

GzillaImgSink *gzilla_image_new (gint width,
				 gint height,
				 const char *alt,
				 gint32 bg_color);
Gzw *gzilla_image_gzw (GzillaImgSink *imgsink);

#endif /* __GZILLA_IMAGE_H__ */
