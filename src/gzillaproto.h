#ifndef __GZILLA_PROTO_H__
#define __GZILLA_PROTO_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

void gzilla_proto_init (void);
int gzilla_proto_find (const char *url);
void gzilla_proto_get_url (const char *url,
			   gint proto_num,
			   GzillaByteSink *bytesink,
			   void (*status_callback) (const char *status, void *data),
			   void *data);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_PROTO_H__ */
