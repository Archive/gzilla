#ifndef __GZILLA_FD_H__
#define __GZILLA_FD_H__


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/* list may be NULL if empty, otherwise it contains a list of open
   file descriptors. The fd's in this list should be closed upon
   initialization of a forked process. */

extern int *gzilla_fd_list;
extern int gzilla_fd_list_size;
extern int gzilla_fd_list_size_max;

void
gzilla_fd_add (int fd);

void
gzilla_fd_remove (int fd);

void
gzilla_fd_print (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_FD_H__ */
