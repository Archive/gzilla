
#ifndef __GTK_GZW_VIEW_H__
#define __GTK_GZW_VIEW_H__

/* Need to include "gzw.h" before this file. */

#include "gzwregion.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GTK_GZW_VIEW(obj)           GTK_CHECK_CAST (obj, gtk_gzw_view_get_type (), GtkGzwView)
#define GTK_GZW_VIEW_CLASS(klass)   GTK_CHECK_CLASS_CAST (klass, gtk_gzw_view_get_type (), GtkGzwViewClass)
#define GTK_IS_GZW_VIEW(obj)        GTK_CHECK_TYPE (obj, gtk_gzw_view_get_type ())

typedef struct _GtkGzwView        GtkGzwView;
typedef struct _GtkGzwViewClass   GtkGzwViewClass;

struct _GtkGzwView {
  GtkContainer container;

  /* stuff borrowed from gtkviewport.h */

  gint shadow_type;
  GdkWindow *main_window; /* same as gtkviewport - probably superfluous! */
  GdkWindow *view_window; /* same as gtkviewport */
  GtkAdjustment *hadjustment;
  GtkAdjustment *vadjustment;

  /* The gzw that's contained in this widget. */
  Gzw *gzw;

  /* The container data structure for this scroller. */
  GzwContainer *gzw_container;

  /* The region that's requested a repaint. */
  GzwRegion *repaint_region;

  gint repaint_idle_tag;
};

struct _GtkGzwViewClass
{
  GtkContainerClass parent_class;
};


guint      gtk_gzw_view_get_type  (void);
GtkWidget* gtk_gzw_view_new       (GtkAdjustment *hadjustment,
				       GtkAdjustment *vadjustment);
void       gtk_gzw_view_set_gzw   (GtkGzwView *gzw_view,
				       Gzw *gzw);
GtkAdjustment* gtk_gzw_view_get_hadjustment (GtkGzwView   *gzw_view);
GtkAdjustment* gtk_gzw_view_get_vadjustment (GtkGzwView   *gzw_view);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_GZW_VIEW_H__ */
