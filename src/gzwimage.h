#ifndef __GZW_IMAGE_H__
#define __GZW_IMAGE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct _GzwImage GzwImage;
typedef struct _GzwImageInfo   GzwImageInfo;
typedef union  _GzwDitherInfo    GzwDitherInfo;

typedef enum {
  GZW_IMAGE_RGB
} GzwImageType;

struct _GzwImageInfo
{
  GdkVisual *visual;
  GdkColormap *cmap;

  gulong *color_pixels;
  gulong *gray_pixels;
  gulong *reserved_pixels;

  gulong *lookup_red;
  gulong *lookup_green;
  gulong *lookup_blue;

  GzwDitherInfo *dither_red;
  GzwDitherInfo *dither_green;
  GzwDitherInfo *dither_blue;
  GzwDitherInfo *dither_gray;
  guchar ***dither_matrix;

  guint nred_shades;
  guint ngreen_shades;
  guint nblue_shades;
  guint ngray_shades;
  guint nreserved;

  guint bpp;
  gint cmap_alloced;
  gdouble gamma;
};

union _GzwDitherInfo
{
  gushort s[2];
  guchar c[4];
};

struct _GzwImage {
  Gzw gzw;

  GzwImageType type;

  guchar *buffer;
  gint buffer_width;
  gint buffer_height;
};

Gzw *gzw_image_new (GzwImageType type);
void gzw_image_size (GzwImage *image, gint width, gint height);
void gzw_image_draw_row (GzwImage *image, guchar *buf, gint x, gint y,
			 gint width);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZW_IMAGE_H__ */
