/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

#include <ctype.h> /* for isspace and tolower */
#include <string.h> /* for memcpy and memmove */
#include <stdlib.h> /* for atoi */
#include <stdio.h> /* for sprintf */

#include <gtk/gtk.h>
#include "gzillabytesink.h"
#include "gzillalinkblock.h"
#include "gzillahtml.h"
#include "gzillaweb.h"
#include "gzillaurl.h"
#include "gzillaimage.h"

#include "gzw.h"
#ifdef USE_GZW
#include "gzwpage.h"
#include "gzwbullet.h"
#include "gzwembedgtk.h"
#else
#include "gtkpage.h"
#include "gtkbullet.h"
#endif

static void gzilla_html_class_init   (GzillaHtmlClass *klass);
static void gzilla_html_init         (GzillaHtml      *html);
static void gzilla_html_write        (GzillaByteSink *bytesink,
				      char *bytes,
				      gint num);
static void gzilla_html_close        (GzillaByteSink *bytesink);
static void gzilla_html_status       (GzillaByteSink *bytesink,
				      GzillaStatusDir dir,
				      gboolean abort,
				      GzillaStatusMeaning meaning,
				      const char *text);
static void gzilla_html_set_base_url (GzillaLinkBlock *linkblock,
				      const char *url,
				      GzillaByteSink *bytesink);
static void gzilla_html_destroy      (GtkObject         *object);



static GzillaByteSinkClass *parent_class = NULL;

static void gzilla_html_lb_class_init (GzillaHtmlLBClass *klass);
static void gzilla_html_lb_init       (GzillaHtmlLB      *html_lb);
static void gzilla_html_lb_destroy    (GtkObject         *object);

static GzillaLinkBlockClass *lb_parent_class = NULL;

static gint gzilla_html_tag_param (const char *tag, gint tagsize,
				const char *param,
				char *data, gint datasize);


/* We'll do the linkblock first to get it out of the way. */

guint
gzilla_html_lb_get_type ()
{
  static guint html_lb_type = 0;

  if (!html_lb_type)
    {
      GtkTypeInfo html_lb_info =
      {
	"GzillaHtmlLB",
	sizeof (GzillaHtmlLB),
	sizeof (GzillaHtmlLBClass),
	(GtkClassInitFunc) gzilla_html_lb_class_init,
	(GtkObjectInitFunc) gzilla_html_lb_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      html_lb_type = gtk_type_unique (gzilla_linkblock_get_type (),
				      &html_lb_info);
    }

  return html_lb_type;
}

static void
gzilla_html_lb_class_init (GzillaHtmlLBClass *class)
{
  GtkObjectClass      *object_class;

  lb_parent_class = gtk_type_class (gzilla_linkblock_get_type ());

  object_class = (GtkObjectClass*) class;

  object_class->destroy = gzilla_html_lb_destroy;
}

static void
gzilla_html_lb_init (GzillaHtmlLB *html_lb)
{

  html_lb->num_forms_max = 2;
  html_lb->num_forms = 0;
  html_lb->forms = g_new (GzillaHtmlForm, html_lb->num_forms_max);
}

static void
gzilla_html_lb_destroy (GtkObject *object)
{
  gint i, j;
  GzillaHtmlLB *html_lb;
  GzillaHtmlForm *form;

  html_lb = GZILLA_HTML_LB (object);

  for (i = 0; i < html_lb->num_forms; i++)
    {
      form = &html_lb->forms[i];
      for (j = 0; j < form->num_inputs; j++)
	{
	  g_free (form->inputs[j].name);
	  g_free (form->inputs[j].init_str);
	  /* todo: free junk in the select. */
	}
    }
  g_free (html_lb->forms);

  if (GTK_OBJECT_CLASS (lb_parent_class)->destroy)
    (* GTK_OBJECT_CLASS (lb_parent_class)->destroy) (object);
}

GzillaHtmlLB *
gzilla_html_lb_new (void)
{
  GzillaHtmlLB *html_lb;

  html_lb = gtk_type_new (gzilla_html_lb_get_type ());

  return html_lb;
}

/* Ok, linkblock is done, now for the bytesink. */

guint
gzilla_html_get_type ()
{
  static guint html_type = 0;

  if (!html_type)
    {
      GtkTypeInfo html_info =
      {
	"GzillaHtml",
	sizeof (GzillaHtml),
	sizeof (GzillaHtmlClass),
	(GtkClassInitFunc) gzilla_html_class_init,
	(GtkObjectInitFunc) gzilla_html_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      html_type = gtk_type_unique (gzilla_bytesink_get_type (), &html_info);
    }

  return html_type;
}

void
gzilla_html_class_init (GzillaHtmlClass *class)
{
  GzillaByteSinkClass *bytesink_class;
  GtkObjectClass *object_class;

  parent_class = gtk_type_class (gzilla_bytesink_get_type ());

  object_class = (GtkObjectClass*) class;
  bytesink_class = (GzillaByteSinkClass*) class;

  object_class->destroy = gzilla_html_destroy;

  bytesink_class->write = gzilla_html_write;
  bytesink_class->close = gzilla_html_close;
  bytesink_class->status = gzilla_html_status;
}

void gzilla_html_init (GzillaHtml *html) {

  html->size_inbuf_max = 1024;
  html->size_inbuf = 0;
  html->inbuf = g_new (char, html->size_inbuf_max);

  html->num_states_max = 16;
  html->num_states = 0;
  html->stack = g_new (GzillaHtmlState, html->num_states_max);

  html->base_url = NULL;

  html->stash_size_max = 1024;
  html->stash_size = 0;
  html->stash_buf = g_new (char, html->stash_size_max);
}

#ifdef USE_GZW
static void
gzilla_html_handle_link (void *data, char *url)
{
  GzillaLinkBlock *parent = data;
  gzilla_linkblock_link (parent, url);
}

/* This function handles the status function generated by the gzw scroller,
   and converts it into a bytesink status. */
static void
gzilla_html_handle_status (void *data, char *url)
{
  GzillaLinkBlock *parent = data;
  gzilla_linkblock_status (parent, GZILLA_STATUS_DIR_UP, FALSE,
			   GZILLA_STATUS_MEANING_SHOW, url);
}
#else
static void gzilla_html_handle_link (GtkPage *page,
				     char *url,
				     GzillaByteSink *parent) {
  gzilla_bytesink_link (parent, url);
}

static void gzilla_html_handle_status (GtkPage *page,
				       char *string,
				       GzillaByteSink *parent) {
  gzilla_bytesink_status (parent, string);
}
#endif

static gint gzilla_html_form_new (GzillaHtml *html,
				  GzillaHtmlMethod method,
				  const char *action,
				  GzillaHtmlEnc enc) {
  GzillaHtmlLB *html_lb;
  gint form;

  html_lb = html->linkblock;

  form = html_lb->num_forms++;
  if (form == html_lb->num_forms_max) {
    html_lb->num_forms_max <<= 1;
    html_lb->forms = g_realloc (html_lb->forms, html_lb->num_forms_max *
				sizeof(GzillaHtmlForm));
  }
  html_lb->forms[form].method = method;
  html_lb->forms[form].action = g_strdup (action);
  html_lb->forms[form].enc = enc;
  html_lb->forms[form].num_inputs = 0;
  html_lb->forms[form].num_inputs_max = 4;
  html_lb->forms[form].inputs = g_new (GzillaHtmlInput,
				       html_lb->forms[form].num_inputs_max);
  return form;
}

GzillaByteSink *gzilla_html_new (void) {
  GzillaHtml *html;
#ifdef USE_GZW
  Gzw *page;
  GzwPageFont font;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageFont font;
  GtkPageAttr attr;
#endif

  html = gtk_type_new (gzilla_html_get_type ());
#ifdef USE_GZW
  page = gzw_page_new ();
  html->gzw = page;
#else
  page = gtk_page_new ();

  html->bytesink.widget = page;
#endif

  /* Create a dummy font, attribute, and tag for the bottom of the stack. */

#if 0
  font.font = NULL;
#endif
  font.name = "times";
  font.size = 14;
  font.bold = FALSE;
  font.italic = FALSE;

#ifdef USE_GZW
  gzw_page_init_attr ((GzwPage *)page, &attr);
  attr.font = gzw_page_find_font ((GzwPage *)page, &font);
#else
  gtk_page_init_attr (GTK_PAGE (page), &attr);
  attr.font = gtk_page_find_font (GTK_PAGE (page), &font);
#endif

  html->linkblock = gzilla_html_lb_new ();

  gtk_signal_connect (GTK_OBJECT (html->linkblock), "set_base_url",
		      GTK_SIGNAL_FUNC (gzilla_html_set_base_url),
		      GTK_OBJECT (html));

  html->stack[0].tag = g_strdup ("gzilla");
#ifdef USE_GZW
  html->stack[0].attr = gzw_page_find_attr ((GzwPage *)page, &attr);
#else
  html->stack[0].attr = gtk_page_find_attr (GTK_PAGE (page), &attr);
#endif
  html->stack[0].parse_mode = GZILLA_HTML_PARSE_MODE_INIT;
  html->stack[0].list_level = 0;
  html->stack[0].form = gzilla_html_form_new (html,
					      GZILLA_HTML_METHOD_GET,
					      "",
					      GZILLA_HTML_ENC_URLENCODING);
  html->num_states++;

  gzw_page_set_callbacks ((GzwPage *)page,
			  gzilla_html_handle_link,
			  html->linkblock,
			  gzilla_html_handle_status,
			  html->linkblock);

  return GZILLA_BYTESINK (html);
}

GzillaLinkBlock *
gzilla_html_get_linkblock (GzillaHtml *html)
{
  return &html->linkblock->linkblock;
}

#if 0
/* apparently this is now provided in glib */
static gint g_strcasecmp (const char *s1, const char *s2) {
  gint i;

  for (i = 0; s1[i] != '\0'; i++) {
    if (tolower (s1[i]) != tolower (s2[i])) {
      if (tolower (s1[i]) < tolower (s2[i])) return -1;
      else return 1;
    }
  }
  return 0;
}
#endif

/* Initialize the stash buffer */

static void gzilla_html_stash_init (GzillaHtml *html) {
  html->stack[html->num_states - 1].parse_mode = GZILLA_HTML_PARSE_MODE_STASH;
  html->stash_size = 0;
  html->stash_space = 0;
}

/* Convert the entities in a token to plain ISO character codes. Takes
   a token in buf, bufsize format and returns a newly allocated
   string. */

static char *gzilla_html_parse_entities (char *token, gint num) {
  char *new_str;
  int in_index, out_index;
  struct {
    char *entity;
    gint isocode;
  } entities[] = {
    { "quot", 34 },
    { "amp", 38 },
    { "lt", 60 },
    { "gt", 62 },
    { "nbsp", 32 },     /* should maybe be 160? */
    { "Aacute", 0301 },
    { "aacute", 0341 },
    { "Acirc",  0302 },
    { "acirc",  0342 },
    { "AElig",  0306 },
    { "aelig",  0346 },
    { "Agrave", 0300 },
    { "agrave", 0340 },
    { "Aring",  0305 },
    { "aring",  0345 },
    { "Atilde", 0303 },
    { "atilde", 0343 },
    { "Auml",   0304 },
    { "auml",   0344 },
    { "Ccedil", 0307 },
    { "ccedil", 0347 },
    { "Eacute", 0311 },
    { "eacute", 0351 },
    { "Ecirc",  0312 },
    { "ecirc",  0352 },
    { "Egrave", 0310 },
    { "egrave", 0350 },
    { "ETH",    0320 },
    { "eth",    0360 },
    { "Euml",   0313 },
    { "euml",   0353 },
    { "Iacute", 0315 },
    { "iacute", 0355 },
    { "Icirc",  0316 },
    { "icirc",  0356 },
    { "Igrave", 0314 },
    { "igrave", 0354 },
    { "Iuml",   0317 },
    { "iuml",   0357 },
    { "Ntilde", 0321 },
    { "ntilde", 0361 },
    { "Oacute", 0323 },
    { "oacute", 0363 },
    { "Ocirc",  0324 },
    { "ocirc",  0364 },
    { "Ograve", 0322 },
    { "ograve", 0362 },
    { "Oslash", 0330 },
    { "oslash", 0370 },
    { "Otilde", 0325 },
    { "otilde", 0365 },
    { "Ouml",   0326 },
    { "ouml",   0366 },
    { "szlig",  0337 },
    { "THORN",  0336 },
    { "thorn",  0376 },
    { "Uacute", 0332 },
    { "uacute", 0372 },
    { "Ucirc",  0333 },
    { "ucirc",  0373 },
    { "Ugrave", 0331 },
    { "ugrave", 0371 },
    { "Uuml",   0334 },
    { "uuml",   0374 },
    { "Yacute", 0335 },
    { "yacute", 0375 },
    { "yuml",   0377 },
    { "reg", 174 },
    { "copy", 169 }

    /* There are also the "spyglass" entities, but I think these will do. */
  };
  gint isocode;
  gint i;
  gint n = sizeof (entities) / sizeof (entities[0]);
  gint len;

  new_str = g_new (char, num + 1);
  out_index = 0;
  for (in_index = 0; in_index < num; in_index++) {
    if (token[in_index] == '&') {

      /* it's an entity */
      in_index++;

      if (in_index < num && token[in_index] == '#') {

	/* numeric token */
	isocode = atoi (token + in_index + 1);
	if (isocode != 0)
	  new_str[out_index++] = isocode;

      } else {

	/* search for named entity */
	for (i = 0; i < n; i++) {
	  len = strlen (entities[i].entity);
	  if (in_index + len + 1 <= num &&
	      token[in_index + len] == ';' &&
	      !memcmp (token + in_index, entities[i].entity, len)) {
	    new_str[out_index++] = entities[i].isocode;
	    break;
	  }
	}
      }

      /* skip past closing semicolon */
      while (in_index < num && token[in_index] != ';')
	in_index++;
    } else {
      new_str[out_index++] = token[in_index];
    }
  }
  new_str[out_index] = '\0';
  return new_str;
}

static void gzilla_html_process_word (GzillaHtml *html,
				      char *word,
				      gint size) {
  if (html->stack[html->num_states - 1].parse_mode ==
      GZILLA_HTML_PARSE_MODE_STASH)
    {
      if (html->stash_size + size + 2 > html->stash_size_max) {
	do {
	  html->stash_size_max <<= 1;
	} while (html->stash_size + size + 2 > html->stash_size_max);
	html->stash_buf = g_realloc (html->stash_buf, html->stash_size_max);
      }
      if (html->stash_space)
	html->stash_buf[html->stash_size++] = ' ';
      html->stash_space = FALSE;
      memcpy (html->stash_buf + html->stash_size, word, size);
      html->stash_size += size;
      html->stash_buf[html->stash_size] = '\0';
    } else {
      if(word[0] == '\n')
#ifdef USE_GZW
	gzw_page_linebreak((GzwPage *)html->gzw);
#else
	gtk_page_linebreak(GTK_PAGE(html->bytesink.widget));
#endif

      /* this works because of the same side-effect that causes the
       * problem.  newlines (and tabs) passed in the string are
       * basically ignored, no need to change the size and word
       * pointer.
       *
       * right now, this support for <pre> tags ignore tabs entirely.
       * I can't find a working way to insert more than one space
       * (i.e., a tab).. when I tried inserting a string of spaces,
       * the program would seg fault when it tried to follow a link.
       * Using gtk_page_add_space multiple times also failed. -Otto
       */

#ifdef USE_GZW
      gzw_page_add_text ((GzwPage *)html->gzw,
			 gzilla_html_parse_entities (word, size),
			 html->stack[html->num_states - 1].attr);
#else
      gtk_page_add_text (GTK_PAGE (html->bytesink.widget),
			 gzilla_html_parse_entities (word, size),
			 html->stack[html->num_states - 1].attr);
#endif
    }
}

static void gzilla_html_process_space (GzillaHtml *html,
				  char *space,
				  gint spacesize) {
  if (html->stack[html->num_states - 1].parse_mode ==
      GZILLA_HTML_PARSE_MODE_STASH) {
      html->stash_space = (html->stash_size > 0);
  } else {
#ifdef USE_GZW
    gzw_page_add_space ((GzwPage *)html->gzw,
		       html->stack[html->num_states - 1].attr);
#else
    gtk_page_add_space (GTK_PAGE (html->bytesink.widget),
		       html->stack[html->num_states - 1].attr);
#endif
  }
}

/* Does the tag in tagstr (e.g. "p") match the tag in the tag, tagsize
   structure, with the initial < skipped over (e.g. "P
   align=center>")? */
static gboolean gzilla_html_match_tag (const char *tagstr,
				    const char *tag,
				    gint tagsize) {
  int i;

  for (i = 0; i < tagsize && tagstr[i] != '\0'; i++) {
    if (tolower (tagstr[i]) != tolower (tag[i]))
      return FALSE;
  }
  /* The test for '/' is for xml compatibility: "empty/>" will be matched. */
  if (i < tagsize && (isspace (tag[i]) || tag[i] == '>' || tag[i] == '/'))
    return TRUE;
  return FALSE;
}

/* Push the tag (copying attributes from the top of the stack) */
static void gzilla_html_push_tag (GzillaHtml *html,
			       char *tag,
			       gint tagsize) {
  char *tagstr;
  gint tag_end;
  gint tag_index;

  /* Copy the tag itself (no parameters) into tagstr. */
  tag_end = 1;
  while (tag_end < tagsize && isalnum (tag[tag_end]))
    tag_end++;
  tagstr = g_new (char, tag_end);
  memcpy (tagstr, tag + 1, tag_end - 1);
  tagstr[tag_end - 1] = 0;

  tag_index = html->num_states;
  if (html->num_states == html->num_states_max ) {
    html->num_states_max <<= 1;
    html->stack = g_realloc (html->stack,
			     html->num_states_max * sizeof (GzillaHtmlState));
  }

  /* maybe we should copy the entire stack and just change the tag,
     instead of copying all fields except for tag. */
  html->stack[tag_index].tag = tagstr;
  html->stack[tag_index].attr = html->stack[tag_index - 1].attr;
  html->stack[tag_index].parse_mode = html->stack[tag_index - 1].parse_mode;
  html->stack[tag_index].list_level = html->stack[tag_index - 1].list_level;
  html->stack[tag_index].form = html->stack[tag_index - 1].form;
  html->num_states++;
}

static void gzilla_html_cleanup_tag (GzillaHtml *html, char *tag) {
  gint num_states;

  num_states = html->num_states;
  if (num_states > 0 &&
      gzilla_html_match_tag (html->stack[num_states - 1].tag,
			     tag, strlen (tag))) {
    g_free (html->stack[--num_states].tag);
    html->num_states = num_states;
  }
}

/* Do a paragraph break and push the tag. This pops unclosed <p> tags
   off the stack, if there are any. */

static void gzilla_html_par_push_tag (GzillaHtml *html,
				      char *tag,
				      gint tagsize) {
  gzilla_html_cleanup_tag (html, "p>");
  gzilla_html_push_tag (html, tag, tagsize);
#ifdef USE_GZW
  gzw_page_parbreak ((GzwPage *)html->gzw, 9);
#else
  gtk_page_parbreak (GTK_PAGE (html->bytesink.widget), 9);
#endif
}

/* Pop the tag. At the moment, this assumes tags nest! */
static void gzilla_html_pop_tag (GzillaHtml *html,
			       char *tag,
			       gint tagsize) {
  gint num_states;
  gint tag_index;

  /* todo: handle non-nesting case more gracefully (search stack
     downwards for matching tag). */

  num_states = html->num_states;
  for (tag_index = num_states - 1; tag_index >= 0; tag_index--) {
#if 0
    g_print ("%s %c%c%c%c%c\n", html->stack[tag_index].tag, tag[0], tag[1], tag[2], tag[3], tag[4]);
#endif
    if (gzilla_html_match_tag (html->stack[tag_index].tag,
			       tag + 2, tagsize - 2)) {
      while (num_states > tag_index) {
	g_free (html->stack[--num_states].tag);
      }
      html->num_states = num_states;
      return;
    }
  }
  /* Not found, just ignore. */
}

static void gzilla_html_tag_open_title (GzillaHtml *html,
				     char *tag,
				     gint tagsize) {

  gzilla_html_push_tag (html, tag, tagsize);
  gzilla_html_stash_init (html);
}

static void gzilla_html_tag_close_title (GzillaHtml *html,
				     char *tag,
				     gint tagsize) {

#ifdef VERBOSE
  g_print ("gzilla_html_tag_close_title: %s\n", html->stash_buf);
#endif
  gzilla_linkblock_title (&html->linkblock->linkblock, html->stash_buf);
  gzilla_html_pop_tag (html, tag, tagsize);
}

static void gzilla_html_tag_open_body (GzillaHtml *html,
				    char *tag,
				    gint tagsize) {

#if 0
  /* todo: get bgcolor working */
  GdkColor color;

  gdk_color_white (gtk_widget_get_colormap (GTK_WIDGET (html)), &color);
  gdk_window_set_background (GTK_WIDGET (html)->window, &color);
#endif
  gzilla_html_push_tag (html, tag, tagsize);
  html->stack[html->num_states - 1].parse_mode = GZILLA_HTML_PARSE_MODE_BODY;
}

static void gzilla_html_tag_open_p (GzillaHtml *html,
				 char *tag,
				 gint tagsize) {

  gzilla_html_par_push_tag (html, tag, tagsize);
}

static void gzilla_html_tag_open_h (GzillaHtml *html,
				 char *tag,
				 gint tagsize) {
#ifdef USE_GZW
  GzwPage *page;
  GzwPageFont font;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageFont font;
  GtkPageAttr attr;
#endif
  gint level;
  gint sizes[] = {24, 18, 18, 14, 12, 10};

  gzilla_html_par_push_tag (html, tag, tagsize);
  level = tag[2] - '0';
  level = MAX (1, level);
  level = MIN (6, level);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif
  attr = page->attrs[html->stack[html->num_states - 1].attr];
  font = page->fonts[attr.font];
  font.size = sizes[level - 1];
  font.bold = TRUE;
#ifdef USE_GZW
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif
}

static void gzilla_html_tag_open_br (GzillaHtml *html,
				 char *tag,
				 gint tagsize) {

#ifdef USE_GZW
  gzw_page_linebreak ((GzwPage *)html->gzw);
#else
  gtk_page_linebreak (GTK_PAGE (html->bytesink.widget));
#endif
}

static void gzilla_html_tag_open_b (GzillaHtml *html,
				 char *tag,
				 gint tagsize) {
#ifdef USE_GZW
  GzwPage *page;
  GzwPageFont font;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageFont font;
  GtkPageAttr attr;
#endif

  gzilla_html_push_tag (html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif
  attr = page->attrs[html->stack[html->num_states - 1].attr];
  font = page->fonts[attr.font];
  font.bold = TRUE;
#ifdef USE_GZW
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif
}

static void gzilla_html_tag_open_i (GzillaHtml *html,
				 char *tag,
				 gint tagsize) {
#ifdef USE_GZW
  GzwPage *page;
  GzwPageFont font;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageFont font;
  GtkPageAttr attr;
#endif

  gzilla_html_push_tag (html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif
  attr = page->attrs[html->stack[html->num_states - 1].attr];
  font = page->fonts[attr.font];
  font.italic = TRUE;
#ifdef USE_GZW
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif
}

static void gzilla_html_tag_open_tt (GzillaHtml *html,
				 char *tag,
				 gint tagsize) {
#ifdef USE_GZW
  GzwPage *page;
  GzwPageFont font;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageFont font;
  GtkPageAttr attr;
#endif

  gzilla_html_push_tag (html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif
  attr = page->attrs[html->stack[html->num_states - 1].attr];
  font = page->fonts[attr.font];
  font.name = "courier";
#ifdef USE_GZW
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif
}

/* todo: remove this - it's not standard html */
static void gzilla_html_tag_open_button (GzillaHtml *html,
				      char *tag,
				      gint tagsize) {

#ifdef USE_GZW
  /* don't bother */
#else
  GtkWidget *button;
  char buttontext[80];

  if (!gzilla_html_tag_param (tag, tagsize, "text",
			  buttontext, sizeof (buttontext)))
    strcpy (buttontext, "button");
  button = gtk_button_new_with_label (buttontext);
  gtk_widget_show (button);
  gtk_page_add_widget (GTK_PAGE (html->bytesink.widget), button,
		       html->stack[html->num_states - 1].attr);
#endif
}

static void gzilla_html_tag_open_img (GzillaHtml *html,
				      char *tag,
				      gint tagsize) {
  GzillaImgSink *imgsink;
  Gzw *gzw;
  char src[1024];
  char url[1024];
  char num[64];
  char alt[1024];
  gint width, height;
  char *alt_ptr;

  if (!gzilla_html_tag_param (tag, tagsize, "src",
			   src, sizeof (src)))
    return;
  if (!gzilla_url_relative (html->base_url, src, url, sizeof (url)))
    return;
  /* todo: handle redirect requests from the image bytesink */

  width = 0;
  if (gzilla_html_tag_param (tag, tagsize, "width", num, sizeof (num)))
    width = atoi (num);
  height = 0;
  if (gzilla_html_tag_param (tag, tagsize, "height", num, sizeof (num)))
    height = atoi (num);
  alt_ptr = NULL;
  if (gzilla_html_tag_param (tag, tagsize, "alt", alt, sizeof (alt)))
    alt_ptr = alt;

#if 0 /* 1 to disable decompressed image cache */
  image = gzilla_web_new_image (width, height, alt_ptr, 0xd6d6d6);
  gtk_widget_show (image->widget);
  gtk_page_add_widget (GTK_PAGE (html->bytesink.widget), image->widget,
		       html->stack[html->num_states - 1].attr);
  gzilla_bytesink_request_url (GZILLA_BYTESINK (html), image, url);
#else
  imgsink = gzilla_image_new (width, height, alt_ptr, 0xd6d6d6);
  gzw = gzilla_image_gzw (imgsink);
  /* note: request_url_img comes before add_widget because request_url_img
     may set image size, preventing a resize later */
  gzilla_linkblock_request_url_img (&html->linkblock->linkblock, imgsink, url);
#ifdef USE_GZW
  gzw_page_add_widget ((GzwPage *)html->gzw,
		       gzw,
		       html->stack[html->num_states - 1].attr);
#else
  gtk_page_add_widget (GTK_PAGE (html->bytesink.widget), widget,
		       html->stack[html->num_states - 1].attr);
#endif
#endif

}

static void gzilla_html_tag_open_a (GzillaHtml *html,
				    char *tag,
				    gint tagsize) {
#ifdef USE_GZW
  GzwPage *page;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageAttr attr;
#endif
  char href[1024];
  char url[1024];

  gzilla_html_push_tag (html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif

  if (!gzilla_html_tag_param (tag, tagsize, "href",
			      href, sizeof (href)))
    return;
  if (!gzilla_url_relative (html->base_url, href, url, sizeof (url)))
    return;
  attr = page->attrs[html->stack[html->num_states - 1].attr];
#ifdef USE_GZW
  attr.link = gzw_page_new_link (page, url);
  attr.color = gzw_page_find_color (page, 0x0000ff);
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  attr.link = gtk_page_new_link (page, url);
  attr.color = gtk_page_find_color (page, 0x0000ff);
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif
}

static void gzilla_html_tag_open_blockquote (GzillaHtml *html,
					     char *tag,
					     gint tagsize) {
#ifdef USE_GZW
  GzwPage *page;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageAttr attr;
#endif

  gzilla_html_par_push_tag (html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif

  attr = page->attrs[html->stack[html->num_states - 1].attr];
  attr.left_indent_first = attr.left_indent_rest + 40;
  attr.left_indent_rest += 40;
  attr.right_indent += 40;
#ifdef USE_GZW
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif
}

static void gzilla_html_tag_open_ul (GzillaHtml *html,
				     char *tag,
				     gint tagsize) {
#ifdef USE_GZW
  GzwPage *page;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageAttr attr;
#endif

  gzilla_html_par_push_tag (html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif

  attr = page->attrs[html->stack[html->num_states - 1].attr];
  attr.left_indent_first = attr.left_indent_rest + 40;
  attr.left_indent_rest += 40;
#ifdef USE_GZW
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif
}

static void gzilla_html_tag_open_li (GzillaHtml *html,
				     char *tag,
				     gint tagsize) {
#ifdef USE_GZW
  GzwPage *page;
  GzwPageAttr attr;
  Gzw *bullet;
#else
  GtkPage *page;
  GtkPageAttr attr;
  GtkWidget *bullet;
#endif
  gint indent;

  gzilla_html_cleanup_tag (html, "p>");
  gzilla_html_cleanup_tag (html, "li>");
  gzilla_html_push_tag (html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;

  gzw_page_parbreak (page, 1);
#else
  page = GTK_PAGE (html->bytesink.widget);

  gtk_page_parbreak (page, 1);
#endif

  attr = page->attrs[html->stack[html->num_states - 1].attr];
  indent = attr.left_indent_rest;
  if (indent < 40)
    indent = 40;
  attr.left_indent_first = indent - 12;
  attr.left_indent_rest = indent;
#ifdef USE_GZW
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
  html->stack[html->num_states - 1].list_level++;
#else
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
  html->stack[html->num_states - 1].list_level++;
#endif

  switch (html->stack[html->num_states - 1].list_level) {
  case 1:
#ifdef USE_GZW
    bullet = gzw_bullet_new (GZW_BULLET_DISC);
#else
    bullet = gtk_bullet_new (GTK_BULLET_DISC);
#endif
    break;
  case 2:
#ifdef USE_GZW
    bullet = gzw_bullet_new (GZW_BULLET_CIRCLE);
#else
    bullet = gtk_bullet_new (GTK_BULLET_CIRCLE);
#endif
    break;
  default:
#ifdef USE_GZW
    bullet = gzw_bullet_new (GZW_BULLET_SQUARE);
#else
    bullet = gtk_bullet_new (GTK_BULLET_SQUARE);
#endif
    break;
  }
#ifdef USE_GZW
  gzw_page_add_widget (page, bullet,
		       html->stack[html->num_states - 1].attr);
  gzw_page_add_space (page, html->stack[html->num_states - 1].attr);
#else
  gtk_widget_show (bullet);
  gtk_page_add_widget (page, bullet,
		       html->stack[html->num_states - 1].attr);
  gtk_page_add_space (page, html->stack[html->num_states - 1].attr);
#endif
}

/* gzilla_html_tag_open_hr() */
static void
gzilla_html_tag_open_hr(GzillaHtml *html,
			char *tag,
			gint tagsize)
{
  GtkWidget *hrule;
  int width, height;

  /* todo: make rules in gzw - width needs to be dynamic */
  hrule = gtk_hseparator_new();
  width = ((GzwPage *)html->gzw)->width;
  /* set the height to 0, since otherwise the hrule takes up quite a
   * bit of space.. not sure why, but this works well.
   */
  height = 0;
  gtk_widget_set_usize(hrule, width, height);

  gtk_widget_show(hrule);

#ifdef USE_GZW
  gzw_page_add_widget((GzwPage *)html->gzw, gzw_embed_gtk_new (hrule),
		      html->stack[html->num_states - 1].attr);
  gzw_page_linebreak((GzwPage *)html->gzw);
#else
  gtk_page_add_widget(GTK_PAGE(html->bytesink.widget), hrule,
		      html->stack[html->num_states - 1].attr);
  gtk_page_linebreak(GTK_PAGE(html->bytesink.widget));
#endif
} /* end gzilla_html_tag_open_hr() */

/* gzilla_html_tag_open_dl() */
static void
gzilla_html_tag_open_dl(GzillaHtml *html,
			char *tag,
			gint tagsize)
{
  /* may want to actually do some stuff here. */
  gzilla_html_par_push_tag(html, tag, tagsize);

} /* end gzilla_html_tag_open_dl() */

/* gzilla_html_tag_open_dt() */
static void
gzilla_html_tag_open_dt(GzillaHtml *html,
			char *tag,
			gint tagsize)
{
#ifdef USE_GZW
  GzwPage *page;
  GzwPageFont font;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageFont font;
  GtkPageAttr attr;
#endif

  gzilla_html_cleanup_tag (html, "p>");
  gzilla_html_cleanup_tag(html, "dd>");
  gzilla_html_cleanup_tag(html, "dt>");

  gzilla_html_par_push_tag(html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif
  attr = page->attrs[html->stack[html->num_states - 1].attr];
  font = page->fonts[attr.font];
  font.bold = TRUE;
#ifdef USE_GZW
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif

  /* I really want less space between this and the description, but
   * for some reason using gtk_page_linebreak doesn't work, so I've
   * made the "close tag" function gzilla_html_par_close which, of
   * course, gives me an awful lot of vertical space after a <dt>.
   * Purely aesethic, I guess... it'd just be nice to know why.
   */

}

/* gzilla_html_tag_open_dd() */
static void
gzilla_html_tag_open_dd(GzillaHtml *html,
			char *tag,
			gint tagsize)
{
#ifdef USE_GZW
  GzwPage *page;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageAttr attr;
#endif

  gzilla_html_cleanup_tag (html, "p>");
  gzilla_html_cleanup_tag(html, "dd>");
  gzilla_html_cleanup_tag(html, "dt>");

  gzilla_html_par_push_tag(html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif

  attr = page->attrs[html->stack[html->num_states - 1].attr];
  attr.left_indent_first = attr.left_indent_rest + 40;
  attr.left_indent_rest += 40;
  attr.right_indent += 40;
#ifdef USE_GZW
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif

} /* end gzilla_html_tag_open_dd() */

/* gzilla_html_tag_open_pre() */
static void
gzilla_html_tag_open_pre(GzillaHtml *html,
			 char *tag,
			 gint tagsize)
{
#ifdef USE_GZW
  GzwPage *page;
  GzwPageFont font;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageFont font;
  GtkPageAttr attr;
#endif

  gzilla_html_par_push_tag(html, tag, tagsize);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;
#else
  page = GTK_PAGE (html->bytesink.widget);
#endif
  attr = page->attrs[html->stack[html->num_states - 1].attr];
  font = page->fonts[attr.font];
  font.name = "courier";
#ifdef USE_GZW
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gzw_page_find_attr (page, &attr);
#else
  attr.font = gzw_page_find_font (page, &font);
  html->stack[html->num_states - 1].attr = gtk_page_find_attr (page, &attr);
#endif

  /* the placement of this statement right? */
  html->stack[html->num_states - 1].parse_mode = GZILLA_HTML_PARSE_MODE_PRE;

} /* end gzilla_html_tag_open_pre() */


static void gzilla_html_tag_open_form (GzillaHtml *html,
					char *tag,
					gint tagsize) {
  char method_str[80];
  char action_str[1024];
  char enc_str[80];
  char action[1024];
  GzillaHtmlMethod method;
  GzillaHtmlEnc enc;

  gzilla_html_par_push_tag(html, tag, tagsize);

  method = GZILLA_HTML_METHOD_GET;
  if (gzilla_html_tag_param (tag, tagsize, "method", method_str,
			     sizeof (method_str))) {
    if (!g_strcasecmp (method_str, "post"))
      method = GZILLA_HTML_METHOD_POST;
    /* todo: maybe deal with unknown methods? */
  }
  if (gzilla_html_tag_param (tag, tagsize, "action", action_str,
			     sizeof (action_str))) {
    gzilla_url_relative (html->base_url, action_str, action, sizeof(action));
  } else {
    /* careful about buffer overflow here! */
    strcpy (action, html->base_url);
  }
  enc = GZILLA_HTML_ENC_URLENCODING;
  if (gzilla_html_tag_param (tag, tagsize, "encoding", enc_str,
			     sizeof (enc_str))) {
    /* todo: maybe deal with unknown encodings? */
  }
  html->stack[html->num_states - 1].form =
    gzilla_html_form_new (html, method, action, enc);
}

/* Set the history of the menu to be consistent with the active menuitem. */

static void gzilla_html_select_set_history (GzillaHtmlInput *input) {
  gint i;

  for (i = 0; i < input->select->num_options; i++) {
    if (GTK_CHECK_MENU_ITEM (input->select->options[i].menuitem)->active) {
      gtk_option_menu_set_history (GTK_OPTION_MENU (input->widget), i);
      break;
    }
  }
}

/* Reset the input widget to the initial value. */
static void gzilla_html_reset_input (GzillaHtmlInput *input) {
  gint i;

  switch (input->type) {
  case GZILLA_HTML_INPUT_TEXT:
  case GZILLA_HTML_INPUT_PASSWORD:
    gtk_entry_set_text (GTK_ENTRY (input->widget), input->init_str);
    break;
  case GZILLA_HTML_INPUT_CHECKBOX:
  case GZILLA_HTML_INPUT_RADIO:
    gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON (input->widget),
				 input->init_val);
    break;
  case GZILLA_HTML_INPUT_SELECT:
    if (input->select != NULL) {
      /* this is in reverse order so that, in case more than one was
	 selected, we get the last one, which is consistent with handling
	 of multiple selected options in the layout code. */
      for (i = input->select->num_options - 1; i >= 0 ; i--) {
	if (input->select->options[i].init_val) {
	  break;
	}
      }
      if (i < 0)
	i = 0;
      gtk_menu_item_activate (GTK_MENU_ITEM (input->select->options[i].menuitem));
      gzilla_html_select_set_history (input);
    }
    break;
  case GZILLA_HTML_INPUT_SEL_LIST:
    if (input->select != NULL) {
      for (i = 0; i < input->select->num_options; i++) {
	if (input->select->options[i].init_val) {
	  if (input->select->options[i].menuitem->state == GTK_STATE_NORMAL)
	    gtk_list_select_child (GTK_LIST (input->select->menu),
				   input->select->options[i].menuitem);
	} else {
	  if (input->select->options[i].menuitem->state == GTK_STATE_SELECTED)
	    gtk_list_unselect_child (GTK_LIST (input->select->menu),
				     input->select->options[i].menuitem);
	}
      }
    }
    break;
  /* textarea */
  default:
    break;
  }
}


/* Add a new input to the form data structure, setting the initial
   values. */

static void gzilla_html_add_input (GzillaHtmlForm *form,
				   GzillaHtmlInputType type,
				   GtkWidget *widget,
				   const char *name,
				   const char *init_str,
				   gboolean init_val) {
  GzillaHtmlInput *input;

  if (form->num_inputs == form->num_inputs_max) {
    form->num_inputs_max <<= 1;
    form->inputs = g_realloc (form->inputs, form->num_inputs_max *
			      sizeof(GzillaHtmlInput));
  }
  input = &(form->inputs[form->num_inputs]);
  input->type = type;
  input->widget = widget;
  if (name != NULL)
    input->name = g_strdup (name);
  else
    input->name = NULL;
  if (init_str != NULL)
    input->init_str = g_strdup (init_str);
  else
    input->init_str = NULL;
  input->select = NULL;
  input->init_val = init_val;
  gzilla_html_reset_input (input);
  form->num_inputs++;
}


/* Reset all inputs in the form containing reset to their initial values.
   In general, reset is the reset button for the form.
 */

static void gzilla_html_reset_form (GtkWidget *reset,
				    GzillaHtmlLB *html_lb) {
  gint form_index;
  gint input_index;
  GzillaHtmlForm *form;

  for (form_index = 0; form_index < html_lb->num_forms; form_index++) {
    form = &(html_lb->forms[form_index]);
    for (input_index = 0; input_index < form->num_inputs; input_index++) {
      if (form->inputs[input_index].widget == reset) {
	break;
      }
    }
    if (input_index < form->num_inputs) {
      for (input_index = 0; input_index < form->num_inputs; input_index++) {
	gzilla_html_reset_input (&(form->inputs[input_index]));
      }
      break;
    }
  }
}


/* Urlencode the value and store in the buffer, returning the number
   of characters written into the buffer. */

static int gzilla_html_urlencode (char *buf,
				  gint bufsize,
				  const char *val) {
  gint i, j;
  const char hex[] = "0123456789ABCDEF";

  j = 0;
  if (val == NULL) return j;
  for (i = 0; val[i] != '\0'; i++) {
    if (j == bufsize) return j;
    if (val[i] == ' ') {
      buf[j++] = '+';
    } else if (isalnum (val[i])) {
      buf[j++] = val[i];
    } else {
      buf[j++] = '%';
      if (j == bufsize) return j;
      buf[j++] = hex[(val[i] >> 4) & 15];
      if (j == bufsize) return j;
      buf[j++] = hex[val[i] & 15];
    }
  }
  return j;
}


/* Append the name, value pair to an existing url, the tail of which is
   given in (buf, bufsize), i.e. buf should point immediately after the
   '?' or '&' character. */

static int gzilla_html_append_input (char *buf,
				     gint bufsize,
				     const char *name,
				     const char *value) {
  gint i;

  i = gzilla_html_urlencode (buf, bufsize, name);
  if (i < bufsize)
    buf[i++] = '=';
  i += gzilla_html_urlencode (buf + i, bufsize - i, value);
  if (i < bufsize)
    buf[i++] = '&';
  return i;
}


/* Submit the form containing the submit input by making a new query
   URL and generating a link signal on it. */

static void gzilla_html_submit_form (GtkWidget *submit,
				     GzillaHtmlLB *html_lb) {
  gint form_index;
  gint input_index;
  GzillaHtmlForm *form;
  GzillaHtmlInput *input;
  char new_url[1024];
  gint new_url_index;
  gint i;

  /* todo: POST method */

  for (form_index = 0; form_index < html_lb->num_forms; form_index++) {
    form = &(html_lb->forms[form_index]);
    for (input_index = 0; input_index < form->num_inputs; input_index++) {
      if (form->inputs[input_index].widget == submit) {
	break;
      }
    }
    if (input_index < form->num_inputs) {
      if (form->method == GZILLA_HTML_METHOD_GET) {
	strcpy (new_url, form->action);
	new_url_index = strlen (new_url);
	if (new_url_index < sizeof(new_url))
	  new_url[new_url_index++] = '?';
	for (input_index = 0; input_index < form->num_inputs; input_index++) {
	  input = &(form->inputs[input_index]);
	  switch (input->type) {
	  case GZILLA_HTML_INPUT_TEXT:
	  case GZILLA_HTML_INPUT_PASSWORD:
	    new_url_index +=
	      gzilla_html_append_input (new_url + new_url_index,
					sizeof(new_url) - new_url_index,
					input->name,
					gtk_entry_get_text (GTK_ENTRY
							    (input->widget)));
	    break;
	  case GZILLA_HTML_INPUT_CHECKBOX:
	  case GZILLA_HTML_INPUT_RADIO:
	    if (GTK_TOGGLE_BUTTON (input->widget)->active &&
		input->name != NULL &&
		input->init_str != NULL) {
	      new_url_index +=
		gzilla_html_append_input (new_url + new_url_index,
					  sizeof(new_url) - new_url_index,
					  input->name,
					  input->init_str);
	    }
	    break;
	  case GZILLA_HTML_INPUT_HIDDEN:
	    new_url_index +=
	      gzilla_html_append_input (new_url + new_url_index,
					sizeof(new_url) - new_url_index,
					input->name,
					input->init_str);
	    break;
	  case GZILLA_HTML_INPUT_SELECT:
	    for (i = 0; i < input->select->num_options; i++) {
	      if (GTK_CHECK_MENU_ITEM (input->select->options[i].menuitem)->
		  active) {
		new_url_index +=
		  gzilla_html_append_input (new_url + new_url_index,
					    sizeof(new_url) - new_url_index,
					    input->name,
					    input->select->options[i].value);
		break;
	      }
	    }
	    break;
	  case GZILLA_HTML_INPUT_SEL_LIST:
	    for (i = 0; i < input->select->num_options; i++) {
	      if (input->select->options[i].menuitem->state ==
		  GTK_STATE_SELECTED) {
		new_url_index +=
		  gzilla_html_append_input (new_url + new_url_index,
					    sizeof(new_url) - new_url_index,
					    input->name,
					    input->select->options[i].value);
	      }
	    }
	    break;
	  /* textarea */
	  default:
	    break;
	  }
	}
	if (new_url_index < sizeof(new_url)) {
	  new_url[new_url_index - 1] = '\0';
#ifdef VERBOSE
	  g_print ("gzilla_html_submit_form: %s\n", new_url);
#endif
	  gzilla_linkblock_link (&html_lb->linkblock, new_url);
	}
      } else if (form->method == GZILLA_HTML_METHOD_GET) {
	g_print ("POST method not supported yet\n");
      }
    } /* if (input_index < form->num_inputs) */
  } /* for input_index = [0..form->num_inputs) */
}

static void gzilla_html_tag_open_input (GzillaHtml *html,
					char *tag,
					gint tagsize) {
  GzillaHtmlForm *form;
  GtkWidget *widget;
  char type[80];
  char value[1024];
  char name_str[1024];
  char checked[80];
  char *name, *init_str;
  gboolean init_val;
  gint input_index;
  GSList *group;
  GzillaHtmlLB *html_lb;

  html_lb = html->linkblock;

  form = &(html_lb->forms[html->stack[html->num_states - 1].form]);
  widget = NULL;
  if (!gzilla_html_tag_param (tag, tagsize, "type", type, sizeof (type)))
    strcpy (type, "text");

  if (gzilla_html_tag_param (tag, tagsize, "name", name_str, sizeof(name_str)))
    name = name_str;
  else
    name = NULL;
  /* move value param from case analysis to here? */
  if (!g_strcasecmp (type, "text")) {
    widget = gtk_entry_new ();
    if (gzilla_html_tag_param (tag, tagsize, "value", value, sizeof (value)))
      init_str = value;
    else
      init_str = "";
    gzilla_html_add_input (form, GZILLA_HTML_INPUT_TEXT,
			   widget, name, init_str, FALSE);
  } else if (!g_strcasecmp (type, "password")) {
    widget = gtk_entry_new ();
    gtk_entry_set_visibility (GTK_ENTRY (widget), FALSE);
    if (gzilla_html_tag_param (tag, tagsize, "value", value, sizeof (value)))
      init_str = value;
    else
      init_str = "";
    gzilla_html_add_input (form, GZILLA_HTML_INPUT_PASSWORD,
			   widget, name, init_str, FALSE);
  } else if (!g_strcasecmp (type, "checkbox")) {
    widget = gtk_check_button_new ();
    if (gzilla_html_tag_param (tag, tagsize, "value", value, sizeof (value)))
      init_str = value;
    else
      init_str = "";
    init_val = gzilla_html_tag_param (tag, tagsize, "checked", checked,
				      sizeof(checked));
    gzilla_html_add_input (form, GZILLA_HTML_INPUT_CHECKBOX,
			   widget, name, init_str, init_val);
  } else if (!g_strcasecmp (type, "radio")) {
    group = NULL;
    for (input_index = 0; input_index < form->num_inputs; input_index++) {
      if (form->inputs[input_index].type == GZILLA_HTML_INPUT_RADIO &&
	  !strcmp (form->inputs[input_index].name, name)) {
	group = gtk_radio_button_group (GTK_RADIO_BUTTON
					(form->inputs[input_index].widget));
	break;
      }
    }
    widget = gtk_radio_button_new (group);
    if (gzilla_html_tag_param (tag, tagsize, "value", value, sizeof (value)))
      init_str = value;
    else
      init_str = "";
    init_val = gzilla_html_tag_param (tag, tagsize, "checked", checked,
				      sizeof(checked));
    gzilla_html_add_input (form, GZILLA_HTML_INPUT_RADIO,
			   widget, name, init_str, init_val);
  } else if (!g_strcasecmp (type, "hidden")) {
    if (gzilla_html_tag_param (tag, tagsize, "value", value, sizeof (value)))
      init_str = value;
    else
      init_str = "";
    gzilla_html_add_input (form, GZILLA_HTML_INPUT_HIDDEN,
			   NULL, name, init_str, FALSE);
  } else if (!g_strcasecmp (type, "submit")) {
    if (gzilla_html_tag_param (tag, tagsize, "value", value, sizeof (value)))
      init_str = value;
    else
      init_str = "";
    widget = gtk_button_new_with_label (init_str);
    gzilla_html_add_input (form, GZILLA_HTML_INPUT_SUBMIT,
			   widget, name, init_str, FALSE);
    gtk_signal_connect (GTK_OBJECT (widget), "clicked",
			GTK_SIGNAL_FUNC (gzilla_html_submit_form),
			GTK_OBJECT (html_lb));
  } else if (!g_strcasecmp (type, "reset")) {
    if (gzilla_html_tag_param (tag, tagsize, "value", value, sizeof (value)))
      widget = gtk_button_new_with_label (value);
    else
      widget = gtk_button_new_with_label ("Reset");
    gzilla_html_add_input (form, GZILLA_HTML_INPUT_RESET,
			   widget, NULL, NULL, FALSE);
    gtk_signal_connect (GTK_OBJECT (widget), "clicked",
			GTK_SIGNAL_FUNC (gzilla_html_reset_form),
			GTK_OBJECT (html_lb));
  }

  if (widget != NULL) {
    gtk_widget_show (widget);
#ifdef USE_GZW
    gzw_page_add_widget ((GzwPage *)html->gzw,
			 gzw_embed_gtk_new (widget),
			 html->stack[html->num_states - 1].attr);
#else
    gtk_page_add_widget (GTK_PAGE (html->bytesink.widget), widget,
			 html->stack[html->num_states - 1].attr);
#endif
  }
}

/* The select tag is quite tricky, because of gorpy html syntax. */

static void gzilla_html_tag_open_select (GzillaHtml *html,
					 char *tag,
					 gint tagsize) {
  GzillaHtmlForm *form;
  char name_str[1024];
  char size_str[80];
  char multiple[80];
  char *name;
  gint size;
  GtkWidget *widget;
  GtkWidget *menu;
  GzillaHtmlSelect *select;
  GzillaHtmlLB *html_lb;

  html_lb = html->linkblock;

  form = &(html_lb->forms[html->stack[html->num_states - 1].form]);
  if (gzilla_html_tag_param (tag, tagsize, "name", name_str, sizeof(name_str)))
    name = name_str;
  else
    name = NULL;
  if (gzilla_html_tag_param (tag, tagsize, "size", size_str, sizeof(size_str)))
    size = atoi (size_str);
  else
    size = 1;
  if (size < 1) size = 1;

  if (size == 1) {
    menu = gtk_menu_new ();
    widget = gtk_option_menu_new ();
    
    gzilla_html_add_input (form, GZILLA_HTML_INPUT_SELECT,
			   widget, name, NULL, FALSE);
  } else {
    menu = gtk_list_new ();
    
    gzilla_html_add_input (form, GZILLA_HTML_INPUT_SEL_LIST,
			   menu, name, NULL, FALSE);
    if (gzilla_html_tag_param (tag, tagsize, "multiple", multiple,
			       sizeof(multiple)))
      gtk_list_set_selection_mode (GTK_LIST (menu), GTK_SELECTION_MULTIPLE);
  }

  select = g_new (GzillaHtmlSelect, 1);
  select->menu = menu;
  select->size = size;
  select->num_options = 0;
  select->num_options_max = 16;
  select->options = g_new (GzillaHtmlOption, select->num_options_max);

  /* for consistency, the select should probably be another argument
     to gzilla_html_add_input, but I felt bad about the number of
     arguments that it had already, so I just set it here. */
  form->inputs[form->num_inputs - 1].select = select;

  gzilla_html_stash_init (html);
}

static void gzilla_html_option_finish (GzillaHtml *html) {
  GzillaHtmlForm *form;
  GzillaHtmlInput *input;
  GtkWidget *menuitem;
  GSList *group;
  GzillaHtmlLB *html_lb;

  html_lb = html->linkblock;

  form = &(html_lb->forms[html->stack[html->num_states - 1].form]);
  input = &(form->inputs[form->num_inputs - 1]);
  /* maybe break into two nested if's (num_options test on outside? */
  if (input->type == GZILLA_HTML_INPUT_SELECT &&
      input->select->num_options > 0) {
    if (input->select->num_options == 1)
      group = NULL;
    else
      group = gtk_radio_menu_item_group (GTK_RADIO_MENU_ITEM
					 (input->select->options[0].menuitem));
    menuitem = gtk_radio_menu_item_new_with_label (group, html->stash_buf);
    input->select->options[input->select->num_options - 1].menuitem = menuitem;
    if (input->select->options[input->select->num_options - 1].value == NULL)
      input->select->options[input->select->num_options - 1].value =
	g_strdup (html->stash_buf);
    gtk_menu_append (GTK_MENU (input->select->menu), menuitem);
    if (input->select->options[input->select->num_options - 1].init_val)
      gtk_menu_item_activate (GTK_MENU_ITEM (menuitem));
    gtk_widget_show (menuitem);
  } else if (input->type == GZILLA_HTML_INPUT_SEL_LIST &&
      input->select->num_options > 0) {
    menuitem = gtk_list_item_new_with_label (html->stash_buf);
    input->select->options[input->select->num_options - 1].menuitem = menuitem;
    if (input->select->options[input->select->num_options - 1].value == NULL)
      input->select->options[input->select->num_options - 1].value =
	g_strdup (html->stash_buf);
    gtk_container_add (GTK_CONTAINER (input->select->menu), menuitem);
    if (input->select->options[input->select->num_options - 1].init_val)
      gtk_list_select_child (GTK_LIST (input->select->menu), menuitem);
    gtk_widget_show (menuitem);
  }
}

static void gzilla_html_tag_open_option (GzillaHtml *html,
					 char *tag,
					 gint tagsize) {
  GzillaHtmlForm *form;
  GzillaHtmlInput *input;
  char value[1024];
  char selected[80];
  GzillaHtmlLB *html_lb;

  html_lb = html->linkblock;

  form = &(html_lb->forms[html->stack[html->num_states - 1].form]);
  input = &(form->inputs[form->num_inputs - 1]);
  if (input->type == GZILLA_HTML_INPUT_SELECT ||
      input->type == GZILLA_HTML_INPUT_SEL_LIST) {
    gzilla_html_option_finish (html);
    if (input->select->num_options == input->select->num_options_max) {
      input->select->num_options_max <<= 1;
      input->select->options =
	g_realloc (input->select->options, input->select->num_options_max *
		   sizeof(GzillaHtmlOption));
    }
    input->select->options[input->select->num_options].menuitem = NULL;
    if (gzilla_html_tag_param (tag, tagsize, "value", value, sizeof (value)))
      input->select->options[input->select->num_options].value =
	g_strdup (value);
    else
      input->select->options[input->select->num_options].value = NULL;
    input->select->options[input->select->num_options].init_val =
      gzilla_html_tag_param (tag, tagsize, "selected", selected,
			     sizeof(selected));
    input->select->num_options++;
  }
  gzilla_html_stash_init (html);
}

static void gzilla_html_tag_close_select (GzillaHtml *html,
					  char *tag,
					  gint tagsize) {
  GzillaHtmlForm *form;
  GzillaHtmlInput *input;
  GtkWidget *scrolledwindow;
  GzillaHtmlLB *html_lb;

  html_lb = html->linkblock;

  form = &(html_lb->forms[html->stack[html->num_states - 1].form]);
  input = &(form->inputs[form->num_inputs - 1]);
  if (input->type == GZILLA_HTML_INPUT_SELECT) {
    gzilla_html_option_finish (html);

    gtk_option_menu_set_menu (GTK_OPTION_MENU (input->widget),
			      input->select->menu);
    gzilla_html_select_set_history (input);
#if 0
    gtk_option_menu_set_history (GTK_OPTION_MENU (input->widget), 1);
#endif

    gtk_widget_show (input->widget);
#ifdef USE_GZW
    gzw_page_add_widget ((GzwPage *)html->gzw,
			 gzw_embed_gtk_new (input->widget),
			 html->stack[html->num_states - 1].attr);
#else
    gtk_page_add_widget (GTK_PAGE (html->bytesink.widget), input->widget,
			 html->stack[html->num_states - 1].attr);
#endif
  } else if (input->type == GZILLA_HTML_INPUT_SEL_LIST) {
    gzilla_html_option_finish (html);

    if (input->select->size < input->select->num_options) {
      scrolledwindow = gtk_scrolled_window_new (NULL, NULL);
      gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow),
				      GTK_POLICY_AUTOMATIC,
				      GTK_POLICY_AUTOMATIC);
      gtk_container_add (GTK_CONTAINER (scrolledwindow), input->widget);
      gtk_widget_show (input->widget);
      input->widget = scrolledwindow;

      /* todo: better size negotiation. Ideally, the scrolledwindow
	 would be the same width as the list, and would be size times
	 the height of a single list element. It might be possible to
	 do this by explicitly doing a size_request on the list, then
	 doing a gtk_widget_set_usize on the scrolledwindow, but that
	 seems heavyhanded. It might be the right way to do it,
	 though. */
    }

    gtk_widget_show (input->widget);

    /* note: In this next call, scrolledwindows get a g_warning from
       gdkwindow.c:422. I'm not really going to sweat it now - the
       embedded widget stuff is going to get massively redone anyway. */
#ifdef USE_GZW
    gzw_page_add_widget ((GzwPage *)html->gzw,
			 gzw_embed_gtk_new (input->widget),
			 html->stack[html->num_states - 1].attr);
#else
    gtk_page_add_widget (GTK_PAGE (html->bytesink.widget), input->widget,
			 html->stack[html->num_states - 1].attr);
#endif
  }
  gzilla_html_pop_tag (html, tag, tagsize);
}

/* Default close for most tags - just pop the stack. */

static void gzilla_html_tag_default_close (GzillaHtml *html,
				 char *tag,
				 gint tagsize) {
  gzilla_html_pop_tag (html, tag, tagsize);
}

/* Default close for paragraph tags - pop the stack and break. */

static void gzilla_html_tag_par_close (GzillaHtml *html,
				 char *tag,
				 gint tagsize) {
  gzilla_html_pop_tag (html, tag, tagsize);
#ifdef USE_GZW
  gzw_page_parbreak ((GzwPage *)html->gzw, 9);
#else
  gtk_page_parbreak (GTK_PAGE (html->bytesink.widget), 9);
#endif
}

/* Default close for tags with no close (e.g. <br>) - do nothing */
static void gzilla_html_tag_nop_close (GzillaHtml *html,
				    char *tag,
				    gint tagsize) {
}

/* Process a tag, given as buf and bufsize (includes the enclosing angle
   brackets. */

static void gzilla_html_process_tag (GzillaHtml *html,
				  char *tag,
				  gint tagsize) {
  struct {
    char *tag;
    void (* open) (GzillaHtml *html, char *tag, gint tagsize);
    void (* close) (GzillaHtml *html, char *tag, gint tagsize);
  } tags[] = {
    { "title", gzilla_html_tag_open_title, gzilla_html_tag_close_title },
    { "body", gzilla_html_tag_open_body, gzilla_html_tag_default_close },
    { "p", gzilla_html_tag_open_p, gzilla_html_tag_par_close },
    { "h1", gzilla_html_tag_open_h, gzilla_html_tag_par_close },
    { "h2", gzilla_html_tag_open_h, gzilla_html_tag_par_close },
    { "h3", gzilla_html_tag_open_h, gzilla_html_tag_par_close },
    { "h4", gzilla_html_tag_open_h, gzilla_html_tag_par_close },
    { "h5", gzilla_html_tag_open_h, gzilla_html_tag_par_close },
    { "h6", gzilla_html_tag_open_h, gzilla_html_tag_par_close },
	{ "tr", gzilla_html_tag_open_br, gzilla_html_tag_nop_close },
	/* { "td", gzilla_html_tag_open_br, gzilla_html_tag_nop_close }, */
    { "br", gzilla_html_tag_open_br, gzilla_html_tag_nop_close },
    { "b", gzilla_html_tag_open_b, gzilla_html_tag_default_close },
    { "i", gzilla_html_tag_open_i, gzilla_html_tag_default_close },
    { "tt", gzilla_html_tag_open_tt, gzilla_html_tag_default_close },
    { "button", gzilla_html_tag_open_button, gzilla_html_tag_nop_close },
    { "img", gzilla_html_tag_open_img, gzilla_html_tag_nop_close },
    { "a", gzilla_html_tag_open_a, gzilla_html_tag_default_close },
    { "blockquote", gzilla_html_tag_open_blockquote, gzilla_html_tag_par_close },
    { "ul", gzilla_html_tag_open_ul, gzilla_html_tag_par_close },
    { "li", gzilla_html_tag_open_li, gzilla_html_tag_default_close },
    { "hr", gzilla_html_tag_open_hr, gzilla_html_tag_nop_close },
    { "address", gzilla_html_tag_open_i, gzilla_html_tag_par_close },
    { "dl", gzilla_html_tag_open_dl, gzilla_html_tag_par_close },
    { "dt", gzilla_html_tag_open_dt, gzilla_html_tag_par_close },
    { "dd", gzilla_html_tag_open_dd, gzilla_html_tag_par_close },
    { "pre", gzilla_html_tag_open_pre, gzilla_html_tag_par_close },
    { "form", gzilla_html_tag_open_form, gzilla_html_tag_default_close },
    { "input", gzilla_html_tag_open_input, gzilla_html_tag_nop_close },
    { "option", gzilla_html_tag_open_option, gzilla_html_tag_nop_close },
    { "select", gzilla_html_tag_open_select, gzilla_html_tag_close_select }
  };
  gint i;
  gint n = sizeof (tags) / sizeof (tags[0]);
  char *tagstart;
  gint tagsize2;

  tagstart = tag + 1;
  tagsize2 = tagsize - 1;
  if (tag[1] == '/') {
    tagstart++;
    tagsize2--;
  }
  for (i = 0; i < n; i++) {
    if (gzilla_html_match_tag (tags[i].tag, tagstart, tagsize2))
      break;
  }
  if (i < n) {
    if (tag[1] != '/') {
      (*(tags[i].open)) (html, tag, tagsize);
    } else {
      (*(tags[i].close)) (html, tag, tagsize);
    }
  } else {
    /* tag not found - just ignore it */
  }
}

/* Does the tag in tagstr (e.g. "p") match the tag in the tag, tagsize
   structure, with the initial < skipped over (e.g. "P
   align=center>")?

   Note: duplication with gzilla_html_match_tag - we could probably use the
   code in this function for both purposes. */

static gboolean gzilla_html_match_param (const char *tagstr,
				      const char *tag,
				      gint tagsize) {
  int i;

  for (i = 0; i < tagsize && tagstr[i] != '\0'; i++) {
    if (tolower (tagstr[i]) != tolower (tag[i]))
      return FALSE;
  }
  if (i < tagsize && (isspace (tag[i]) || tag[i] == '>' || tag[i] == '='))
    return TRUE;
  return FALSE;
}


/* If pointing to a parameter value, return an index to the end of
   the value. */

static gint gzilla_html_skip_param (const char *tag, gint tagsize) {
  gint i;

  if (tag[0] == '"') {
    /* skip to end quote */
    for (i = 1; i < tagsize && tag[i] != '"'; i++);
    if (i < tagsize) i++;
    return i;
  } else {
    for (i = 0; i < tagsize && !(isspace (tag[i]) || tag[i] == '>'); i++);
    /* xml support: allow tag to be closed by "/>" */
    if (i == tagsize - 1 && i >= 1 && tag[i - 1] == '/') i--;
    return i;
  }
}

/* a note: proper HTML lingo is apparently "attribute" rather than
   "parameter". Whatever. */

/* Return 0 if param is not present, 1 if present, -1 if datasize is too
   small. If nonzero, param is stored into data. */

/* todo: support for no-value parameters (attributes) */

static gint gzilla_html_tag_param (const char *tag, gint tagsize,
				const char *param,
				char *data, gint datasize) {
  gint i, param_start, value_start, size;

  for (i = 1; i < tagsize && !isspace (tag[i]); i++)
    ;
  for (; i < tagsize - 1; i++) {
    while (i < tagsize - 1 && isspace (tag[i]))
      i++;
    param_start = i;
    while (i < tagsize && tag[i] != '=')
      i++;
    if (i < tagsize) i++;
    while (i < tagsize && isspace (tag[i]))
      i++;
    value_start = i;
    i += gzilla_html_skip_param (tag + i, tagsize - i);
    if (gzilla_html_match_param (param,
			      tag + param_start, tagsize - param_start)) {
      /* found the param! */
      size = i - value_start;
      if (i < tagsize && tag[value_start] == '"') {
	value_start++;
	/* it is possible to prove size >= 2 if we got here. */
	size -= 2;
      }
      if (size + 1 > datasize) {
	memcpy (data, tag + value_start, datasize - 1);
	data[datasize - 1] = '\0';
	return -1;
      } else {
	memcpy (data, tag + value_start, size);
	data[size] = '\0';
	return 1;
      }
    }
  }
  return 0;
}

static void gzilla_html_write (GzillaByteSink *bytesink,
			       char *bytes,
			       gint num) {
#ifdef USE_GZW
  GzwPage *page;
#else
  GtkPage *page;
#endif
  GzillaHtml *html;
  char *buf;
  gint bufsize, token_start, buf_index;

  g_return_if_fail (bytesink != NULL);
  g_return_if_fail (GZILLA_IS_HTML (bytesink));
  g_return_if_fail (bytes != NULL);

  html = GZILLA_HTML (bytesink);

#ifdef USE_GZW
  page = (GzwPage *)html->gzw;

  gzw_page_update_begin (page);
#else
  page = GTK_PAGE (bytesink->widget);

  gtk_page_update_begin (page);
#endif

  /* Here's where we parse the html and put it into the page structure. */

  /* Concatenate with the partial token, if any. */
  if (html->size_inbuf) {
    bufsize = html->size_inbuf + num;
    if (html->size_inbuf_max < bufsize) {
      do {
	html->size_inbuf_max <<= 1;
      } while (html->size_inbuf_max < bufsize);
      html->inbuf = g_realloc (html->inbuf, html->size_inbuf_max);
    }
    buf = html->inbuf;
    memcpy (buf + html->size_inbuf, bytes, num);
  } else {
    buf = bytes;
    bufsize = num;
  }

  /* Now, buf and bufsize define a buffer aligned to start at a token
     boundary. Iterate through tokens until end of buffer is reached. */
  buf_index = 0;
  token_start = buf_index;
  while (buf_index < bufsize) {

#ifdef VERBOSE
    g_print ("handling token %c%c%c\n", buf[buf_index], buf[buf_index + 1],
	     buf[buf_index + 2]);
#endif

    /* invariant: buf_index == bufsize || token_start == buf_index */

    if (isspace (buf[buf_index])
	&& (html->stack[html->num_states - 1].parse_mode
	    != GZILLA_HTML_PARSE_MODE_PRE))
      {
      /* whitespace : consume all available whitespace */
      do {
	buf_index++;
      } while (buf_index < bufsize && isspace (buf[buf_index]));
      gzilla_html_process_space (html, buf + token_start,
			      buf_index - token_start);
      token_start = buf_index;
    } else if (buf[buf_index] == '<') {
      if (buf_index + 4 <= bufsize &&
	  buf[buf_index + 1] == '!' &&
	  buf[buf_index + 2] == '-' &&
	  buf[buf_index + 3] == '-')
	{
	  /* a comment: search for close of comment, skipping over
	     everything except a matching "-->" string. */
	  buf_index += 6;
	  if (buf_index > bufsize)
	    buf_index = bufsize;

	  while (buf_index < bufsize &&
		 (buf[buf_index] != '>' ||
		  buf[buf_index - 1] != '-' ||
		  buf[buf_index - 2] != '-'))
	    buf_index++;

	  if (buf_index < bufsize)
	    {
	      buf_index++;
	      /* if you ever want to pass the comments downstream, put
		 code here. You'll need to do this for J*Script and
		 style sheets. */
	      token_start = buf_index;
	    }
	}
      else
	{
	  /* a tag : search for close of tag (skipping over quoted strings) */
	  buf_index++;

	  while (buf_index < bufsize && buf[buf_index] != '>') {
	    if (buf[buf_index] == '"') {
	      /* Skip over quoted string */
	      do {
		buf_index++;
	      } while (buf_index < bufsize && buf[buf_index] != '"');
	      if (buf_index < bufsize) {
		/* Skip closing quote */
		buf_index++;
	      }
	    } else {
	      buf_index++;
	    }
	  }

	  if (buf_index < bufsize) {
	    buf_index++;
	    gzilla_html_process_tag (html, buf + token_start,
				     buf_index - token_start);
	    token_start = buf_index;
	  }
      }
    } else {
      /* a word : search for whitespace or tag open */
      do {
	buf_index++;
      } while (buf_index < bufsize && !isspace (buf[buf_index]) &&
	       buf[buf_index] != '<');
      if (buf_index < bufsize) {
	/* successfully found end of token */
	gzilla_html_process_word (html, buf + token_start,
			       buf_index - token_start);
	token_start = buf_index;
      }
    }
  }

  /* put partial token in inbuf, if any. */
  html->size_inbuf = bufsize - token_start;
  if (html->size_inbuf > 0) {
    if (html->size_inbuf_max < html->size_inbuf) {
      do {
	html->size_inbuf_max <<= 1;
      } while (html->size_inbuf_max < bufsize);
      html->inbuf = g_realloc (html->inbuf, html->size_inbuf_max);
    }
    memmove (html->inbuf, buf + token_start, html->size_inbuf);
  }

#ifdef USE_GZW
  gzw_page_update_end (page);
#else
  gtk_page_update_end (page);
#endif
}

static void gzilla_html_set_base_url (GzillaLinkBlock *linkblock,
				      const char *url,
				      GzillaByteSink *bytesink) {
  GzillaHtml *html;

  g_return_if_fail (bytesink != NULL);
  g_return_if_fail (GZILLA_IS_HTML (bytesink));
  g_return_if_fail (url != NULL);

  html = GZILLA_HTML (bytesink);

  if (html->base_url != NULL)
    g_free (html->base_url);
  html->base_url = g_strdup (url);
#ifdef VERBOSE
  g_print ("gzilla_html_set_base_url: %s\n", url);
#endif
}

static void
gzilla_html_close (GzillaByteSink *bytesink)
{
  gtk_object_destroy (GTK_OBJECT (bytesink));
}

static void
gzilla_html_status (GzillaByteSink *bytesink,
		    GzillaStatusDir dir,
		    gboolean abort,
		    GzillaStatusMeaning meaning,
		    const char *text)
{
#ifdef VERBOSE
  g_print ("gzilla_html_status: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (abort)
    gtk_object_destroy (GTK_OBJECT (bytesink));
}

static void
gzilla_html_destroy (GtkObject *object)
{
  GzillaHtml *html;
  gint i;

  html = GZILLA_HTML (object);

  g_free (html->inbuf);

  for (i = 0; i < html->num_states; i++)
    g_free (html->stack[i].tag);
  g_free (html->stack);

  g_free (html->base_url);

  g_free (html->stash_buf);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}
