#ifndef __GZILLA_BOOKMARK_H__
#define __GZILLA_BOOKMARK_H__

void gzilla_bookmark_add(GtkWidget *widget, gpointer client_data);
void gzilla_bookmarks_load(char *file);
void gzilla_bookmarks_close(void);
/*void gzilla_bookmark_load(char *title, char *url); */
void gzilla_bookmarks_init (BrowserWindow *bw);

#endif /* __GZILLA_BOOKMARK_H__ */
