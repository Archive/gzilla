#include <stdio.h>		/* for sprintf */

#include <gtk/gtk.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>		/* needed before including gzillasocket.h */

#include "gzillabytesink.h"
#include "gzillaweb.h"
#include "gzillaurl.h"
#include "gtkstatuslabel.h"
#include "gzillacache.h"
#include "gzillasocket.h"
#include "interface.h"
#include "gzilla.h"
#include "gzillanav.h"
#include "gzillabrowser.h"
#include "commands.h"
#include "menus.h"
#include "config.h"
#include "gzillabookmark.h"

#include "gzw.h"
#include "gtkgzwscroller.h"
#include "gzwborder.h"

#include "pixmaps.h"

/* BrowserWindow holds all the widgets (and perhaps more) 
 * for each new_browser
 * 
 * delcared like this in interface.h:
 * BrowserWindow *browser_window[MAX_BROWSER_WINDOWS];
 *
 */


BrowserWindow **browser_window;
gint num_bw, num_bw_max;

void gzilla_bw_init (void)
{
	num_bw = 0;
	num_bw_max = 16;
	browser_window = g_new (BrowserWindow *, num_bw_max);
}

/* Delete the bytesink from the bytesinks structure. */
void gzilla_bw_delete_bytesink (BrowserWindow *bw, GzillaByteSink *bytesink)
{
	gint i;

	for (i = 0; i < bw->num_bytesinks; i++)
		{
			if (bw->bytesinks[i] == bytesink)
			break;
		}
	if (i < bw->num_bytesinks)
	{
		bw->bytesinks[i] = bw->bytesinks[--bw->num_bytesinks];
	}
	else
		g_warning ("gzilla_bw_delete_bytesink: trying to delete nonexistent bytesink\n");
}

/* A handler for the close signal on bytesinks. This function gets
   called whenever a bytesink feeding one of the widgets on this
   window is closed. If the number of bytesinks reaches zero, then
   make the stop button and menu entry insensitive.
*/
void gzilla_bw_close_handler (GzillaByteSink *bytesink, BrowserWindow *bw)
{
	gzilla_bw_delete_bytesink (bw, bytesink);

	if (bw->num_bytesinks == 0 && bw->num_imgsinks == 0)
	{
		gzilla_status ("", bw);
		gtk_widget_set_sensitive (bw->stop_button, FALSE);
		gtk_widget_set_sensitive (bw->stop_menuitem, FALSE);
	}
}

void gzilla_bw_status_handler (GzillaByteSink *bytesink, GzillaStatusDir dir,
	gboolean abort, GzillaStatusMeaning meaning,
	const char *text, BrowserWindow *bw)
{
	if (meaning == GZILLA_STATUS_MEANING_SHOW)
		gzilla_status (text, bw);

	if (abort)
	{
		gzilla_bw_delete_bytesink (bw, bytesink);

		if (bw->num_bytesinks == 0 && bw->num_imgsinks == 0)
		{
			gzilla_status ("", bw);
			gtk_widget_set_sensitive (bw->stop_button, FALSE);
			gtk_widget_set_sensitive (bw->stop_menuitem, FALSE);
		}
	}
}

static gint gzilla_bw_sens_idle_func (BrowserWindow *bw)
{
	gboolean back_sensitive, forw_sensitive;

	/* todo: put stop sensitivity in here too */
	back_sensitive = bw->gzilla_nav_ptr > 1;
	gtk_widget_set_sensitive(bw->back_button, back_sensitive);
	gtk_widget_set_sensitive(bw->back_menuitem, back_sensitive);
	forw_sensitive = (bw->gzilla_nav_ptr < bw->size_gzilla_nav &&
		!bw->last_waiting);
	gtk_widget_set_sensitive(bw->forw_button, forw_sensitive);
	gtk_widget_set_sensitive(bw->forw_menuitem, forw_sensitive);

	bw->sens_idle_tag = 0;
	return FALSE;
}

/* Set the sensitivity on back/forw buttons and menu entries. */
void gzilla_bw_set_button_sens (BrowserWindow *bw)
{
	if (bw->sens_idle_tag == 0)
	{
		bw->sens_idle_tag = gtk_idle_add
			((GtkFunction) gzilla_bw_sens_idle_func, bw);
	}
}

static void gzilla_bw_have_gzw (GzillaByteSink *doc, Gzw *gzw, BrowserWindow *bw)
{
	gtk_gzw_scroller_set_gzw (GTK_GZW_SCROLLER (bw->docwin),
		gzw_border_new (gzw, 5));
	gzilla_bw_connect_linkblock (bw, bw->linkblock_loading);
	bw->linkblock_loading = NULL;
	if (bw->last_waiting)
	{
		bw->gzilla_nav_ptr = bw->gzilla_nav_loading_ptr;
		gzilla_bw_set_button_sens (bw);
		gtk_entry_set_text (GTK_ENTRY(bw->location),
			bw->gzilla_nav[bw->gzilla_nav_ptr - 1].url);
		bw->last_waiting = 0;
	}
}

/* Add a pointer to the bytesink to the browser window's bytesinks
   structure. This helps us keep track of which bytesinks are active
   in the window so that it's possible to abort them.

   Also connects the close handler to the close signal on the
   bytesink.

   Note: this function doesn't register the bytesink as active.

   If it's the first bytesink added, a number of additional signal
   handlers are added. This is a hack, I know.
*/
void gzilla_bw_add_bytesink (BrowserWindow *bw, GzillaByteSink *bytesink)
{
	if (bw->num_bytesinks == 0)
	{
		gtk_widget_set_sensitive (bw->stop_button, TRUE);
		gtk_widget_set_sensitive (bw->stop_menuitem, TRUE);
	}

	if (bw->num_bytesinks == bw->num_bytesinks_max)
	{
		bw->num_bytesinks_max <<= 1;
		bw->bytesinks = g_realloc (bw->bytesinks, bw->num_bytesinks_max *
			sizeof (GzillaByteSink *));
	}
	bw->bytesinks[bw->num_bytesinks++] = bytesink;

	gtk_signal_connect (GTK_OBJECT (bytesink), "close",
		(GtkSignalFunc) gzilla_bw_close_handler, bw);

	gtk_signal_connect (GTK_OBJECT (bytesink), "status",
	(GtkSignalFunc) gzilla_bw_status_handler, bw);

	#if 0
		/* todo: this seems still not quite right, but it does seem to work. */
		if (bw->num_bytesinks == 1)
		gtk_signal_connect (GTK_OBJECT (gzilla_web_get_linkblock
			(GZILLA_WEB (bytesink))), "have_gzw",
			(GtkSignalFunc) gzilla_bw_have_gzw, bw);
	#endif
}

/* todo: this isn't exported, so maybe make static */
void gzilla_bw_connect_linkblock (BrowserWindow *bw, GzillaLinkBlock *linkblock)
{
	if (bw->linkblock != NULL)
		gtk_object_destroy (GTK_OBJECT (bw->linkblock));

	bw->linkblock = linkblock;
		       
	gtk_signal_connect (GTK_OBJECT (linkblock), "request_url",
		GTK_SIGNAL_FUNC (request_url), bw);
	gtk_signal_connect (GTK_OBJECT (linkblock), "link",
		GTK_SIGNAL_FUNC (follow_link), bw);
	gtk_signal_connect (GTK_OBJECT (linkblock), "status",
		GTK_SIGNAL_FUNC (show_page_status), bw);
	gtk_signal_connect (GTK_OBJECT (linkblock), "title",
		GTK_SIGNAL_FUNC (title), bw);
	gtk_signal_connect (GTK_OBJECT (linkblock), "request_url_img",
		GTK_SIGNAL_FUNC (request_url_img), bw);
}

void gzilla_bw_connect_linkblock_loading (BrowserWindow *bw,
	GzillaLinkBlock *linkblock)
{
	if (bw->linkblock_loading != NULL)
	gtk_object_destroy (GTK_OBJECT (bw->linkblock_loading));

	bw->linkblock_loading = linkblock;

	gtk_signal_connect (GTK_OBJECT (linkblock), "have_gzw",
		(GtkSignalFunc) gzilla_bw_have_gzw, bw);
	gtk_signal_connect (GTK_OBJECT (linkblock), "redirect",
		GTK_SIGNAL_FUNC (redirect), bw);
}

/* And now for the imgsinks... */
static void gzilla_bw_img_close_handler (void *data, GzillaImgSink *imgsink)
{
	gint i;
	BrowserWindow *bw = data;

	for (i = 0; i < bw->num_imgsinks; i++)
		if (bw->imgsinks[i] == imgsink)
		break;
	if (i < bw->num_imgsinks)
		bw->imgsinks[i] = bw->imgsinks[--bw->num_imgsinks];
	else
		g_warning ("gzilla_bw_img_close_handler: trying to delete nonexistent imgsink\n");

	#ifdef VERBOSE
		g_print ("gzilla_bw_img_close_handler: num_imgsinks = %d\n", bw->num_imgsinks);
	#endif
	if (bw->num_bytesinks == 0 && bw->num_imgsinks == 0)
	{
		gzilla_status ("", bw);
		gtk_widget_set_sensitive (bw->stop_button, FALSE);
		gtk_widget_set_sensitive (bw->stop_menuitem, FALSE);
	}
}

static void gzilla_bw_img_status_handler (void *data, GzillaImgSink *imgsink,
			      GzillaStatusDir dir, gboolean abort,
			      GzillaStatusMeaning meaning, const char *text)
{
	gint i;
	BrowserWindow *bw = data;

	if (meaning == GZILLA_STATUS_MEANING_SHOW)
		gzilla_status (text, bw);

	if (!abort) return;

	for (i = 0; i < bw->num_imgsinks; i++)
		if (bw->imgsinks[i] == imgsink)
			break;
	if (i < bw->num_imgsinks)
		bw->imgsinks[i] = bw->imgsinks[--bw->num_imgsinks];
	else
		g_warning ("gzilla_bw_img_status_handler: trying to delete nonexistent imgsink\n");

	#ifdef VERBOSE
		g_print ("gzilla_bw_img_status_handler: num_imgsinks = %d\n", bw->num_imgsinks);
	#endif
	if (bw->num_bytesinks == 0 && bw->num_imgsinks == 0)
	{
		gzilla_status ("", bw);
		gtk_widget_set_sensitive (bw->stop_button, FALSE);
		gtk_widget_set_sensitive (bw->stop_menuitem, FALSE);
	}
}

/* Add a pointer to the imgsink to the browser window's imgsinks
   structure. This helps us keep track of which imgsinks are active
   in the window so that it's possible to abort them.
*/
void gzilla_bw_add_imgsink (BrowserWindow *bw, GzillaImgSink *imgsink)
{
	if (bw->num_imgsinks == bw->num_imgsinks_max)
    {
		bw->num_imgsinks_max <<= 1;
		bw->imgsinks = g_realloc (bw->imgsinks, bw->num_imgsinks_max *
			sizeof (GzillaImgSink *));
	}
	bw->imgsinks[bw->num_imgsinks++] = imgsink;

	gzilla_imgsink_set_close_handler (imgsink, gzilla_bw_img_close_handler, bw);
	gzilla_imgsink_set_status_handler2 (imgsink, gzilla_bw_img_status_handler,
		bw);

	#ifdef VERBOSE
		g_print ("gzilla_bw_add_imgsink: num_imgsinks = %d\n", bw->num_imgsinks);
	#endif
}

/* Abort all active bytesinks and imgsinks in the window. */
void gzilla_bw_abort_all (BrowserWindow *bw)
{
	while (bw->num_bytesinks > 0)
	{
		gzilla_bytesink_status (bw->bytesinks[0], GZILLA_STATUS_DIR_ORIG,
			TRUE, GZILLA_STATUS_MEANING_DEBUG, "aborted by gzilla_bw_abort_all");
	}

	while (bw->num_imgsinks > 0)
	{
		gzilla_imgsink_status (bw->imgsinks[0], GZILLA_STATUS_DIR_ORIG, TRUE,
			GZILLA_STATUS_MEANING_DEBUG, "aborted by gzilla_bw_abort_all");
	}
	#ifdef VERBOSE
	g_print ("exit gzilla_bw_abort_all\n");
	#endif
}

/* Remove a browser window. This includes destroying all open windows
   (except for the main browser window, which is presumed to be
   destroyed in the caller), freeing all resources associated with the
   browser window, and exiting gtk if no windows are left.
*/
static gboolean gzilla_bw_quit (GtkWidget *widget, BrowserWindow *bw)
{
	gint i;

	/* todo: should probably abort any open connections. */
	if (bw->open_dialog_window != NULL)
		gtk_widget_destroy (bw->open_dialog_window);
	if (bw->openfile_dialog_window != NULL)
		gtk_widget_destroy (bw->openfile_dialog_window);
	if (bw->quit_dialog_window != NULL)
		gtk_widget_destroy (bw->quit_dialog_window);

	if (bw->sens_idle_tag)
		gtk_idle_remove (bw->sens_idle_tag);

	for (i = 0; i < num_bw; i++)
		if (browser_window[i] == bw)
		{
			browser_window[i] = browser_window[--num_bw];
			break;
		}
	for (i = 0; i < bw->size_gzilla_nav; i++)
	{
		g_free (bw->gzilla_nav[i].url);
		if (bw->gzilla_nav[i].title != NULL)
		g_free (bw->gzilla_nav[i].title);
	}
	g_free (bw->gzilla_nav);
	for (i = 0; i < bw->num_bookmarks; i++)
	{
		g_free (bw->bookmarks[i].title);
		g_free (bw->bookmarks[i].url);
	}
	g_free (bw->bookmarks);
	g_free (bw);
	if (num_bw == 0)
		gtk_main_quit ();

	return FALSE;
}

#if 0
/* Find the browser window that the bytesink belongs to. */
/* I'm not sure we need this. */
	BrowserWindow * gzilla_bw_find (GzillaByteSink *bytesink)
{
	gint i, j;

	for (i = 0; i < num_bw; i++)
		for (j = 0; j < browser_window[i]->num_bytesinks; j++)
 			if (browser_window[i]->bytesinks[j] == bytesink)
				return browser_window[i];
	return NULL;
}

/* Find the browser window struct given some widget that's contained
   inside. */
/* I don't think we need this either. */
BrowserWindow * gzilla_bw_find_from_widget (GtkWidget *widget)
{
	GtkWidget *toplevel;
	gint i;

	toplevel = gtk_widget_get_toplevel (widget);
	for (i = 0; i < num_bw; i++)
		if (browser_window[i]->main_window == toplevel)
			return browser_window[i];
	return NULL;
}
#endif

/*

 Creates a box with a pixmap and a label in it and returns it.

*/

GtkWidget *gzilla_xpm_label_box(GtkWidget *parent, gchar **data, gchar *lbltxt)
{
	GtkWidget *box;
	GtkWidget *label;
	GtkWidget *pixmapwid;
	GdkPixmap *pixmap;
	GdkBitmap *mask;
	GtkStyle *style;

	box = gtk_vbox_new(FALSE, 0);

	gtk_container_set_border_width(GTK_CONTAINER(box), 0);

	style = gtk_widget_get_style(parent);

	pixmap = gdk_pixmap_create_from_xpm_d(parent->window, &mask,
		 &style->bg[GTK_STATE_NORMAL], data);

	pixmapwid = gtk_pixmap_new(pixmap, mask);

	label = gtk_label_new(lbltxt);

	gtk_box_pack_start(GTK_BOX(box), pixmapwid, FALSE, FALSE, 3);
	gtk_box_pack_start(GTK_BOX(box), label, FALSE, FALSE, 3);

	gdk_pixmap_unref(pixmap);
	gdk_pixmap_unref(mask);

	gtk_widget_show(pixmapwid);
	gtk_widget_show(label);

	return(box);
}

/* this function creates a new browser window and returns the browser
   window struct. */

BrowserWindow * gzilla_new_browser_window (void)
{
	/* used to create new windows - index into browser_window[] 
	* not really implemented. */

	GtkWidget *button;
	GtkWidget *toolbar;
	GtkWidget *box1;
	GtkWidget *box2;
	GtkWidget *box3;
	GtkWidget *button_box1;
	GtkWidget *button_box2;
	GtkWidget *button_box3;
	GtkWidget *button_box4;
	GtkWidget *button_box5;
	GtkWidget *menubar;
	BrowserWindow *bw;

	char buf[256];

	/* allocate and zero memory for new browser_window info */
	/* if I were to use g_malloc() would it zero the memory ?
	* is there a g_calloc() ? :) */
	bw = g_new (BrowserWindow, 1);

	if (num_bw == num_bw_max)
	{
		num_bw_max <<= 1;
		browser_window = g_realloc (browser_window, num_bw_max *
			sizeof (BrowserWindow));
	}
	browser_window[num_bw] = bw;

	/* initialize gzilla_nav struct in browser_window struct */
	gzilla_nav_init (bw);

	bw->main_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

	gtk_window_set_policy (GTK_WINDOW (bw->main_window),
		TRUE, TRUE, FALSE);
	gtk_signal_connect (GTK_OBJECT (bw->main_window), "delete_event",
		GTK_SIGNAL_FUNC (gtk_object_destroy), bw);
	gtk_signal_connect (GTK_OBJECT (bw->main_window), "destroy",
		GTK_SIGNAL_FUNC (gzilla_bw_quit), bw);
	gtk_container_border_width (GTK_CONTAINER (bw->main_window), 0);

	gtk_window_set_wmclass (GTK_WINDOW (bw->main_window), "gzilla", "Gzilla");

	/* set window title */
	sprintf (buf, "Version %s", VERSION);
	gzilla_set_page_title (bw, buf);

	/* todo: I think this should go. */
	/* set the browser_window url to none */
	bw->gzilla_nav->url = g_strdup ("none");

	box1 = gtk_vbox_new (FALSE, 0);

	/* setup the menus */
	#ifdef OLD_MENUS
		menus_get_gzilla_menubar (&menubar, &table);
	#else
		menubar = gzilla_menu_mainbar_new (bw);
	#endif
	gtk_box_pack_start (GTK_BOX (box1), menubar, FALSE, FALSE, 0);
	gtk_widget_show (menubar);

	gzilla_bookmarks_init (bw);

	#ifdef GTK_HAVE_FEATURES_1_1_0
		gtk_window_add_accel_group (GTK_WINDOW (bw->main_window),
			bw->accel_group);
	#else
		gtk_window_add_accelerator_table (GTK_WINDOW (bw->main_window),
			bw->accel_table);
	#endif

	box2 = gtk_hbox_new (FALSE, 0);

	gtk_box_pack_start (GTK_BOX (box1), box2, FALSE, FALSE, 5);
 	gtk_widget_show (box2);

	toolbar = gtk_hbox_new (TRUE, 0);

	bw->sens_idle_tag = 0;

	/* back button */

	bw->back_button = gtk_button_new();
	button_box1 = gzilla_xpm_label_box(bw->main_window, leftarrow_xpm,
                      "Back");
	gtk_widget_show(button_box1);
	gtk_container_add(GTK_CONTAINER(bw->back_button), button_box1);

	/* todo: use sens callback? */
	gtk_widget_set_sensitive (bw->back_button, FALSE);
	gtk_widget_set_sensitive (bw->back_menuitem, FALSE);
	gtk_signal_connect (GTK_OBJECT (bw->back_button), "clicked",
		(GtkSignalFunc) browse_back_cmd_callback, bw);
	gtk_box_pack_start (GTK_BOX (toolbar), bw->back_button,
		TRUE, TRUE, 0);
	gtk_widget_show (bw->back_button);

	/* forward button */
	bw->forw_button = gtk_button_new();
	button_box2 = gzilla_xpm_label_box(bw->main_window, rghtarrow_xpm,
	"Forward");
	gtk_widget_show(button_box2);
	gtk_container_add(GTK_CONTAINER(bw->forw_button), button_box2);

	gtk_widget_set_sensitive (bw->forw_button, FALSE);
	gtk_widget_set_sensitive (bw->forw_menuitem, FALSE);
	gtk_signal_connect (GTK_OBJECT (bw->forw_button), "clicked",
		(GtkSignalFunc) browse_forw_cmd_callback, bw);
	gtk_box_pack_start (GTK_BOX (toolbar), bw->forw_button,
		TRUE, TRUE, 0);
	gtk_widget_show (bw->forw_button);

	/* home button */
	button = gtk_button_new();
	button_box3 = gzilla_xpm_label_box(bw->main_window, home_xpm, "Home");
	gtk_widget_show(button_box3);
	gtk_container_add(GTK_CONTAINER(button), button_box3);

	gtk_signal_connect (GTK_OBJECT (button), "clicked",
		(GtkSignalFunc) browse_home_cmd_callback, bw);
	gtk_box_pack_start (GTK_BOX (toolbar), button, TRUE, TRUE, 0);
	gtk_widget_show (button);

	/* reload button */
	button = gtk_button_new();
	button_box4 = gzilla_xpm_label_box(bw->main_window, reload_xpm,
	"Reload");
	gtk_widget_show(button_box4);
	gtk_container_add(GTK_CONTAINER(button), button_box4);

	
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
		(GtkSignalFunc) browse_reload_cmd_callback, bw);
	gtk_box_pack_start (GTK_BOX (toolbar), button, TRUE, TRUE, 0);
	gtk_widget_show (button);

	/* stop button */
	bw->stop_button = gtk_button_new();
	button_box5 = gzilla_xpm_label_box(bw->main_window, stop_xpm, "Stop");
	gtk_widget_show(button_box5);
	gtk_container_add(GTK_CONTAINER(bw->stop_button), button_box5);

	gtk_widget_set_sensitive (bw->stop_button, FALSE);
	gtk_widget_set_sensitive (bw->stop_menuitem, FALSE);
	gtk_signal_connect (GTK_OBJECT (bw->stop_button), "clicked",
		(GtkSignalFunc) browse_stop_cmd_callback, bw);
	gtk_box_pack_start (GTK_BOX (toolbar), bw->stop_button,
		TRUE, TRUE, 0);
	gtk_widget_show (bw->stop_button);

	/* pack the toolbar */
	gtk_box_pack_start (GTK_BOX (box2), toolbar, FALSE, FALSE, 3);
	gtk_widget_show (toolbar);

	gtk_container_add (GTK_CONTAINER (bw->main_window), box1);
	gtk_widget_show (box1);

	/* location entry */
	bw->location = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (box1), bw->location, FALSE, FALSE, 0);
	gtk_widget_show (bw->location);

	gtk_signal_connect (GTK_OBJECT (bw->location), "activate",
		(GtkSignalFunc) entry_open_url, bw);

	/* the main document window */
	bw->docwin = gtk_gzw_scroller_new (NULL, NULL);
	gtk_gzw_scroller_set_policy (GTK_GZW_SCROLLER (bw->docwin),
		GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (box1), bw->docwin, TRUE, TRUE, 0);
	gtk_widget_show (bw->docwin);

	bw->num_bytesinks = 0;
	bw->num_bytesinks_max = 1;
	bw->bytesinks = g_new (GzillaByteSink *, bw->num_bytesinks_max);

	bw->num_imgsinks = 0;
	bw->num_imgsinks_max = 1;
	bw->imgsinks = g_new (GzillaImgSink *, bw->num_imgsinks_max);

	bw->linkblock = NULL;
	bw->linkblock_loading = NULL;

	#if 1
		gtk_widget_set_usize (bw->main_window, 500, 540);
	#endif

	/* status widget */
	bw->status = gtk_statuslabel_new ("");
	gtk_misc_set_alignment (GTK_MISC (bw->status), 0.0, 0.5);
	box3 = gtk_hbox_new (FALSE, 0);
	
	#if 1
		gtk_box_pack_start (GTK_BOX (box3), bw->status, TRUE, TRUE, 2);
		gtk_widget_show (bw->status);
		/* we could also put a key, a progress bar, and a mail icon here */
		/* I'd like to see a seperate thing for the link your mouse is on top
		* of.. seperated from the current status widget like.. -Ian*/
		gtk_box_pack_start (GTK_BOX (box1), box3, FALSE, FALSE, 2);
		gtk_widget_show (box3);
	#else
		gtk_box_pack_start (GTK_BOX (box1), bw->status, FALSE, FALSE, 2);
		gtk_widget_show (bw->status);
	#endif

	gtk_widget_show (bw->main_window);

	#if 0
		/* Accelerators can't be non-ASCII - looks like we'll have to do all
		our keypresses through the focus mechanism. */
		gtk_accelerator_table_install (table, GTK_OBJECT (location),
				 "destroy", GDK_Return, 0);
	#endif

	/* Zero out a bunch of stuff in the bw struct. */
	bw->open_dialog_window = NULL;
	bw->openfile_dialog_window = NULL;
	bw->quit_dialog_window = NULL;
	num_bw++;
	return bw;
}

/* sets the title of the browser window to title with "Gzilla: "
   prepended to it. Also sets the page_title member of the current
   gzilla_nav structure for use with the bookmarks. */
void gzilla_set_page_title (BrowserWindow *bw, char *title)
{
	char *buf;
	gint nav_index;

	/* free previously allocated string */
	nav_index = bw->gzilla_nav_ptr - 1;
	if (nav_index >= 0)
	{
		if (bw->gzilla_nav[nav_index].title)
			g_free (bw->gzilla_nav[nav_index].title);
		bw->gzilla_nav[nav_index].title = g_strdup (title);
	}

	buf = g_new (char, strlen (title) + 128);
	sprintf (buf, "Gzilla: %s", title);
	gtk_window_set_title (GTK_WINDOW (bw->main_window), buf);
	g_free (buf);
}

/* sets the status string on the bottom of the gzilla window. 
 * TODO: add int win_num to this call */
void gzilla_status (const char *string, BrowserWindow *bw)
{
	gtk_label_set (GTK_LABEL (bw->status), string);
}

/* hmm.. have to see where this is from */
void destroy_window (GtkWidget *widget, GtkWidget ** window)
{
	*window = NULL;
}

void gzilla_quit_all (void)
{
	BrowserWindow **bws;
	gint i, n_bw;

	n_bw = num_bw;
	bws = g_new (BrowserWindow *, n_bw);

	/* we copy into a new list because destroying the main window can
	modify the browser_window array. */
	for (i = 0; i < n_bw; i++)
		bws[i] = browser_window[i];

	for (i = 0; i < n_bw; i++)
		gtk_widget_destroy (bws[i]->main_window);

	g_free (bws);

	#ifdef VERBOSE
		g_print ("gzilla_quit_all: exit\n");
	#endif
}

static void push_help (GtkWidget *widget, char *url, BrowserWindow *bw)
{
	gzilla_nav_push (url, bw);
}

static void openfile_ok_callback (GtkWidget *widget, BrowserWindow *bw)
{
	char *fn;
	char url[1024];

	#ifdef VERBOSE
		g_print ("openfile_ok_callback: %s\n", fn);
	#endif
	fn = gtk_file_selection_get_filename
	(GTK_FILE_SELECTION (bw->openfile_dialog_window));
	if (strlen (fn) + 6 <= sizeof (url))
	{
		sprintf (url, "file:%s", fn);
		gzilla_nav_push (url, bw);
	}
	gtk_widget_destroy (bw->openfile_dialog_window);
}


void gzilla_entry_clear (GtkEntry *entry)
{
	gtk_entry_set_text (entry, "");
	gtk_widget_grab_focus (GTK_WIDGET (entry));
}

void entry_open_url (GtkWidget *widget, BrowserWindow *bw)
{
	char url[1024];
	GtkEntry *entry;

	/* Find which entry to get the URL from. This is a bit of a hack. */

	if (widget == bw->location)
		entry = GTK_ENTRY (bw->location);
	else
		entry = GTK_ENTRY (bw->open_dialog_entry);
	#ifdef VERBOSE
	g_print ("entry_open_url %s\n", gtk_entry_get_text (entry));
	#endif
	if (gzilla_url_relative ("http:/", gtk_entry_get_text (entry),
		url, sizeof (url)))
		gzilla_nav_push (url, bw);
	if (bw->open_dialog_window != NULL)
		gtk_widget_destroy (bw->open_dialog_window);
	/* todo: pull focus away from location widget probably by focusing
	to the page, but at the time this todo was added, the gtk_page
	widget wasn't really focusable. */
}

void create_openfile_dialog (BrowserWindow *bw)
{
	if (!bw->openfile_dialog_window)
	{
		bw->openfile_dialog_window = gtk_file_selection_new ("Open File");
		gtk_signal_connect (GTK_OBJECT (bw->openfile_dialog_window), "destroy",
			(GtkSignalFunc) destroy_window, &(bw->openfile_dialog_window));
		gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION
			(bw->openfile_dialog_window)->ok_button),
			"clicked", (GtkSignalFunc) openfile_ok_callback, bw);
		gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION
			(bw->openfile_dialog_window)->cancel_button),
			"clicked", (GtkSignalFunc) gtk_widget_hide,
			GTK_OBJECT (bw->openfile_dialog_window));
		gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION
			(bw->openfile_dialog_window)->help_button),
			"clicked", (GtkSignalFunc) push_help,
			"http://www.levien.com/gimp/");
	}
	if (!GTK_WIDGET_VISIBLE (bw->openfile_dialog_window))
		gtk_widget_show (bw->openfile_dialog_window);
	else
		gtk_widget_destroy (bw->openfile_dialog_window);
}

void create_open_dialog (GtkWidget *widget, BrowserWindow *bw)
{
	GtkWidget *button;
	GtkWidget *box1;
	GtkWidget *box2;
	GtkWidget *entry;

	if (!bw->open_dialog_window)
	{
		bw->open_dialog_window = gtk_window_new (GTK_WINDOW_DIALOG);
		#if 1
			gtk_signal_connect (GTK_OBJECT (bw->open_dialog_window), "destroy",
				(GtkSignalFunc) destroy_window, &(bw->open_dialog_window));
		#endif
		gtk_window_set_title (GTK_WINDOW (bw->open_dialog_window),
			"Gzilla: Open URL");
		gtk_container_border_width (GTK_CONTAINER (bw->open_dialog_window), 5);

		box1 = gtk_vbox_new (FALSE, 5);
		gtk_container_add (GTK_CONTAINER (bw->open_dialog_window), box1);
		gtk_widget_show (box1);

		entry = gtk_entry_new ();
		bw->open_dialog_entry = entry;
		gtk_widget_set_usize (entry, 250, 0);
		gtk_box_pack_start (GTK_BOX (box1), entry, FALSE, FALSE, 0);
		gtk_widget_show (entry);

		gtk_signal_connect (GTK_OBJECT (entry), "activate", (GtkSignalFunc)
			entry_open_url, bw);

		box2 = gtk_hbox_new (TRUE, 5);
		gtk_box_pack_start (GTK_BOX (box1), box2, FALSE, FALSE, 0);
		gtk_widget_show (box2);

		button = gtk_button_new_with_label ("OK");
		gtk_signal_connect (GTK_OBJECT (button), "clicked", (GtkSignalFunc)
			entry_open_url, bw);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (box2), button, FALSE, TRUE, 0);
		gtk_widget_grab_default (button);
		gtk_widget_show (button);
		gtk_signal_connect_object (GTK_OBJECT (entry), "focus_in_event",
			(GtkSignalFunc) gtk_widget_grab_default, GTK_OBJECT (button));

		button = gtk_button_new_with_label ("Clear");
		gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			(GtkSignalFunc) gzilla_entry_clear, GTK_OBJECT (entry));
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (box2), button, FALSE, TRUE, 0);
		gtk_widget_show (button);

		button = gtk_button_new_with_label ("Cancel");
		gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			(GtkSignalFunc) gtk_widget_destroy,
			GTK_OBJECT (bw->open_dialog_window));
 		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (box2), button, FALSE, TRUE, 0);
 		gtk_widget_show (button);

		gtk_widget_grab_focus (entry);

    }

	/* todo: hiding vs. destroying leaves two problems open. First, the
	entry is not focussed when it's re-shown. Second, closing the
	window (from the window manager) really does destroy the contents.

	Perhaps a better solution is to keep a spare copy of the entry
	text around (updated only when the Ok button is clicked), and use
	it to initialize the entry. Also delete rather than hide the
	window.

	But it's ok for now.
	*/
	/* if you take a look at the "OK" button, it calls entry_open_url which
	calls gtk_destroy_widget on your open_dialog_window.. maybe this is
	what's recking it for you ? -Ian */

	if (!GTK_WIDGET_VISIBLE (bw->open_dialog_window))
		gtk_widget_show (bw->open_dialog_window);
	else
		gtk_widget_hide (bw->open_dialog_window);
}

/* shouldn't all this stuff go in menu.c or something ? 
 * why do we need global window GtkWidgets for the windows ?? 
 * I see you use it in the destroy() function, but I don't 
 * really grasp it's use just yet :) */

/* The way gimp does it is to have a menus.c for the menus themselves
 * and a commands.c for the *_cmd callbacks. They also have an
 * interface.c where most of the interface stuff happens. We should
 * probably follow their lead.
 * 
 * We have globals for the windows so I can destroy all windows before
 * quitting. Some window managers (including fvwm2-95) really like
 * this. -Raph
 */

/* funny you say that, cause if you look at the quit_dialog_window I made, I just
 * gtk_destroy_widget(main_window).. it seems to work ok, but should we change
 * it ? -Ian
 */

/* No, the way it is now is fine. When you destroy the main window,
   that invokes the destroy signal handler, which destroys all of the
   other windows cleanly. 

   Anyway, I wrote the above explanation before we had multiple
   windows. Another reason to do it this way is that when you close
   one of several main windows, its various subsidiary windows go away
   too. -Raph */

void create_quit_dialog (BrowserWindow *bw)
{
	GtkWidget *button;
	GtkWidget *label;

	if (!bw->quit_dialog_window)
	{
		bw->quit_dialog_window = gtk_dialog_new ();
		gtk_signal_connect (GTK_OBJECT (bw->quit_dialog_window), "destroy",
			(GtkSignalFunc) destroy_window, &(bw->quit_dialog_window));
		gtk_window_set_title (GTK_WINDOW (bw->quit_dialog_window),
			"Really Quit ?");
		gtk_container_border_width (GTK_CONTAINER (bw->quit_dialog_window), 0);

		label = gtk_label_new ("Really Quit Gzilla ?");
		gtk_misc_set_padding (GTK_MISC (label), 10, 10);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (bw->quit_dialog_window)->vbox),
			label, TRUE, TRUE, 0);
		gtk_widget_show (label);

		button = gtk_button_new_with_label ("OK");
		/* they said OK, is there some cleanup we have to do before
		destroying the main window ? - set it to call
		gzilla_quit_all which cleans things up a bit..
		hopefuly an improvement. -Ian
		*/
		gtk_signal_connect (GTK_OBJECT (button), "clicked",
			(GtkSignalFunc) gzilla_quit_all, NULL);
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG
			(bw->quit_dialog_window)->action_area), button, TRUE, TRUE, 0);
		gtk_widget_grab_default (button);
		gtk_widget_show (button);

		button = gtk_button_new_with_label ("Cancel");
		gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
			(GtkSignalFunc) gtk_widget_destroy,
			GTK_OBJECT (bw->quit_dialog_window));
		GTK_WIDGET_SET_FLAGS (button, GTK_CAN_DEFAULT);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG
			(bw->quit_dialog_window)->action_area), button, TRUE, TRUE, 0);
		gtk_widget_show (button);
	}

	if (!GTK_WIDGET_VISIBLE (bw->quit_dialog_window))
		gtk_widget_show (bw->quit_dialog_window);
	else
		gtk_widget_destroy (bw->quit_dialog_window);
	/* I much prefer it not to retain the last thing I typed in there
	you know that :)  I always have to clear  the  dumb netscape
	one.. I find it rather nice this way. */
}
