#ifndef __GZILLA_GET_URL_H__
#define __GZILLA_GET_URL_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Create a new network connection for URL url, and asyncrhonously
   feed the bytes that come back to bytesink. */
void gzilla_get_url (const char *url,
                     GzillaByteSink *bytesink,
		     void (*status_callback) (const char *status, void *data),
		     void *data);

/* And that's it! The resulting module is expected to be sensitive to
   ABORT signals from the bytesink. */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_GET_URL_H__ */
