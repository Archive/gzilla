/* Part of Gzilla, but adapted from gtkgzwscroller.c, so I'm including
   the standard GTK+ license: */

/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gtk/gtk.h>
#include "gtkgzwscroller.h"
#include "gtkgzwview.h"


#define SCROLLBAR_SPACING  5


static void gtk_gzw_scroller_class_init         (GtkGzwScrollerClass *klass);
static void gtk_gzw_scroller_init               (GtkGzwScroller      *gzw_scroller);
static void gtk_gzw_scroller_destroy            (GtkObject              *object);
static void gtk_gzw_scroller_map                (GtkWidget              *widget);
static void gtk_gzw_scroller_unmap              (GtkWidget              *widget);
static void gtk_gzw_scroller_draw               (GtkWidget              *widget,
						    GdkRectangle           *area);
static void gtk_gzw_scroller_size_request       (GtkWidget              *widget,
						    GtkRequisition         *requisition);
static void gtk_gzw_scroller_size_allocate      (GtkWidget              *widget,
						    GtkAllocation          *allocation);
#if 0
static void gtk_gzw_scroller_add                (GtkContainer           *container,
						    GtkWidget              *widget);
static void gtk_gzw_scroller_remove             (GtkContainer           *container,
						    GtkWidget              *widget);
#endif
static void gtk_gzw_scroller_foreach            (GtkContainer           *container,
						    GtkCallback             callback,
						    gpointer                callback_data);
static void gtk_gzw_scroller_viewport_allocate  (GtkWidget              *widget,
						    GtkAllocation          *allocation);
static void gtk_gzw_scroller_adjustment_changed (GtkAdjustment          *adjustment,
						    gpointer                data);


static GtkContainerClass *parent_class = NULL;


guint
gtk_gzw_scroller_get_type ()
{
  static guint gzw_scroller_type = 0;

  if (!gzw_scroller_type)
    {
      GtkTypeInfo gzw_scroller_info =
      {
	"GtkGzwScroller",
	sizeof (GtkGzwScroller),
	sizeof (GtkGzwScrollerClass),
	(GtkClassInitFunc) gtk_gzw_scroller_class_init,
	(GtkObjectInitFunc) gtk_gzw_scroller_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      gzw_scroller_type = gtk_type_unique (gtk_container_get_type (), &gzw_scroller_info);
    }

  return gzw_scroller_type;
}

static void
gtk_gzw_scroller_class_init (GtkGzwScrollerClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkContainerClass *container_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;
  container_class = (GtkContainerClass*) class;

  parent_class = gtk_type_class (gtk_container_get_type ());

  object_class->destroy = gtk_gzw_scroller_destroy;

  widget_class->map = gtk_gzw_scroller_map;
  widget_class->unmap = gtk_gzw_scroller_unmap;
  widget_class->draw = gtk_gzw_scroller_draw;
  widget_class->size_request = gtk_gzw_scroller_size_request;
  widget_class->size_allocate = gtk_gzw_scroller_size_allocate;

#if 0
  container_class->add = gtk_gzw_scroller_add;
  container_class->remove = gtk_gzw_scroller_remove;
#endif

#if 0
  container_class->foreach = gtk_gzw_scroller_foreach;
#endif
}

static void
gtk_gzw_scroller_init (GtkGzwScroller *gzw_scroller)
{
  GTK_WIDGET_SET_FLAGS (gzw_scroller, GTK_NO_WINDOW);

  gzw_scroller->hscrollbar = NULL;
  gzw_scroller->vscrollbar = NULL;
  gzw_scroller->hscrollbar_policy = GTK_POLICY_ALWAYS;
  gzw_scroller->vscrollbar_policy = GTK_POLICY_ALWAYS;
}

GtkWidget*
gtk_gzw_scroller_new (GtkAdjustment *hadjustment,
			 GtkAdjustment *vadjustment)
{
  GtkGzwScroller *gzw_scroller;

  gzw_scroller = gtk_type_new (gtk_gzw_scroller_get_type ());

  gzw_scroller->viewport = gtk_gzw_view_new (hadjustment, vadjustment);
  hadjustment = gtk_gzw_view_get_hadjustment (GTK_GZW_VIEW (gzw_scroller->viewport));
  vadjustment = gtk_gzw_view_get_vadjustment (GTK_GZW_VIEW (gzw_scroller->viewport));

  gtk_signal_connect (GTK_OBJECT (hadjustment), "changed",
		      (GtkSignalFunc) gtk_gzw_scroller_adjustment_changed,
		      (gpointer) gzw_scroller);
  gtk_signal_connect (GTK_OBJECT (vadjustment), "changed",
		      (GtkSignalFunc) gtk_gzw_scroller_adjustment_changed,
		      (gpointer) gzw_scroller);

  gzw_scroller->hscrollbar = gtk_hscrollbar_new (hadjustment);
  gzw_scroller->vscrollbar = gtk_vscrollbar_new (vadjustment);

  gtk_widget_set_parent (gzw_scroller->viewport, GTK_WIDGET (gzw_scroller));
  gtk_widget_set_parent (gzw_scroller->hscrollbar, GTK_WIDGET (gzw_scroller));
  gtk_widget_set_parent (gzw_scroller->vscrollbar, GTK_WIDGET (gzw_scroller));

  gtk_widget_show (gzw_scroller->viewport);
  gtk_widget_show (gzw_scroller->hscrollbar);
  gtk_widget_show (gzw_scroller->vscrollbar);

  return GTK_WIDGET (gzw_scroller);
}

GtkAdjustment*
gtk_gzw_scroller_get_hadjustment (GtkGzwScroller *gzw_scroller)
{
  g_return_val_if_fail (gzw_scroller != NULL, NULL);
  g_return_val_if_fail (GTK_IS_GZW_SCROLLER (gzw_scroller), NULL);

  return gtk_range_get_adjustment (GTK_RANGE (gzw_scroller->hscrollbar));
}

GtkAdjustment*
gtk_gzw_scroller_get_vadjustment (GtkGzwScroller *gzw_scroller)
{
  g_return_val_if_fail (gzw_scroller != NULL, NULL);
  g_return_val_if_fail (GTK_IS_GZW_SCROLLER (gzw_scroller), NULL);

  return gtk_range_get_adjustment (GTK_RANGE (gzw_scroller->vscrollbar));
}

void
gtk_gzw_scroller_set_policy (GtkGzwScroller *gzw_scroller,
				GtkPolicyType      hscrollbar_policy,
				GtkPolicyType      vscrollbar_policy)
{
  g_return_if_fail (gzw_scroller != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (gzw_scroller));

  if ((gzw_scroller->hscrollbar_policy != hscrollbar_policy) ||
      (gzw_scroller->vscrollbar_policy != vscrollbar_policy))
    {
      gzw_scroller->hscrollbar_policy = hscrollbar_policy;
      gzw_scroller->vscrollbar_policy = vscrollbar_policy;

      if (GTK_WIDGET (gzw_scroller)->parent)
	gtk_widget_queue_resize (GTK_WIDGET (gzw_scroller));
    }
}


static void
gtk_gzw_scroller_destroy (GtkObject *object)
{
  GtkGzwScroller *gzw_scroller;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (object));

  gzw_scroller = GTK_GZW_SCROLLER (object);

  gtk_widget_destroy (gzw_scroller->viewport);
  gtk_widget_destroy (gzw_scroller->hscrollbar);
  gtk_widget_destroy (gzw_scroller->vscrollbar);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gtk_gzw_scroller_map (GtkWidget *widget)
{
  GtkGzwScroller *gzw_scroller;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (widget));

  if (!GTK_WIDGET_MAPPED (widget))
    {
      GTK_WIDGET_SET_FLAGS (widget, GTK_MAPPED);
      gzw_scroller = GTK_GZW_SCROLLER (widget);

      if (GTK_WIDGET_VISIBLE (gzw_scroller->viewport) &&
	  !GTK_WIDGET_MAPPED (gzw_scroller->viewport))
	gtk_widget_map (gzw_scroller->viewport);

      if (GTK_WIDGET_VISIBLE (gzw_scroller->hscrollbar) &&
	  !GTK_WIDGET_MAPPED (gzw_scroller->hscrollbar))
	gtk_widget_map (gzw_scroller->hscrollbar);

      if (GTK_WIDGET_VISIBLE (gzw_scroller->vscrollbar) &&
	  !GTK_WIDGET_MAPPED (gzw_scroller->vscrollbar))
	gtk_widget_map (gzw_scroller->vscrollbar);
    }
}

static void
gtk_gzw_scroller_unmap (GtkWidget *widget)
{
  GtkGzwScroller *gzw_scroller;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (widget));

  if (GTK_WIDGET_MAPPED (widget))
    {
      GTK_WIDGET_UNSET_FLAGS (widget, GTK_MAPPED);
      gzw_scroller = GTK_GZW_SCROLLER (widget);

      if (GTK_WIDGET_MAPPED (gzw_scroller->viewport))
	gtk_widget_unmap (gzw_scroller->viewport);

      if (GTK_WIDGET_MAPPED (gzw_scroller->hscrollbar))
	gtk_widget_unmap (gzw_scroller->hscrollbar);

      if (GTK_WIDGET_MAPPED (gzw_scroller->vscrollbar))
	gtk_widget_unmap (gzw_scroller->vscrollbar);
    }
}

static void
gtk_gzw_scroller_draw (GtkWidget    *widget,
			  GdkRectangle *area)
{
  GtkGzwScroller *gzw_scroller;
  GdkRectangle child_area;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (widget));
  g_return_if_fail (area != NULL);

  if (GTK_WIDGET_DRAWABLE (widget))
    {
      gzw_scroller = GTK_GZW_SCROLLER (widget);

      if (gtk_widget_intersect (gzw_scroller->viewport, area, &child_area))
	gtk_widget_draw (gzw_scroller->viewport, &child_area);

      if (gtk_widget_intersect (gzw_scroller->hscrollbar, area, &child_area))
	gtk_widget_draw (gzw_scroller->hscrollbar, &child_area);

      if (gtk_widget_intersect (gzw_scroller->vscrollbar, area, &child_area))
	gtk_widget_draw (gzw_scroller->vscrollbar, &child_area);
    }
}

static void
gtk_gzw_scroller_size_request (GtkWidget      *widget,
				  GtkRequisition *requisition)
{
  GtkGzwScroller *gzw_scroller;
  gint extra_height;
  gint extra_width;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (widget));
  g_return_if_fail (requisition != NULL);

  gzw_scroller = GTK_GZW_SCROLLER (widget);

  requisition->width = 0;
  requisition->height = 0;

  if (GTK_WIDGET_VISIBLE (gzw_scroller->viewport))
    {
      gtk_widget_size_request (gzw_scroller->viewport, &gzw_scroller->viewport->requisition);

      requisition->width += gzw_scroller->viewport->requisition.width;
      requisition->height += gzw_scroller->viewport->requisition.height;
    }

  extra_width = 0;
  extra_height = 0;

  if ((gzw_scroller->hscrollbar_policy == GTK_POLICY_AUTOMATIC) ||
      GTK_WIDGET_VISIBLE (gzw_scroller->hscrollbar))
    {
      gtk_widget_size_request (gzw_scroller->hscrollbar,
			       &gzw_scroller->hscrollbar->requisition);

      requisition->width = MAX (requisition->width, gzw_scroller->hscrollbar->requisition.width);
      extra_height = SCROLLBAR_SPACING + gzw_scroller->hscrollbar->requisition.height;
    }

  if ((gzw_scroller->vscrollbar_policy == GTK_POLICY_AUTOMATIC) ||
      GTK_WIDGET_VISIBLE (gzw_scroller->vscrollbar))
    {
      gtk_widget_size_request (gzw_scroller->vscrollbar,
			       &gzw_scroller->vscrollbar->requisition);

      requisition->height = MAX (requisition->height, gzw_scroller->vscrollbar->requisition.height);
      extra_width = SCROLLBAR_SPACING + gzw_scroller->vscrollbar->requisition.width;
    }

  requisition->width += GTK_CONTAINER (widget)->border_width * 2 + extra_width;
  requisition->height += GTK_CONTAINER (widget)->border_width * 2 + extra_height;
}

static void
gtk_gzw_scroller_size_allocate (GtkWidget     *widget,
				   GtkAllocation *allocation)
{
  GtkGzwScroller *gzw_scroller;
  GtkAllocation viewport_allocation;
  GtkAllocation child_allocation;
  guint previous_hvis;
  guint previous_vvis;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (widget));
  g_return_if_fail (allocation != NULL);

  gzw_scroller = GTK_GZW_SCROLLER (widget);
  widget->allocation = *allocation;

  gtk_gzw_scroller_viewport_allocate (widget, &viewport_allocation);

#if 0
  gtk_container_disable_resize (GTK_CONTAINER (gzw_scroller));
#endif

  if (GTK_WIDGET_VISIBLE (gzw_scroller->viewport))
    {
      do {
	gtk_gzw_scroller_viewport_allocate (widget, &viewport_allocation);

	child_allocation.x = viewport_allocation.x + allocation->x;
	child_allocation.y = viewport_allocation.y + allocation->y;
	child_allocation.width = viewport_allocation.width;
	child_allocation.height = viewport_allocation.height;

	previous_hvis = GTK_WIDGET_VISIBLE (gzw_scroller->hscrollbar);
	previous_vvis = GTK_WIDGET_VISIBLE (gzw_scroller->vscrollbar);

	gtk_widget_size_allocate (gzw_scroller->viewport, &child_allocation);
      } while ((previous_hvis != GTK_WIDGET_VISIBLE (gzw_scroller->hscrollbar)) ||
	       (previous_vvis != GTK_WIDGET_VISIBLE (gzw_scroller->vscrollbar)));
    }

  if (GTK_WIDGET_VISIBLE (gzw_scroller->hscrollbar))
    {
      child_allocation.x = viewport_allocation.x;
      child_allocation.y = viewport_allocation.y + viewport_allocation.height + SCROLLBAR_SPACING;
      child_allocation.width = viewport_allocation.width;
      child_allocation.height = gzw_scroller->hscrollbar->requisition.height;
      child_allocation.x += allocation->x;
      child_allocation.y += allocation->y;

      gtk_widget_size_allocate (gzw_scroller->hscrollbar, &child_allocation);
    }

  if (GTK_WIDGET_VISIBLE (gzw_scroller->vscrollbar))
    {
      child_allocation.x = viewport_allocation.x + viewport_allocation.width + SCROLLBAR_SPACING;
      child_allocation.y = viewport_allocation.y;
      child_allocation.width = gzw_scroller->vscrollbar->requisition.width;
      child_allocation.height = viewport_allocation.height;
      child_allocation.x += allocation->x;
      child_allocation.y += allocation->y;

      gtk_widget_size_allocate (gzw_scroller->vscrollbar, &child_allocation);
    }

#if 0
  gtk_container_enable_resize (GTK_CONTAINER (gzw_scroller));
#endif
}

#if 0
static void
gtk_gzw_scroller_add (GtkContainer *container,
			 GtkWidget    *widget)
{
  GtkGzwScroller *gzw_scroller;

  g_return_if_fail (container != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (container));
  g_return_if_fail (widget != NULL);

  gzw_scroller = GTK_GZW_SCROLLER (container);
  gtk_container_add (GTK_CONTAINER (gzw_scroller->viewport), widget);
}

static void
gtk_gzw_scroller_remove (GtkContainer *container,
			    GtkWidget    *widget)
{
  GtkGzwScroller *gzw_scroller;

  g_return_if_fail (container != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (container));
  g_return_if_fail (widget != NULL);

  gzw_scroller = GTK_GZW_SCROLLER (container);
  gtk_container_remove (GTK_CONTAINER (gzw_scroller->viewport), widget);
}
#endif

void
gtk_gzw_scroller_set_gzw (GtkGzwScroller *gzw_scroller, Gzw *gzw)
{
#ifdef VERBOSE
  g_print ("gtk_gzw_scroller_set_gzw\n");
#endif

  gtk_gzw_view_set_gzw (GTK_GZW_VIEW (gzw_scroller->viewport), gzw);
}

static void
gtk_gzw_scroller_foreach (GtkContainer *container,
			     GtkCallback   callback,
			     gpointer      callback_data)
{
  GtkGzwScroller *gzw_scroller;

  g_return_if_fail (container != NULL);
  g_return_if_fail (GTK_IS_GZW_SCROLLER (container));
  g_return_if_fail (callback != NULL);

  gzw_scroller = GTK_GZW_SCROLLER (container);

  (* callback) (gzw_scroller->viewport, callback_data);
}

static void
gtk_gzw_scroller_viewport_allocate (GtkWidget     *widget,
				       GtkAllocation *allocation)
{
  GtkGzwScroller *gzw_scroller;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (allocation != NULL);

  gzw_scroller = GTK_GZW_SCROLLER (widget);

  allocation->x = GTK_CONTAINER (widget)->border_width;
  allocation->y = GTK_CONTAINER (widget)->border_width;
  allocation->width = widget->allocation.width - allocation->x * 2;
  allocation->height = widget->allocation.height - allocation->y * 2;

  if (GTK_WIDGET_VISIBLE (gzw_scroller->vscrollbar))
    allocation->width -= gzw_scroller->vscrollbar->requisition.width + SCROLLBAR_SPACING;
  if (GTK_WIDGET_VISIBLE (gzw_scroller->hscrollbar))
    allocation->height -= gzw_scroller->hscrollbar->requisition.height + SCROLLBAR_SPACING;
}

static void
gtk_gzw_scroller_adjustment_changed (GtkAdjustment *adjustment,
					gpointer       data)
{
  GtkGzwScroller *scrolled_win;
  GtkWidget *scrollbar;
  gint hide_scrollbar;
  gint policy;

  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (data != NULL);

  scrolled_win = GTK_GZW_SCROLLER (data);

  if (adjustment == gtk_range_get_adjustment (GTK_RANGE (scrolled_win->hscrollbar)))
    {
      scrollbar = scrolled_win->hscrollbar;
      policy = scrolled_win->hscrollbar_policy;
    }
  else if (adjustment == gtk_range_get_adjustment (GTK_RANGE (scrolled_win->vscrollbar)))
    {
      scrollbar = scrolled_win->vscrollbar;
      policy = scrolled_win->vscrollbar_policy;
    }
  else
    {
      g_warning ("could not determine which adjustment scrollbar received change signal for");
      return;
    }

  if (policy == GTK_POLICY_AUTOMATIC)
    {
      hide_scrollbar = FALSE;

      if ((adjustment->upper - adjustment->lower) <= adjustment->page_size)
	hide_scrollbar = TRUE;

      if (hide_scrollbar)
	{
	  if (GTK_WIDGET_VISIBLE (scrollbar))
	    gtk_widget_hide (scrollbar);
	}
      else
	{
	  if (!GTK_WIDGET_VISIBLE (scrollbar))
	    gtk_widget_show (scrollbar);
	}
    }
}
