/*

   gzilla

   Copyright 1997 Raph Levien <raph@acm.org>

   This code is free for commercial and non-commercial use,
   modification, and redistribution, as long as the source code release,
   startup screen, or product packaging includes this copyright notice.

 */

/* gzilla_plain: a bytesink for decoding a text/plain object into
   a gtk_page widget. */

#include <string.h>		/* for memcpy and memmove */

#include <gtk/gtk.h>
#include "gzillabytesink.h"
#include "gzillaplain.h"
#include "gzw.h"
#ifdef USE_GZW
#include "gzwpage.h"
#else
#include "gtkpage.h"
#endif

static void gzilla_plain_class_init (GzillaPlainClass * klass);
static void gzilla_plain_init (GzillaPlain * plain);
static void gzilla_plain_destroy (GtkObject * object);
static void gzilla_plain_write (GzillaByteSink * bytesink,
				char *bytes,
				gint num);
static void gzilla_plain_close (GzillaByteSink * bytesink);
static void gzilla_plain_status (GzillaByteSink *bytesink,
				 GzillaStatusDir dir,
				 gboolean abort,
				 GzillaStatusMeaning meaning,
				 const char *text);

static GzillaByteSinkClass *parent_class = NULL;

guint
gzilla_plain_get_type ()
{
  static guint plain_type = 0;

  if (!plain_type)
    {
      GtkTypeInfo plain_info =
      {
	"GzillaPlain",
	sizeof (GzillaPlain),
	sizeof (GzillaPlainClass),
	(GtkClassInitFunc) gzilla_plain_class_init,
	(GtkObjectInitFunc) gzilla_plain_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      plain_type = gtk_type_unique (gzilla_bytesink_get_type (), &plain_info);
    }

  return plain_type;
}

void
gzilla_plain_class_init (GzillaPlainClass * class)
{
  GtkObjectClass      *object_class;
  GzillaByteSinkClass *bytesink_class;

  parent_class = gtk_type_class (gzilla_bytesink_get_type ());

  object_class = (GtkObjectClass*) class;
  bytesink_class = (GzillaByteSinkClass *) class;

  object_class->destroy = gzilla_plain_destroy;

  bytesink_class->write = gzilla_plain_write;
  bytesink_class->close = gzilla_plain_close;
  bytesink_class->status = gzilla_plain_status;
}

void 
gzilla_plain_init (GzillaPlain * plain)
{

  plain->size_inbuf_max = 1024;
  plain->size_inbuf = 0;
  plain->inbuf = g_new (char, plain->size_inbuf_max);
}

GzillaByteSink *
gzilla_plain_new (void)
{
  GzillaPlain *plain;
#ifdef USE_GZW
  Gzw *page;
  GzwPageFont font;
  GzwPageAttr attr;
#else
  GtkWidget *page;
  GtkPageFont font;
  GtkPageAttr attr;
#endif

  plain = gtk_type_new (gzilla_plain_get_type ());
#ifdef USE_GZW
  page = gzw_page_new ();

  plain->gzw = page;
#else
  page = gtk_page_new ();

  plain->bytesink.widget = page;
#endif

  /* Create the font and attribute for the page. */

#if 0
  font.font = NULL;
#endif
  font.name = "courier";
  font.size = 12;
  font.bold = FALSE;
  font.italic = FALSE;

#ifdef USE_GZW
  gzw_page_init_attr ((GzwPage *)page, &attr);
  attr.font = gzw_page_find_font ((GzwPage *)page, &font);

  plain->attr = gzw_page_find_attr ((GzwPage *)page, &attr);
#else
  gtk_page_init_attr (GTK_PAGE (page), &attr);
  attr.font = gtk_page_find_font (GTK_PAGE (page), &font);

  plain->attr = gtk_page_find_attr (GTK_PAGE (page), &attr);
#endif

  return GZILLA_BYTESINK (plain);
}

/* Process a line (has end of line characters stripped off) */
static void
gzilla_plain_process_line (GzillaPlain * plain,
			   char *line,
			   gint size)
{
  char *linecopy;

  linecopy = g_malloc (size + 1);
  memcpy (linecopy, line, size);
  linecopy[size] = '\0';
#ifdef VERBOSE
  g_print ("|%s\n", linecopy);
#endif
#ifdef USE_GZW
  gzw_page_add_text ((GzwPage *)plain->gzw,
		     linecopy,
		     plain->attr);
  gzw_page_linebreak ((GzwPage *)plain->gzw);
#else
  gtk_page_add_text (GTK_PAGE (plain->bytesink.widget),
		     linecopy,
		     plain->attr);
  gtk_page_linebreak (GTK_PAGE (plain->bytesink.widget));
#endif
}

static void 
gzilla_plain_write (GzillaByteSink * bytesink,
		    char *bytes,
		    gint num)
{
#ifdef USE_GZW
  GzwPage *page;
#else
  GtkPage *page;
#endif
  GzillaPlain *plain;
  char *buf;
  gint bufsize, line_start, line_end, buf_index;

  g_return_if_fail (bytesink != NULL);
  g_return_if_fail (GZILLA_IS_PLAIN (bytesink));
  g_return_if_fail (bytes != NULL);

  plain = GZILLA_PLAIN (bytesink);

#ifdef USE_GZW 
  page = (GzwPage *)plain->gzw;

  gzw_page_update_begin (page);
#else
  page = GTK_PAGE (bytesink->widget);

  gtk_page_update_begin (page);
#endif

  /* Here's where we parse the text and put it into the page structure. */

  /* Concatenate with the partial line, if any. */
  if (plain->size_inbuf)
    {
      bufsize = plain->size_inbuf + num;
      if (plain->size_inbuf_max < bufsize)
	{
	  do
	    {
	      plain->size_inbuf_max <<= 1;
	    }
	  while (plain->size_inbuf_max < bufsize);
	  plain->inbuf = g_realloc (plain->inbuf, plain->size_inbuf_max);
	}
      buf = plain->inbuf;
      memcpy (buf + plain->size_inbuf, bytes, num);
    }
  else
    {
      buf = bytes;
      bufsize = num;
    }

  /* Now, buf and bufsize define a buffer aligned to start at a line
     boundary. Iterate through lines until end of buffer is reached. */
  buf_index = 0;
  line_start = buf_index;
  while (buf_index < bufsize)
    {

      while (buf_index < bufsize && buf[buf_index] != '\n')
	buf_index++;

      if (buf_index < bufsize)
	{
	  line_end = buf_index;
	  if (line_end > line_start && buf[line_end - 1] == '\r')
	    line_end--;
	  buf_index++;
	  gzilla_plain_process_line (plain, buf + line_start,
				     line_end - line_start);
	  line_start = buf_index;
	}
    }

  /* put partial line in inbuf, if any. */
  plain->size_inbuf = bufsize - line_start;
  if (plain->size_inbuf > 0)
    {
      if (plain->size_inbuf_max < plain->size_inbuf)
	{
	  do
	    {
	      plain->size_inbuf_max <<= 1;
	    }
	  while (plain->size_inbuf_max < bufsize);
	  plain->inbuf = g_realloc (plain->inbuf, plain->size_inbuf_max);
	}
      memmove (plain->inbuf, buf + line_start, plain->size_inbuf);
    }

#ifdef USE_GZW
  gzw_page_update_end (page);
#else
  gtk_page_update_end (page);
#endif
}

static void 
gzilla_plain_close (GzillaByteSink * bytesink)
{
#ifdef USE_GZW
  GzwPage *page;
#else
  GtkPage *page;
#endif
  GzillaPlain *plain;

  g_return_if_fail (bytesink != NULL);
  g_return_if_fail (GZILLA_IS_PLAIN (bytesink));

  plain = GZILLA_PLAIN (bytesink);

#ifdef USE_GZW 
  page = (GzwPage *)plain->gzw;

  gzw_page_update_begin (page);
#else
  page = GTK_PAGE (bytesink->widget);

  gtk_page_update_begin (page);
#endif

  /* Process the partial line at the end, if any. */
  if (plain->size_inbuf)
    gzilla_plain_process_line (plain, plain->inbuf, plain->size_inbuf);

#ifdef USE_GZW
  gzw_page_update_end (page);
#else
  gtk_page_update_end (page);
#endif
}

static void
gzilla_plain_status (GzillaByteSink *bytesink,
		     GzillaStatusDir dir,
		     gboolean abort,
		     GzillaStatusMeaning meaning,
		     const char *text)
{
#ifdef VERBOSE
  g_print ("gzilla_plain_status: %d %d %d %s\n",
	   dir, abort, meaning, text);
#endif

  if (abort)
    gtk_object_destroy (GTK_OBJECT (bytesink));
}

static void
gzilla_plain_destroy (GtkObject *object)
{
  GzillaPlain *plain;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GZILLA_IS_PLAIN (object));

  plain = GZILLA_PLAIN (object);
  g_free (plain->inbuf);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

