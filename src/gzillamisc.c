#include <gtk/gtk.h>

#include <stdio.h>
#include <stdlib.h>


/* returns the home directory of the user
 * DON'T try to free this one cause it's really a pointer
 * into the environment structure */
char *gzilla_misc_user_home(void)
{
    static char *home_dir;
    static int init = 1;
    
    if (init) {
	home_dir = getenv("HOME");
	init = 0;
    }
    
    return(home_dir);
}

/* pass in a string, and it will add the users home dir ie,
 * pass in .gzilla/bookmarks.html and it will return
 * /home/imain/.gzilla/bookmarks.html
 * 
 * Remember to g_free() returned value! */
char *gzilla_misc_prepend_user_home(char *file)
{
    char *home_return;
    home_return = g_malloc (strlen(file) + strlen(gzilla_misc_user_home()) + 3);
    
    sprintf(home_return, "%s/%s", gzilla_misc_user_home(), file);
    return(home_return);
}
/* very similar to above, but adds $HOME/.gzilla/ to beginning
 * This is meant to be the most useful version.
 * Perhaps this should be named gzilla_home_file ?
 */
char *gzilla_misc_file(char *file)
{
    char *home_return;
    home_return = g_malloc (strlen(file) + strlen(gzilla_misc_user_home()) + 16);
    
    sprintf(home_return, "%s/.gzilla/%s", gzilla_misc_user_home(), file);
    return(home_return);
}
