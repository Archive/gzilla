 /*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>            /* for errno */

#include <sys/wait.h>
#include <gtk/gtk.h>

#include "gzillabytesink.h"
#include "gzillaproto.h"

/* This module handles the Gzilla plug-in api. */

/* todo: get abort to work. */

typedef struct _GzillaProto GzillaProto;

struct _GzillaProto {
  const char *prefix;
  const char *handler;
};

static GzillaProto plugins[] = {
  { "test:", "/home/raph/gzilla/get_test" },
  { "ftp:", "/home/raph/gzilla/get_ftp" }
};

typedef struct _GzillaPPICon GzillaPPICon;

struct _GzillaPPICon { /* Protocol Plug_In Connection */
  gint fd;
  GzillaByteSink *bytesink;
  pid_t pid;
  gint rd_tag;
};

static gint num_ppic;
static gint num_ppic_max;
static GzillaPPICon *ppics;

void
gzilla_proto_init (void)
{
  num_ppic = 0;
  num_ppic_max = 16;
  ppics = g_new (GzillaPPICon, num_ppic_max);

  /* todo: scan through the plugin directory, etc. */
}

static gboolean
gzilla_proto_prefix (const char *url, const char *prefix)
{
  return !strncmp (url, prefix, strlen (prefix));
}

/* Find the plug-in number to handle a given URL, or -1 if there is no
   plug-in for it. */

gint
gzilla_proto_find (const char *url)
{
  gint i;

  for (i = 0; i < sizeof (plugins) / sizeof (GzillaProto); i++)
    if (gzilla_proto_prefix (url, plugins[i].prefix))
      return i;
  return -1;
}

#define MAX_ENV 32

static void
gzilla_proto_add_env (char **env, gint *penv_index,
		      const char *name, const char *val)
{
  gint name_len, val_len;
  char *buf;

  if (*penv_index == MAX_ENV)
    {
      g_error ("gzilla_proto_add_env: environment overflow");
    }
  name_len = strlen (name);
  val_len = strlen (val);
  buf = g_malloc (name_len + val_len + 2);
  memcpy (buf, name, name_len);
  buf[name_len] = '=';
  memcpy (buf + name_len + 1, val, val_len);
  buf[name_len + 1 + val_len] = '\0';
#ifdef VERBOSE
  g_print ("env[%d] = %s\n", *penv_index, buf);
#endif
  env[(*penv_index)++] = buf;
}

static void
gzilla_proto_input_func (gpointer data,
			gint source,
			GdkInputCondition condition)
{
  GzillaByteSink *bytesink = (GzillaByteSink *)data;
  gint i;
  char buf[8192];
  gint num_bytes;
  int stat;

  for (i = 0; i < num_ppic; i++)
    {
      if (ppics[i].bytesink == bytesink)
	break;
    }
  if (i < num_ppic)
    {
      num_bytes = read (ppics[i].fd, buf, sizeof (buf));
      if (num_bytes < 0)
	{
	  if (errno == EINTR || errno == EAGAIN)
	    return;
	  num_bytes = 0;
	}
      if (num_bytes > 0)
	gzilla_bytesink_write (ppics[i].bytesink, buf, num_bytes);
      else
	{
#if 0
	  gtk_signal_disconnect (GTK_OBJECT (http[i].bytesink),
				 http[i].abort_id);
#endif
#ifdef VERBOSE
	  g_print ("gzilla_proto_input_func: close\n");
#endif
	  gdk_input_remove (ppics[i].rd_tag);
	  close (ppics[i].fd);
	  gzilla_bytesink_close (ppics[i].bytesink);
	  waitpid (ppics[i].pid, &stat, 0);
	  ppics[i] = ppics[--num_ppic];
	}
    }
  else
    g_warning ("gzilla_proto_input_function: trying to write into ppic that has already been removed\n");
}

void
gzilla_proto_get_url (const char *url,
		      gint proto_num,
		      GzillaByteSink *bytesink,
		      void (*status_callback) (const char *status, void *data),
		      void *data)
{
  char **env;
  gint env_index = 0;
  pid_t child_pid;
  int filedes[2];

  env = g_new (char *, MAX_ENV);

  gzilla_proto_add_env (env, &env_index, "PATH", getenv ("PATH"));

  gzilla_proto_add_env (env, &env_index, "REQUEST_URI", url);

  env[env_index] = NULL;

  if (pipe (filedes) != 0)
    {
      g_error ("gzilla_proto_get_url: pipe call failed\n");
    }

  child_pid = fork ();

  if (child_pid == -1)
      g_error ("gzilla_proto_get_url: can't fork to invoke plugin\n");

  if (!child_pid) {
    /* this is the child */

    /* redirect stdout to the pipe we just created */
    if (close (STDOUT_FILENO) != 0)
      g_error ("gzilla_proto_get_url: error closing stdout\n");
    if (dup (filedes[1]) != STDOUT_FILENO)
      g_error ("gzilla_proto_get_url: error in dup call for stdout\n");
    close (filedes[0]);
    close (filedes[1]);

    {
      gint i = 0;

      fprintf (stderr, "proto_num = %d, %s\n",
	       proto_num, plugins[proto_num].handler);
      while (env[i])
	fprintf (stderr, "%s\n", env[i++]);
    }

    execle (plugins[proto_num].handler,
	    plugins[proto_num].handler,
	    (char *)NULL,
	    env);
    
    g_error ("invocation of plugin %s failed!\n",
	     plugins[proto_num].handler);
  }

  /* this is the parent */

  /* set nonblocking */
  fcntl (filedes[0], F_SETFL, O_NONBLOCK | fcntl(filedes[0], F_GETFL));

  /* set close-on-exec */
  fcntl (filedes[0], F_SETFD, FD_CLOEXEC | fcntl(filedes[0], F_GETFD));

  close (filedes[1]);

  if (num_ppic == num_ppic_max)
    {
      num_ppic_max <<= 1;
      ppics = g_realloc (ppics, num_ppic_max * sizeof(GzillaPPICon));
    }
  ppics[num_ppic].fd = filedes[0];
  ppics[num_ppic].bytesink = bytesink;
  ppics[num_ppic].pid = child_pid;
  ppics[num_ppic].rd_tag =
    gdk_input_add (filedes[0],
		   GDK_INPUT_READ,
		   (GdkInputFunction) gzilla_proto_input_func,
		   bytesink);

  num_ppic++;
}
