/* to be renamed gzillasocket.c */

/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */


/* This module provides an API for asynchronously opening a client
   socket using the GTK framework.
   */

#include <gtk/gtk.h>

#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>

#include <netdb.h>		/* for gethostbyname, etc. */
#include <sys/socket.h>		/* for lots of socket stuff */
#include <netinet/in.h>		/* for ntohl and stuff */

#include "gzilladns.h"

static void gzilla_socket_input_handler(gpointer data,
					gint source,
					GdkInputCondition condition);

typedef struct _GzillaSocket GzillaSocket;

typedef enum {
  GZILLA_SOCKET_INIT,
  GZILLA_SOCKET_DNS_WAIT,
  GZILLA_SOCKET_CONNECTING
} GzillaSocketState;

struct _GzillaSocket {
  gint tag;
  gint dns_tag;
  gint input_tag;
  gint port;
  struct sockaddr_in sin;
  gint fd;
  void (* callback) (gint fd, void *callback_data);
  void *callback_data;
  GzillaSocketState state;
};

static GzillaSocket *sockets;
static gint num_sockets;
static gint num_sockets_max;
static gint socket_tag = 0;

/* Initialize the socket data structures. */
void
gzilla_socket_init (void)
{
  num_sockets = 0;
  num_sockets_max = 16;
  sockets = g_new (GzillaSocket, num_sockets_max);
}


/* Try connecting the socket. On success or error, delete the socket
   structure, remove the input tag (if any), and call the callback. */

static void
gzilla_socket_try_connect (gint socket_index)
{
  gint status;
  gint fd;
  void (* callback) (gint fd, void *callback_data);
  void *callback_data;

  fd = sockets[socket_index].fd;
  if (fd >= 0)
    {
      status = connect(fd,
		       (struct sockaddr *) &(sockets[socket_index].sin),
		       sizeof(struct sockaddr_in));
      /* Note: handling of the EISCONN error condition was added by
	 Tristan Tarrant on 28 Nov 1997. This apparently fixes some
	 problems on Solaris 2.5.

	 What's still totally unclear to me is why there would be an
	 EISCONN error return. Is it possible that this routine is
	 getting called to many times? */
      if (status < 0 && errno == EISCONN)
        {
          status = 0;
        }
      else if (status < 0 && !(errno == EINPROGRESS ||
	       		       errno == EAGAIN ||
			       errno == EINTR))
	{
	  /* todo: convey an error message to the user. This will
	     usually be "connection refused by host". */
	  close (fd);
	  fd = -1;
	}
    }
  if (fd < 0 || status >= 0)
    {
      callback = sockets[socket_index].callback;
      callback_data = sockets[socket_index].callback_data;

      if (sockets[socket_index].input_tag)
	gdk_input_remove (sockets[socket_index].input_tag);

      sockets[socket_index] = sockets[--num_sockets];

      /* Note: do the callback after changes to socket structure are
	 complete, just in case callback wants to allocate any new
	 sockets or abort any old ones. */

      callback (fd, callback_data);
    }
  else
    {
      if (sockets[socket_index].input_tag == 0)
	sockets[socket_index].input_tag =
	  gdk_input_add (fd,
			 GDK_INPUT_WRITE,
			 (GdkInputFunction) gzilla_socket_input_handler,
			 (void *)sockets[socket_index].tag);
    }
}


/* The input handler. Try to connect the socket. */

static void
gzilla_socket_input_handler (gpointer data,
			     gint source,
			     GdkInputCondition condition)
{
  gint tag = (gint)data;
  gint i;

  for (i = 0; i < num_sockets; i++)
    {
      if (sockets[i].tag == tag)
	break;
    }
  if (i < num_sockets)
    gzilla_socket_try_connect (i);
  else
    g_warning ("gzilla socket structure inconsistent!\n");
}

/* The DNS callback. Create the socket. Try connecting it if in
   DNS_WAIT state. If it's not in DNS_WAIT state, then it's in INIT
   state and the try_connect call will happen in gzilla_socket_new. */

static void
gzilla_socket_dns_callback (guint32 ip_addr, void *callback_data)
{
  gint tag = (gint)callback_data;
  gint i;
  gint fd;
  GzillaSocketState old_state;

  for (i = 0; i < num_sockets; i++)
    {
      if (sockets[i].tag == tag)
	break;
    }
  if (i < num_sockets)
    {
      /* Found the socket structure. */
      fd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
      sockets[i].fd = fd;
      if (fd >= 0)
	{
	  /* set nonblocking */
	  fcntl (fd, F_SETFL, O_NONBLOCK | fcntl(fd, F_GETFL));

	  /* set close-on-exec */
	  fcntl (fd, F_SETFD, FD_CLOEXEC | fcntl(fd, F_GETFD));

	  /* set up sin structure, in preparation for the connect call */
	  sockets[i].sin.sin_family = AF_INET;
	  sockets[i].sin.sin_port = htons (sockets[i].port);
	  sockets[i].sin.sin_addr.s_addr = htonl (ip_addr);
	}
      old_state = sockets[i].state;
      sockets[i].state = GZILLA_SOCKET_CONNECTING;
      if (old_state == GZILLA_SOCKET_DNS_WAIT)
	gzilla_socket_try_connect (i);
    }
  else
    g_warning ("gzilla socket structure inconsistent!\n");
}


/* Create a new connection to (hostname, port). When the socket is
   created, call the callback with the file descriptor of the socket,
   or -1 on failure. The callback may be called inside the call to
   this function, or later from a GTK input handler. */

gint
gzilla_socket_new (const char *hostname,
		   gint port,
		   void (* callback) (gint fd, void *callback_data),
		   void *callback_data)
{
  gint i;

  if (num_sockets == num_sockets_max)
    {
      num_sockets_max <<= 1;
      sockets = g_realloc (sockets, num_sockets_max * sizeof(GzillaSocket));
    }
  i = num_sockets++;
  sockets[i].tag = ++socket_tag;
  sockets[i].input_tag = 0;
  sockets[i].port = port;
  sockets[i].callback = callback;
  sockets[i].callback_data = callback_data;
  sockets[i].state = GZILLA_SOCKET_INIT;
  /* Note: dns lookup is done after socket structure is already set up,
     in case callback is called immediately.

     Also note: the integer tag is casted to a void * so that it can
     pass through the dns callback mechanism. Allocating a new memory
     structure to hold the tag would have been cleaner from a type
     point of view, but less efficient.

     Final note: the socket_try_connect call is deferred until after
     the call to gzilla_dns_lookup, because (if successful), it might
     deallocate the socket structure, and it wouldn't be good to have
     that happen before the following assignment to it. */
  sockets[i].dns_tag = gzilla_dns_lookup (hostname,
					  gzilla_socket_dns_callback,
					  (void *)(sockets[i].tag));
  if (sockets[i].state == GZILLA_SOCKET_INIT)
    sockets[i].state = GZILLA_SOCKET_DNS_WAIT;
  else
    /* DNS succeeded */
    gzilla_socket_try_connect (i);
  return socket_tag;
}


/* Abort the connection. After an abort call, the callback associated
   with the tag will not be called. Also frees resources used in
   setting up the connection (aborting a pending dns request, closing
   the socket, and removing an input handler, depending on the state
   of the connection). */

void
gzilla_socket_abort (gint tag)
{
  gint i;

  for (i = 0; i < num_sockets; i++)
    {
      if (sockets[i].tag == tag)
	break;
    }
  if (i < num_sockets)
    {
      /* Found the socket structure. */

      if (sockets[i].state == GZILLA_SOCKET_DNS_WAIT)
	gzilla_dns_abort (sockets[i].dns_tag);
      else
	close (sockets[i].fd);

      if (sockets[i].input_tag)
	gdk_input_remove (sockets[i].input_tag);

      sockets[i] = sockets[--num_sockets];
    }
  else
    g_warning ("trying to abort nonexistent socket tag!\n");
}
