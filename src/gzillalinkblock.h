/* Header file for "link block" abstraction within gzilla. */

#ifndef __GZILLA_LINKBLOCK_H__
#define __GZILLA_LINKBLOCK_H__

#include <gtk/gtkobject.h>

#include "gzw.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define GZILLA_LINKBLOCK(obj) GTK_CHECK_CAST (obj, gzilla_linkblock_get_type (), GzillaLinkBlock)
#define GZILLA_IS_LINKBLOCK(obj) GTK_CHECK_TYPE (obj, gzilla_linkblock_get_type ())

typedef struct _GzillaLinkBlock GzillaLinkBlock;
typedef struct _GzillaLinkBlockClass GzillaLinkBlockClass;

struct _GzillaLinkBlock {
  GtkObject object;
};

struct _GzillaLinkBlockClass {
  GtkObjectClass parent_class;
  void (* request_url)     (GzillaLinkBlock *linkblock,
			    GzillaByteSink *bytesink,
			    const char *url);
  void (* request_url_img) (GzillaLinkBlock *linkblock,
			    GzillaImgSink *imgsink,
			    char *url);
  void (* link)            (GzillaLinkBlock *linkblock,
			    const char *url);
  void (* status)          (GzillaLinkBlock *linkblock,
			    GzillaStatusDir direction,
			    gboolean abort,
			    GzillaStatusMeaning meaning,
			    const char *text);
  void (* redirect)        (GzillaLinkBlock *linkblock,
			    const char *url);
  void (* title)           (GzillaLinkBlock *linkblock,
			    char *title);
  void (* have_gzw)        (GzillaLinkBlock *linkblock,
			    Gzw *gzw);
  void (* set_base_url)    (GzillaLinkBlock *linkblock,
			    const char *url);

  /* maybe more, who knows? */
};

guint  gzilla_linkblock_get_type        (void);
GzillaLinkBlock *gzilla_linkblock_new   (void);

void   gzilla_linkblock_request_url     (GzillaLinkBlock *linkblock,
					 GzillaByteSink *bytesink,
					 const char *url);
void   gzilla_linkblock_request_url_img (GzillaLinkBlock *linkblock,
					 GzillaImgSink *sink,
					 const char *url);
void   gzilla_linkblock_link            (GzillaLinkBlock *linkblock,
					 const char *url);
void   gzilla_linkblock_status          (GzillaLinkBlock *linkblock,
					 GzillaStatusDir direction,
					 gboolean abort,
					 GzillaStatusMeaning meaning,
					 const char *text);
void   gzilla_linkblock_redirect        (GzillaLinkBlock *linkblock,
					 const char *url);
void   gzilla_linkblock_title           (GzillaLinkBlock *linkblock,
					 const char *title);
void   gzilla_linkblock_have_gzw        (GzillaLinkBlock *linkblock,
					 Gzw *gzw);
void   gzilla_linkblock_set_base_url    (GzillaLinkBlock *linkblock,
					 const char *url);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_LINKBLOCK_H__ */
