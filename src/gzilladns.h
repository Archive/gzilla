#ifndef __GZILLA_DNS_H__
#define __GZILLA_DNS_H__


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
    
void gzilla_dns_init (void);

/* Returns tag */
guint32 gzilla_dns_lookup (const char *hostname,
			   void (* callback) (guint32 ip_addr, void *callback_data),
			   void *callback_data);
    
void gzilla_dns_abort (guint32 tag);
    
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GZILLA_DNS_H__ */
