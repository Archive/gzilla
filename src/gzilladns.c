/* 
 * Copyright (C) 1997 Ian Main
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* I have no idea if I'm allowed to get away with licensing my code
 * differently than yours in the same program :) 
 * -Ian */

/* todo: get rid of getdtablesize, putting in my own api for closing
   all open files. */

#include <gtk/gtk.h>

#include <sys/types.h>
#include <netdb.h>
#include <sys/socket.h>	
#include <netinet/in.h>	
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>

#include "gzillafd.h"
#include "gzilladns.h"

/* #undef VERBOSE */

/* These defines cause two servers to be initialized, and disallow
   dynamic forking of new servers. I believe that this configuration
   has very good virtual memory performance. -RLL

   Note: set to 0 for gprof profiling. */
/* Configuration is 1 for publius */
#define GZILLA_DNS_NUM_INIT 2

/* the maximum nuber of servers to spawn */
/* Currently, servers do not get dynamically spawned */
#define GZILLA_DNS_MAX_SERVERS GZILLA_DNS_NUM_INIT

static void gzilla_dns_callback(gpointer server_num, gint source,
			 GdkInputCondition condition);
static gint gzilla_dns_create_server(void);
static void gzilla_dns_server(gint pipefd[2]);

typedef struct
{
    /* boolean to tell if server is doing a lookup */
    gboolean in_use;
    /* pipefd's to communicate to server with */
    gint pipefd[2];
} DnsServer;

static DnsServer dns_server[GZILLA_DNS_MAX_SERVERS];
static int num_servers;

typedef struct
{
    /* host hame for cache */
    char *hostname;
    gint server; /* -1 if complete, -2 if waiting for server,
		    otherwise index to dns_server */
    /* address of host - may be 0 on failure. */
    guint32 ip_addr;
} GzillaDnsCache;

static GzillaDnsCache *dns_cache;
static int dns_cache_size, dns_cache_size_max;

typedef struct
{
  gint tag;
  gint server; /* -1 if waiting, otherwise index to dns_server[] */
  char *hostname;
  void (*callback) (guint32 ip_addr, void *callback_data);
  void *callback_data;
} GzillaDnsCon;

static GzillaDnsCon *dns_con;
static gint dns_con_size, dns_con_size_max;

static gint dns_con_tag = 0;


/*
 *--------------------------------------------------------------
 * gzilla_dns_init
 *
 *   Initialize the dns functions for use.
 *
 * Arguments:
 *   
 * Results:
 *   gzilla_dns_lookup() will be ready for use.
 *
 * Side effects:
 *   The library is initialized.
 *
 *--------------------------------------------------------------
 */


void gzilla_dns_init (void)
{
  dns_con_size = 0;
  dns_con_size_max = 16;
  dns_con = g_new (GzillaDnsCon, dns_con_size_max);

  dns_cache_size = 0;
  dns_cache_size_max = 16;
  dns_cache = g_new (GzillaDnsCache, dns_cache_size_max);

  num_servers = 0;
  while (num_servers < GZILLA_DNS_NUM_INIT) {
    if ( (gzilla_dns_create_server()) < 0) {
	fprintf(stderr, "gzilla: creation of dns server failed - Exiting.\n");
	/* is this ok to exit without shuting down gtk ? */
	exit(1);
    }
  }
}


/* Send the request to the server. */
void
gzilla_dns_server_req (gint server, const char *hostname) {
#ifdef VERBOSE
  g_print ("gzilla_dns_server_req (%d, \"%s\")\n", server, hostname);
#endif
  dns_server[server].in_use = TRUE;
#if 0
#endif
  write (dns_server[server].pipefd[1], hostname, strlen (hostname) + 1);
}

/*
 *--------------------------------------------------------------
 * gzilla_dns_lookup
 *
 * looks up an address and returns a tag for use with
 * gzilla_dns_abort() if desired.  May not return -1 if
 * hostname was in cache.
 *
 * Arguments:
 * char *hostname - hostname to lookup
 * callback - function to call when dns lookup is complete.
 * callback_data - data to pass to this function.
 *   
 * Results:
 * callback function is called when dns_lookup is complete.
 * returns a tag identifying this lookup or -1 if lookup was
 * in cache.
 * 
 * Side effects:
 * a new dns server may be spawned if all the current servers
 * are in use.
 *
 *--------------------------------------------------------------
 */


guint32 gzilla_dns_lookup (const char *hostname, 
			   void (* callback) (guint32 ip_addr, void *callback_data),
			   void *callback_data)
{
  gint i;
  gint tag;
  gint server;
    
  /* check for cache hit. */
  for (i = 0; i < dns_cache_size; i++)
    if (!strcmp (hostname, dns_cache[i].hostname))
      break;

  /* if it hit, call the callback immediately. */
  if (i < dns_cache_size && dns_cache[i].server == -1) {
    callback (dns_cache[i].ip_addr, callback_data);
    return 0;
  }

  /* It didn't hit in the cache with an answer - need to put request
     into the dns_con table. */
  if (i < dns_cache_size) {
    /* hit in cache but answer hasn't come back yet. */
    server = dns_cache[i].server;
  } else {
    /* missed in cache -- create a cache entry */
    if (dns_cache_size == dns_cache_size_max) {
      dns_cache_size_max <<= 1;
      dns_cache = realloc (dns_cache, dns_cache_size_max * sizeof (GzillaDnsCache));
    }
    dns_cache[dns_cache_size].hostname = g_strdup (hostname);
    /* Find a server we can send the request to. */
    for (server = 0; server < num_servers; server++)
      if (!dns_server[server].in_use) {
	break;
      }
    if (server < num_servers) {
      /* found an unused server - give it the request */
      gzilla_dns_server_req (server, hostname);
      dns_cache[dns_cache_size].server = server;
    } else {
      /* no unused servers - wait for one to become available. */
      server = -1;
      dns_cache[dns_cache_size].server = -2;
    }
    dns_cache_size++;
  }

  if (dns_con_size == dns_con_size_max) {
    dns_con_size_max <<= 1;
    dns_con = g_realloc (dns_con, dns_con_size_max * sizeof (GzillaDnsCon));
  }
  tag = dns_con_tag++;
  dns_con[dns_con_size].tag = tag;
  dns_con[dns_con_size].server = server;
  dns_con[dns_con_size].hostname = g_strdup (hostname);
  dns_con[dns_con_size].callback = callback;
  dns_con[dns_con_size].callback_data = callback_data;
  dns_con_size++;
    
  return tag;
}

/*
 *--------------------------------------------------------------
 * gzilla_dns_abort
 *
 * aborts a previous call to gzilla_dns_lookup().
 *
 * Arguments:
 * gint tag32 - the tag returned from previous call to gzilla_dns_lookup().
 *   
 * Results:
 *   callback function is not called.
 *
 * Side effects:
 * 
 *--------------------------------------------------------------
 */


void
gzilla_dns_abort (guint32 tag)
{
  gint i;

  for (i = 0; i < dns_con_size; i++) {
    if (dns_con[i].tag == tag) {
      g_free (dns_con[i].hostname);
      dns_con[i] = dns_con[--dns_con_size];
      return;
    }
  }
  g_warning ("gzilla_dns_abort: aborting a nonexistent tag\n");
}

/*
 *--------------------------------------------------------------
 * gzilla_dns_callback
 *
 * internal function - called when dns lookup completes.. this 
 * function dispatches the callback passed to gzilla_dns_lookup().
 *
 * Arguments:
 * gint server_num - the index into the dns_server struct.
 * gint source - unkown.
 * GdkIputCondition - unkown.
 *   
 * Results:
 *   callback function passed to gzilla_dns_lookup is called.
 *
 * Side effects:
 * this dns_server[server_num]->in_use set to 0 for future lookups.
 * ie, the lock is removed.
 * 
 *--------------------------------------------------------------
 */

void gzilla_dns_callback(gpointer serv_num, gint source,
			 GdkInputCondition condition)
{
    guint32 ip_addr;
    gint server_num;
    gint i;
    gint j;
    
    /* cast it back to an integer */
    server_num = (int) serv_num;

#ifdef VERBOSE    
    printf("gzilla_dns_callback\n");
#endif
    /* read ip from server.  It's done as a single int rather than a string.
     * hopefully it works ok */
    if ( (read(dns_server[server_num].pipefd[0], &ip_addr, sizeof(guint32))) < 0)
	    fprintf(stderr, "gzilla: reading from pipe: %s\n", strerror(errno));
#ifdef VERBOSE    
    printf("read completed\n");
#endif
    

#ifdef VERBOSE
    printf("ip_addr in callback is %x\n", ip_addr);
#endif

    /* write ip address into cache. */
    for (i = 0; i < dns_cache_size; i++)
      if (dns_cache[i].server == server_num)
	break;
    if (i < dns_cache_size) {
      dns_cache[i].ip_addr = ip_addr;
      dns_cache[i].server = -1;
    } else {
      g_warning ("gzilla_dns_callback: no cache item for server\n");
    }

    /* Give answer to all callbacks. */
    for (i = 0; i < dns_con_size; i++) {
      if (dns_con[i].server == server_num) {
	dns_con[i].callback (ip_addr, dns_con[i].callback_data);
	g_free (dns_con[i].hostname);
	dns_con[i--] = dns_con[--dns_con_size];
      }
    }

    dns_server[server_num].in_use = FALSE;

    /* See if there's an outstanding request and, if so, serve it. */
    for (i = 0; i < dns_cache_size; i++) {
      if (dns_cache[i].server == -2) {
	dns_cache[i].server = server_num;
#ifdef VERBOSE
	g_print ("serving outstanding request for %s\n",
		 dns_cache[i].hostname);
#endif
	for (j = 0; j < dns_con_size; j++) {
	  if (!strcmp (dns_con[j].hostname, dns_cache[i].hostname))
	    dns_con[j].server = server_num;
	}
	gzilla_dns_server_req (server_num, dns_cache[i].hostname);
	break;
      }
    }
}

    
/*
 *--------------------------------------------------------------
 * gzilla_dns_create_server
 *
 * internal function - creates a new server (currently using fork()).
 *
 * Arguments:
 *   
 * Results:
 * initializes the first free dns_server structure and returns
 * the index into the dns_server structure which is 
 * also the tag.  Returns -1 on error.
 *
 * Side effects:
 * 
 *--------------------------------------------------------------
 */

gint gzilla_dns_create_server(void)
{

    int pid;
    int index = 0;
    

    /* for talking to dns server.  Once setup, always write to pipefd[1],
     * and read from pipefd[0]. */
    int pipefd[2];
    int pipefd0[2];
    int pipefd1[2];

    /* check that we're not spawning too many servers.. this should really
     * only be a sanity check..  I'm hoping that we'll never really spawn
     * anywhere near the max allowed servers. */
    if (num_servers >= GZILLA_DNS_MAX_SERVERS) {
	fprintf(stderr, "gzilla: spawned too many dns processes - limit set to %d.\n", 
		GZILLA_DNS_MAX_SERVERS);
	return(-1);
    }
    
    /* create pipe to write to dns_server with */
    if ( (pipe(pipefd0)) < 0) {
	fprintf(stderr, "gzilla: creating pipe: %s\n", strerror(errno));
	return(-1);
    }
    
    /* create pipe to read from dns_server */
    if ( (pipe(pipefd1)) < 0) {
	fprintf(stderr, "gzilla: creating pipe: %s\n", strerror(errno));
	return(-1);
    }
    
    gzilla_fd_add (pipefd0[1]);
    gzilla_fd_add (pipefd1[0]);

    pid = fork();
    if (pid < 0) {
	fprintf(stderr, "gzilla: forking: %s\n", strerror(errno));
	return(-1);
    }
    
    /* start server in child */
    if (pid == 0) {
	pipefd[0] = pipefd0[0];
	pipefd[1] = pipefd1[1];
	gzilla_dns_server(pipefd);
	/* does not return */
    }

    index = num_servers;

    /* initialize structure */
    
    dns_server[index].in_use = FALSE;
    dns_server[index].pipefd[1] = pipefd0[1];
    dns_server[index].pipefd[0] = pipefd1[0];
    close(pipefd0[0]);
    close(pipefd1[1]);
    /* only used as a sanity check to gzilla_dns_abort() */
    num_servers++;
    
    gdk_input_add(dns_server[index].pipefd[0],
		  GDK_INPUT_READ,
		  (GdkInputFunction) gzilla_dns_callback,
		  (gpointer) index);

    return(index);
}

/*
 *--------------------------------------------------------------
 * gzilla_dns_server
 *
 * internal function - this is the actual server process.  
 * Called from gzilla_dns_server_create().
 *
 *
 * Arguments:
 * 
 * pipefd[2] - an open set of pipes - 0 for writing, 1 for
 * reading.
 * 
 * Results:
 * sets up a server listening for char *hostnames on pipefd[0], 
 * and writing results of dns lookups as guints into pipefd[1].
 *
 * Side effects:
 * 
 *--------------------------------------------------------------
 */


void gzilla_dns_server(gint pipefd[2])
{
    char hostname[4096];
    gint nread;
    struct hostent *host;
    gint i;
    guint32 ip_addr;


    /* todo: just close the pipe fd's */
    /* close all file descriptors except ones used by the pipes. */

    if (gzilla_fd_list != NULL)
      for (i = 0; i < gzilla_fd_list_size; i++)
	close (gzilla_fd_list[i]);

#ifdef VERBOSE
    g_print ("gzilla_dns_server: started\n");
#endif
    while(1) {
	/* block on read from client */
	nread = read(pipefd[0], hostname, 4096);

	/* will return 0 if parent exits. */
	/* where are those errors coming from anyway ?? 
	 *  "** ERROR **: an x io error occurred" */
	if (nread == 0) {
#ifdef VERBOSE
	    printf("---> dns_server - returned 0 - Exiting.\n");
#endif
	    exit(0);
	}
	
	if (nread < 0) {
	    fprintf(stderr, "gzilla_dns_server: reading from pipe: %s\n", strerror(errno));
	    exit(0);
	}
	
#ifdef VERBOSE
	printf("---> dns_server - looking up >%s<\n", hostname);
#endif
	
	host = gethostbyname(hostname);
	
	if (host == NULL) {
#ifdef VERBOSE
	    printf("---> dns_server - NULL return\n");
#endif
	    ip_addr = 0;
	} else {
#ifdef VERBOSE
	    printf("---> dns_server - good return\n");
#endif
	    memcpy(&ip_addr, host->h_addr_list[0], sizeof(ip_addr));
	    ip_addr = ntohl(ip_addr);
	}
	
#ifdef VERBOSE
	printf("gzilla_dns_server: ip of %s is %x\n", hostname, ip_addr);
#endif
	/* write hostname to client */
	if ( (write(pipefd[1], &ip_addr, sizeof(ip_addr))) < 0)
		fprintf(stderr, "gzilla_dns_server: writing to pipe: %s\n", strerror(errno));
    }
}
