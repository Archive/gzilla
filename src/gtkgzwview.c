/*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

/* This module is a GTK+ widget that also serves as a container for a
   Gzw widget. It also adds scrollbars with an automatic scrolling
   policy. It's heavily adapted from gtkscrolledwindow.c in GTK+. */

#include <gtk/gtk.h>

#include "gzw.h"
#include "gtkgzwview.h"
#include "gtkgzwscroller.h"

static void gtk_gzw_view_class_init    (GtkGzwViewClass     *klass);
static void gtk_gzw_view_init          (GtkGzwView          *gzw_view);
static void gtk_gzw_view_map           (GtkWidget           *widget);
static void gtk_gzw_view_unmap         (GtkWidget           *widget);
static void gtk_gzw_view_realize       (GtkWidget           *widget);
static void gtk_gzw_view_unrealize     (GtkWidget           *widget);
static void gtk_gzw_view_destroy       (GtkObject           *object);
static void gtk_gzw_view_size_request  (GtkWidget           *widget,
					GtkRequisition      *requisition);
static void gtk_gzw_view_size_allocate (GtkWidget           *widget,
					GtkAllocation       *allocation);
static void gtk_gzw_view_paint         (GtkWidget           *widget,
					GdkRectangle        *area);
static void gtk_gzw_view_draw          (GtkWidget           *widget,
					GdkRectangle        *area);
static gint gtk_gzw_view_expose        (GtkWidget           *widget,
					GdkEventExpose      *event);

static gint gtk_gzw_view_button        (GtkWidget       *widget,
					GdkEventButton  *event);
static gint gtk_gzw_view_motion_notify (GtkWidget       *widget,
					GdkEventMotion  *event);


static GtkContainerClass *parent_class = NULL;

#define GZW_VIEW_MAX_WINDOW_SIZE 32760
#define GZW_VIEW_BUMP 30000

guint
gtk_gzw_view_get_type ()
{
  static guint gzw_view_type = 0;

  if (!gzw_view_type)
    {
      GtkTypeInfo gzw_view_info =
      {
	"GtkGzwView",
	sizeof (GtkGzwView),
	sizeof (GtkGzwViewClass),
	(GtkClassInitFunc) gtk_gzw_view_class_init,
	(GtkObjectInitFunc) gtk_gzw_view_init,
	(GtkArgSetFunc) NULL,
	(GtkArgGetFunc) NULL
      };

      gzw_view_type = gtk_type_unique (gtk_container_get_type (), &gzw_view_info);
    }

  return gzw_view_type;
}

static void
gtk_gzw_view_class_init (GtkGzwViewClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkContainerClass *container_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;
  container_class = (GtkContainerClass*) class;

  parent_class = gtk_type_class (gtk_container_get_type ());

  object_class->destroy = gtk_gzw_view_destroy;

  widget_class->map = gtk_gzw_view_map;
  widget_class->unmap = gtk_gzw_view_unmap;
  widget_class->realize = gtk_gzw_view_realize;
  widget_class->unrealize = gtk_gzw_view_unrealize;
  widget_class->draw = gtk_gzw_view_draw;
  widget_class->expose_event = gtk_gzw_view_expose;
  widget_class->size_request = gtk_gzw_view_size_request;
  widget_class->size_allocate = gtk_gzw_view_size_allocate;

  widget_class->button_press_event = gtk_gzw_view_button;
  widget_class->button_release_event = gtk_gzw_view_button;
  widget_class->motion_notify_event = gtk_gzw_view_motion_notify;
}

/* I can never figure out what to put in the _init function and what
   to put in the _new function. As far as I can figure out from
   looking at the Gtk+ source, the rule of thumb seems to put value
   initializations only in the _init function, and anything involving
   allocation or state change of other objects in the _new function.
   In other words, the _init function tends to be idempotent, and the
   _new function tends to accumulate everything that can't be done
   idempotently.

   Note added 11 Nov 1997: actually, the real rule is that the stuff
   in the init function is called when instantiations of subclasses
   are created, while stuff in the new function is not. Just don't
   subclass this widget and everything will be fine.

*/

static void
gtk_gzw_view_init (GtkGzwView *gzw_view)
{
  GTK_WIDGET_UNSET_FLAGS (gzw_view, GTK_NO_WINDOW);
#if 0
  GTK_WIDGET_SET_FLAGS (gzw_view, GTK_BASIC);
#endif

  gzw_view->shadow_type = GTK_SHADOW_IN;
  gzw_view->main_window = NULL;
  gzw_view->view_window = NULL;
  gzw_view->hadjustment = NULL;
  gzw_view->vadjustment = NULL;

  gzw_view->gzw = NULL;

  gzw_view->repaint_idle_tag = 0;

}

static void
gtk_gzw_view_adjustment_changed (GtkAdjustment *adjustment,
				 gpointer       data)
{
  GtkGzwView *viewport;

  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (data != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (data));

  viewport = GTK_GZW_VIEW (data);
}

static void
gtk_gzw_view_unmap_callback (GtkWidget *widget,
			     gpointer   data,
			     GtkAllocation *allocation)
{
  gtk_widget_unmap (widget);
}

static void
gtk_gzw_view_size_allocate_callback (GtkWidget *widget,
				     gpointer   data,
				     GtkAllocation *allocation)
{
  gtk_widget_size_allocate (widget, allocation);
}

static void
gtk_gzw_view_adjustment_value_changed (GtkAdjustment *adjustment,
				       gpointer       data)
{
  GtkGzwView *viewport;
  gint x_scroll, y_scroll; /* delta scroll amount */
  gint x_top, y_top;   /* toplevel coordinates */
  gint x_win, y_win;   /* window coordinates */
#if 0
  gint width, height;
#endif
  GzwRect *visible;
  gboolean bumped;
  GzwRect old_win, new_win;
  GzwRect intersect;
  GzwRect empty = {0, 0, 0, 0};

  /* todo: no doubt this routine will need some fiddling when the offsets
     get implemented. */

  g_return_if_fail (adjustment != NULL);
  g_return_if_fail (data != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (data));

  viewport = GTK_GZW_VIEW (data);

  if (TRUE)
    {
#if 0
      gdk_window_get_size (viewport->view_window, &width, &height);
#endif

#ifdef VERBOSE
      g_print ("h value = %g, v value = %g\n",
	       viewport->hadjustment->value,
	       viewport->vadjustment->value);
#endif

      x_top = 0;
      y_top = 0;

      if (viewport->hadjustment->lower != (viewport->hadjustment->upper -
					   viewport->hadjustment->page_size))
	x_top = viewport->hadjustment->lower - viewport->hadjustment->value;

      if (viewport->vadjustment->lower != (viewport->vadjustment->upper -
					   viewport->vadjustment->page_size))
	y_top = viewport->vadjustment->lower - viewport->vadjustment->value;

      if (GTK_WIDGET_REALIZED (viewport))
	{
	  visible = &viewport->gzw_container->visible;
	  bumped = FALSE;
	  /* old_win is the current location of the X window in Gzw
	     toplevel coordinates. */
	  old_win.x0 = -viewport->gzw_container->x_offset;
	  old_win.x1 = old_win.x0 + GZW_VIEW_MAX_WINDOW_SIZE;
	  old_win.y0 = -viewport->gzw_container->y_offset;
	  old_win.y1 = old_win.y0 + GZW_VIEW_MAX_WINDOW_SIZE;

	  if (-x_top + viewport->gzw_container->x_offset +
	      visible->x1 - visible->x0 >
	      GZW_VIEW_MAX_WINDOW_SIZE)
	    {
	      viewport->gzw_container->x_offset -= GZW_VIEW_BUMP;
	      bumped = TRUE;
	    }
	  else if (-x_top + viewport->gzw_container->x_offset < 0)
	    {
	      viewport->gzw_container->x_offset += GZW_VIEW_BUMP;
	      if (viewport->gzw_container->x_offset > 0)
		viewport->gzw_container->x_offset = 0;
	      bumped = TRUE;
	    }

	  if (-y_top + viewport->gzw_container->y_offset +
	      visible->y1 - visible->y0 >
	      GZW_VIEW_MAX_WINDOW_SIZE)
	    {
	      viewport->gzw_container->y_offset -= GZW_VIEW_BUMP;
	      bumped = TRUE;
	    }
	  else if (-y_top + viewport->gzw_container->y_offset < 0)
	    {
	      viewport->gzw_container->y_offset += GZW_VIEW_BUMP;
	      if (viewport->gzw_container->y_offset > 0)
		viewport->gzw_container->y_offset = 0;
	      bumped = TRUE;
	    }

	  /* note: repaint is not necessary, because the window
	     move will generate an expose. */

	  x_win = x_top - viewport->gzw_container->x_offset;
	  y_win = y_top - viewport->gzw_container->y_offset;
	  gdk_window_move (GTK_WIDGET (viewport)->window,
			   x_win, y_win);

	  /* Note: (todo/write up eager unmap) */

	  if (bumped)
	    {
	      /* old_win is the new location of the X window in Gzw
		 toplevel coordinates. */
	      new_win.x0 = -viewport->gzw_container->x_offset;
	      new_win.x1 = new_win.x0 + GZW_VIEW_MAX_WINDOW_SIZE;
	      new_win.y0 = -viewport->gzw_container->y_offset;
	      new_win.y1 = new_win.y0 + GZW_VIEW_MAX_WINDOW_SIZE;

	      /* If bumped, two things need to be done:

		 First, all embedded gtk's visible in the old but not
		 the new are unmapped.

		 Second, all embedded gtk's visible in the intersection
		 of old and new are size_allocated.

		 */

	      if (viewport->gzw != NULL)
		gzw_gtk_foreach (viewport->gzw,
				 gtk_gzw_view_unmap_callback,
				 NULL,
				 &old_win,
				 &new_win);

	      gzw_rect_intersect (&intersect, &old_win, &new_win);
	      if (viewport->gzw != NULL)
		gzw_gtk_foreach (viewport->gzw,
				 gtk_gzw_view_size_allocate_callback,
				 NULL,
				 &intersect,
				 &empty);
	    }

	  /* Update visible rectangle. */
	  x_scroll = viewport->gzw_container->visible.x0 + x_top;
	  y_scroll = viewport->gzw_container->visible.y0 + y_top;
	  viewport->gzw_container->visible.x0 -= x_scroll;
	  viewport->gzw_container->visible.y0 -= y_scroll;
	  viewport->gzw_container->visible.x1 -= x_scroll;
	  viewport->gzw_container->visible.y1 -= y_scroll;
	}
    }
}

GtkAdjustment*
gtk_gzw_view_get_hadjustment (GtkGzwView *viewport)
{
  g_return_val_if_fail (viewport != NULL, NULL);
  g_return_val_if_fail (GTK_IS_GZW_VIEW (viewport), NULL);

  return viewport->hadjustment;
}

GtkAdjustment*
gtk_gzw_view_get_vadjustment (GtkGzwView *viewport)
{
  g_return_val_if_fail (viewport != NULL, NULL);
  g_return_val_if_fail (GTK_IS_GZW_VIEW (viewport), NULL);

  return viewport->vadjustment;
}

void
gtk_gzw_view_set_hadjustment (GtkGzwView   *viewport,
			      GtkAdjustment *adjustment)
{
  g_return_if_fail (viewport != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (viewport));
  g_return_if_fail (adjustment != NULL);

  if (viewport->hadjustment)
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (viewport->hadjustment), (gpointer) viewport);
      gtk_object_unref (GTK_OBJECT (viewport->hadjustment));
    }

  viewport->hadjustment = adjustment;
  gtk_object_ref (GTK_OBJECT (viewport->hadjustment));

  gtk_signal_connect (GTK_OBJECT (adjustment), "changed",
		      (GtkSignalFunc) gtk_gzw_view_adjustment_changed,
		      (gpointer) viewport);
  gtk_signal_connect (GTK_OBJECT (adjustment), "value_changed",
		      (GtkSignalFunc) gtk_gzw_view_adjustment_value_changed,
		      (gpointer) viewport);

  gtk_gzw_view_adjustment_changed (adjustment, (gpointer) viewport);
}

void
gtk_gzw_view_set_vadjustment (GtkGzwView   *viewport,
			      GtkAdjustment *adjustment)
{
  g_return_if_fail (viewport != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (viewport));
  g_return_if_fail (adjustment != NULL);

  if (viewport->vadjustment)
    {
      gtk_signal_disconnect_by_data (GTK_OBJECT (viewport->vadjustment), (gpointer) viewport);
      gtk_object_unref (GTK_OBJECT (viewport->vadjustment));
    }

  viewport->vadjustment = adjustment;
  gtk_object_ref (GTK_OBJECT (viewport->vadjustment));

  gtk_signal_connect (GTK_OBJECT (adjustment), "changed",
		      (GtkSignalFunc) gtk_gzw_view_adjustment_changed,
		      (gpointer) viewport);
  gtk_signal_connect (GTK_OBJECT (adjustment), "value_changed",
		      (GtkSignalFunc) gtk_gzw_view_adjustment_value_changed,
		      (gpointer) viewport);

  gtk_gzw_view_adjustment_changed (adjustment, (gpointer) viewport);
}

static void
gtk_gzw_view_destroy (GtkObject *object)
{
  GtkGzwView *gzw_view;

  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (object));

  gzw_view = GTK_GZW_VIEW (object);

  if (gzw_view->repaint_idle_tag)
    gtk_idle_remove (gzw_view->repaint_idle_tag);

  if (gzw_view->gzw)
    gzw_destroy (gzw_view->gzw);

  gzw_region_drop (gzw_view->repaint_region);

  g_free (gzw_view->gzw_container);

  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
gtk_gzw_view_map (GtkWidget *widget)
{
  GtkGzwView *gzw_view;

#ifdef VERBOSE
  g_print ("gtk_gzw_view_map\n");
#endif

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (widget));

  if (!GTK_WIDGET_MAPPED (widget))
    {
      GTK_WIDGET_SET_FLAGS (widget, GTK_MAPPED);
      gzw_view = GTK_GZW_VIEW (widget);

      gdk_window_show (gzw_view->main_window);

      /* should probably map children - perhaps using a gtk_foreach
	 method on the embedded gzw. */
    }
}

static void
gtk_gzw_view_unmap (GtkWidget *widget)
{
  GtkGzwView *gzw_view;
  GzwRect empty = {0, 0, 0, 0};

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (widget));

  gzw_view = GTK_GZW_VIEW (widget);

  if (GTK_WIDGET_MAPPED (widget))
    {
      GTK_WIDGET_UNSET_FLAGS (widget, GTK_MAPPED);

      gdk_window_hide (gzw_view->main_window);

      /* should probably unmap children - perhaps using a gtk_foreach
	 method on the embedded gzw. */
    }
  /* clear out repaint region, if the widget is not mapped then repaints
     are pointless. It's explicitly cleared to help satisfy the invariant
     that the paint method on the child gzw will only be called when
     the toplevel widget is mapped and visible. */

  gzw_region_drop (gzw_view->repaint_region);
  gzw_view->repaint_region = gzw_region_from_rect (&empty);
}


static void
gtk_gzw_view_realize (GtkWidget *widget)
{
  GtkGzwView *gzw_view;
  GdkWindowAttr attributes;
  gint attributes_mask;

#ifdef VERBOSE
  g_print ("gtk_gzw_view_realize\n");
#endif

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (widget));

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
  gzw_view = GTK_GZW_VIEW (widget);

  attributes.x = widget->allocation.x + GTK_CONTAINER (widget)->border_width;
  attributes.y = widget->allocation.y + GTK_CONTAINER (widget)->border_width;
  attributes.width = widget->allocation.width - GTK_CONTAINER (widget)->border_width * 2;
  attributes.height = widget->allocation.height - GTK_CONTAINER (widget)->border_width * 2;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.visual = gtk_widget_get_visual (widget);
  attributes.colormap = gtk_widget_get_colormap (widget);
  attributes.event_mask = gtk_widget_get_events (widget) |
    GDK_EXPOSURE_MASK |
    GDK_BUTTON_PRESS_MASK |
    GDK_BUTTON_RELEASE_MASK |
    GDK_POINTER_MOTION_MASK |
    GDK_ENTER_NOTIFY_MASK |
    GDK_LEAVE_NOTIFY_MASK;

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

  gzw_view->main_window = gdk_window_new (widget->parent->window, &attributes, attributes_mask);
  gdk_window_set_user_data (gzw_view->main_window, gzw_view);

  attributes.x += widget->style->klass->xthickness;
  attributes.y += widget->style->klass->ythickness;
  attributes.width -= widget->style->klass->xthickness * 2;
  attributes.height -= widget->style->klass->ythickness * 2;

  gzw_view->view_window = gdk_window_new (gzw_view->main_window, &attributes, attributes_mask);
  gdk_window_set_user_data (gzw_view->view_window, gzw_view);

  attributes.x = 0;
  attributes.y = 0;

  attributes.width = GZW_VIEW_MAX_WINDOW_SIZE;
  attributes.height = GZW_VIEW_MAX_WINDOW_SIZE;

  widget->window = gdk_window_new (gzw_view->view_window, &attributes, attributes_mask);
  gdk_window_set_user_data (widget->window, gzw_view);

  widget->style = gtk_style_attach (widget->style, gzw_view->main_window);
  gtk_style_set_background (widget->style, gzw_view->main_window, GTK_STATE_NORMAL);
  gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);
  
  gdk_window_show (widget->window);
  gdk_window_show (gzw_view->view_window);
}

static void
gtk_gzw_view_unrealize (GtkWidget *widget)
{
  GtkGzwView *gzw_view;

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (widget));

  GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
  gzw_view = GTK_GZW_VIEW (widget);

  gtk_style_detach (widget->style);

  gdk_window_destroy (widget->window);
  gdk_window_destroy (gzw_view->view_window);
  gdk_window_destroy (gzw_view->main_window);

  widget->window = NULL;
  gzw_view->view_window = NULL;
  gzw_view->main_window = NULL;
}

/* Allocate the child window & update the adjustments. */
static void
gtk_gzw_view_allocate_child (GtkWidget *widget)
{
  GtkGzwView *gzw_view;
  GtkAllocation view_allocation;
  gint natural_width, natural_height;
  gint actual_width, actual_height;
  GzwRect gzw_allocation;
 
  gzw_view = GTK_GZW_VIEW (widget);

  view_allocation = widget->allocation;

  view_allocation.x += widget->style->klass->xthickness;
  view_allocation.width -= widget->style->klass->xthickness * 2;

  view_allocation.y += widget->style->klass->ythickness;
  view_allocation.height -= widget->style->klass->ythickness * 2;

  /* Size negotiation stuff. The sequence is as follows:

     1. Determine the "natural width." This is the width of the parent
        gtkgzwscroller, minus some space set aside from the scrollbar.

     2. Call size_nego_x on the child with the natural width.

     3. The actual width is the max of the natural width and the
        requisition minimum width. Similarly for actual height.

     4. Call size_nego_y on the child with the actual width and
        height.

     5. The hadjustment upper is the actual width.

     6. The vadjustment upper is the actual height.

	*/

  /* 1. Determine natural width. Note. This depends on the allocation
   of the parent (gtkgzwscroller) being set before the size_allocate
   method of the child (gtkgzwview) is called. */

  if (GTK_IS_GZW_SCROLLER (widget->parent))
    {
      natural_width = (widget->parent->allocation.width -
		       (25 +
			widget->style->klass->xthickness * 2));
    }
  else
    {
      natural_width = view_allocation.width;
    }
  natural_height = view_allocation.height;

#ifdef VERBOSE
  g_print ("natural = %d x %d\n", natural_width, natural_height);
#endif

  /* 2. Call size_nego_x on child. */
  if (gzw_view->gzw != NULL)
    gzw_size_nego_x (gzw_view->gzw, natural_width);

  /* 3. Determine actual width. */

  actual_width = natural_width;
  actual_height = natural_height;

  if (gzw_view->gzw != NULL)
    {
      actual_width = MAX (actual_width, gzw_view->gzw->req_width_min);
      actual_height = MAX (actual_height, gzw_view->gzw->req_height);
    }

#ifdef VERBOSE
  g_print ("actual = %d x %d\n", actual_width, actual_height);
#endif

  /* 4. Call size_nego_y on child. */

  gzw_allocation.x0 = 0;
  gzw_allocation.y0 = 0;
  gzw_allocation.x1 = actual_width;
  gzw_allocation.y1 = actual_height;
  if (gzw_view->gzw != NULL)
    gzw_size_nego_y (gzw_view->gzw, &gzw_allocation);

#ifdef VERBOSE
  g_print ("before, h = (%g -> %g, %g, +%g, ++%g, =%g)\n",
	   gzw_view->hadjustment->lower,
	   gzw_view->hadjustment->upper,
	   gzw_view->hadjustment->page_size,
	   gzw_view->hadjustment->page_increment,
	   gzw_view->hadjustment->step_increment,
	   gzw_view->hadjustment->value);
  g_print ("before, v = (%g -> %g, %g, +%g, ++%g, =%g)\n",
	   gzw_view->vadjustment->lower,
	   gzw_view->vadjustment->upper,
	   gzw_view->vadjustment->page_size,
	   gzw_view->vadjustment->page_increment,
	   gzw_view->vadjustment->step_increment,
	   gzw_view->vadjustment->value);
#endif

  gzw_view->hadjustment->page_size = view_allocation.width;
  gzw_view->hadjustment->page_increment = gzw_view->hadjustment->page_size / 2;
  gzw_view->hadjustment->step_increment = 10;
  gzw_view->hadjustment->lower = 0;
  gzw_view->hadjustment->upper = actual_width;

  gzw_view->vadjustment->page_size = view_allocation.height;
  gzw_view->vadjustment->page_increment = gzw_view->vadjustment->page_size / 2;
  gzw_view->vadjustment->step_increment = 10;
  gzw_view->vadjustment->lower = 0;
  gzw_view->vadjustment->upper = actual_height;

  gzw_view->hadjustment->value = MIN (gzw_view->hadjustment->value,
				      gzw_view->hadjustment->upper -
				      gzw_view->hadjustment->page_size);
  gzw_view->hadjustment->value = MAX (gzw_view->hadjustment->value, 0);

  gzw_view->vadjustment->value = MIN (gzw_view->vadjustment->value,
				      gzw_view->vadjustment->upper -
				      gzw_view->vadjustment->page_size);
  gzw_view->vadjustment->value = MAX (gzw_view->vadjustment->value, 0);

#ifdef VERBOSE
  g_print ("after, h = (%g -> %g, %g, +%g, ++%g, =%g)\n",
	   gzw_view->hadjustment->lower,
	   gzw_view->hadjustment->upper,
	   gzw_view->hadjustment->page_size,
	   gzw_view->hadjustment->page_increment,
	   gzw_view->hadjustment->step_increment,
	   gzw_view->hadjustment->value);
  g_print ("after, v = (%g -> %g, %g, +%g, ++%g, =%g)\n",
	   gzw_view->vadjustment->lower,
	   gzw_view->vadjustment->upper,
	   gzw_view->vadjustment->page_size,
	   gzw_view->vadjustment->page_increment,
	   gzw_view->vadjustment->step_increment,
	   gzw_view->vadjustment->value);
#endif

  gtk_signal_emit_by_name (GTK_OBJECT (gzw_view->hadjustment), "changed");
  gtk_signal_emit_by_name (GTK_OBJECT (gzw_view->vadjustment), "changed");
}

/* This routine does resizing and repainting of the widget, as
   initiated by gzw_request_resize and gzw_request_repaint calls. */
static gint
gtk_gzw_view_idle_func (GtkGzwView *gzw_view)
{
  GzwRect rect;
  GzwRect clip_rect;
  GzwPaint paint;

  if (gzw_view->gzw != NULL && (gzw_view->gzw->flags & GZW_FLAG_REQ_RESIZE))
      gtk_gzw_view_allocate_child (GTK_WIDGET (gzw_view));

  gzw_region_tile (&rect, gzw_view->repaint_region, 1024, 256, 1000);

  /* Intersect with the visible rect again because it might have changed
     since the rect was added to the region. */

  gzw_rect_intersect (&clip_rect, &rect, &gzw_view->gzw_container->visible);

  if (!gzw_rect_empty (&clip_rect))
    {
      /* No assumptions are made about the existing screen state.

	 todo: perhaps extend this so that we know about "old valid
	 data" (i.e. GZW_PAINT_TOP_SCREEN). For the time being, the
	 added complexity is probably not worth it. */
      paint.flags = GZW_PAINT_BG_UNDER;

      /* todo: get bg_color from the "right place". */
      paint.bg_color = 0xd6d6d6;

      /* todo: perhaps move some of this logic into its own routine,
	 to avoid code duplication. */

      /* todo: set clip rectangle to clip_rect. */

      if (gzw_view->gzw != NULL)
	gzw_paint (gzw_view->gzw, &clip_rect, &paint);

      gzw_paint_to_screen (GTK_WIDGET (gzw_view), gzw_view->gzw_container,
			   &clip_rect, &paint);

      /* todo: set clip rectangle back to whole window (?) */
    }

  gzw_view->repaint_region = gzw_region_minus (gzw_view->repaint_region,
					       gzw_region_from_rect (&rect));

  if (gzw_region_empty (gzw_view->repaint_region))
    {
      gzw_view->repaint_idle_tag = 0;
      return FALSE;
    }
  else
    return TRUE;
}

/* This routine gets called when any child widget needs a repaint.
   What we do is add the repaint rectangle to the request repaint
   region, and start an idle process to repaint it if necessary.

   An interesting question: where do we intersect the rectangle
   with the visible rect? Here? For the time being, yes.

   */
static void
gtk_gzw_view_request_paint (GzwContainer *container,
			    GzwRect *rect)
{
  GtkWidget *widget;
  GtkGzwView *gzw_view;
  GzwRect clip_rect;

  gzw_rect_intersect (&clip_rect, &container->visible, rect);

  if (gzw_rect_empty (&clip_rect))
    return;

  widget = container->widget;
  gzw_view = GTK_GZW_VIEW (widget);
  gzw_view->repaint_region =
    gzw_region_union (gzw_region_from_rect (&clip_rect),
		      gzw_view->repaint_region);

  if (gzw_view->repaint_idle_tag == 0)
    gzw_view->repaint_idle_tag =
      gtk_idle_add ((GtkFunction) gtk_gzw_view_idle_func,
		    gzw_view);
}

/* This routine gets called when any child widget requests a resize. */
static void
gtk_gzw_view_request_resize (GzwContainer *container)
{
  GtkGzwView *gzw_view;
  GtkWidget *widget;

  widget = container->widget;
  gzw_view = GTK_GZW_VIEW (widget);

  if (gzw_view->repaint_idle_tag == 0)
    gzw_view->repaint_idle_tag =
      gtk_idle_add ((GtkFunction) gtk_gzw_view_idle_func,
		    gzw_view);
}

static const GzwContainerClass gtk_gzw_view_container_class =
{
  gtk_gzw_view_request_paint,
  gtk_gzw_view_request_resize
};

GtkWidget *
gtk_gzw_view_new (GtkAdjustment *hadjustment,
		  GtkAdjustment *vadjustment)
{
  GtkGzwView *gzw_view;
  GzwRect empty;
  GzwContainer *gzw_container;

  gzw_view = gtk_type_new (gtk_gzw_view_get_type ());

  if (!hadjustment)
    hadjustment = (GtkAdjustment*) gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

  if (!vadjustment)
    vadjustment = (GtkAdjustment*) gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

  gtk_gzw_view_set_hadjustment (gzw_view, hadjustment);
  gtk_gzw_view_set_vadjustment (gzw_view, vadjustment);

  empty.x0 = 0;
  empty.y0 = 0;
  empty.x1 = 0;
  empty.y1 = 0;

  gzw_view->repaint_region = gzw_region_from_rect (&empty);

  /* I'm going to make the container here so that it can respond
     to stuff like size allocation. */
  gzw_container = g_new (GzwContainer, 1);

  gzw_container->klass = &gtk_gzw_view_container_class;

  gzw_container->widget = GTK_WIDGET (gzw_view);

  /* todo: make visible rectangle reflect actual widget size */
  gzw_container->visible.x0 = 0;
  gzw_container->visible.y0 = 0;
  gzw_container->visible.x1 = 100;
  gzw_container->visible.y1 = 100;

  /* todo: support real offsets */
  gzw_container->x_offset = 0;
  gzw_container->y_offset = 0;

  gzw_view->gzw_container = gzw_container;

  return GTK_WIDGET (gzw_view);
}

/* Add a Gzw to the gzw_view.

   If there was already a gzw present, delete it. */
void
gtk_gzw_view_set_gzw (GtkGzwView *gzw_view,
		      Gzw *gzw)
{

  g_return_if_fail (gzw_view != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (gzw_view));

  if (gzw_view->gzw != NULL)
    gzw_destroy (gzw_view->gzw);
  gzw_view->gzw = gzw;
  gzw->container = gzw_view->gzw_container;

  /* Call size allocation & paint functions on child if we're mapped. */
  if (GTK_WIDGET_VISIBLE (gzw_view) && GTK_WIDGET_MAPPED (gzw_view))
    {
      gtk_gzw_view_allocate_child (GTK_WIDGET (gzw_view));
      gtk_gzw_view_request_paint (gzw_view->gzw_container,
				  &gzw_view->gzw_container->visible);
    }
}

static void
gtk_gzw_view_paint (GtkWidget    *widget,
		    GdkRectangle *area)
{
  GtkGzwView *gzw_view;
  GtkStateType state;
  gint x, y;

#ifdef VERBOSE
  g_print ("gtk_gzw_view_paint\n");
#endif

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (widget));
  g_return_if_fail (area != NULL);

  if (GTK_WIDGET_DRAWABLE (widget))
    {
      gzw_view = GTK_GZW_VIEW (widget);

      state = widget->state;
      if (!GTK_WIDGET_IS_SENSITIVE (widget))
        state = GTK_STATE_INSENSITIVE;

      x = GTK_CONTAINER (gzw_view)->border_width;
      y = GTK_CONTAINER (gzw_view)->border_width;

      gtk_draw_shadow (widget->style, gzw_view->main_window,
		       GTK_STATE_NORMAL, gzw_view->shadow_type,
		       0, 0, -1, -1);
    }
}

static void
gtk_gzw_view_draw (GtkWidget    *widget,
		       GdkRectangle *area)
{
  GtkGzwView *gzw_view;

#ifdef VERBOSE
  g_print ("gtk_gzw_view_draw\n");
#endif

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (widget));
  g_return_if_fail (area != NULL);

  if (GTK_WIDGET_DRAWABLE (widget))
    {
      gzw_view = GTK_GZW_VIEW (widget);

      /* todo: draw children. */

      gtk_gzw_view_paint (widget, area);

    }
}

#define REQUEST_EXPOSE
static gint
gtk_gzw_view_expose (GtkWidget *widget,
			 GdkEventExpose *event)
{
  GtkGzwView *gzw_view;
#ifndef REQUEST_EXPOSE
  GzwPaint paint;
#endif
  GzwRect expose_rect;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GZW_VIEW (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  gzw_view = GTK_GZW_VIEW (widget);
  if (GTK_WIDGET_VISIBLE (widget) && GTK_WIDGET_MAPPED (widget))
    {

      if (gzw_view->gzw != NULL &&
	  (gzw_view->gzw->flags & GZW_FLAG_REQ_RESIZE))
	gtk_gzw_view_allocate_child (widget);

      if (event->window == gzw_view->main_window)
	{
	  gtk_gzw_view_paint (widget, &event->area);
	}
      else if (event->window == widget->window)
	{
#ifdef REQUEST_EXPOSE
	  expose_rect.x0 = event->area.x - gzw_view->gzw_container->x_offset;
	  expose_rect.y0 = event->area.y - gzw_view->gzw_container->y_offset;
	  expose_rect.x1 = expose_rect.x0 + event->area.width;
	  expose_rect.y1 = expose_rect.y0 + event->area.height;
	  gtk_gzw_view_request_paint (gzw_view->gzw_container, &expose_rect);
#else
	  /* we assume that X cleared the expose rectangle before calling us.
	     
	     Note: this is, in general, a _bad_ assumption. It is entirely
	     possible that whatever it is that caused the expose event
	     (usually scrolling, sometimes unobscuring the window) is
	     interleaved with drawing commands to the window. In that
	     case, the window will _not_ be cleared to the background
	     color.

	     I'm not sure what's the best way to fix this, really. The
	     easiest thing to do would be to not set SCREEN_UNDER and
	     BG_SCREEN. That would, however, cause unnecessary rectangle
	     clears.

	     That's what the code does now, so no action is necessary for
	     correctness, only efficiency.

	     */
	  paint.flags = GZW_PAINT_BG_UNDER;

	  /* todo: get bg_color from the "right place". */
	  paint.bg_color = 0xd6d6d6;

	  expose_rect.x0 = event->area.x - gzw_view->gzw_container->x_offset;
	  expose_rect.y0 = event->area.y - gzw_view->gzw_container->y_offset;
	  expose_rect.x1 = expose_rect.x0 + event->area.width;
	  expose_rect.y1 = expose_rect.y0 + event->area.height;

	  /* todo: set clip rectangle to expose_rect. */

	  if (gzw_view->gzw != NULL)
	    gzw_paint (gzw_view->gzw, &expose_rect, &paint);

	  gzw_paint_to_screen (widget, gzw_view->gzw_container,
			   &expose_rect, &paint);

	  /* todo: set clip rectangle back to whole window (?) */

	  /* todo: remove rectangle from repaint region (?) */
#endif
	}
    }


  return TRUE;
}

static void
gtk_gzw_view_size_request (GtkWidget           *widget,
			   GtkRequisition      *requisition)
{
  GtkGzwView *gzw_view;

#ifdef VERBOSE
  g_print ("gtk_gzw_view_size_request\n");
#endif

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (widget));
  g_return_if_fail (requisition != NULL);

  gzw_view = GTK_GZW_VIEW (widget);

  requisition->width = 20;
  requisition->height = 20;
}

static void
gtk_gzw_view_size_allocate (GtkWidget           *widget,
			    GtkAllocation       *allocation)
{
  GtkGzwView *gzw_view;
  GtkAllocation view_allocation;

#ifdef VERBOSE
  g_print ("gtk_gzw_view_size_allocate\n");
#endif

  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_GZW_VIEW (widget));
  g_return_if_fail (allocation != NULL);

  gzw_view = GTK_GZW_VIEW (widget);

  widget->allocation = *allocation;
  view_allocation = *allocation;

  if (GTK_WIDGET_REALIZED (widget))
    {
      gdk_window_move_resize (gzw_view->main_window,
			      view_allocation.x,
			      view_allocation.y,
			      view_allocation.width,
			      view_allocation.height);

      view_allocation.x = widget->style->klass->xthickness;
      view_allocation.width -= widget->style->klass->xthickness * 2;

      view_allocation.y = widget->style->klass->ythickness;
      view_allocation.height -= widget->style->klass->ythickness * 2;

      gdk_window_move_resize (gzw_view->view_window,
			      view_allocation.x,
			      view_allocation.y,
			      view_allocation.width,
			      view_allocation.height);

      gzw_view->gzw_container->visible.x1 =
	gzw_view->gzw_container->visible.x0 + view_allocation.width;
      gzw_view->gzw_container->visible.y1 =
	gzw_view->gzw_container->visible.y0 + view_allocation.height;
    }

  gtk_gzw_view_allocate_child (widget);
}

static int
gtk_gzw_view_handle_event (GtkWidget *widget,
			   GdkEvent *event)
{
  GtkGzwView *gzw_view;

  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_GZW_VIEW (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  gzw_view = GTK_GZW_VIEW (widget);

  if (event->any.window != widget->window)
    return FALSE;

  if (gzw_view->gzw != NULL)
    gzw_handle_event (gzw_view->gzw, event);
  return TRUE;
}

static int
gtk_gzw_view_button (GtkWidget *widget,
		     GdkEventButton *event)
{
  return gtk_gzw_view_handle_event (widget, (GdkEvent *)event);
}

static int
gtk_gzw_view_motion_notify (GtkWidget *widget,
			    GdkEventMotion *event)
{
  return gtk_gzw_view_handle_event (widget, (GdkEvent *)event);
}
