/*

   gzilla

   Copyright 1997 Raph Levien <raph@acm.org>

   This code is free for commercial and non-commercial use,
   modification, and redistribution, as long as the source code release,
   startup screen, or product packaging includes this copyright notice.

   Directory scanning code added by Jim McBeath <jimmc@globes.com> Jan 1998.
 */


#include <stdio.h>		/* for sprintf */
#include <ctype.h>		/* for tolower */
#include <time.h>		/* for time and ctime */
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <string.h>

#include <errno.h>		/* for errno */

#include <gtk/gtk.h>

#include "gzillabytesink.h"

typedef struct _GzillaFile GzillaFile;

struct _GzillaFile
  {
    gint fd;
    GzillaByteSink *bytesink;
    gint abort_id;
    gboolean isdir;
    char *dirname;
    DIR *dir;
    int state;			/* for use when converting directory into html text */
  };

typedef struct fileinfo
  {
    char *name;			/* name of file within directory */
    char *dirname;		/* full directory name */
    int size;
    char *symlink;
    char *date;
    gboolean isdir;
    gboolean isexec;
  }
GzillaFileInfo;

static gint num_files;
static gint num_files_max;
static GzillaFile *files;

static gint idle_tag = 0;

/* Forward references */
void gzilla_file_get_dir (const char *url,
			  GzillaByteSink *bytesink,
			  void (*status_callback) (const char *status, void *data),
			  void *data);

char *gzilla_dir_file_html (GzillaFile *gzfile);

void
gzilla_file_init (void)
{
  num_files = 0;
  num_files_max = 16;
  files = g_new (GzillaFile, num_files_max);
}

/* Return a pointer to a new GzillaFile structure. */
GzillaFile *
gzilla_file_alloc ()
{
  if (num_files == num_files_max)
    {
      num_files_max <<= 1;
      files = g_realloc (files, num_files_max * sizeof (GzillaFile));
    }
  return files + (num_files++);
}

/* Close a file or directory. */
void
gzilla_file_close (GzillaFile *gzfile)
{
  if (gzfile->isdir)
    {
      if (gzfile->dir)
	closedir (gzfile->dir);
      g_free (gzfile->dirname);
    }
  else
    {
      close (gzfile->fd);
    }
}

/* Read a chunk of text from a file, or a line of generated html
 * if it is a directory.
 */
int				/* return number of bytes of data */
gzilla_file_read (GzillaFile *gzfile,
		  char *buf,
		  int bufsize)
{
  char *s;

  if (!gzfile->isdir)
    {
      /* plain file, just do a read */
      int num_bytes;
      num_bytes = read (gzfile->fd, buf, sizeof (buf));
      return num_bytes;
    }

  /* it's a directory, we scan the directory and convert to html */

  switch (gzfile->state)
    {
    case 0:			/* output header */
      gzfile->state = 10;	/* continue header on next call */
      sprintf (buf, "<HTML><HEADER><TITLE>%s %s</TITLE></HEADER>\n",
	       "Directory listing of",
	       gzfile->dirname);
      break;
    case 10:
      gzfile->state++;
      sprintf (buf, "<BODY><H1>%s %s</H1>\n",
	       "Directory listing of",
	       gzfile->dirname);
      return strlen (buf);
    case 11:
      gzfile->state = 1;	/* ready to do files */
      strcpy (buf, "<pre>\n");
      break;
    case 1:			/* do files */
      /* we could sort the directory here */
      s = gzilla_dir_file_html (gzfile);
      if (s)
	{
	  strcpy (buf, s);
	  break;
	}
      /* end of directory, clean up */
      gzfile->state = -1;
      closedir (gzfile->dir);
      gzfile->dir = (DIR *) 0;
      strcpy (buf, "</pre>\n");
      break;
    default:
      buf[0] = 0;
      break;
    }
  return strlen (buf);
}

/* The idle function. For each open file, try reading a buffer and
   writing it out to the bytesink. If we reach eof, then close the
   file and bytesink. */

static gint
gzilla_file_idle_func (void)
{
  gint i;
  char buf[8192];
  gint num_bytes;

  for (i = 0; i < num_files; i++)
    {
      num_bytes = gzilla_file_read (files + i, buf, sizeof (buf));
      if (num_bytes < 0)
	{
	  if (errno == EINTR)
	    break;
	  num_bytes = 0;
	}
      if (num_bytes > 0)
	gzilla_bytesink_write (files[i].bytesink, buf, num_bytes);
      else
	{
	  gtk_signal_disconnect (GTK_OBJECT (files[i].bytesink),
				 files[i].abort_id);
	  gzilla_file_close (files + i);
	  gzilla_bytesink_close (files[i].bytesink);
	  files[i--] = files[--num_files];
	}
    }
  if (num_files > 0)
    return TRUE;
  else
    {
      idle_tag = 0;
      return FALSE;
    }
}

/* This function gets called when the bytesink we're writing into gets
   aborted. It shuts down the connection and deletes the file
   structure, freeing any resources that were taken. */

void
gzilla_file_status (GzillaByteSink *bytesink,
		    GzillaStatusDir dir,
		    gboolean abort,
		    GzillaStatusMeaning meaning,
		    const char *text,
		    void *data)
{
  gint i;

  if (!abort)
    return;

  for (i = 0; i < num_files; i++)
    {
      if (files[i].bytesink == bytesink)
	break;
    }
  if (i < num_files)
    {
      close (files[i].fd);
      files[i] = files[--num_files];
    }
  else
    g_warning ("gzilla_file_abort: trying to abort a nonexistent file\n");
}

/* Return TRUE if the extension matches that of the filename. */

static gboolean
gzilla_file_ext (const char *filename, const char *ext)
{
  gint i, j;

  i = strlen (filename);
  while (i > 0 && filename[i - 1] != '.')
    i--;
  if (i == 0)
    return FALSE;
  for (j = 0; ext[j] != '\0'; j++)
    if (tolower (filename[i++]) != tolower (ext[j]))
      return FALSE;
  return (filename[i] == '\0');
}

/* Based on the extension, return the content_type for the file. */
char *
gzilla_file_content_type (char *filename)
{
  if (gzilla_file_ext (filename, "gif"))
    {
      return "image/gif";
    }
  else if (gzilla_file_ext (filename, "jpg") ||
	   gzilla_file_ext (filename, "jpeg"))
    {
      return "image/jpeg";
    }
  else if (gzilla_file_ext (filename, "html") ||
	   gzilla_file_ext (filename, "htm"))
    {
      return "text/html";
    }
  else
    {
      return "text/plain";
    }
}

/* Plug in the bytesink and set up the idle func and abort_id */
void
gzilla_file_set_bytesink (GzillaFile *gzfile, GzillaByteSink *bytesink)
{
  gzfile->bytesink = bytesink;
  if (idle_tag == 0)
    idle_tag = gtk_idle_add ((GtkFunction) gzilla_file_idle_func, NULL);

  /* add abort handler */
  gzfile->abort_id =
    gtk_signal_connect (GTK_OBJECT (bytesink),
			"status",
			(GtkSignalFunc) gzilla_file_status,
			NULL);
}

/* Give a file-not-found error. */
void
gzilla_file_not_found (char *filename, GzillaByteSink *bytesink)
{
  char header[1024];
  sprintf (header,
	   "HTTP/1.0 404 Not Found\n"
	   "Server: gzilla internal\n"
	   "Content-type: text/html\n"
	   "\n"
	   "<html><head><title>404 Not Found</title></head>\n"
	   "<body><h1>404 Not Found</h1>"
       "<p>The requested file %.750s was not found in the filesystem.</p>\n"
	   "</body></html>\n",
	   filename);
  gzilla_bytesink_write (bytesink, header, strlen (header));
  gzilla_bytesink_close (bytesink);
}

/* Create a new file connection for URL url, and asynchronously
   feed the bytes that come back to bytesink. */
void
gzilla_file_get (const char *url,
		 GzillaByteSink *bytesink,
		 void (*status_callback) (const char *status, void *data),
		 void *data)
{
  char *filename;
  gint fd;
  char *content_type;
  char header[1024];
  struct stat sb;
  GzillaFile *gzfile;
  int t;

  filename = (char *) url + 5;
  t = stat (filename, &sb);
  if (t != 0)
    {
      /* stat failed, give file-not-found error. */
      gzilla_file_not_found (filename, bytesink);
      return;
    }
  if (S_ISDIR (sb.st_mode))
    {
      /* set up for reading directory */
      gzilla_file_get_dir (url, bytesink, status_callback, data);
      return;
    }
  fd = open (filename, O_RDONLY);
  if (fd >= 0)
    {
      /* set close-on-exec */
      fcntl (fd, F_SETFD, FD_CLOEXEC | fcntl (fd, F_GETFD));

      /* could set nonblocking too, which might be helpful on some file
         systems (like NFS), but we won't bother. */

      content_type = gzilla_file_content_type (filename);
      /* todo: add size fields, etc. */
      /* size is now in sb.st_size */
      sprintf (header,
	       "HTTP/1.0 200 Document found\n"
	       "Server: gzilla internal\n"
	       "Content-type: %s\n"
	       "\n",
	       content_type);
      gzilla_bytesink_write (bytesink, header, strlen (header));
      gzfile = gzilla_file_alloc ();
      gzfile->isdir = FALSE;
      gzfile->fd = fd;
      gzilla_file_set_bytesink (gzfile, bytesink);
    }
  else
    {
      gzilla_file_not_found (filename, bytesink);
    }
}

/* Create a new file connection for URL url
   which is a directory, and asynchronously
   feed the bytes that come back to bytesink. */
void
gzilla_file_get_dir (const char *url,
		     GzillaByteSink *bytesink,
		   void (*status_callback) (const char *status, void *data),
		     void *data)
{
  char *filename;
  DIR *dir;
  char header[1024];
  GzillaFile *gzfile;

  filename = (char *) url + 5;
  dir = opendir (filename);
  if (dir)
    {
      /* todo: add size fields, etc. */
      sprintf (header,
	       "HTTP/1.0 200 Document found\n"
	       "Server: gzilla internal\n"
	       "Content-type: text/html\n"
	       "\n");
      gzilla_bytesink_write (bytesink, header, strlen (header));
      gzfile = gzilla_file_alloc ();
      gzfile->isdir = TRUE;
      gzfile->dir = dir;
      gzfile->dirname = g_strdup (filename);
      gzilla_file_set_bytesink (gzfile, bytesink);
      gzfile->state = 0;
    }
  else
    {
      /* can't open directory */
      gzilla_file_not_found (filename, bytesink);
    }
}

char *
gzilla_fileinfo_html (GzillaFile *file, GzillaFileInfo *fileinfo)
{
#define FILEINFOHTMLBUFSIZE 1200
  static char buf[FILEINFOHTMLBUFSIZE];
  int size;
  char *sizeunits;
#define MAXNAMESIZE 12
  char namebuf[MAXNAMESIZE + 1];
  char *name;
  char *namefill;
  char *ref;
  char anchor[1024];
  char *cont;
  char *longcont;
  char *icon;
  char *icon_prefix;

  if (!fileinfo->name)
    return (char *) 0;
  if (strcmp (fileinfo->name, "..") == 0)
    {
      char *e = strrchr (fileinfo->dirname, '/');
      char *p;
      strcpy (anchor, "file:");
      p = anchor + strlen (anchor);
      strcpy (p, fileinfo->dirname);
      if (e)
	p[e - fileinfo->dirname] = 0;
      sprintf (buf, "<a href=\"%s\">%s</a>\n",
	       anchor,
	       "Up to higher level directory");
      return buf;
    }
  if (fileinfo->size <= 9999)
    {
      size = fileinfo->size;
      sizeunits = "bytes";
    }
  else if (fileinfo->size / 1000 <= 9999)
    {
      size = fileinfo->size / 1000;
      sizeunits = "Kb";
    }
  else
    {
      size = fileinfo->size / 1000000;
      sizeunits = "Mb";
    }
  if (fileinfo->symlink)
    {
      /* we could note it's a symlink... */
    }
  if (fileinfo->isdir)
    {
      cont = "application/directory";
      longcont = "Directory";
    }
  else if (fileinfo->isexec)
    {
      cont = "application/executable";
      longcont = "Executable";
    }
  else
    {
      cont = gzilla_file_content_type (fileinfo->name);
      longcont = cont;
      if (!cont)
	{
	  cont = "unknown";
	  longcont = "";
	}
    }
  icon = 0;
/* TBD - need to translate from content-type to icon */
  if (!icon)
    icon = "unknown";
  if (strlen (fileinfo->name) > MAXNAMESIZE)
    {
      strncpy (namebuf, fileinfo->name, MAXNAMESIZE - 3);
      strcpy (namebuf + (MAXNAMESIZE - 3), "...");
      name = namebuf;
      namefill = "";
    }
  else
    {
      static char *spaces = "                        ";
      name = fileinfo->name;
      namefill = spaces + strlen (spaces) - MAXNAMESIZE + strlen (name);
    }
  if (fileinfo->symlink)
    ref = fileinfo->symlink;
  else
    ref = fileinfo->name;
  if (ref[0] == '/')
    sprintf (anchor, "file:%s", ref);
  else
    sprintf (anchor, "file:%s/%s", fileinfo->dirname, ref);
#define ICON_PREFIX "internal:icons/"
  icon_prefix = ICON_PREFIX;
  sprintf (buf, "\
<img src=\"%s%s\">  <a href=\"%s\">%s</a>%s %5d %-5.5s  %-12s  %s\n",
	   icon_prefix, icon, anchor, name, namefill, size, sizeunits,
	   fileinfo->date, longcont);
  return buf;
}

char *
gzilla_dir_file_html (GzillaFile *gzfile)
{
  struct dirent *de;
  char *s;
  struct stat sb;
#ifndef MAXPATHLEN
#define MAXPATHLEN 1024
#endif
  char fname[MAXPATHLEN + 1];
  char abuf[MAXPATHLEN + 1];
  char *ds;
  char datebuf[40];
  int t;
  time_t currenttime;
  GzillaFileInfo fileinfo;

  time (&currenttime);
  while ((de = readdir (gzfile->dir)) != 0)
    {
      if (strcmp (de->d_name, ".") == 0)
	continue;		/* skip "." */
      sprintf (fname, "%s/%s", gzfile->dirname, de->d_name);
#ifndef __EMX__      
      t = lstat (fname, &sb);	/* we want to see S_ISLNK if symlink */
      if (t)
	continue;		/* ignore files we can't stat */
#endif
      memset ((char *) (&fileinfo), 0, sizeof (fileinfo));
      fileinfo.dirname = gzfile->dirname;
      fileinfo.name = de->d_name;
#ifndef __EMX__            
      if (S_ISLNK (sb.st_mode))
	{
	  abuf[0];
	  (void) readlink (fname, abuf, sizeof (abuf));
	  if (abuf[0])
	    fileinfo.symlink = abuf;
	}
      else
#endif	  
	{
	  /* not a symbolic link */
	  fileinfo.symlink = (char *) 0;
	}
      ds = ctime (&(sb.st_mtime));
      if (currenttime - sb.st_mtime > 15811200)
	{
	  /* over about 6 months old */
	  sprintf (datebuf, "%6.6s  %4.4s", ds + 4, ds + 20);
	}
      else
	{
	  /* less than about 6 months old */
	  sprintf (datebuf, "%6.6s %5.5s", ds + 4, ds + 11);
	}
      if S_ISDIR
	(sb.st_mode)
	  fileinfo.isdir = 1;
      if (sb.st_mode & (S_IXUSR | S_IXGRP | S_IXOTH))
	fileinfo.isexec = 1;
      fileinfo.date = datebuf;
      fileinfo.size = sb.st_size;
      s = gzilla_fileinfo_html (gzfile, &fileinfo);
      if (s)
	return s;
    }
  return (char *) 0;
}
