 /*

 gzilla

 Copyright 1997 Raph Levien <raph@acm.org>

 This code is free for commercial and non-commercial use,
 modification, and redistribution, as long as the source code release,
 startup screen, or product packaging includes this copyright notice.

 */

/* Simple primitives for manipulating rectangles.

   This code is adapted from xink.

   */

#include <gtk/gtk.h>

#include "gzwrect.h"

/* Make a copy of the rectangle. */
void gzw_rect_copy (GzwRect *dest, const GzwRect *src) {
  dest->x0 = src->x0;
  dest->y0 = src->y0;
  dest->x1 = src->x1;
  dest->y1 = src->y1;
}

/* Find the smallest rectangle that includes both source rectangles. */
void gzw_rect_union (GzwRect *dest, const GzwRect *src1, const GzwRect *src2) {
  if (gzw_rect_empty (src1)) {
    gzw_rect_copy (dest, src2);
  } else if (gzw_rect_empty (src2)) {
    gzw_rect_copy (dest, src1);
  } else {
    dest->x0 = MIN (src1->x0, src2->x0);
    dest->y0 = MIN (src1->y0, src2->y0);
    dest->x1 = MAX (src1->x1, src2->x1);
    dest->y1 = MAX (src1->y1, src2->y1);
  }
}

/* Return the intersection of the two rectangles */
void gzw_rect_intersect (GzwRect *dest, const GzwRect *src1, const GzwRect *src2) {
  dest->x0 = MAX (src1->x0, src2->x0);
  dest->y0 = MAX (src1->y0, src2->y0);
  dest->x1 = MIN (src1->x1, src2->x1);
  dest->y1 = MIN (src1->y1, src2->y1);
}

/* Return true if the rectangle is empty. */
gboolean gzw_rect_empty (const GzwRect *src) {
  return (src->x1 <= src->x0 || src->y1 <= src->y0);
}

gboolean gzw_rect_point_inside (GzwRect *rect, GzwPoint *point) {
  return (point->x >= rect->x0 && point->y >= rect->y0 &&
	  point->x < rect->x1 && point->y < rect->y1);
}
